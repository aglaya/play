{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 159.0, 130.0, 1043.0, 769.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 48.0, 44.0, 62.0, 22.0 ],
					"text" : "route load"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 27.5, 684.0, 55.0, 22.0 ],
					"text" : "pipe 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 27.5, 717.0, 108.0, 22.0 ],
					"text" : "prepend loadpatch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 666.0, 225.0, 67.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 666.0, 187.0, 85.0, 22.0 ],
					"text" : "presentation 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 0.0 ],
					"bordercolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.29 ],
					"fontsize" : 8.0,
					"id" : "obj-26",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 235.0, 288.0, 60.0, 68.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 234.0, 186.0, 104.0, 19.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 235.0, 382.0, 59.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 136.0, 382.0, 33.0, 22.0 ],
					"text" : "keys"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 48.0, 382.0, 65.0, 22.0 ],
					"text" : "instrument"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 235.0, 236.0, 29.5, 22.0 ],
					"text" : "ct"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 136.0, 236.0, 77.0, 22.0 ],
					"text" : "multisampler"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 48.0, 236.0, 51.0, 22.0 ],
					"text" : "sampler"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-10",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 136.0, 288.0, 74.0, 68.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.0, 110.0, 102.0, 99.530278232405891 ]
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-9",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 48.0, 288.0, 74.0, 68.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.0, 110.0, 101.0, 99.530278232405891 ]
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-8",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 235.0, 148.0, 74.0, 68.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 234.0, 4.469721767594109, 104.0, 96.0 ]
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-7",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 136.0, 148.0, 74.0, 68.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 128.0, 4.469721767594109, 98.0, 96.0 ]
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-6",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 48.0, 148.0, 74.0, 68.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.0, 4.469721767594109, 101.0, 101.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 382.0, 86.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 41448, "png", "IBkSG0fBZn....PCIgDQRA..BLF..DfhHX.....PqYjp....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6clGeSTm+++URZSaSSuKk1BEZJWJnEDDAV0Ro0JGxOZ4FUDJ34prq90yUW8q3wp6ptdqqGHT7DjyxWU.QNUVAUP.ETok1BEZgRuaZZaZaxu+X5L4ZlIIsIYlj994iG4Q674ymYl2IsSl2y6SEfffn2Do10KqIZ.LJAV+n5ZdwXT.HpdjT0y3n.Xjdwi+oAPYhL+Q.P87LdYhre6omHPDRNoBGuNhuwDabqmef8TAxOk8Jxb6QfwE55MmMmrFERs.PPP3TrWgnTA+e4t8qKZ3cURgv6g0J.NQWX8VeSs8z09djtdQXgLs52SE7ecTl7Llq72.h.KXul5Hf45o8.u30SjxXDDxCxDLJSkJrnTEoHEQOkF.vlAvxg3V2KPhQY2qTQuWKOQ3Y4z.n..7pvO0BbDDD1xn.yEzGA.loWzKevqWEN2ky9ijJ.tOvnzY8P5+bldE3+pdv7+bdLBzsLlXwBi6rFuM0Cu6WR5L+nuGu34lvBQClKfyGzSpSHMbZ.jGBLbeYdf4ZobkX4fn2KEBl+GrGakL4lxXYZ217E7v1uF10IkAPbfJGEB+OY6wI6aYc8JSW37TOrbyAq+8.EXUB69Puv+OM2EjmKu1VMzJ19V1lWTZH.iqKyD9uWmkGXrxW.8CzLk7lCBUaD8niQsUbVrucsCOjDQH.GELWO0iTHyanLFqBTr+jOKOMQuv4kHvhiBlaVr4td4uRdfIFCjTkv3SgnHRNBnJTkNLthjE93zll1gAsc5IEMGH1h0.S5MYyXphWILo11wZJZinC6FiknpSMT1tiu2..TZTI5rZG2uNa0DZphlrYrnSKJne3FA.PD0qVPYVniIK0WRCBNWsWnV7s6deBNuWhF.y2KWlu9D2CHZvbsjWyRX5zkFF4U6d2dJtgboNLlpniEHp33c85SHYzt5P6Vxm6RvFaEZ+0eDc1hAAkq55eZt7wSSi0hPZT.cNZnFzY805vv5q3LnslaFgDd3HpIjsCmunptRnr0V38PpzXKn8JOqfxSME8ahJuspuIrsMudts46uuGc+6EkVZIhdbbA1KbMCOHH8TkwRsKAHSvbgMEvwDdCZ.LOI7xkX4vco..rX2cmt1IkAhsuwhnSiQ+MqUNRLEPH58BqxmJZTIZ8bsgC7UGvUtAyQgzGhFtJiBLVi2sdnlLxJGDax8mSgIUCbH..nsHiFFhLVOrHRDHQLmsDfFpAsT5Iw2U35ckqmdJzCtGU2QYrTAi6VxCA3lIlP1wQAi+48GbuRAvEUDaxyXJHwKquPQx.0kPqdWohnWCwVrFrm22oO0eO5FH9HbYEwxHqbv.S+JP3CeTnw9OHelEnHBrIXishv9w8hO3IeXmszq.cy6O4NJikIXtnkbwHgTRCfQgL4rqKK.NQQrqcRYfzlfNXbvde29Qz6kfLpD5+75DKN7j6tqLUvbyMQUD6VtuGFgMpI3VtbifvcIppqD67Quawd.mts6JcEkwREL2bwuPIrry3ZPJ8Q3DS7xRPqac7FnFfvUX1kWeYsoDu3WbPa9ikNcogIN5gay5BOzfgtHCQviyuVkdAmq7KVO1499NWRdl0TyA+oTiCU0Rmnpl32u7t6wTlvjf7LKPyC.aRnI0oKMj4sOQT6fM3CEIhdy3BJjsZv7.NxQNBDI7Wl+ctLD5jmM4xQBeFtfBYcq6M4LkwxGLwpS2N3isVQDmo.hPzmPTf9DjswIS3JLgAYVXkK5Mx4UnFUYNXaFKczbO93pWgJThYgM2eYsoDF5v0UXkOXU9buG9DtZvTJGeh9nAi7v60KycoyClxREEyWD9bBxnRb3m3fhcskNHutVBfIbXdE9lPmtzPN22ig5FwX8whDAAPLG+Gw6cu2lPS2srNlXJiU.bifON6LtFbIozGnKxPvvC2LRPQ6HQyFcW4gf.mRQX3KuHvKTv5b1RkaAf7xAvSx2D238bSvvDI2QRHcDawZvpdrUIzzuF7vEwRO.0CddvFc5RCY+7uMZH9jj.Qhffg1K3Uwl+XAudxsicLgTFq.3BJhkcFWCl8HSAiHzNHqTQ3w47JTim6WaBabqhVmb9e.i0akCv6MOl6RmGLNE4VI8in2H09epTnxnQCPdUc9yG.7dmt6nfMQwFFgjilFqEqdVSRnocaW+yWg3Ie3DEwxe1SGe0CNer5qJZLiPZhTDivqPhlMhWeDgfGN+4J1xVNjG2DIeHvSwaJKU9dogffGFbNCRnohBLw6nbAdkka8odARQLBYAFhLVL+6bYBMsaesj8JikJDwJCYmw0fu5AmOdVcdlXQhfvU3uFeKXVSMGglNJHObuRl7N3sOQJFwHjMT6fM.c5DTYF4hxXQCdJrq5zkFZYr9E4QFQuDhYL+Iglxse3F6UFa4PffON+YOc7ViKNRILBIgG6xhPrahHGTFygK7zoKMJqIIjcb0y7pEZJ4hxXYx6fKXwTcCiPVQMoMBO1C2XsxXoBAbO48byyDOqN.slo.PlPZHQyFw8LkqRnoiBRap4mJ34gXF+zFuuWRHHbBJGlfSEE5gszEOD7lTNpF4370xAAgSIyEHXTcko6bbrVYLdstP1YbM3ujL4lEBomoqsEwdJj78ghh8v6MOBseteYbgfvaSCwXDW6jxPnoyzGJJBQl1OfNcoQYOIgrjPzMDglZfvMx1eqUFiWSp8.WU+HKhQHKPq4NEy5XSDLVnRJf2K3ZMw170xAAgKQ+Gd+EZpL8ghgP3PB43tMuaBBeENwUkY5pGGVkwFE3oOSNqolCEiXDxJxTqn0tNoJlW3MaNo1bDgbkvFjfVsUNn0iCUb+XRIUIPLHHbMtpqeZBMUlt5wvZkwbfbGV7tmDQP3kIQyFEKyJkJkwb35mIOioHExAAgKQCh2P5yzGIFtLJCMLoVDHHDjnFtfdizscSIu6vHBlbyBg7iqavB9PBxgmpG..gpgx5KB4Kcn1DxcAB9rKY5CEEW5bGbRB5VUBBIm1RQv522.gKF9LBpLV1YbMT6LhPVxUEpnOjfbI87IHj0DcZB1xgkSsXL..XRMYYLB4KFhLVLk7liPS6RWOInxXWRJ8oaJVDDdWbhqJkhaj3v4TsF0RfXPP35nHYAmR1oLFAgbmjtzKWnobKkwbrMtDIkV9DxWFYJwJzTY5CECVb35GMIROIOg7l5DNtwFHjGsXLBB+FDoDWjoqr+JAcQGgeHiMRAmR1D2XDDxcDItwHqiQP3Fzhv0AOW1xX7tvgGt4toHQP38wIkbkL8QhAAgeMhD2XY5CECqgTBjvuDCQFqP0arnfKDD+12aJIH7aH+YOcglh9BcBBW.UwK3s.jpqgHO0P32hHEmXmd8DoLFgeKWVBZEZJIWYr5KoAoVDHHbJsDsfYlrjeMDAg+FwMjKUnoHkwHBbQDWoS2HgfvEPjtDgCcjEBBBwQj5gGoLFQfKhD2XNzNU7hjoO7bQP3wQlV7WIH76niHELK+S0Y6qRPWvQ3GiH0arL8ghAAgeKZhSiPSQwuEAgaPCBmQkN0.AjkwH7qYv8Uv6WjpOTLHH7aQcTAKzTj69IHbSDoR7mpX6GoLFgeMoFQPBMEciDBBW.QpD+o56jBBh.CBUaDBMUphsejxXD90jZHlDZJIUYLQb8CAg+BoJAmyLkfyIAgGitaFUJnYEHH7GHMEB1RWjTkwTGUvv.DLS0HHjMHRaQpWo0kC1XqPaUUvrQC0fNquVAWqpvz.jP+rYr1hLZXP3.4lH.GUg08hASRYLB+ZzZtSnSWZnzRKw9oDrzhSPP3Rzq4ZnXNaInguem3G95uhuuKwiPFYkChMYlRePLojJBNw9g1RYPNn3llFqEgT7wQmsXvlwUMPA68gNE8IjLZWcnc68umPvFaEZK5WPm0WKzWwYPaMyjE7QDeef5XhG.VduUW+4sB16egcJmaEjkwHBrYhid3B8EnYBf83SEFBB+PxcA4gBWyl4apTAPY9TgwGilu3iv68puD215zkFt1qIcLhgIdxjdwZZGUUMihE502F13l1pnqee6ZG7NddKbIH37uO..ncmaFq54eR2Q76wj2BWhCiYuq1BNo9CSpCC.tlk+h4veK5rECvTqsf8rlU60TvkOlRdywl31hUoufSp+nlzFgOSN3AxxXDA1jPDgIzT9hTymR+eh.YREAvJiEyYKgSQrYMyoh4OijvUdo000rtRWzPQW+LT7hOxLEbUUUWHn55YrL0oqTIJq71vQNV43a149vl+3Ug6H8w..f2yGqHF.vl+3U416yc7Zq.5GxkKn01duGdY8Twpay1175EbtLyNaL3a+gDqDTziQDq6kpX6WPfBXRB+bRHLUBM0n..uOtuGjdkwUCQfEQjrfY.lr3gM7Vtup8+3X..35xNC7hORn.nNw2gtIIDSaHgXXZ8TCWWWCN+3v5mT93Qd7BPUG6Pn1JNK.XpchQpID..T9EqG6beemWQl5I7d26sI3bSIu4fa+e7J38+6+Obi8WdfEhXhWZr8ScU2AN9QNC10N2G1yN2INcIkhre921qpPl.HZWsfrLFgeOCT3DWTVbiD+YznWEB8Lpg43MCSAaBMDiQoVjH7BnJTQaX3d6GnQxntxKC..4LozfqYILOKCePL+zZqS85iHDKKPWz.W0zE8XbJEgglMKbgQnYyJvoMH3z..nzFaCM2Z6baGdnACcQFhMqw91OWYsoDF5vL90pziB1vWvM91175whG7kvscVYmAlwhbh.3kYFKJdb86Yo3ucuqDkVZIn1u7yfpEe+dsyWFYkiftkVHHkwH76IbETOpzawkq9Rwis7mfa6ol6zfl4FE5PsfkTDB+PTpVgyWT.L8IVAsttWkgqqmq.3fL2h3KPAvDB2IGDGlu8tdILoGB.BA.5.VxCMW7zGrRNq30dKVZUcK5t0Aflbh.38YLYVO9ee97wS+nEfM9QqF2Q1440r3JahZvCYBAhiYpNiQ32SBJD+KMH5dnQuJzzoZEO9SXINV1ZgeEhnd0RnTQ3Mvb7B9.Mo5CECICCsJ36eeNmRgfw.qrkAYtE7.WkkrHzPC0C.fbm4TQ+FrzqHFKW0jrXgNVWTKWPPkwBWA8juD9GjnYAcc1D8kxQfFgXHX77O4+.2vMbCX8arPoVbjEDSUghnORnH1SDFhopPgF8RiEU7gjpTK.9BzDp7wxf8E9mgBP5nYG5UvWxkkfDIM7SHg0Ats+77A.Pk+1u30NOwjRpt89HnxXN0zmDDDBhRsAVFcd1ybFHmblrTKFRNI0X+vpetUiUs7BPAKa03.OyAjZQJPCIIzB5SLxmBzrVyxGYwc4OkZb..noZqF..FLH+duj7.Xr7nXYcYOEkgJn0MyTv8wqHIDD9Xxe1BFjqo5CECNLGYfmkkuoEdKRsHHoDawZv4J5b34+WuDzoiIVSJszRPPF8++ZTQpB+9Z50TnYCDgMYpZuUl+epECxuPHIoTrb8plFEt6J3qw++aQHHDmTkZAHPgKYXC0gwznWEhoJF21E2YCr6GmlzaBe3pVMxbhYf248detwCvigNxU+DtL1mLUrVgRNQXgYwZcgzX8dkygHsDIAgxlRh.BBOzfkZQvulDqIR70uvNwnlvU.Sl5jotS0UhF0XSLAf63G2XsYeh6rZvJueKojedydV.yu2weGt9qKKjSNSF6XGaWpEEhd.cmBdJgqSLwI+hoR0g5CbcpvsDIAK2RjkwHBHv9ZhiUjpOTL7KQidU.EEDJszRvl9zMfBWylwG+xeDTTMSPMexSVDu6WmUawUr268c+n4FaFXis4SjYo.Uwa6WWNi7DthqSPP.DSbcH0hfCzm9Ko07LAiIRkfLCMQfMoJ0BfbmPLDLd2W9+f23sdGrpU+wht1EsjEyojFK4jyjwSs7mDoLfA.Up32X6QUm+uq7LYWsU6JGyncXMwTUnH1h0fnORn9ctsM2EjmTKBDAHzT8dmNYPfLjkwHHH3H+EcyXo25sK5ZLYz13BYF4MSDUTQB.fM9ILYnTPFUBUE1IJbdqEENu0Bi6W9kUUhQh0DImr29mnGp2lYnrCa+5xK8RFlMaG6IBCErrUiU8XqBq94VMp++pOfPITPsLOuFmnz.y7U3mO7OA.HqpwXxcHkwHBHPjVhj2lTkryrah5sXlSAiBm2ZQrEy+GZS6FXxLUSNwCCJRl4mrVHZHC0hxIQTuZNEylybmOpoxZPgyasbyyF3+wTUnxtrQTidUP0oBgqTd7UE9kXcq7yg45sUITVEPYwZkTe7m3IwFW+5PC6ltYTOkLxJGmuHBBYD5SHY2dejWeKHAQ2DIrkHkpW936QHhCnF+z9+I73OwSxojQsGieWIL1qjQ4Ji05dEeRsZ05vX268c+Xce9ZPJCX.bi05G2H9rk9onfksZTvxVMRqh9IqTHKDCAi29kdS7zOyy3zhcaVW204vXK8Vuc7LO8xEzBiwTUnAJVLymfHsVFOFCHwlc9hHbJk0l745Xoj1UGpPSIX.7SYSIQfN8pZV3wVrFaTxJ1ziA0NXCnoJZBCdvCAOySubblxOKF3.RQviw.Rw8t4GaVVxmxXCdHLkCijS1xSJt8srUjSNSFyHuYhKb9Jwy9vOEx+MWrM05pfLpDpMp.Fz58buYT0oFJaWIZJZiB1qMm8LmAV5sd6XkefkRYw4pnRteu+o3XVSwZYQ.f1aocDLBAQUmZ7s+y8iRKsD..rfEcS.h2+mI7gnML4Wfl6OhgNr7Pwr0hO4LcGKX0CYjBMAoFKAQ.BwVrFT9tOGJbMal60pdLKotOq0oFPJ8G5zkFTnzyb4+AN3OB.K0gL9r5SeSLI..t1GTJCX.XY28cha3FtA..nnZEHtypgyEpaXgeFh7WixqZAo19t1QAKa0XCK7yPgyasHhCv+4xZkq..prxJsYayUX656WxLuWiHhHvWU3WB..U0DDmhXO9S7jnlJqA0+NW..LJdx5x1XOQXRVKVJ5zBLieIBeOYjUNXbW6XjZwvoHhEr74PJi4k4TJBCGCgiyqfbKg2jdS8RUqu4s08GQS5MguYGeMLa1LLa1rnAh+jxJaDjRuyM8U1tvesRHFBFSLqLcXbSFMyUpLt2669wa7VuCd2W9+30h4JUE1Ip870g23sdGN211TE1dt9oCcX..bICaHtzwToZlrLk0RgrVEDvRGY34+WuDdlmd4HkAL.r2csG..z75pmyksqZ4Ef9WcxxJ21h.TqKGrQYSWGfvGx4JNBoVD3EdcSY1YbM9Z4Hfk7W+g3dhX.FS2NwQObDdnAiLSLDLAE5kPoKvgdK8R0fLpDoUQ+vK9vuD2X5zkFF+SLd29Xs9OdcH2YLeOlrkbWVDxYDcBQixOyY.fiYjH.vBVvBv3G2Xwd2ytQmJ6.8D0rS3WhD0UpkprcnIDJZZ7FgREJ3rN2L9+cCh511QL7KE..sVk3wPm43ELtE4HyIlgCissBYba6C9vOLN7g+Y7nO7CZiaaYcmZaZZ2q51VQXT.XyRwI1ah1ppv4KxKS7QSJDJUH2biJuO9UJ8If7AgjDl3XGkM+QuzRKAErgu.u0mrIL2+8ZvoTH+ZWDAXDPUG8hr3PvK9vuDd7m3Iwa7VuCV5sd6nzRKAgXHXTeIM3QOWAq0wmUSeSNFny+9ebR.XarlwpHwENOiK8F5Ps05RrUtd1LRzv4cTY5HiLRroOcCcGQG.LIsPYG3LHxPhFQFRz3P6+P3ie4Oxg04twHmqh8umAr7Y0ULZacgSJCX.35utr3TVSoQkH1hYba6GdmeDJXYqFQ9qQIYtvjv6PBwXaQRdVSkxbTeEi7p8t2ZHuEtD2Z8xJagGnwVZKBTvmuQbu+O2Oz2TiXu68awi92eBaVSYsS4PAgqCa4S3Ftga.K6tuSGtotmDEAovgw135WuCioWusV2UoUtXqhJXr9PjQv3Z.6KXrrXzfiVcx5f9WLDxssMUQSXG6X63CVw6gOXEuGlTVYK3wvY0VM2A1Xni88bLUYItTr+yJghItNq1DZuZlfJ+Mdq2gyssF9ZIs5gS3kIRMB1IQ76PInjhvcfzDvKBalkbgKvDntiYziDiYziD8su8E22ecY..3hsYl9q.geE0WRC.iJDzll1AfEqeA.j80kCSreIfwlrufwBvTA+UnTIW6F5mNzgw3G2X4B5+npSMZHF9cQnF8pP+qNYdcaquPskyUdEn+I08yHKksqDYjkkmPOhHrjQppBk4yia4luQDUTQhW9kdAnPoR3bmgRPPHiIU.Tl8CRVFyGvErK6ql5jISQSzyvUiOK.asLimD13Wh05W..8KE9ULwZkLXyZOVKHkx.F.N52eDt1MDq0iXKUFhkP.pKNXAcaq6RqFb+32YmeyNbXLgZr5Vi0kAjXRHFr6csS.XI9z5rUKQJ2u86+A.XR5hi98GwskQWE668lDD8DZtIJd3DfT4aP5pOIf9kbRxtfGLPfdSwag6DmSMEs6U7VYomFCZW6jx.M1Xi.vhRFBQokVBmb9duyaC.KkJCEM57ulxS311suks0s2WqguFqNaLzUbQLwLF66MVrNIe.bLyNEZcdRru2aRPzco7ROEJbSaUpECQYye7pb9h7gPJiIQLoIkkTKBAbDHEuEBg8JHs28ra..XJXguQJeEzTWMdr.r0Mj7AalQB.nRoEKREaeiEqecq0l0VeIM.kZs8qcXkkNTaBW6jx.kVZI3Me62kKX2Mo24JI3NVJzai8wEl0JS1TS1pjk80nLqgsTYXeP+SAwOgbGu4CMDnBoLlDQeSR9byCB+SZngF4T1QnXpRHXiGKWAqcCIevlYj..idzWofVTiMH4Yq4V1KKQUmZj1DzA.f+x8bWXIKdg.vhRIhg2JiHAr3NU0QKtqOYUHtnS9GbisnkrXmpLol3X5QnG+D+F.X9bxXKF4JUFm+7m2l02cbAKAgTPi0z6s9ZFR3g6VqmBcbIh9129J0hPuIxD.6QhkAOJG3f+HWQIcxyXJRlbnSWZB9Tvp0v7EwMzPi1zTsYcGIaf5yhx1Uh5FUqXIO2R3TfQQx.0lfv0PN6U7yYVwyafwFXRjA2QgXqquZpihQ4plZxhE01ZgeEtwbuI..rquYG3u8vO.WoxPouufvlou9DRDXPS0EBhLttWXR3ui1jGfyWjUPVFifvK.alF5ogUAmIL9qB+k64t..PhWlEE6mybsTDW28t1oKEah7ESYYectV72k93RWv4zjHSMziM.zGxPGFZuk1QGpMAc5RCaYyaBmo7y5POsr1Aa.0OpVQ8ipUa5WktBG7.G..VhQKOQrYZcxGvGFpw0xayIOiov4RWVkSM1P6BZ4OCZ6DW6jx.6XGaGu4a+t3vG9mA.35TAAfjoTK.DdVN+YIWp6pPVFShXHCdP..npV5DfGqYdJEgglMqDmnYEn4NMiRqoYT9EqGozmnwypyGKrDtMdqJktorTgkLf74JQDJRF1nvRjQxbS9B9vOAkVZIH2EjGuGGqiqI9hor9kRxngVb8.32ZqeoToJapZ9rMVasZ0hupvuD4dyyGieZiGe1a8o1T46UznRfDb4SoCblxOqMtLE.hVawrlgLTG6D.r3rjOvdrNF5hIp3PMsdQ..DplPQgawRgreNyc9vPMFPHWpstc7JF8X.9f2moaKLAc3a2893T7Fv0baKAgbfh+8lwXxTpkB+CHkw5FnWgJ7OKw1a1pKtvQ3pX9RxwpsSLHysfgGtvUDHsgy7z1U0TK.wa6bqoYs3A+OqQv8cIOzb60z9eHrkNTaB0Nb9+auRkpvt20Nwbm2B3hkrHRNBzDLBUpr8R8hN4eHnhZcG9se+O3b4XsmuFDM5KTzUNBTTQNlcgFmfYrj9XQoRkpUfZGb2qxfoSWZ3Me62kKSEYeeYeiPmMyN4C6sNWOAqUHbvCYnnrusD.3XxkDYjQhZZ9hbVQ82+iSZiaaind0ntQ0Jx+MWLLWASomvjZSh511dBhkDHDDcG1419Qj2huRDRXxmB.ac0HOU6QdJUxb9ESggB1fvJKoSWZ3am8v61G+esJG6WkO5e+IPyMoGu9q+J3yqnC7nT7+6.WVBdtan5ORjIGAJszR3hgqLxZhnyKi4gFTpvh0TZngFwJ+f22inLVDI6XS2cu6ZOH26xh6ROzO8iNrFwTpzcHrvCCkVZI1X4n3tpXPMvfMMBc1jcPr2yW6jbruQxGhEWZW6jx.e6t2GuyYc8Ua7iarHhHh.00Rsvf1NgNcogmd4+uH4jSxgiecIzZOxhgtJtaRfPPHDrwRZokVB90ClIFSl067cxGww+Yl6uNk7liTIBox2fjxXcC9xxX9i4q95uI..Jo3SgW+0eEt4YtYX2WYLqYNyYd3e7OdFzujSBqXUeH..ZtUuS7HQ3eS8WUKH2O21F+sA3n6R+nO4yboiGa7oIFrUId1Bc5UNlQyMGq6SW+5VKNvAe.NkLDqh56tz9j.x+JVL21soocTiVGsv1K8ueYa19P6+P13ByhK5jH19FqfmmEsDKmCwxtTwNFrv9Y0fGxPwVJbyH87FKt5Yd03ie4OBSNGKxDYoJB+UF1HGM2CE9tu5dvKc4WkrHP9arF0XEc40o9jZZPhJKsox2fjxX8.thQxzdi..tm64tvxV1ekyEEGiu.Aqavi8XOJ5WW0PoDoLvjnGP4m4L3LkeVNqHoINM7prFKrAfu0Xe.wyF+Rm7jEgq+5xhKCOYYJ4NUrsB2JtoabAbe4rXUTe2kNTaRvf7+P6+P..HszFD24N1KIZTKZAkVZIXRvhhOm6bmCvCFqwG+D+FWrloRUPnS.t5qF6mU.LO3V5XrnowajKKRUpUI5LtNHKU4kQoQ4WndnKtvAPyRsXzioOoZ46IJszRvG9FCCy+1hC8o+RWuUssVBBaccVBcnvF0DjJkw3ERYLOD8K4jvsda2lCAPb2AV2sszkdaXnCYPbimThI1iO1D8dYG6X6HyIZoOHFZ+BAFbyN33jxJaTSyWjaa1Zgk8DjQknC0lPBSKdLKsLtCH8wkNhNsnb6LjrKspg5V...H.jDQAQ0xeZZ+I7Iu0Gys8LuoYiFGbaNrtyT9YcpKLcUXsln0kohe9+dXjdtikq9ps28rarr69NcXe6twLGQ2i1q7rRsH3.rwcr+NpiIdL+6bYXsuKi2iJbSaEEtIfaZQyDC9Rih28otp6.0bQOqhnZBWMRd.gg5ptCb7ibFrqcxDBA4svkf55u7pK3PJi4AIyLtFO5wK8QMJdGu7KVOftnsYru2rVbZC.k1Xan4VaGkew5wN222A.fUee2HxNH9auJ8RHZmuj.ahX3ZQd2zrPPJUgwb0iAgDUHtzM+myBmqKc7YCddtyW8pQcIzJi0cxk0jSpP89vmEU+DaG4NQKts0D.LAGc82K+xLgXfZMpQO0VT7YMQNqe0UIDg0ss1+YFgzvhtEOWhrPXAU4dKXIw1Grpm+I4F6S+vMIgRDCy+NWFTk6s.ucv9nJLMt05Ikw.v4UnFZQmPqY2qbDnuYaCzdsZ8LtlzYry88c.W0z419TJBCy8kDNgBVdgGDY2CRnf..FE.1rSWkGDOYbQ4In1Aa.XvAy8EPs6juJ5CW0pwherE6v3ImbxnlhrXYLVqbw1leruU.Imo7ybFbfC9i30dUl3ISShgAidHkEsOyHAXboJqaamv3uJtwka+uBOjpO57HehxahtEVm7YsqNTzd14g6ZPWBp6G+VTc4mFs2ZWeeQ80gRKoXj9ns8ZjfCMTDQrVJu.QDee5wxTPgENTFZXPUXZPaCdDvPjwxyij4EHg94VKuWuxXmRQXXhu35DbdwrpTQEeJLwq8psYL9pH46d26pmKnhPylsMFbxImIi4ufaDCYvCBSbhWKJszRv4ULJjnYY8W3GPgmLtn70rgMsEAmquIlD9khNpMioSWZXkev6iOXEuGuYNobkcrisaSXEnJdl+l4NMV84L24i1gEWexdLDRozDlV73F6+MAC0X.QjbDPc+CB0Dir28jCzGcdNhO57P3CHppqDmcMq.qayqWv0rucsCenDwjAkgpMBD2PtTnZfCQV4pxd8Ji8zGT71mxhe0OCm8AmtnqwZlzjxxAkwDqoo1ng1.e0fH9fMYADi4Lm4g27MdMGrRWUlCFI1kSXNuB03GZMDXnCl38w5ml4FRUKlfB+GqaP34n3hNINvA+Q7POv+C.XB3716ncm9umibBihq4daeiAWtxRdtkfZOVcbaGcZQgZ5J3h4qH3dtxq.8OIGat5r0JLVL009Ze4oPidUvf1NYr.1DA.BAMAi.8XGiRPH+vbGsic8n2sSaX3yZlSEZ05Z2+ycohJpEeyNssLyrM6TL7NJXSxFEx50qLFabUs289sXLidj3PG9n361++E+8G6Q7Im+Mt0cfWeD7qrWIEeJA2O8JTwqaUWxRWhMJhYuk5NuB0Xtq+HBdQRA.3mdnYQVQqWHu1q9xbtrCvwF5M.PRIkDTnTIrNr8iYnLUdeqq0W8zpou2FF21Z4l.hEKaMzPiXmeyNvhuJGcaq8vZUM1xeAabgEhgf8ZckAh.GFn6ElQxVZ7rml6dLyZlSE2yhhColjuNKQiC3eLS..nukfvYNe3vPqJQYmyDN3gt.13l1JZ362IvbkGJi4+5KEuDiYziD26e4OiktzaSxjA1KH0qW3ftuDygx63161zIMorrY6pLGrMJh8n+8m.u5q+lXEePAbksfpLaa6YweAw53ADhyTxcp1r8TycZ7l0i8K4jroXpB.z3faC238bSH2EjGV38eKXIKO+.pLCjstrwWaH5JF8XrYaVqpsxO38A.vIN9w8xRGQfDgqHv36vp5zkx86RihX1h1v5.CWWC3Juz5vbttFv8rn3..3x1S4.AbVFyYV94gyet3uFuyquLW+jmLV4JWgmV7bIb2KHSSgyC53xZSIRODfzspF17n+8m.+8+1Cws8+c+emSMqLQfIgbyQhEMsaAJaWILErI2Jfx6PsIzgMtdKvfhK5jXCaZK3keoW..LkwCWI6Ry6llE17mtQ7D+uK2iTpaHH72nne6WA.v0kcFRthX7g0xTTUWIZHdouk1DvYYr2rDihpPwKTv5voT3X5maO8zZ5UYs0y+nkudo2bly7bXLWIKPYiOLqYJW+0y6ZOQyVr.vN6HB73kBtW8+k9BtWOdoLtKkHvfFhwnkxQgHvVLUCz40d0WFyYV4x88ItZEwm0ssO6y7Tbi4uTM88j8qThduTYkLwL4PGr7s1Xlc1L0bQksJOJ9uAbVFiE1X.C.Pu9lwZV2Fv88WWF.bL6C4CqCV9igvswhRtBF5vrqFW9Bx5W+miBVksVmKxHiTz84bUTIWE6G.XPCdHBt1DSz1f5QqVG6yfK9UEt04TvF9BbCOvBn.9O.Gqenfe5PGlqtYEHyjmwTw12xV419lumEhFhwHTY11GpYnCcH3a91sYyXMN31vRVtklfthjApKF4Ts9lfv2v.SQC.ZPpECdoeIGiTcpSkuACXUFyZzpMbbaKYQ36129v5W+m6yNugqP3mF1UxLRwf0si1y4OeU1nLV3hT6yrdc..oM3AIvJY5CmWwHsHyuwa7FX8q+ywoM.LAeS4UiPhfICIYpic+7g6cXUrPWXjH2EZofwpuq5xVmc1gMqKxHb7AX7TMAcBB+c5Sr9AdNopyA3aynxT4aP+BkwXqt77w.0.W1xLNypR1yRW5s0iharAY168Extqk290pzCnq6uVq6Cm.LAtruTwVBok27seWbgyWIWfoSvvu+GTUzmfPH5SLx+LHtyVjGIajrWYrszVD3teCgcUF.vden45UU7wWiXtDkOWI1c4PG9n1nfk1v05x6KqE2bGk7H7ew5xVAgEzqWON1AOFReVA1tskfvUXuG9DRsH32hrWYriWKSrVrzkdaNzqF+x+u+OricrcWJFvBTPHWIlcFWCWMSyZr10hrj+rmNJXCegCiODdN170QA..Rru80UDW4BoJ0BPfDW6jxPpEAIE1tMPCMzH1xl2TuhXnifvUv96ULbcxy3EyZTEcrRsH..+fror4VYhUizG0nvssjEYyqTRIEGVefll4G5vGk2wqpEaM+aJ8goWXWj.EJVlJ8usT44OOuq0ZE0l3nY5ok+7QsUNXy1T+jOuSUpE.+UTqQsCik1DHSg9lu86ha61uCpzUPP3uSTwI0R..7CrLlqPylU.zU0XPrxZg6F+W7UZIbF1a8NWAgpl9hQUM0BP7NNt8MubV3qR+e9KbA25bZMQDAiKMY97tWcSHOflvFWHXwItXTeIMfjFXhvTrlvE7CdZWuMjaaIHH7jH6sLFK74tMVDJ39sG1JLu00QKwvWEf5BUM88DvpzjqfXY34ErSwsgNDKtz77Jbz5I8hHft4F2PLFQ8ipUfYEBpbL0QJhwCSdFSQpEg.Z17GuJoVDHH7532nLlm.6aMPxYxe172uJESoT9vZklXQWbNuVTvVXbSHBledgJEtgp6u19j7PTuTK.D9N3yss8KSou5cSDXPylcMCEPD3gr2MkhESRcG2H5IvUTlwdrOyEcUDxsi1CqRSh0bwOkhvvfL2BBWEyE7G6HGA.Kh20xlTDIDFScho7xK2g0vV5Or1MwDDAxv51VVLOPSnlXjGoFOg+OTcar2KxdKiIVLf4sciXN4LYdGmUYFeABEP91mMjrJMIVyE2Ux5T6aP58IDl2qhEnxtpahIH72g0ssrubmd3IAQuEXCIHBWGdu6reRFxwAqBCdZ3KaM8Gw99Y4.0H7Z6aRLtbgs2Z1mfrzEANWE15pRwZ0RDDDDDtGM2oi8PX+QF4UOQoVDjbpyMqp+7pLlXViRpPLW7YsBCtx58GH7P4ONrrueR5JXemGHbEBeAee6p9gw1Xwst.zd9yWksGGqK7qDDDDD8HJsF2qGHSzyoySWjTKB.vOvMkdC7VVRycQH2fB.nKR960Q12OIYQLqcwZAK1rHUrdlIeE9U1jI361++0lw8yJ7qDDDDxZ3qdP5ufdELgJSFYkiDKI9mHqUFiMi9bFhobAevmkzjBbE2fxDj87i0e9vZsK9pkZ12rvYacT7sV1VhToMZ4KEtpjYZAS+8G6QvJV0GhCc3ihu3q1NN5wNF.jOt0NMEsJ0h.GJpVdnvOAgeFmVpE.ojMt0cH0hP2lewDy8ihM49KwRh+Ix5roTr.N25JSu2puT1S5CjU0Rm.d4rhwUaCT7YAK11mjd8MCsVorFq6cY67A..YEpAt1hz88WWlCGqq3R3uEM4qwcKbtdSLYLvH1OHH7wTF.FnTKDRIrY8t+F+XMV992it+8JgRh+IxZkwXguLyfsU9Lqo55lDkuxyfXHTefzUfsB4KTucrm.eGyDTzt.q1RqKx5fCks8Icne9HXhW6UK54Sq4Nw5lynvmewwv79hUNhKbDtJEX5ZaAPFoHDAAAg+JUYREFjelg0OkhvvKTv531l89S5aIHnMrNjJwxuBYsaJYw9h05gN7Qwi92dD..LxTbrIexWrXoWeybkmA20sl8DX6sitKCObgsrBeEu1DMaIE6sueVx05hrJ3PYqUZG4HGi2yg8tdLQyFweM9VvypCbut0HaFKHb8xJKRQPPP3Oi0VXRtywP3XKsEAxe8Gh24Oy4ohllqRPfwG8xZyBuxUtBA6qji0pDEjMFp3KVrNzOaI1qbUS.2rdoOyV18t2kfy4pEaU9pB+opgQO7U79uKt0krHNWUxpHG0yIIrlATWevabmuI..x6llEzlX3nowS0XKBBOMuPAqCI7mW.Fd3lQZJZ0kdXW8JT4RsUulMqnGUWHatSynzZZFkew5wN22208OPDNPPPF6i9zTzJl0Tyg2fZTmtzvxycbHcXoHmxFCU74NxMr9M31m+SUrzmxqh4hS6qVy4O6oiB1vWfhJ9TNszdLhfai63+bO+KfG6QeXnUa3XCaXidD4lHvgfLpDIg9gbxYxXG6X6XyeJy+ij+atXTWBxmjlfHvEMMVKLDoidAIPkG7+rFa1dVSMGDoFlLreuG9DxxxOE.y8kie5yGvOnehNhgEsW83GyYcu+FIqiYLsl6Du9HBAu9HlNNlUQCe3JL0k0s3uZyae0h+PG9nbVVSnd9nbidhqTsuodCvbQxdO7I.zwXsqDMajS4sW+0eE75u9q3v93uFHo9X1C.dRoVH7lDQ8pwisrGEuwa8NXbie73YelmRpEIImfLpDg2bPTE32GQHMVe.uxXSZxSE6d6ak245tYYYdKbINcMwjRpPYnhW4BTMPwKv2VWfSav0DMB6PVqLl0XcwGEtPhpcxhNEF5PFDNYQmB4mukdIGSObz0TvvcC3+tKk0lRjtckULwTBRnJeOabfwmE8lzjxpKERs35wkjVXn7IlA14d2mMqM6LtFjwkzOLHyRuaZIjW7LO8xwAOvADs8X0afTaIQ7uuSlGfQmtzvUk0UgNljBzgZ4QYygv+iXSLYjTRIgqa92hnqKnvBWTkmblhS7QaQFc2RY2fM1JzVUELVAppygNaw.TLrzc6iCgejxXtKO2y87XHCaX34+GOiMiOhncs2xNDv+dwJUfgNLCveMdE.N1jwsutgwMdW8LSwhyLqYPlaAqdrQhyeUyBUYNXaU3EjhXD7yMsvaoWsxXAYTIhyTBXo25siU9AuOJszRPoePIH+qfbaq+HLtqR5smSI+xQQkUVI9nW8EjZQoGA0WJ6dDPoLl0t1iulH9rl10irCheWaZeLV0cB3ewPeyd9VFzuVkd.cV1lsJ7WZokvYYPWgDMaDIBxcKDtFi8JGM..LWA.3o6boQuJXPq+SFg4tv511u+.+.thQOF7Wtm6RpEIh..94C+S..3Vt2+JBIln3FuoycdzVKd+vEIl9mLTEpHVEvN44n6++xarqIWimM4NBpL1wP31YoD4OCxbKX022MhcWtiJboKtvw7ipUWtdXYeq+gOJo3SI3b16NzhJ9TNsddYOrElUWEq64jG9mOpMJi0XiMB.+y+t5Ohgy2BboTcUFRzGgIqrTpVA5LIGudYDC+RsY6fLpDc7Esf5ppNrucsW7+aly.JuQWq6Y3Oy4pnRrr69NwV17l5UaoPOLkAfd0cY5vG4khZRNNqF4xA+cpXOKtt4BXjmqbgy.Y2jADTSs.zndT1t2O1Vga16IfA3DPYYL.frCpIjsN9loYW1UimqhJcv8l7gd8NpzGa6DJgvT4ZmLQfsvrZOB0SHsVIq0tlOCKXdyhaa9rTXfFGya2xCbCLZvHD02yxTtrnzg+wy8OsYrkr77cXcyb1yl62ind0nf07Yba+qG4WwDhaBn4qOvtXOVYkUB.fYj2LIkw7bTlTK.DtNMDgFfHz.jbbHU..RYrtM9EE8UeMu0a8Nb+t6Tg+A3uQa6ogsh5yGYmw0..lLJcMeNSIHv9h.KAAeDSUgh+wj+m3wehmDK8VuctwasJGcgcDQpk2iw2efe.SJqrwmthOwqImxMtxwLZAmSidUPi9d9ClQPH6QfuSfv0HfyxXcWXCR9074azlx7.ascQJH7PE23zkew5AzYq0yFSZIgc1UxQda2Z93TkbJ7S+vOvMuqVnXI58ReSLI7LO8xQ4m4LtkEet2669w3G2XwW9keoMiG2Y0fNq1DpujtBR5Y4+XwvnpSMZ+XcB0QwbsnR0Jf43s0D6iebiE..JZTIPBL6SUeU0XaExTlB5s31Vhd2XqqUIbWBB.0K0BgbfMrgMhe9nG0gFgMS4hPZhwJcQJ9Ms1499Nfqx15l175SmXcV06Js1cq5zkFRPYmd0LCsWH9cW+DwATilpnInViZnIwvf4AZh25k0c9muaricrcDrVW6Y1F0UvXgn9lXRbiEUcpwJueaK.j20P+ynxKq1dv6.eCAYTI5WS8CO+a4b21pSWZvjdlDHRwoUxoHlNcogV02FF3O0WbwqrQutLGvRUmCn+8NxRulhOJmuHYJdidwbuEBB.GA.4J0BhTCeE8T.aa2RtCk1XavaEBSCanCVv4RzrQrt4LJ7CsNNlRlA.zDjBjZHlPBJZGIREwUOMGw4KQ9PrEqAk+KmCeyN9ZtwlZtSCpu4HPaZrsYyecYkI..TDjilRc.oMPTdKm1lwtjgMTa1NHiJgx1YhDBc5RCe5msFrm8tO7nOxChb+74iXKVCp8X0AkJUgi78+Ll9eaZ3LwbQOv6ROCQTuZ77OHiaaapolvq8purfq8Zy7Z49cUwaI5O9+9hu.u7K+JXmqamH8qbrdU4MPlNaoGzCe7yvnZ+WGVMxq9OQJi0MoWeLiIVLg8HKc9c6LOr4Va24KxE4mOpsw7kVApyXrjnYiXFgzDVP35wBBWOlQHMgzQy1zLwIBrIlpBEQejPQLUw7hMtkp8X0guYGeMLa1L99Cv395sV3WA.3P4nHpnD9IQhIJgKPjrwPUD0qlar63ttaL9wMVnUKSbkDUcpQSmPOJbMaFpZOHLlqbr3Mty2DwdB4m675ahIgW8U92XNyc9..vXChmQ1l5pvu93OwSxk4oVeCpXOQX.arMnXysii8H+nWRpIHjdFtNou9s4uf+qJ3dHdtKSCtsQv7krk0lRXnCynOgn.oFbGdjJPOSItn6QeBw4A2kdEpboFIKQuGznWEzTVD3seo2jaLc5RCo+ur0xLiebikqeSJDKZIKl2wO9QON.+I0qMvZsMVkvXQY6JQGl5DK8Vuc7Aq38vAN3Oh0ut0BSFMifLpDZOgZtXLKhjivq1TxYcaKKwldLn1A6nkXVvMdSLxXat10arts8JF8X.9f2G.LJIupkW.2ZzoKMnpvNQm4JOBx+VMz6qn0NbueNWQHiwTq9buEUFeCJnkwZ1buin7Vq4NQ5nYjNZlyZRYGTStTgdM8QMJGFy9h6pXk3B91eqoOA47VqRIlC0oqgv2SgqQ5Rwa0EGLd6W5MwpV8Giu+.+.d7m3IEz0AoLfAzsOObAjucDQDVT7Rrh+pYSlvJ6RIkjS1Rblo8DpwpetUiBWylQgqYyX+aZ+HhCnVnCSOhXKVCtvuTEN1AOF24qtek+2WrE6VWk9Y06I6ImblLpu9FvLxMOrwOY8bxhwOoIT37VKJbdqECnt93VmOOAaeKaymeNsC+J29S3+SckWlu9Tx6ITPkwNcuGWz6QoHQJDrDDdRhpN0.arMtWp2FiUkX4RF1Pw3G2X4Bn9npyQEZhHhHD8bnTgJzYqLOTvENek7tFVqectJXl29hBK.vOe3CA.gKCDCHk9C.1hkKCe+A9AX1rYLorxF6eS6WT4zYv51V1WwcVl1UQsGqNricrcTRImB+5wOA..BRI+O.EqL5tjTRLe9acItXF4MSDUTQhAODKwY2pdrUge+X+NV5sd6XNyc93Mty2jSN6EgeWBwPP3InWuaJ8z7c6aeNeQtIG6HGA.KxiebI7eIp5Ti52ViXKapPtXYZcqbsH+QuXASVV1fo2ZRLojE87nTgJzTELY9XEUTAuqg05WrEAUVTTsBdaWR.c0Jk3AiFLBMvQW6WZokfzQ2K.3iopPQq+P6XMe3mZy3494y2lsGwvuTnSWZ3P6+PH8Yv+4Zlyd1PcztW8Pm0BYgXP38KlpBESK2a.IFex1311Nq1DhJb0ngcy3FUkJUgvzDZ.eA0kfn2F85Cf+dBW3BWvlsOWEU5VU5d1p0uPvl7.kWd41L9IKhr9lLCedTpprckXKapP78G3Gv5970fG3Ad.GVSxh3lLVJ5j+g2P7P1WWNvjQwqgJp0Xqk5xImICEJUBEcoe3u+GmD.cEyU.c6hmp4J.VyG9oX8arPN21JDSJqrEMavDpX2pTgqIalBV3POPc3gv01xrFEmVImKTiIrXYJntarMW57Enfw5pVpEABBuJJAYV3tME8G1dir+0+5Ecq82UpV+Ymw0fcrisaSUz+v+rkeur1H8ocAJyKe784w4hRiN+u6cW2pYMrwzkq.qqHA.5WJ7awMqUPTShLV.6.GjIiBSY.C.G86s7Qod81F+khYYI.fDqIRt3spv4sVz1mznMtsc1ybF131VOIV6NW64Rujgw86r0yM12artvDfIF5V+5VK.rTHYs9X98G3GvGrh2CK8VucXrkdWYFcSUKeJ4IDDdCTBJfI61r90+430di+CNWEUh+w+7EwJW4J3lioXw1yYpif4Fp4m+hwW7UaG68a2O9GO6SyMOasDiPTJSpE.OMcVss23+mNzg8om+xOyYDcdEJrnDzjmwTvt20NAfEEDERwEqsJEaLpMzgND.HtBnwTUn3c+yuOlybmOV5sd6HmblL1Vga0lxqg8DjKnPq8zTiB2NksNiLAr72D9JQHrVjj0ElB8dy9iIKrkiDBBh.CHypzC4u+XOBF1vFlCMV7T03Y9nc5ZaAyZ5SEkVZIXAyet3Fl1Ts4FVCrWW785+P24l89ZFxPGlyWDOvW4vvZqnEcjw.iMvDX+gpITGb8GeJYjbxLVSqtDXJuBG7.G...Q1URFXuBnVihFY9r9AdfG.evJdOLi7loSeOHlhZ.vghfK.vl1vF3deIDrxu8HTbxAH96MVZrIlOy5t+MqGvoc9RHHHrgpNmasbACf+l6jr3hP3LEfzoKMLtfM3z1NzXF8Hc54Rq4NwqeIpvMNrE3PFtN7vMizgvOoNgzRD0qVvaL6owdW54pXe8+xSwfGxPQYeaI.vwV5UN4LY..tXCiUICqy5yIOiohsukshu9a1kCU1e9fsUD4IQnxxggZXtPr3hNI2X6dW6DoOtzsYcVOuP0qMqQSbLewxYJ+rX.ozerza81QkUeNtOmN4IKBW+0kE2eyhopP8U++UY9hSRWrGe34hfvqg610HD7Q2KsFooeL5OvDTnGq6AV.d37mKxe1SG5zYomocO27LQAyYLd7Bw5DTnmqh5y9p61c.HBbfM1h102rCtwDp9e4pHTIrvZXUnhkIOioH3ZsOP8SY.C.JTZ4qdN4IKxl4U1tRzmAGO..doW3EPlSbh.n6+9x5xkgmjlZxh083Kn+sddgv53IisYjWQEV972Z2QxWblQPPDXf72OJxTlfB83uFeK3Y0A7syd33rO3zwYevoiGMo1coBFKAQOAqcU1YJ+rbtMjOKkvdSb9b6FeXcIrvdktXw9hEanZDt3Caef5mbxIi5NesNHqVqjQKitCL2kNOTbwEgzGW5H2EjGhZRhWSzDBiFjlfc25Liz5rsL2EjGmKcYimLwTXronYjeVEtYiyLEU26nvbC.TaEmUpEABBuJjxXDD8b1iu9Dx5pLEJTfANfT..yM44C1fEWrpguP3tUn+yTNyMM0pUKBNH9y9w9lXRXu6ZO..HirlH1xl2D.rRIiFUhNTaBFmhBlV3zrBAXVgvkIhhAqkBEy5dVm0m1CaYzvS.alQB.LrgMbAU3RmtzfQCFsoAiaurbsSJCricrc70eyt35vANqzgDHw910Nb9hHH7iIHPYSIQ.Bmn4dOVJHpIEAxEVT9RsF0HrwEBZ.1pvxYJ+rXkev6aiqz6NLmENWGFSkpff8p2UQEUhAjR+wkLrghkr30gbmw7gRsLJY76+wI4TVhk9co8Ce1a8o35u9ofnhNZ.z8h+K6Cp9O4i+H.vu0.Y65.hUyu7znUq1trPmiwP2jxJaTSyWjqAietJrUQxHpWMB6JFH91cCL4bxlabmkHAtKc253FQOiqK6Lv2rSlhEddKbgRrzP3CnL9FLHP0YLBBuBJZTofUf9dJMDiQFKF0EFAfQ6TDaCaZK3c+OuM.fCAWNKNKv+iHhHPMMyeMdplJqFQ2U2BOhjE1EhlijQIiye9yC.6h4ow.L0bmF1ZgeElXVYhbWPdv7.cekjXsTH.vwOwugRKsDnSWZ7ZMP1X4xUrzV2kbWPdB1eRY+rhMP8AXTrk0cj12IC.Xba6MdO2D26yHRNBnbX.vA0g693r53lOjF.PTRsP3qH4jiUpEgd0zp9lfO9+7KiuAI2TRP3kvajgetBrAL+blUtbwRVroGCuq8P+zOJ5whs2IZxritDi0Ui..pBU3uJg05TrmKV2QFSUgBCZ6Dpu4HPte97Qz2UecY2QJDJTn.W1HFNvUEgTB...B.IQTPTA.txIck7tF9px8tJK8Vu8t89xlzBreVwFn9WwnGC13mrdzgZSPmtzvV17lPCMznM8wyNTaBFlXmbtrsowazqpLoDC4sFBeFaayqWpEA.HRosnfM7E3Yevo6Kkk.RXKCF125jHH7VXJKUXwIZoTJnHYfZSvwzr93m32v5W2Zwzx8Fb5wryNcsdg3OcnC6fqHYsN05W2ZwYJ+k7JEnV6caaDIGAZYzc..aUHlsmOJV1e1cPH2.efC9ibedXr41PvHXnTMi6zs2cj..ieZiGe1a8oXtycdbiYtB30rvJg7hPBKLHMOBGgTCYYLuLgqfwhBWfGWOXcKNhvulxjZAvZ5PsIT+nZk6kP0hpW9keE..DbX8bizaevmyRT0wXktoj6TA.vC7.OH16d1cO97YObts0JKG0gZaus1+7E923ltwE..fHi2wphOfqUVO3iIkU11rs8kyiqbLiFeUgeI..LGOy2ITTQ1VRO..vX.VvsdiHhDh.ZhKLL2kNOne3ArV.ysPSi0J0hfWmH5WhRsHP3gvTqtWUUPPKiQ3YgOWiTYWwPCgWmx7yO9dbdh+2kyUdEzDmFXvEh8H1VZDevF741GCZJamQIsjmXh.EZICC0oKMWtTazSgUwnG8QdPtwBezgAivQkTO3AN.lZtSSviUxImLpoHm2mDYKmG7AahCXuKhCxnRFqHNY..U.PUWQAnjaqj8H0B..PHMVOLDIEeUD9GTW4k4VqmUYr8BfI5oEFBKr90+4nfUsBaF6nG6XRjzzqixjZAPt.avi+rOySA.FkhBNcUv5.AmssDAXaUj25BaJaFIxBqhUrkQC6ol9a.y9iuQDQ8pQSQyX0JWQAPOAlxRExezVbaaaZZG0o0QEwNvA+QricrcGJQHVasr9lXR3WJ5n7lco7AethjMVuXcaKK9xN1.AAg7BxxX9PNYQmBCcHCB..502rC8yRh.K7zkd.OAMMdiH+2zhhIMEsQzfZacCFaaIB.3bm6bPSBZrIaEArDX+rvFWXrMPb9hKrNTaRRT1vUOu+uOwSv63VWDbEifCJXv9Wb1x4AeYFI.iaa2VgaEOvC7fNMIJ5ExdfOz3.m3T.CWmu5rQHWHbq5ezZZrVI2pqTLi4C4jEUL2uul0sAITRH7EXuBLxEpKgV4dYebUYMmo7yh0ut0x0ldbFSM2owk8lrtqToePyRG.XtyaAbxdzoYaUUvZKExZkL9xtTqgsbdXOwTESmJH4IlHzoKMr90sVTZokfoj6T4JsERErMa8dKnuhyH0hP.Ga9i+XoVDbYFXRVtFMjF8oU3KdOYhZYrigvo9enGjud6aGSeZSF502LdsW4kkZwIfipZw231KdnLo5D6MfUgC1.7mO3q+HpNLlXy5q+lcw05d5rZS.82KHjdHXcaKarrMwrxDFGrsVzzZKEVQEUfnSKJd6SlFZx.BFcUc76JtvXq1+1aovZ5uAj9+ZrHcXIyS6PhiMLopTrHUzVyz81HjD3szsPtozGxJW4Jve5puFrss9U1D+Mrk+BhdFU0jj0SPKSpNwdCd1m4ovu+6+NmBJrkhg4L24ysF1ZEl0vZQo65Ntcdab1xQZZ7FQte974p97Fz1onwx1oOcYn+8MYGFeHCcXXkev6ibuYlOiXiKrdRMMivNZnFf92y5jDVSLojJ..tXMRa3Dn0tLuMPhppKDjPLsI0hAuLbcVdfp1hLZITRXf0tzkIkBQfLInncapAQ21slOV+5+bts0oKMb4JoFKNg7.1+W059pXqCfQwhHij+xAAKFGLy+qypH1LuoY2spl9RAFz1oS6cmG+D+F142XoGIxFeb.Ls6H6YhYkI2miVmHDDNEdciSm06YKsEJCkIiWKoLosjYLvTr7z3c1p7TwktKm479GVZPpiWL.KVFqLoTHBjIQyFw5lynvwaeb3302AJ9B0iMtUluPO+YOcrjzBCZMSJiQHO3Z+aWMx3zWK21JRF7l4g7gAscZiq2LA3PuxzejKb9JQCMzHV9xeJaF2YMQ8nSf4os2vl1B1RgLsFIuYKxxKhuth36SOeabSaEOzcr.IyBNVaYt5NaE95VyiWgK+xub7K+xufe7Xsgq7RkZoQXVzsjG9vOh+1VVOkM+wqxsVO4lRe.IZ1HRLHiH63AP7gfWeDV0YCHEw7ETlTbRO1AOFReVi04KTFQCwXDf+NmTuVd1m4ovAOvA3Bve1LkzZrt0EwBqaamyrxkaL+z3xJfr+EqZfCg6222gBEy45jFkwr1xbGc++WbkKbFRhb3IIo92e7K+xufW5U9DL0LVHRMIJ97bFhl9LMaVguRNHH7lTlO3brW6GveItobUXKRr8lf0ssrJhoSWZn0DcsaZabvsiLxhoBML0bmFxcA4QUSeYD00+z3966i73EfxpLbetLTUcgfMtosxscokVBh62OsOWN7Tv94YeujQxM1sc++Wr2CK8tAjOZRuQLk7liTKF.vIVF6zF.lfu++OIHHjYXe4dXO6ceRjj3aY7Ow3QFEy31VkpUfVGfQmFaYrXPamHl6JQj6cYIwGj5LlTLLbd4sU5MVW0d7iYV4eW3CdxGF.LJM7.+kLwT+S04wOO7g9VBBu36YIHxm4srDroOZUX6u1+AS9d+ynlKYft0wKlZZDJaqcXppZfoVaCc1Zanty57ZjWDwEKBIFau9NnniDHRl3frljiyoGi3K87n1C+KbaGVpCF2wqsB7d26sgRKsDrz6tDbcYmAFU5of9DWvH09oDZBk4ZAqCjdeA5aIH76kEA16AziMsoMg6589TTiu6zKXOPjUYLecLAPP3woQCAVA+pbju9a1E5WxIg26cdaoVT7IXPamvvnjrRlhOEiFjMVsi26G0T0WziGOUsL1Ihoj2bv1175QokVBV18ynzPd2vfw.SxjWQQA8sDDNzuEI9zMTD9lcx7PMy+NWFhXRSG59t8hRKsD7N22CA.f7V3B4UYopKtTte2WUauzoKMLxq9psYrZqnBrOdZSZ5Gxki1UGJtiB1DpdmaFa7iVM9lctO7MB2Q0rgqK6LPxI6Ysl129cGyFuUnSWZ3NdsUfZRaDdzyiSPPW9GjyV.Ag+BrIFAg2i65Ntc.D34B1tCVmMkM1TSRnjDvgO69QsqNTD6c7PX98MQr128MA.5RoAas76htEKsHKsZTaSFPZsUdDhSWoRbwZ5.kdlFwG9Qqyl4l4sjOTk6sfFTGJx94eaT8WrVtf+1cTzhQYIF2hG2P7rQMupvz.jP+bX7D.vkrnkwrQUmCsWWMH3D6GpSMSwMtt9mFTs36G21MbiP4YJFsWWMb8rQwBvc6+72SQdKbIHh36CBS2Pg9gb4bxob.J.9IH7LTF3oEtDSUgFP0uAIkvr.abjA.7s6Kf1ssAzdNoc0gBL2aG2wDxFsbjuGm9X+L12tr8A67FYbWFYkCtjbmOpaDikqMZ0P7Igfy+9vhm0hPXUWIZuxyJ39GbR8GlTGFLEZXng3sst+IIU2NQpAbMDeR.cIirV2bt4ee.foUDYSEvugZ5QkwDqSLC.lZHl0kthV65k2jXNq6+8jjxXDDdFJSpE.eM12Ps6MxW+M6BM0jdtFud.J8J7bRc8OMf9mF56zuYbSOXqPaULwaUmmtHdWuoVagyJO..Gc+6UzGVIuEtDDR3gCsIO.nXXoiFhOIHTzoYHxXYTfv25BMIAt2qr3AKru9SHpaJ+0pzCPMPUB+abHKGIbeTEusId87tk4iPuF0AD0QrtCp6ppoO4bxVhkDOGsZPVYA2SC.ahf8M+wqhyZJdaZWcnLJmAHpxAVGCaWY92GtRmbbMAIxpUDxdn.3mffvoTS+Mfb+bKYEXa.nsdoJhA.nIwvjZQviy12x1jZQvZJC1oLFAQ..6QnIDsNiQP3ufdEpjZQnL9FTQ0Ts5q2BycoySpEABBB+THkwHBHnDyRdVwTFeCZxnYerXP3KPQxLExU.fLyNarz68Vgorj7GHvafusHPQPDHPUmys2EQCf+xuX8.5j9tYNAAAgbh5RnUn9li.4dyLttsFnWhkHuFRUHrTF3K6jOaIVhkKBBYJc1hA2der1xXNDny6beeWOQdHHjCHoYBVmsJeq35DDxXJSpE.BBeIjaJIBzwW8j87W0vqfJFnDxehoJI2M+DD8FPv6GQJiQP3YnWQsXhfPRoaDKNDDxHD79DJc1hNuB0dbogfvSyIZlxZQBh.H30BBcmXwgfvWi9JNiyWjcXsxX79O+UY1S2ZVIH58Pgqwy2FUHH5E.YoYB+VZq4lc68gbSIAgmCpZ+S3WhhF8OtUP2whCDD9C3zq.a1L49GB+Z1iTK.AYz+3FcD8dwjdYWV+xqkw5NVbffve.quKQY7sfSStnmfnGQD0SwcIgeK6QhNuTK5inWENUYLBB+A90pjEEcyxjZAffHPlM+wqRpEABBmRsUbV2debtaJ6jZmKDDtHkw2fT+ojfnaA0JlH7KYe6ZGt893TkwJsFxG8DD8Dn9SIAQ2BdcUYLmsDesbPP3onLglvok1BBBBWl8v2f0WB8.9DDdLZnFoVBHH5tTlPS3zh9Z4WjJ2KDD8DN1AOlTKBDDhhHOvPY9Pwvd1CeC1Y805iECBBuON0MkTyBmve.YxCMrG9FrzRKAZzqxGKJDDdDJSBO27dQcME8a9Z4ffvkQSiB9vBh5hD6UF6n7sHpkHQH2QF8PCmluAC87g3qkCBBWlVMzpTKB7AugNCkQkDxYBoQAMLfngBl8Jiw6QgZIRD9w3qiExx3avVOWa9Xwffv0Y6aYaBM0d7ghg8H30twUxw8kxAAgKiRisHzTh59F6UFqL9VTYsQUPbB+V709ubO7M3A9pC3iECBBWCQbgtTm4I0CArzbymfx2LB4IsWof0XL2xxXkw2hJqoNbeIhfn2I6guAKszRPrEqwGKJDDNGQbgtbPim8v2feWgqGAaTV5ZUhd4HRLM5VJiw6hOTIU1MDIBBeCmRQXBMEuOUsWl8HzD0dr57ghAAgqgHtPeO9PwPH1CeCVZokfv9w85iEEBBwIXisJVLM1ysL1N222QAwOgrklMKnazKyGJFVSg7N3Z1LhopP80xBAgnHhKz2iOTLDhMKzD6pf2grNFgrBsE8KBM0ogStejKYYL.fenUJavHjmHRLMVlOTLrFAuAxo1Po9R4ffPThsXMnzR4sh12.jGJiUOD3gaHqiQH2npeb+BMkf2SfE9tKFu+282Tb0tgHQP36PjXZTph4kBf.A+72t68gnOBYcLB4AEuiSIzTN8lG9PJPnI9fm7gQTUSgQCgzSbkbbwbQYANa+4SYr8v2B23V2gXwlCAgjgHwznTF.xupPSr5ma0j6JIjbh9Hghuc26SnoKvGJJNiMCQh+yS9d+axckDRJAarUbh0HnhXmFtv8h3SYLAehnu7htlfQP3q37JTKVAecO9PQwddUHRoAXu+6uEQUGEGlDRCwVrFr5ma0BM8dg7vEkVyxEZh8sqcfZeuWjTHiPRHXishZeuWD6aW6PnkrbW43vWAl47.XI.HZ6mX+G4DH2qdjHVPk5BB4AeQyggu9m9U9lZuPZe59V650T3ax5quNz3w0igeoCAsDY69VIinWMwdhvvpdhBDaIKARaaPhONB.lD.RkuIK92OA5Wy0iDRannMMQ3SELhdunowZQ0q7Uw1175EZIGE.2kqbrDpZ+EM.xjuIZLp9golPPtxwlfvqhdEpv8tkeF0WOukLhWC.RckV8..Xl.HQ9lr95qC+71OBFaJiFlRvLLoxruU5H5UgF8pfhcYBexq7IhsrBAv+zGIRtKGA.2H.30G+E+6m.0cn+KFQ+6GZMoA5akLhdcDyw+Qr6m9gvOc.ACZe.l+esLW43oPfwSE.Bl1WuzedAXAgq2UN9DDdM9fFCGO46sVglNF36q997Qpf4lHQI1ht1IkAFbNCB0NXC9Dghn2CQUmZz9w5De1a8oNaomF.iBxiqaDh7AfSaNk4svkfDyHGTSZiv6KQD8ZHXisBsE8K32Kbsh4VRVdJ3htnDPXkw.XbwyhEZxUee2HxNnlb0yCAgGku2rVL2+8ZDZ5UCluzVtvn.vO6JKTmtzvUOyqFJGFPCwXzKKVDAhDjQkHppBEFOaG3z+7oEKH8slF.i2PjCUcemwqBf60UVXFYkCFz3tFnZjiCMDeRdYwhHPjXNaI.UcNzvo9cr128Mc0cysuGjXJikJDw5X.jBYDRCGCgi6YC+nP0GI..cP9EyK4AlGvQTKjYM5zkFReboinSKJnTqRXNRS..nonMhNTax6HkDxdhpN0PY6L4dkhpU.SFMiNa0DZphlPsWnVWU4KqweRQLVJ.hXr.9PmtzvHu5Ih3FxkB..UCbHbyUW+SySJaD9ADrwVg1ppfa6NOcQ..vTqsf5JuLTaEm0Ur9EezsLFfXJiAvXhsmTrEPtrjvWxVZKBb2uwmI1RdM.be9HwwcYTfIak8XAzx0NoLPr8MVtsUqQMzjnskfFkpU.ywKb7n0ll1gAsc5oDIhtvUJeIlqvwwL1P6vPMVbWcqFZEaeKaySJZVyQAihXxYWSJD4CWvkktKYjUNH1j6O21gDd3PaxCvg0oJ5XAhJNmd7zmPxnc09tRYSTUWIT1ZK9ryG.foPCyiX4QMMVKBoQ27eEq5bnyVbL7NLVW0nopssDPHRc.ySw+CDorFIFNSYL.lmVZjhsfYM0bvsMhXQ5n4tiLPP3TNFBGq330hMtUQeRE+gXdIZv7T84JwxAQuaZ.L2zX4RrbzSYTf45o++s28d7MQc99i+WoIogzoIMMz6PflRaEnbU.wUErX0EP4hhqhHdP1U.83EVcU+54bVU.c28r9Sbc08xYUb2UYEPbOhVvarJRAbc8BbVvUPDJzBgRokRujzz1zj196OFRZljYRSZlao88yGOxCIyLYl2M1z7d9b48mH9cTDhDaOf8lCpt+dBhljwxGQw.PF.X4277vMjep36ogZoLR76bZRFeYGFvacHGQpVh4WhVWsTJX+hvqVYCCxfL9SB6WC08MsDqVNX+7DMMJIxo8.1euqh38DEMIiAvd2GUfnb7tX2dA3Vt5ofRrnC4q2GFUOxaSlRR7bBMFg6dRBGwsFTe6cgCbxZilDv7KQKQrfUJX+hjaDwv3IiPhQkC1tH+cv.qjvB0MB1OOQs7LQpbJv94neMDwwlbzlLFPbNdWVzbuNXNE1Ea7wkUpA19HSAfQSuimkBzzARsGZ7qjH6qACmmWsmjPa9X++wt6pGT0E5s6re025ci2KWhbhXg5FA6OKSBTKlQ5+ZArednhf9uCjS.SHzmmHhg8.1OG4+yRUKEWjXIYL.1w6x6.Y9WrKalWErkYXKH.AXenLfQar9iBIRpxoG3tiHWY3cb9likVuRJrGv9GbGn9EM4ewGSBre1yxE+2AyBnwKy.c6gms0L3dCHUbw+6Aw.2OODuxOjGHj+cnnD3F33Tf+jnB8yQA+7Jj1Phq9aFLKGrMQG0sJDkvo.6LlTv0QUBG9SlKZTZbdsh2WuZW0H1uy3nIAoJ5GwBQ4EKe1p+b78kREwykTIzDdhUQ6MXTQbbMRnYAregX0.nG5A8PFdTATWEyUBgPHj3lX02dkB1tK5FAMaVHhmV.aBXU.1VAqZELVHDBgPjDRw.sJev1TrSB81DpWsDbcHCb3u+782b1UidGvjDBgPHCnI2i587Q3CVR91VnJMFtFThehKgF3iAq5HbLBsOZfFSHDBg.4OYrDAh8.rTMIdGHkDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHDBgPHxGMJc.DmlD.rDCa2uRiyqYZ8wwbH.zbbbMBkE.LQA12dhwyU0W7AeNH3Otizqgj3xBX+84nc69k+EezeulB86x90BX+cQwzUKv1OEhse2tYHbrUcDNWUDCWCBgLHibmLFeIIw2e3musEMIAQjew5WlEIUbw+6Au34Tr+B4Dc4C9SBhuOWUJOu1QJ1ADQTDq2PkPp9hO7mvXEhz4kPHRLwLYrIA1+fu++a9H5tKXBQHs.1uP4WiA9ewh+a.oTv8lQnaBgDOND.d0K9PLasdBgHhhmjw7+EG2HnuvfH8ND.dPLvIoLKf8yNkdwGTqVQjRs.f0B1argPHI3r.1uPrZ.zC8fdn.OVKRrsb.7NP4eejdL37QEHximVBgnhkOXalak9OjPOnG8.1eWLQyxAcSLzC0wiCBJgLBQUIZ5lx0B1VCSUzMj5zoGFMZLrsmZpLPmN879ZLYxLmm6xkSTSMmAc2c2HiLxDYkU1vnQivfACwc74wiG3vwoQ6s2NuwkNc5fQiofibjuIv1rXwBxKugK34zkKm7tce97hVa0cXauyN8.Od7.CFLfrxJ6v942+9Eha2sBud8w69Zu81gOedE70JidAv96kpckB1tFR0L1Im+bVHRwXpb11PRNEjo4r383S2PFHYM55yyaZ9FpnDeGy2ggiFpVv3J6gjSLc9Zsq1fau7+Yny6rdzQmsE1125auI..bmKdkXbYMgv1eccbNAudN83Bs35B7tu1ZuUriOr7nIrkZGBr+tIMNxHDUfHkLlEv1cJWc+4DaxjYnWuNvvv9G8MXv.RNYtI6Te80gFZ37Ad9XG63fEKR+MrczidDzXiMF34WwUbUR90LXM2bybRFylsQ.a1FgrFCQha2sBmNcBqVGpnjfJeZt4v+Nf1auMzQGsiFarwHlrXPtIv96npUOH.d99yKzeBSYmddHYsIij0Z.oqO8vNNltMCccy+MgPF3pcssiN0vMIR+Ic5ngpCjLYenbvNtEIDhBSna20BXGaAQ0cyyvv.ylMizRyBXXRMp+B7PawG856669dfft6tKNOOzVtRI4vwogCGmF..m8r0foLkoIIWG9R51+1raeTn4laFNbbZAaUvK5UAaWnqFu69WE.2Yzbf1sW.ttYLOjQJYfz0mtvsvD+MVIYPHicYDFA2dHHMLT.c.EmSI3Jt+qAN7bRr9M7yizoYgf8FFnA0Ognv3K6mnJQL+cAVVYksn05I9aEMoFCSpbZYL4la2b6ZQ0TRnA2hUc0U2w84qppNAZrwFACCCF0nJB50GcshiEKVfEKVPs0dVTUUmTnCKMv9EIKOtCTw0ZQTjH1pV1pwHMaGY1U18tQJgKhHvXWFQw5JAO+peIr8C9lX26cWBcnqETYufPTb7kEPDGeKFLX.1rMBjUVYKzgP5Ctc2JmmKWIgFM5ryd6dP9FadwB2taE0Vas.fcrzkSNti4tgN2byCZ0pEUV4wE5PtSv9EJU2+iTQUo.XMQ5.V8O5QwnMNN1tWrqHcjDR7gwqIbKieY..BkPVZfs0wVq7EUDBITIExyKEQ3N5yM2bwDlvjnDwhSAO33kpwjU+UviUq3sE6b5LhcwXTKqrxtuFScqUTtPhiWUncL+4rP77q9kv3LLYZbdQjM55VOVvjt0HcHOHnYWIgnnBMYrWUnCrvBKB1sOpntalHBK3wAUnSpAkTnCZ93sE674iaetEOszlMai.LLLBs66D8+0JQwzZg.Eu06bwqDycTKBLdMIuQDg.1VH6QV4OUnc6u0wHDhBI3jwJEB7EI1sWfjzZX7Mi5FnyqWtkEhTSUvDLjc9KGG9oSW70xXg1crwaq.NxQZOR6d4w0IWbrb9137myBwzy3pnVCinnJVWIXVyrLg18xkwPgPHgH3jwVNeGfUqVQt4lm7DMJjnrLJHJBcv6KTsQSID73EC.vnwThqymX2crVrXIRsN1xi6KP74FAO2Lic6EfqsvEPIhQTE9diUvJUzHQ3Kt7DBQlDbxX7VuYraeTxTnnbBsEgjRglviZprVH1IkF76qhU2wFgaLXjn2EWak.ue94lu1k.icEeSDBBQrLbsiD1sWfP6d4xXnPHjf3OYrRAOUXewrrUPXEZBOpoxZQnUV+3s.7F74Sr94L8zsFoVSb4hxEo+oT91XAIWrLGFDhvz0sdbcyXdBs6RkwPgPHAI3jwBiUqVkuHYPBwdPxKl3aoUp+Rp94Tud8Q52KKUTtHwt7AOcQ4ct3URcOIQ0YjlEbrWpzstLgLnk+jw38CfVsJNq0bQC0T20IkBtq6TyI6Fu++Corqeiv6aSDJyrpj2qY9CsPYNLHj9Vlckcj5pRZ4QhPT.9SFKr9iZvRxQxsfGyXSdpSVAijv0GK8PwDobIepONWkJZWnnGuWSK5nR2DQch5pRBQcwexXgMEah2puNgebJppFTucgU7VxMBcViJlTocUYXnZJFQsJ2TGlP6RvoaIgPjNgVzWCfF39huPGGUoMzvlyDJlPqIXhcI2HBkjh9kzRSvVcRIFyKkF5Fl+bVnBDFDRzIaM4DocWpLEFDB4hRBBLdWh2B9YzPNKoDpAg9yq6lktVOJVEbMACP7K3qh8J2PDFOiSDpfk1kTLpdlXFDRnz0s9HcCCzf3mPjYBlLV7VvOiFgVJEjKEOlwnHW2vJppoJ8uGGsjxB9pTvfACQp0aouLgP5CWhswIztnO+PHxLA6lxAxrjYlJx0UNqz+wJoL1jpICRD5pxRkjKnvxWludDRbK6gHXWUVpLFFDBACRSFSsfwj5cRRDuE7UwblYJDylELIO49N6CqFiYKi7k4PfPhMo4Svt5ejPEzU+DxfICJSFyfBUnUUyKL5RYrIUqx.QXRAPcyBgDEnwMFgnNjDTI2AjbVJMRNEwcl80eLyqYlJcHHHwelTJMI+xvjpPwJcm8DRTvddBtbcUpLFFDxfdIAUxc.MXnTZDbgPMiby.G6PmPAiFtBd.7GuIFKms.XDhUUwuWSHpYoOjLDZW4KigAgLn2fxtoToHkEB03kZdxEDIQXrsUpLFFg47NqWIu7DRTIBqRDzMyPHxnAkIioypx2CVCwXxJcHHn3s56GJobo0JBsudIl4A..f.PRDEDUnZ9R1EMJzQmsojWdBIpDgUIhIJmwAgLX2fxjwTCRaXJ+3VyOwt56GZMKSJEgAwe9xTHPsf.IgFMH9IDkmhkLlZdlEJETy+7J1EnU4rKOivjCPtVi8T9lYkPhCVSKKg1U9xXXPHCpMnrkw5JUkeopInwxupiXuNRJ0hPWURIJQH8gLMKXxXTKiQHxjAkIi0sFk+G6t81iRGBAzd6bGeSIkjVQ87GuEP19RxISKKRDR+U5FnYTIgnzDbsojHsLNT0y.32mOoccjTpEgj8xWFCCNRyjfU2bBQUIYMBVTlyWFCCBYPsAcIiYwpUkNDT8h2toTEM93xWotvlMH3rTiPTUhvxhT9xXXPHCpoX8WWtYwsowkqwoj0gOLY45jHIzYSod8haE3WpEgRmQ9xXXPHCzD1ZtJgPjFJWxXV41xAh83TRsQNV3r6uD6YSoU0SqOluRG.DRhfEeSKUnckuLFFDxfVJ+HYePpd7odF.+ASLZgxzBZLbIGs3YDFyXzrojPhO4qzA.gLXffIiImKb2xIi4jqRGB..nylTO01hfKRqhQKTZIsdGCJJbKdJGUQ7RkgqAgHoxN87T5PfPFTSvjwj5Et6gYVvYvijKIU7ZDoRPrKRqZ0J+++1ACKz7DhTIYsBN6tKUFCCBYPKEqaJqyUmJ0kFc0pKY+ZpSmxk7YrPu93ON0oeHhPjDanZMFgPHjDUC5FyXZXRQQttFMpLW2XUDVdghZG4v6WDhDQCMtwHj9P1CIGg1E84GBQFnXIikhdk4R6cHCLGKb8WRQMAqlZpQzOm8kApiwQBQgQsrLgHCRBJzc9LjDidsSxzXssnzgv.JQXLioHeYRxZowvFgPHjniNHOy3LUCe5zAMJcP..WMK+iasnQ7N11F9HtDfO6SEonQTnH2rQ55SGHwdUlJpzt11QmZZCc1iOzjmFBrcF8lQpZSA5zjLX7pNWMBbq2E70Smn0tZCt81ac.LcCYfj0nCI2SJvXWCNZwUltErvISHDYfh09T5zvMkH4palZJ0TfpojjpBEuisMkXv6SjOsqscTmuZvIp66vWdv+ApppSFUut4OmEB64ULxNk7PlcksDGk7ysdWnpVqD00zYwVe6ME0utEeSKE1xHeLLiiP0lXY7RW2Btpab0xYbPHCVoXIiwjD25r0.8RSPnElTe9TG0YLwdkAHYlrD0yWzJBKIRDQPMZbfiV6WGSIwDrc7gkG3ea2dA3lu1kfBRt3HkDfnvWRdwY55T3ebj8fcu2c0uNGA+y77myBwkVvUfg0iMwJDIDBQ4RFavtyWa8fgYf2cYqIIU25ZYoJc.jHqEcW.683eLmjohWUU0Iw52vOOPRYEqqDQ6bGrZz3.a78eont06hF63CKG6.kiYMyxv0Ng4oXsxGgPFXYPUosXDEWLuaWt5hzA5s9G.P25nNAdfhi46v3Q+MOfnlHVv7mT1NpZqvsdwaLT5KIuXWm68v598OlnlHVv18d2E9o+1GB+8F2M7kjWI4ZH2hv5SIgPjXJRxXidDJSWYoaH7mLjbkjTvElzCcnCIKWyXUDVmGiJc0C0XqCDre2eNV+F94xx0ZGeX43E2wyHJIj4KIu3CNw152cmZr5015FvGbhsMfIgLAPk2BBQhoHIiYTmFzQ2CpZTN.DdKv40qx+Gvc6tUQ87cttGZeePR.4XAIW.4qTWXoRMZbfWdiunrdMqppSJJIj8EM7oRVK4Ijc7gkOPOgLpvuRHRLdyHRNFLzm1o7ubHYPgGj2g1BbtUAqQld8Nvn9KnWufiUMo9KRxWhO+xpZz3.a7CdIE4Z6Ogr9aRMeim+IdsstAQNphN63CKGeQCppR5BgPRfnHMOkECJSk9JIkq0S.P3sdS6s2lBEIRi7Gkprj0oJCJ0nZz3PRGmUQipp5j3ftNPL+51u6OGu3e5YkfHJ58ZacC37ZqSQiABgjXRQRFKMccqDWVEmZrkwjBe+YeiJcHPhQ9SDSM3k23Kh101dTe7628mK6cqpP93u9cU5PneK6zySoCABYPqAUCbqjRlsqr71h3VashVgtHbK1iWq3U718zFLML1+qw3ewFmHebq2kh00jB4vNitI3R08TopIQL.1YYYhZqikr1jU5PfPFzRQRFaHZTlBdZmWLYi1jfEG6nUvI731saEeP72YmdTzqOQY4KIuX6G7MUztljOez956VXxsdW3W7GdRYHZhMG67GQoCAwVoJc.PHCzoHIikcOb6BhAC0eK+RMUtiaLwtB3Gq73Q7RFyqVkojk3GUE9icGs8uoeWY5kRUU0I6yVX5SN16KSQSrQolDADBIwkpnaJCt9aMPWZowcx80RKJWqzIGZu8ner+PjWsqscEePuGI001YEbe0nwgrWBKhEsn6BJcHPHjDHphjwjKMlpxNaJABu0ab5TYaYLwDe0XLe9TE0do7U5.PMJZGWVJk5ZR3jw16272jwHI1UWGmSoCABgj.QQRFKSOh2ReRrnKcZCaaxcWaoWudT12ur.O2sa2hZWER3U9Jc.n1zt11UUC7c97kG7ev61qQiCUYWqFry6rdkNDhYoaHCkNDHjAsTjjwL3SYF.+pECuPtSg7FaTczkFgNd1hEAWiwLlJs9Tp1UmuZT5PnOIzjJn5KToLGIwtN5LwqFBlrFZoLiPTJJV2T1Z6xa2Wk8vsIqWuHIiR3V5GN6YUGewnNcBVE6isyiAZ0SQs6eUcrWXUUBgVuw7kjWZ.xSHjAbj8jwFQ1rsZR4+sOQVutLVUOIHnMUf4rv4F34d73QQp4Xh40zeMFiOxU2vpWOcm8QCeI4UUO32CVmZ31BSMooQEJRhMx0BUNgPFXP1SFanLhSquDOJeKJ+en7Rt5Qv440VasxdLHpqKkZ5sfQlTHEOR4ZFUFZQ0kvO2Ik3NoQhzLrTMwt8BT5PPLUpRG.Dx.cCplMkpI5FkGN+A65qutD5Axu2j5skG6VWlJXjnNjbOonzgffRjloeLcycB1DoYXoZxkMoumRGBDBIAhpHYrt6tKzbyMilatYIKgDi4jqjbd6u7kbO3pm+UxYa0Weh4xnB.fOHbKdNXrJ+arKiJcHHnN6pSkNDhZ55l6uW0V6pqkPLBgPDCx5frowFu.ZNSS32iz4rc2tciibjuo2fRmdX0pUjQFYBKVj1w5kRLNizjL6Bk9PuR8.AUcAps1ZQN4jKzqW46J2XUCA0BFsqgaqBkH2heCDknz5R70UeIJi0szLEdM2iPHDgHKYhTe80AGNNM73wCN5Q+197384yKpu95P80WGLYxLJt3KQxVxjThwYTGV5...9L2EV9p+2vq9h+E1m6yKN0opFEVXQxdLEOrXYnH31ZwE39dZyM2LrYi6XjSlkuRdwI8OIxc0mYClT5PHlklOJARBQoHocSoWudw+5e80nxJOd+t0Qb4xINzgNX7Oy+rjV785kHC8J41JX0WecJxLqLdXYnCOh6u6tU75JW9Jc.Phc1xHekND52xdH4nzg.gPRfHYsLlWudwQNx2.2tcGwiage+qAiMaFLjjX65tZZC3i1+2xofO5ymWb3CeXTRIkzuaIKeZY+QUamphkmm.7YtKrhG6GhW4Y9yA1VkUdbLxQZOvyYXXT0ccoV8QtUK6qeGfHuFRxp2IWPvR2PF.gjGuc6EHXwfUMIzId.gPHQhjkLVekH1OdI2.tNisfz6rUfdZs2+nqAfEe0YiOcliAu5d6MoL+IjM4IeowUhIVbpLKES941cq.e6PPms3C0UYSngZa.68S1aHGC2wPmelLYFolJCRKMKvpUwuKE5uKMT5FReuLp7ke4WDXcpjggAIkjVXwhEXxjYIebAR3JSyYozgPTI8dBekb3xlz2S0mL1huokF1DOfPHjHQRRFygiSyIQL61K.+WWaQH2tbiifzvXQKH8NOOf.Spqg3qKbs37XbkMBrwSmefBDqOedwINwwwnG8Xi4XxmNkuffVUUm.0Vas3P+jC1ud8tb4Dtb4D0VasPmN8H2byUcLf+CpFiYBsB8s7UgcHAufg6+2Mb4hsdWYvfAjUVYG2+rHUiqvAZXzq9a0l6bwqj2DZRDFX7iN2I.ziRGEDBIQhnmghGOdfCGmly1VeYi.4zdS..3Jw4i5yUNczFV4vSFecPcMQiM1HZt4li4VSooTU9tlo0VEutqymOuvgiSiZqsVTXgEJIsTVzxaRVfwt6DFa4ui270i8EeZ++NS80WGrauf98OKImr5IYL25cAFupyAwsEcp+Vhr3LGaXcQI.PFon9WLqyVSNCzRFS8+KLDRBNQOYryd1yv44++8itAjSGQeBXgJ8N6D+WWaQXkan2tlvgiSKZcsU+sq45ORMUl.sFTvl40LSTxLJBLiPafs0Sp9flVY+eOdaQCZo51vI+lSEVWZ5ymWbzi9sH2byE1sOJo8G.Ano61Qye8VvN12tD7XVzstJLDSrCp4NbcNb9ZOA1WHGu+YaqMaiPom8kwMe8ndqkWLdMgYMyxvt2qv++KkzrlYYHytxl28ksNgW1sTCV0xV8.wtnbhJc.PHCzI5Ii0Xi8t1wU5Ud43J6r+mHleis8lvRW3rwlJem.fs6s73wSBU2R40qWNu232JdreHR6J5A9R1G7hPVdhLewlFHOfzFSRXxy0Nl77FC9mu62FVRY9WNkj6DxZt4lwm99OeDGGOyXFkAmYNWDHMzg.XHSfEOgkhdZ4fg0ZZ9KCJIZk3iDIiuvKU0lL1LG22WvVVxXWFw7myBUs0arRLOQdaQOBgPhDQszV31cqbJgEW+kHdccVogLliarwKD0u1Blv3C7uSRglYeG8neKm2arau.7S9c2MXJsa3K4XnOMFSGXxOpcrhG6GF1tps1ZQyM2bTepZu8156CJBb6tUbri8c84.p1VwWIua+BHczXZyBK999S36O6ajy97Wm4HRiQkxnU5PfWyZlkgg0isHdLiO+oHSQSrYUKa0p5UdABgndIpIi4zI2tfarnEQ6bO11ahSE4tkVh9jNBVWsJ+ylx5qutv5dxa9oKCcUPG86yISocie7u5tCa6m3DGOpOG970+Wnvc33z3vG9vbFX97wt8BfqgTRDOlKfzggBWRXIjEO0mNRjYrKiXw2zRU5vHLW6DlWedLCW6HUcKD21sW.aqhQHDR+fnlLVve4tc6Efz6TbG2LWwD5saqRT9RZud8hpqtZNa6G+qta3cnhv6MioivZgLOd7H4snTkUdb3vwoELQrYLixv7uw6.K4NeLLoqecncjLuGWvZGIyaBYG6XemnDyjvc41loRGBbbmKdkBNVwBltt0ikL6kK8ATLXIyd4TqhQHj9MQMYrf6hrfSbRrTrkdGhawRgD0PDFj9LLLwUL0WZpoF4jzxsuhaCXL8+VDKTLk1Ml40v8KUqsVoasGrppNgfI6MiYTFt868kfgIb+n8gcy37od4gszHEIsijQ5Et.NaykKmIbqHAIJX7ZBq9G8nJcX..1tmb5YbUQ8wmulBw7myBkvHJ5cmKdkHeMEpzggnPM1ZoDxfAR5xgjZQRQHgKotFcEZY9H2qS7KwFSddigyyc61M75U7WoAb6t0.STfPcq2wpgkIrJTul3qfhdAjNt865I4rMgtlwfIEumfApFmgIq3I0X2dAXAS5Vi4Yg30T70q3cW47myBiojHIDBgOCJRFKXdaI7RKgTIzIzvxW8+F7YVBlpUioiv9RonokCGwvi7ZJYnhThXMl1rhptiLZz9P31pp7MKTiQTcRJBl6nVjhlTyJtgGneUS1X7ZB267dHIHhhN1sW.l6nVz.wRYAgPjYhZxX502a2HVSCh2f2Ov4reN4+RJ4d+iksECy1v3UnIQX8RjtBO6juhXuwerZJ15hV95tvEcqqBMl1rh4qcj3BohEcqqJvy84yK0UkRHccqGqd9OFl0LKSVut1sW.942+yGUiSLgjlugh0buOiHFUQm4OmEhG7F9oThXDBQTHpIiE7h3cE+8OGcnSaDN5X2wNaukyhXoXs1oLVXWCVnkYh3Y1SFq5tawuE3Bs01rau.3MSo4KvSwB2h9JsXiKsX7ZB2x3Wlr0kkyZlkgUO+GKtRDyug0iM77q9kjsV2aw2zRwbG0hnArOgPDMhbxXbaokCqU75cnpFhITwe+yC77TSM9G38Rc02uyN6sKJWzRWjjdsBURI02IBakI9ZotockySz5ZxP0stL477DkYOahLccqGy29hwirxepjdcV0xVMtkwuLQc4hhwqI7nW+Zvct3UJZmyPY2dA3+5ddJTVN2.0hXDBQTIpUf+PSt4KZnaLEQ5u29AmmanlVZp+gATvIPXc3oAftkrq011z137biF666ZeDlh9Y5HeRlIxEmSRhoh0UBdg6+OhOqlOAa8s2jncdW7MsTL0ge4HMeCUR9nftt0iqz5rv3dfIf2+PuintBCrpksZTh4IRsFFgPjDhZxX50qGLLLA5RoMU9NwbWQYvdGwWgV8HFSGa502YfmavfgXZwj97V5MIwu9y+GwUrzeoeHIAIKYrucHbdpACFjjkJJc5zyoLczsVko6eIROicYDkkyMfq39uFbXmGBez9d29bkVfO1sW.ttYLOTTZEwlDV+uNCG0Ry2PwRJ4tv0Ng4gic9ifWaqanecdl0LKCiuvKEiJkQylDFsLGQHDIBuIiw2hYczJ2byCUVYuUA9+5o6A++hipcv4FRJ3W7wbqp7YkU+eblze9BE0NGGf65+oUqVipWWpZzDSWGiFMBWt5MYrj5xIft3qTVHjjf5cg1NDUCfqVoCBohwtLhoxb4Xpy4xQK5t.pqiygy6rdzXK0y65C47myBQJFSE1xHej8PxgMAL.YIIrPkYWYiLslMl98cUnIMMhl7z.bzP0n9KbNda0rEeSKECI4TvvR2FxL4r6saTojvHDhDSzWnvyJqrCrPOC.T9e6SP9K4FvspM1WvvO2PRAOxtNMmDnXXXfMaiHBupnWzzUdhEuczsjL5p55fIGVWTlWdQWIqvrlXXMwD.YjQFbRTuS2N.RSZJ1kc49LbdtTO99hCUqzAfbIMeCEooannXqk.XEX9+6KNxu.EHAL9nqa8HSjMxTW1n3bJAHGfkTxcE4Wj3Wl9HDBQPRRcFKzY0zKrk2CuYWYF0ytxNzoEer1LwM+JUvIQLc5ziBKL1pr+YObgGWSRQW4IjFOi3WpOzegjw68xbuC+byMWI6mqP6Z3u5u+tvnD0BVm4j6myyilUJgfKsJDBgPHIJjju8xp0ghrxJaNKaNuvVdObnq7xwhF8PQIc0LFhuva6+yMjTv98XBuwdNLppJtisK1DwJjS4yHZvX0B7WfIL1V6w7OKwifG+b+yO6fv1cLCQ6bqqSM3sdhcwIYUCFLfgO7nuUCMExPXq81aCVrH7Difcr5YMP8SqppNI9dcbXz9PlbrE78gr5odr480aRlVsZMpVoDh0e2XvHeI4EtSxIpqiyAmdbgVbcAAOVaYjOXzaFVSJCUw.WucssiVgSzjmFPmc0IpqItK6WCI4TPllyBL5MiT0lRucQpBKQMtIDh7QxZJgQNx7ga2sxo9PUwe+yQE+c1+8RW3r4b7e1WebAGOW5zoGkTRIw8W1lhLmLlACFB7yeUUcRn8jWmnTqwzegjw27WOSXueM5QOlXZ4cxrOtYiE7B8tPrYaDbJlsa9O9TXw22eBW.oG0W29hqytONOOVlrFjv4KIuntdNGp9BUJdClcYRK5t.NdKGGGu5C2ulcj24hWIxenEhr0jirVNJNu15vobVU+dhOnTwMgPTFRVxX50qGicriCm3DGm2kylMU9N44UEtrxJaLxQle+dMjTWxB2kc5zIscqkUqCkyO6U+YmG1JH9p0G5OqA7V+rONr+.egEVjrzxPLLohbyMWNKMRe46+yvkc8OtnjPVNdOLd8240C7bCFLDWSXiAy7kjWbxNOFdqOdKw8DWY26cWARFhSIpPhTiFGXueyeKtKOE9S9z+r5TpKOERQbO+YcyXzFGGkTFgL.ljlMhd85wnG8XQ80WGmA0ezHqrxF4latwcBF5ivLKznQoa4IB.H8z4ds21l1F9IWwc2uacrN+bc3EV2KG11Krvhh4DVxO6L66CR.Ce3i.Nc5jSq9AQHgrb7UId8W9w4rsQMpXaLBRXcds0gW489MRxrGdqu8lvVwlvpV1pwjLMEQMIA25cgO4XuOuyTy3QUUcR7xU8hvt8BvMesKAEqqDQ872htKH501L.139Eq5Ygc6EfkL6ki70HMSXFBgnrzAfCAfIJkWjrxJ6.igLmNchVZo4vRLyjIyvnQivrYyH8zitwHTrRSSMI5myHQud8gM14d60+Q3le5xf2gF8C7c8m0.9ls6.eX4ePX6q+jHF.PFLFA5N1lMkAhG8rSjhCe3CGnti4Ogrq759Q3b5i8unKGeUhW+kdTNaK2byMhigM0rl71DRSixz8peim+Idw+zyJ39uka81vTlxTQQEWDFVd4A.f7xKW3xkK3xE6Z.ZMm8r33G633.GX+3u9luAummWdiuHl0LKCKXR2pnTM8qQiCrtW7wh6ySjTUUmDqeC+bL+4rPQaQ9t5dpD+heySJBQmvpppShewe3IwhuokhqNuuO0JYDx.L5.frsxY6OoLkR2cJ+KoN9GiUAmzxa8D.2ziz2ieL8m0.N4mz.11ldsv1mNc5QwEeIJVxJLLohRJojvRHqpW9ww7uw6.lxaFndM8cMHyDZEZN+9vq+lbawOFFFX29njjXWNzYWdj31cleBkH1rm8bwptm6AW60LKX1brk3jima83S18dvV1zqictSt2Pvt26tP0NpBqd9OVbkPV08TI9E+OQNglPShbLi9RB6mku3K2Ob5xE9tu6XXu6oBASjbGeX4.yAwcBYGy2gw52vOOlh6oeYSMriIZi6s91aBsMmVEsDIIDh5fF.TA3onUdEWwUI6AiTnja8VPsCks6Bsd3ifM9K+uCruoLkoIKk2hZq8r71cQKZoKB1lRlPeVcCuCsSnyoVnoUcn4i4EG6qNA16mrWdOelLYNlGr9gZ1WRAXxd7hEr4sDXa1rMhXtFt41cqbRHKXyXFkAaEek.FFFmDyLgVAiuyg1Z53XauY3c6JCCCF6XGW+5muO6y9T917d.Pow7IK5rV.rlP23irxepn2UX8E25cgG5EuaNaqvBKBOyytdrnabAb1dve4ueSapSA.rsRlsgOLduFa6c1NdrG8Q3TXmAXGaSO3M7S6WiGK9h6fi+Urx6F29seaBFSQhiyTCJe6uKd2sWdXIRBf3pExji39Ed9mKr2qi23NR104dOgVBrhsJDMgPhIC3KLS9zJ7OhxUcFK2byCtc6lS2UBbw0SxXXo+yfACvlsQHJstnUsIgLZK9Ks3LLohIO4KEG8neaXqbC6ae6BXew1XnIqrxNlqkbD1t3ai63k3rsa4VuM7Ja3kCz5QewWte7Fuwafe8y+bQ047tVwpPQEULl+7uAL1wLZ..rnabA3ZulYgUrxUwo0apppShOqlOAkkyMDSwsa8tvKtimg288fOzCi0s10vo0uNx2dTbzu6X3rms1vNd+ISFbKOYa3CC2+8d2XY2wsiM95aFOv8cObdM63CKGVWbV3JsNqDt319OpXLNChaYkgPHJiA7IiErVN0oUrqcgEVDLXv.b3H1iAc5zibyMWjSN4JpiktraTbJQ550qGie7S.0V6YgCGN3sUx5KFLX.1sW.UFK5GpQiCrtee3i0paaIKAlMaBNc5BqYsqKPRXYkYFHYc5PR8vy+eRiVzMzft5pG7Na6+EWnwFw+wi8HX1ydt3gezGEWWYrcy4qrA1VzL3Dx15auIL56cBXX8DcKf79SnguVM9sd6xCzZdNc5Ba702Ldoe+uAM03EfF.Nwd2ZzCfdPWc0C5zmOzhSW3lVzMioLkoFHQRylMg6+duabMy5pwBWvB3zZSu1V2.F2CLgnd1gFswsiyTC17leC75a7OglZrQjjldflt68ybwab+h+omEO6C7an5RFgL.ffUf+XYlOpl05PjupreewlsQfINwIE0KsOVsZEEVXQ3xtroCa1FgnlHl0tE+Es7byMOL4IeovlsQD0s5HCCCJrvhvTlxznDw5Gbq2UXIhUXgEwIofu3q1O90O+yAc5zigmgIjTmtfu1ZBc1dqg+nsVfu1ZF83oEnuK2HmzLfQjaVX+e0Wfu+0dM3g9IOBb5zUfDxl8rmKmq8d+l+VTG2BkPSgEVDt1qgskp116rcL8oOc7rO8OEMblJQWs0TXwtu1ZhSLmASR3yq3Cwu8WudbYSapXEq7twW7krqnCicLiFG3.GHrVec+m4yi63d1ydtbh6qeNyF+9m++FMblSftZqI30cyQYb+r7F2ku8s2uiaBgntIXxXs2t7VfTkJsKPxXJ0ZcHCSpX7ieBXJSYZnvBKB1rMBX0pUXxjYjat4Ba1FAF6XGGl1zlNF8nGqjMgGFp2v6hxlaN9mKG50qG1rMBLkoLMLwINIX2dAvlsQ.SlLCSlLCqVsBa1FQfDvl3Dmrn8ynWuJxBJXEJwEEfsNhEZWkUXgEgx291CjH1Gsqci68dXGWS4joU3yaru7U0YarIKXKurwu827hXEqbU..vrYSXcO0Sw4X28d2EZQmvU0e+w81O3axaBMO3C8v3.G3.A5hu2+8dOzZKMhNau0XJl85oc3qslfIc9vNeuxwkO8oge6u+kBD2ku8sy4325auI3Vuq3Jtey2bqAh62XKaAM2XC8y3tYXRmO7gu214D2icLiFu9l1bXwc6ZGX72pIjAyjj0lR0pNBZ0.Po4uXlZy1HvnG8Xw3G+Dfc6iB1rMBXwhEIozdH2XKPr4Aa1FAF+3m.F+3m.F8nGafw8lXOl8bqh9+uxgungOkShA9SDy+365Idx0gu+0dMnxJONFdtYCesEeIa60cyHuLLi+5a9FARPX5W1Twsbq2Fmi63sD9.NOXGz0A3sdb83OwZwy+qVeftV8g9IOBd+s+Nwcb6qslw+5wKY...H.jDQAQks7xFOv8cObRr4weh0x43pp0Ji344KZ3Si539y1WEwcb2UaMggmaN3Atu6Au1egMIroeYSMr39Dscz355PHDk2fpjw141eGkNDTMR2s3LdwHJiyqstvVZid8MsYL1wLZ3zoKbqKdI3m8zqElMYB1xxRbmXfec1dqXXWLAAGmoF.vN1zB1Gsu2UvWeK5t.d4M9hgs8G+IVKd5mhcRodju8nX5Se5Xaa4UQOdZQThautaFYkYF3Atu6IPW+spUsBNGy+px+uHF27sTREbb+Ee49wUdEeOrss7pnq1DmZZnu1ZBC0pUr7ksTAi6pN6w36kRHjDHCnSFqfILdA2mQiJ+BerRJiFTjtziHR93ulaBO+le2eHvrw6YW+uB+027MfYSlPp56Bd8HtcikFurs.4l2L6f22+3jxupp5jB1ke7MFmtka81BjPyGsqciEeK2LbV+oh4t3qujbOriC1MrA1jprM7ggG7gd3.6e26cWvWR7+4h2+Pgeib20JVUf3dauy1w2+ZuFzXsUI5wcJ55Nhw8N9vxELtIDRhgj.P0JcPnDjqxZgZ2hl80ozgv.RN8D4weT7nFMN3zcYyd1yEK6Ntc..7a+8uTfVDKU8cgt6J9KeIgxm2NQVYlA18mvFClMaJrtprstaKrWWK5tPX0vpBKrH7bO25A.aKh8S9w2OZr1pkr31rIS3O9JuLNx2x10dyXlyjyw3NImg85B88a+w8ZVySFHtWyi+eJYue60S6gE2SZxWZeF2DBIwwfljwFZCguXkOXUvqKkoZXHJXjLvUKth7fXOdbzZ+ZNOeU2y8.ylMAGmoF7.228.c5zCyFfjjXfeIqSK14N+f.cU4TlB2pJeSdZHrWy2T+WG119wOzCCaCeXri0pe7OFsb9yJows4TYWOZ2wNdO.zaM9xu553bg8ZB88afvi6lqygjF2lRkA..ext2C..l1T4lLVSdk2k5MBgHtFP2MkFGZFA92IMHavcGIYvveWzNPYFzNPV6ZamSqKM6YO2.ybxW9keE..jSlCseMqIiEI0C6XNzeQLkI0Ti3w2t11CaLWUXgEEnE89e9CuLNx270hdWpFpj.65w5wON63rpupN9g99M.6642+8xNCU8G2R962ZXi6C9OYGWa9mjF90YWCLJEQDxfUCnSFCAMiD6nlZTv.IwP+oXsRjWM1M2VbZVWSY.fs.i9yd50BFFF3SjF73QiuZ+G..rKgRAyQCUy44gF2.rstj+Vz6oep0HOwcOrsd0e7U5cY35tVwpD7vqyW3+ci4sfEB.Hqws+hEavwcncMLgPRbEg5LV3i4iDYe9GxccoSopyXpACUeuK7BYmB0MkwIwYZJFkp9BbK+Bye9rK+P9GL8oGiKB38W95jaKXMr7xKhGenwM.vBWv7..arKWwceIzjHqoIGgcLAG2VSKxsHnTxr4Au+MLBYfFASFymuD+RePRIy1xXYdJG7VnFGrxp1d+e6YnaP0JhkT3fx4E6z0x82i82cUG3.rk8.3Ud5NdcIGayF4Pi6a4V6cAzd2extjs3NX9KUDAK6z4lT4d+RtCb+fi6WYCuD5pCoahZPHjAOFP2MkcZxLz1oWT4tBuPMZwhEEHhHj3Svypuf6dM+kxBodrKIji9cbq0U1xHeNOOzYin+A7+Q91ih+wm8oJRbaxT3spUxZSlyyC8l37G2ewWteTecmSRGz9DBYviAzIiA.jx9O.12t49EACl6hR.tqKkiTiFN6ysawsFIQDOgtr2XxDaW64uEdFhLtNr1SRrsnp+YinKWB2BQ7UywxIW1wX1Ws++OvHi07O+wMP3CB9Pw2x5TQEyt1Pdzu6XAlgixA1EUboWahbMRiPHQGcPl6lE4jy8ue7Vu1eNrsmc1Ry58XhhfWWJOO2bwPkUdbjTRZA.agwkggAVsNzD85xlr+63e4A+Gnr4bCh54rSMbGGmiJjEM5j0oE9joFXp6dX+EG+Cb+STI2k.ozMjAvE+0Le8DdPM5KoX.vlDmVsZfbMnH7G2AufaeFG8NtvBNt4i+wF24psVn4hyLS4PWcwdCTAG26ohcG3em8PxAhwah63CKO9OIDBIloCx7.PVN30qWbpSUM9rO6S4c+d73ANbbZ..nSmNXzHasGRudcfgQ4FPtxoVSQC9vLRF+sSdZNaO302QWtXKjjUU0IACCCxM27jrEtbIlr+63CzGihtZ0M6Bc+EG+Te6QNBm8mRRoDwjZ7Kzj3jZtZk82uu5R6cUCXm6r2I2SzF29KMFxEmt4F2Nc5BUFz6cI2SJxZ7PHDw0.tQuc80WGb33zviGgq6N9SDSHLLLfgIUX0pUX05PE6PTw0fFfms05vt9P9SVkOtc6FUV4wQc0UGJt3KIQukxFPvcqb6RodfFANRwkN8ICmM3Bq9AYWRdbblZ3jPic6E.FuQ2riLRcuoXyebCzaErO3AwerD2xI8FLB2svd+D9i6u8neWf8a2dAvXWCtWd2HjDcBlLVjRlQMxs6VwQO52JJwsa2tga2tQ80WGzoSOxM2bQN4jKzqWdF2FRo5quNbGBzhg9snYec3edrSvaq63xkSbnCcPTXgENfLQU0rz7w88a+sNi+tJrCOcB432P6QOC.bgYd0rKkP9q0X9MyKqLNOORsZyjl7kh2Ya+uxZbC.bMy5pA.2XOz3NRxM27Pm95RVFzscqs2xOi+3thJ1SfsEKwMgPTmDLYrDopwta2shCe3CKIEsTe97BGNNMpu95vnG8XRn6Fypp5Dn1ZqUv8a2dAXSSbpH6F8AXaR.1lDN7vMf80sO7+9EGHPxY974EG8neKJrvhRT61Rw1d.vUK2WT+i0IaCeXX1ydtXm67CPNoIssXYRZ0h5N+Evrm8bw0UFaWl89u26w4XxenEhfGNU70pM0b1yF3e2hSWHCFoMsF+wM.aEz2+f2eu6oh.GSnwcnI+B.37hsjW14jCb2V6vjD22BBE2AJkIH73lPHIdR36lRgRDyt8BvLV7hglryg2WWO0wtFz4sEmnwZpA6b6uSDuNd73AG9vGFkTRIIbIj0byMiSbhiGwVMrrq5pvJsOBj8o5fy1K4LdPI.3NtjIiWZJSB+9+2sEXeUV4wCzktD4whuokFX44w+ZCosgOLLsKa5Xm67CfdFKvqaoaHxkjASvmu5vptm6A.rcQYvUEd.fr0jSXIGL+4rPNCNb+KiRSapSA974EIabnnSIbl74OtA3VA8+qu4aDw3dVyrLNkkiu66NFttxlEl1TmBb61MrlkEIcIbJ33dIK8N..a4.ouhaBgjXIgNYLudYaglPSD6e6gdX3bbkfFSNBc9QFVC7O0CfEtvEfzO0owI129BqTX3m+VDZBSXRILcYYjZML61K.+foOEL6tSB1OmWfPRDKXo1VO3gaCXj2wRvi85aIv1USuezc2pqZ9juj7BccKtuuDZQI8q1+AfsgOLLu4cC3m8zqEs0gWIqK+zavHbTac7tdX52pV1p48m4byZjbd9d2SE39u26FS+xlJJrvhfamsHQQM6XE6L0xlPSvqGl9W0BhTbWT9kvIYL+qMjbiaoIYL+ue6OtuoENe..rksr09LtEYR2+ygPH.fsNiUgRGD8Wm4LgOP8+g+reAZ5RmD5JRIhwitRVOZnnQgz9QKGK+IWKrau.dONOd7fycNg6pO0jJq737lHlc6Efm4NVBdmKYx3dNaWrIhEk9Am1Cdza6VB7b0z6GtUYKF7tSxoneNsmZgbdt+tHb5W1TwcshUgKzXiH4TRSzutIoUKZoc1jcW2S8T.n20CyfUTZEE5KE..izrcNO+u9luAb5jsK+tskrTTa80ijMJ9svZRZ0BmA8mHd7mbsvrYSvoSW3U1vK0mwctoxcgD+O9JubXwsdCh+fmO32uCMtC987wXoDQ+ZyiArk+HBQsHgsnu5wimvRzXY+G+m37izVbetannQgo9e7efEtjkx69qu95h6qgTqxJONuw489CVD1zDmJ9Am1CRss9WeabOmsKT1UcUAdds0VK75kVjwkCLdMgYMydGv1+wW4kCLi.W4JWI..N64aRTSPHIsZQqd0Bmtbge4yrdL8KisJzut08TbNtEeSKk2wYE.PlckcX2fyFe8MC.fUspU..fK3pCjjVshVbC.zitTCLNutka81vc9uw1pX+O+gWNPogHRwc1ZxIr39sKeGbh6VZuKQOt8oMk.w8cshUEHte10+qBbLK9lVppb1eRHjXmfIio15xmPEZ4oXgKYonwRFqnc9aOEinqqeNXI+62WX6yiGOp5JUu+IbPvrau.7F+a2Nd35A6.zONc6EzaRu974EM0Tiw84jDclxkb4bd9F1vF..aqi8a9c+A3ymWTaitEkDxBNQrG+IVKdr+erkyhW6ur4vFqXWtsYFwy07m0My44u61KGNc5B1F9vvu428Gfa2tQqd0JZI1nwPZn15qG.rcy2y8bqG.ri4p+iG6QBbbSc3WNuud..ccqGW2LlGmsskM85..AhamtbIZwcRZ0BMFRC0e9FBD2qYMOYf3N3VEqud+lPHINDLYL0VW9DLud8hFaj6W9mboRyjYy0Ub47lPVnWe0B2taMrDUWzruNrgoMMLsHLlvhUkVsGNsXPiMF9RGyfHUHmWrBRtXNu2+GekWFu1egsUlt+68twi+DqMPBYwSWVlrwTQKdzDHQrm9oVC.XSJX4KiaqFu5ezi1msRynRg6xOzN24GDn0wt+68twcshUEHwl3IQRc5SFs0cxARDC.302zlgsgOL3zoKr10tNNwsPsJlekXdhgE2+1e+KEHtuka81Dk3VuAinUuZ4j.Y4ae6BF2TqhQHCbjP1Mktb4jyf1eI+62GZOEoqnG11TmBlwr3VKejhxngX3jmjasAaQy95vZzXNlFWXQqevzmRf+sZM4zAhz0sdrjYubNaa4KaoA5txm9oVSfVH6z0VO7pkIlFOV5MXDZLjFN84t.b61M9kOy54jH1BWvB3b7yeNKDi1335yyqwtLhGYk+TNa6Atu6AG4aOJ..9UO25CjXii5aFZLjFzoOY9NU7JIsZgtTrfyzfq.cwWgEVD9ae7mDnqUe10+qBLSDm0LKKpi6U+idTNa6Ed9mKPb+Ja3kEk31Q8MyItKe6aOPorn+D2wpPW6SIDh7wexXITyVlVZg6T22PA1E3HEGckrdLpqetb1Vqsp9Z4vlat4.KgQ.rcM4iXL898XCqujgNtSFWUbgBtBk3h1YOR2JtX9ZJDK9l315T2wRu8.IHb+26ciO+K9JbK25sgKzXi3zm6BvkOcPaJoijSIMjrwTgN8IijzpEIaLUjbJoAcoXAs0cxvQ8MiZqudL6YOW74ewWEnqI8mHVvKCO1sW.tlhu9ndF8UrtRv7myB4rsEtfE.mNcAylMgWYCuLd7mXs..n15qGmoAWn6jMAcojNuITp2fQnmwB5NYS3rM1VfYMI.ac4p7su8.0Csm3IWGmt4aAS5Vi53dzFGGmwpWkUdb7SdneBm39AenGNpi6jMlZeF29SDKdh6XQnq8oDBQ93OYrDpYKSnIB0jUqBbjhmFxQ8WbSangyy44+2W0kKJiOLgjQHsqZhTgBVNzjmFjzy+Um22OrDDJYriIPWVN8Kap3M25Vva81ki6ZEqBtc6F0T64voqsdb5ycAblFbgy1Xa3zm6B3z0VONSs0Amtbga4VuM7Vuc43C+v2OPKJss2Y6gkHF.vJtgGHl6trqo3qmS2rVYkGGqXkqBNNSMvrYS3oep0f+1G+IX1yl8Ffp+7MfyT64voO2Ev4ZwCmGNpuY33r0EXLV42u7YVOdy2bqBlPyZt2mIlhaccqGKXR2Jm3dm67CvJV4pBjP1y+qVeTG2m9bWPVhaBgjXHgrNiE7jKXFyprXtLVHFLZT8sVvEbWEtrELeQcLhwm1jo0BQB+7mfP0NphyRW0xW1Rw916dvZVySBaCeXXQ23BvhtwEfe0ysd7Ee09w28cGKP8xxuhJpXTTwEgoM0oDXw+Ffs7Urt08TgMX8AXSLHyth8aRgwqIbuy6gvi9adf.a6u9luA9m+eG.u9l1Ll9kMUbckMKbckMKrs2Y638eu2i2qenl8rmKl00TFt8a+1B7y.ew+Zt2mACqmXeVWy30DVwM7.3m9aennJteisrENEm0XMte3G9Q3756uwMgPT+7+soU.dVNWl1zltpnXdFpOKn0VwEtjkhtt94H4WSsc5EkeW+n.O2lsQ.a1FgjeciVtc2JNzg5sAN+iKaInzpk1tM7+cDF3T.XG6XGGrXwhjdMiDGNNsPKB7yBRaWUtb.7mCciOxJ+onXcRecfxsdWX6G7M4TbR86weh0h4MuaHPKbEs9ncsar28r2vpiX.rcM4xl6cG2IFzhtKfe+697gsFn93OwZwpV0J3jTnSmtBjHI.awWM2byCYmSNHu7xEi9RJNPqI4+323quY7BO+ywo07DiDZpQiCrwO3kR3h69RK5t.mDjCxd.PoR5EmPFjKhIioze4pPThjwx7TNve9w+uB77QO5wnpVnrat4lwQNx2D34a+1WBJ4LRaxXOWVfyxizUbEWUDNZo2+5e80bFybAQpSFqT.r6P2nbkLF.a09eOm8uEXoRJTEVXQ31VxRQgEUDF8kTLuGyWs+CfST4ww6tisGV2Q527myBw0T70KZcUlXmHoiyTC17leC7Ja3kBarscuy6g5yYNoXE2KYIKlSRV8ki7sGE6XGuGuw8pm+iIKcMYDRF6E.vCJ4A.gLHVBY2TZxj4.eoa4aYSXdxPxXtNwI37bZ8XD3CNPusDmNcpuVPcvDccqGkkyMfQcOWB1xNe0vZ0lJq7371JWwh.IWJhSLWFulvsL9kgwW3khW7O8rb12O6oWK9YO8ZQgEVDl27W.FUgEg7xKWLr75cIgxoKW369tig5N24vW8keA14N+fvtFqZYqFSxzTfNeh2uiFqw8kbIECyl5MgpXJt8p3e1R5VrSIDB.5MYrJ.OsLVhBys3BNSS5tyQis0N9q+4+Xfmyvv.CFLHYWOwPaZk1wy0WMxgfp1aueguYyzfJNTc1Umx9s6julBwid8qAmryig25i2RXIkEqrau.bcyXdnDySDF6RZFmj55VOFmgIiW39+i3vNODd4M9hb1ekUdb7qe9mKlOu24hWIFWVSfs0v5Vrh1dIUw8pV1pwXrTBaqgIAwMgPTeRHaYLKVrvo6nzcFG.oIdUe+PYrpp377rxR8OyJOfWuXZR34+8awEmmql5xV0h5Z5rXb4LYY+5pqa8nXckf+y47yPMZbfZasF7Q66ci5Dyrau.LyKqLj+PKDYqIG1xnfLrfbXrKiXpLWNJ49mHpyWM3eU8AvN9vxioyw7myBwkXabvlgBXSdT5lLwAHVw83yeJHacCiMtUfxXnTVJVHDRjkPlLlUqV4LPsq5K9RjlHtTHELis0N12V2JmskQFYJIWq3Qnisum8M9qXFRz3F6vC2.13l6crhYvfA0dBpRcoaoZI972uMrdrggwXCScNWNbq2EZq61DrjajtgLPJIkRuiOodt3CYlwtLh70THx2dgXt22hf6jbh553bnyt5D00zY4brYmddHYsIizMjARuGq8V+sTfUysPi6lzzHZxSCQLtydH4.ltMqnwseRcoXgPHBKhIi0Ympyh3ICSpvfACAJxn6a26BKeFy.MTznD8qUmUrGNsnPVYkspbFlBvljZvk2hW3zNvuJ0rE0h9ZqonAuvocvYapoYUp.j5w7R0R74WTv30DXfIjoNARbtKnnICvGccqGo08PQZ5FJfNHbKMpBi6LQ1ruWGo3lZLJBgf9X4PREWQ0Qd4MLNOeO+kMBisItEczz++NHJeK8N6zzoSOF4HyWTuFhoPacpc8oeJVWONQqoHdier00iSrqOs2YypISlU6sJFgPHDhpl+jwpPICh9ibyMONCh9pp5jv4689PamhyfsHiieB7WBYv2ZylMUaqhAvNtsXXX3rsssyOB2328OwgGd7MgCZMEMXcV7hssyOJv1zoSOJt3KItNuhIZE.fPjDUqzA.gLPWB4BEteiZTEw446b6uC5n7sGWsPl1N8BSe1miW8oVKmsa0pUjat4w+KREYjiL70oypp5jXAadKXi1z2uZkrCObC3mzZcXiaeGA1lNc5QIkThpZVkp1V71+xC9OT5PfPDCUqzA.gLPWBcxXVrXIrwqzN296f8+K+kHyS4PfWkvx7TNPiu9lvV9e9cb1NCCSXI9oVw26I9stMsUbie2+DazldTm09dta7UibHXcV7hEr4svoqI8mHFUq033Pgtg3srRPHDBYvgH9MxpsVZfO1rMB3wiGTe80EXaUU0IQUO9+ElwrJCiZFy.tGVdn8T3uFIYtEWH45qGmXe6Cu6tCuZZyvvfwN1wop6dxPw26I9UUUmDqqpSh0AfEM6qCiOCqHEMIgQpQCbqoGzP2.mpCO3CNvA4TGw7yjIyXzidLIRuezhLccnBiIIg14cVuRGBDxfV9SFi2o9eqs5VFCk9uBKrHvvvDVKQrucuKruKlf0LlUYvZN4vY+e8m+OhXqWjat4B61E+YnobPn2SB1114GgsI3d4RmN8vlMaIDcUaHj5xZAgLfPGc1lRGBDxfV9SFKg+t5yM27fYylQkUdb31c3IQtOdZ0KgXvfALpQUjpbc4LV3+8jSdxSJzZ1XexeMDKmbxMQp0vHDBgPRXjPVzWEBCSpXhSbxn95qC0UWcwbBHlLYFYmc1CnJUCLLohwO9I.2taE0WecnwFarOKYILLLvrYyHszrjvTY8c6tUkNDpF7rjh4VuKYYQdlPjPUqzA.gLP2.pjw7KqrXSnxiGOnkVZFNc5Ds2d6nyN8DHQDFFFjTRZQpox.FlTQZoYQUMy.EaLLoB61SE1sOJ30qWda8Pfvqj+IJ75UwqdlUy2F80SmxbXPHhtpU5.fPFnK3jwNE.FYv6LQutM4uK1FH0RWhA850mvlzEgPHDx.MAWZKpNzclHLaJIj9P0Jc.PHIB15auo99fHDhjHgtNiQHQgpkoqCuSBll71jLc4IDBgjnhRFiLfP2cq3qTz7VBM5rK0656JgPHD0gH1Mk.phYoFgzmDZBIPHj3xdT5.fPFLnOSFSELK0HDBgPHjArntojLPmbUPiqluM5nAd2LgnpzhtKnzg.gLnVelLVmcRi4ERBM4Z4PpZY55PHDBY.lfSFi2uzpupV6DhZ.86oDBgPRTEbxXI7qOkjAuTqEn3u7f+CkNDHDBgnx0mcSI0hCDRT6Pgtgpp5jJQbPHwjN6glnVDhRpOSFSs1hCDhJD05xjDRM4oAkNDHjA0BNYrJTpffPFHiloZjDXx0DfgPFTiZYLBQ7PsLFYfF52oIDYPelLFsXgSRD3xkSkND.DnUDn0mRBgPHQRnIiw6ReAsjHQRfo32YOs9TRHDBIRhpJvOMiJIIvjyw7BuWKpJ7SHDBIRBMYLd+xDZQXlPhJ71Jb0egyI2wAgDSNuy5EZWJdKKSHCFDZxX79Aulal97HgDE38lY18d2E7kDM1KIpWczYaBsKZ1TRHxfPSFqB9NHZFURTyhvMKv6XfTB0L.ZguczjlFk4PgPHDRhhPSFqZ9NHe97RsNFgDc3skDNkypj63fPhZa8s2jP6pBYLLHjAs3KYrSw2A1TSTgqjnNEgxZQExXXDwq4Gsu2UlCCBI5ztVA64CdakWBgH93a1Tx6c1e9ySKWFD0oHT5UpVFCC+38yOUU0Iw40VmbGKDRepUH3MyPiWLBQlvWxXuCeGnOedQ80SeYBQ8woSWBsKk3KSpPnc700reYLLHjnScscVg1UExXXPHCpwWxXUHzA6vwoktHgP5GZrwKHzpDQKPYRFqY.TNe6Xqu8ln0oRhpy+px+Og1E0xXDhLgujwpFBLKz73wC05XDUkFaTvYoHusvqLQvq89OymKmwAgDQmWacX26cWBs6JjwPgPFTSnJv+qJzKn5pqFd8R0LIhxys6VizMGnzIiw6fedqu8lP08ToLGNDB+hPWmWNnB9JgHahTxX79kI974EG8neqjEPDRz5jm7jBsqVfxlLVyQ55ukc9pvsdAGmaDhrn5dpLRkzhWUFCEBYPOsQXeCA.kx2N5rSOviGOvp0gJIAEgzWprxiilZRvtn7Yfx2EKGD.OHe6n4laBGq9ifKcLSCI2sAYNrHD.25cgG+28vBs6SAf6QFCGBYPuHkLVE.3GB.K7sS2tcCOd7.SlLCsZizogPDWUV4wiT2S1B.tM.zg7EQ7pY.nABbCMTBYDkRK5t.9su65QyM2jPGxCAZv6SHxJM8w9KE.6NRG.CCCJrvh.CSphVPQH7ws6VQkUd79Zgq+GB0SWrXAreo1HE5.rau.rjYubjulBkunhLnjuj7hi192fW7O8rQ5v1CD3FHHDhzouRFC.3WCfebecP4latX3CeDPud8weTQHAws6VQs0VazLSdKG.2nLDRwhIAf+YecPK9lVJl5vubjlOpq+IhKeI4Emryig25i2BppJAGmk.rsp7jfxTrjIjA0hljw.X6xxqNZNvrxJaX0pUZ7jQhKM2byvkKmnwFuPe0RX9cHvdG8pwY.1xAveNZNv4OmEhKsfq.YqIGnqa5FaH8OsqsczX2MfpuPk3015Fh1W1rfxOVKIjAkh1jwr.1OjNwn8DqSmdX1rIvvjJXXXPRIoEVrv6vOiLHjWudCjjk+IDhGOdP6s2djVqIEhZNQL+VNhxDx7a9yYgvddEizGRFHUsofj6IEXrKiRSzQR33VuK3qmNA.PccbNzYWch5Z5r3KO3+nuZArP0BXmrIup3GkDBIZDsIi42qBf6TLtvlLYNv+VudcgMlyBd+AiRnSYEbRTgp81aC974iy1Zt4dyOp6t6JZakqXQ4fMQG0bhX9sbv1s+oEumnYMyxPVCMm.O2VF4yY+L5MiT0lRXuNltMSs3lBK3jnBVqc0Fb6k6Mhbdm0iN5rs.OOBkhh9qSA1t1mFv9DhBJVSFCT8zwuG..DvXIQTPTQD+BEojPIyIl72hNJM+s7XrPhRLRN0BX+cQkrdh0eLIvdSMQcqLqDBMYOohDjbQLq+9yZ+nEnTadA.rVjXbiLDBgGV.6WnzC8fdHyOZFreARhdSjtbvNPoU52OoGC9dTAnYLIgLfR9fsUxpFJ+efgdLv9wAAaBLI5IgEpkC1e1T52eoGCreTMX+a04CBgn5ze5lRgLIv9EKSBQ4LujPhfCA1jTp3hOpVAiE4P9fcr6T5EenpGF.DUuSAte9gFSXDhJlXlLVnlDX+BlIA1VyXRWb6VfJe7xPjTmBbSrp5fddEfsaHou3f8yN4id6NoRCZeSBTxZCV0BB+yGUbw+a0W7wAAMNvHjDJRYxX8G4i9tYzCNwtnUo8iXouhA0ZBk78GqiEQaxPUiH2ZUUDGw.o+IZ+rQow340+MTIlTysd9dhyWeEQwwzWeNqZLvu0fIDBgPHDBgPHDBgPHDxfZ++CLnUqxJfG+e+.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-3",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 382.0, 187.0, 230.0, 148.31423895253684 ],
					"pic" : "loadbuttons.png",
					"presentation" : 1,
					"presentation_rect" : [ 20.0, 4.469721767594109, 318.0, 205.060556464811782 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-2",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 27.5, 807.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-1",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 48.0, 12.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"bgcolor" : [ 0.811764705882353, 0.811764705882353, 0.811764705882353, 1.0 ],
					"hidden" : 1,
					"id" : "obj-29",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 382.0, 352.0, 230.0, 142.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 360.0, 220.0 ],
					"proportion" : 0.5,
					"prototypename" : "referencepanel_360x220"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 57.5, 273.0, 33.0, 273.0, 33.0, 657.0, 37.0, 657.0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 145.5, 273.0, 33.0, 273.0, 33.0, 657.0, 37.0, 657.0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 244.5, 273.0, 222.0, 273.0, 222.0, 657.0, 37.0, 657.0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 57.5, 405.0, 37.0, 405.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 145.5, 657.0, 37.0, 657.0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 244.5, 657.0, 37.0, 657.0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 391.5, 174.0, 675.5, 174.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0
	}

}
