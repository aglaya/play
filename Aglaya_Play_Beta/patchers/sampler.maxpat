{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 49.0, 99.0, 1061.0, 656.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"isolateaudio" : 1,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 112.0, 2246.0, 99.0, 22.0 ],
					"text" : "prepend OSCout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 112.0, 2281.0, 81.0, 22.0 ],
					"text" : "s #0_OSCout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-192",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3290.5, 996.0, 99.0, 22.0 ],
					"text" : "prepend OSCout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3357.5, 1027.0, 79.0, 22.0 ],
					"text" : "r #0_OSCout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 154.500014999999962, 1695.266601999999921, 81.0, 22.0 ],
					"text" : "routepass 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3402.5, 768.0, 87.0, 22.0 ],
					"text" : "s #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3414.5, 670.0, 75.0, 22.0 ],
					"text" : "route OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4321.0, 246.0, 41.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor2" : [ 1.0, 1.0, 1.0, 0.14 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_color2" : [ 1.0, 1.0, 1.0, 0.14 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontsize" : 7.0,
					"gradient" : 1,
					"id" : "obj-412",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4232.5, 77.200005000000004, 79.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.0, 133.9375, 265.720703000000015, 16.0 ],
					"text" : ".",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"bordercolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.0 ],
					"fontsize" : 8.0,
					"id" : "obj-94",
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 4235.0, 48.200012000000001, 76.5, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.0, 133.9375, 265.720703000000015, 18.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1,
					"wordwrap" : 0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.203922, 0.215686, 0.223529, 0.0 ],
					"floatoutput" : 1,
					"id" : "obj-25",
					"knobcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"knobshape" : 5,
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 399.0, 430.0, 256.0, 26.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 27.0, 150.0, 255.932800499999985, 20.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 683.0, 346.0, 80.0, 22.0 ],
					"text" : "loadmess 0.8"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-189",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4192.0, 841.0, 67.0, 17.0 ],
					"text" : "s #0_ctnumIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-188",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4224.949219000000085, 939.5, 74.0, 17.0 ],
					"text" : "r #0_ctnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 918.5, 377.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 887.0, 377.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 912.0, 340.0, 46.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 912.0, 312.729918999999995, 94.0, 22.0 ],
					"text" : "r #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4232.5, 352.200012000000015, 72.0, 20.0 ],
					"text" : "necessari?"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 780.199951000000056, 327.866698999999983, 130.0, 22.0 ],
					"text" : "receive~ #0_meterR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 765.949951000000056, 293.071411000000012, 128.0, 22.0 ],
					"text" : "receive~ #0_meterL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2514.5, 1106.0, 118.0, 22.0 ],
					"text" : "send~ #0_meterR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2401.0, 1106.0, 116.0, 22.0 ],
					"text" : "send~ #0_meterL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-180",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2348.0, 724.5, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2331.0, 695.5, 73.0, 22.0 ],
					"text" : "routepass 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2331.0, 755.5, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-174",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2331.0, 673.5, 93.0, 17.0 ],
					"text" : "r #0_channel-number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2358.042480000000069, 1402.09997599999997, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 2368.042480000000069, 1225.0, 30.0, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2317.542480000000069, 1298.59997599999997, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2368.042480000000069, 1299.59997599999997, 43.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2368.042480000000069, 1437.59997599999997, 109.0, 22.0 ],
					"text" : "s #0_ctnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2364.042480000000069, 1177.600097999999889, 94.0, 22.0 ],
					"text" : "r #0_ctnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2345.042480000000069, 1480.09997599999997, 107.0, 22.0 ],
					"text" : "sprintf set CT%sR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2234.042480000000069, 1480.09997599999997, 105.0, 22.0 ],
					"text" : "sprintf set CT%sL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2339.042480000000069, 1370.699951000000056, 61.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2241.042480000000069, 1267.59997599999997, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2301.042480000000069, 1267.59997599999997, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 2165.127685999999812, 1193.699951000000056, 46.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"autoscroll" : 0,
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"fontface" : 0,
					"fontname" : "Comic Sans MS",
					"fontsize" : 8.0,
					"hidden" : 1,
					"id" : "obj-143",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2339.042480000000069, 1333.59997599999997, 33.0, 27.502929999999999 ],
					"presentation" : 1,
					"presentation_rect" : [ 313.523100983825657, 200.1484375, 12.0, 22.0 ],
					"text" : "1",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.0 ],
					"bgoncolor" : [ 0.862745, 0.870588, 0.878431, 0.0 ],
					"fontlink" : 1,
					"fontname" : "Comic Sans MS",
					"fontsize" : 8.0,
					"id" : "obj-136",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2203.127685999999812, 1333.59997599999997, 26.257811546325684, 19.1484375 ],
					"presentation" : 1,
					"presentation_rect" : [ 296.894195210662815, 199.0, 26.257811546325684, 19.1484375 ],
					"rounded" : 13.01,
					"text" : "CT",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.29 ],
					"texton" : "CT",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2203.127685999999812, 1139.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2203.127685999999812, 1386.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2283.127685999999812, 1139.0, 81.0, 22.0 ],
					"text" : "r #0_toctIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2234.042480000000069, 1386.0, 96.0, 22.0 ],
					"text" : "s #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2180.08520499999986, 1508.699951000000056, 51.0, 22.0 ],
					"text" : "gate~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2180.08520499999986, 1662.0, 116.0, 22.0 ],
					"text" : "send~ intramoduls2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-170",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2061.5, 1503.699951000000056, 51.0, 22.0 ],
					"text" : "gate~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2029.0, 1662.0, 116.0, 22.0 ],
					"text" : "send~ intramoduls1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-177",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 2101.0, 1577.0, 44.0, 44.0 ],
					"prototypename" : "helpfile"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-141",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "volslider.maxpat",
					"numinlets" : 4,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "float", "float" ],
					"patching_rect" : [ 711.25, 417.799987999999985, 199.75, 248.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 296.095032000000003, 3.0, 53.856137967651364, 197.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-135",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4029.0, 315.700012000000015, 63.0, 17.0 ],
					"text" : "s #0_filepath"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4116.949219000000085, 939.5, 65.0, 17.0 ],
					"text" : "r #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4105.0, 841.0, 58.0, 17.0 ],
					"text" : "s #0_toctIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3976.0, 796.0, 45.0, 22.0 ],
					"text" : "route s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "float", "float", "float", "int", "int", "int", "", "int", "int" ],
					"patching_rect" : [ 3469.5, 734.0, 722.449219000000085, 22.0 ],
					"text" : "unpack f f f i i i s i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 9,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3469.5, 1007.0, 771.0, 22.0 ],
					"text" : "pak f f f i i i s i i"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3590.0, 841.0, 80.0, 17.0 ],
					"text" : "s #0_pos-sliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3469.5, 841.0, 86.0, 17.0 ],
					"text" : "s #0_speedsliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3691.0, 841.0, 74.0, 17.0 ],
					"text" : "s #0_volsliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3574.0, 939.5, 87.0, 17.0 ],
					"text" : "r #0_pos-sliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3469.5, 939.5, 93.0, 17.0 ],
					"text" : "r #0_speedsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3666.0, 939.5, 82.0, 17.0 ],
					"text" : "r #0_volsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4020.0, 841.0, 62.0, 17.0 ],
					"text" : "s #0_loadfile"
				}

			}
, 			{
				"box" : 				{
					"comment" : "load info",
					"id" : "obj-100",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3414.5, 620.700012000000015, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3861.0, 57.333336000000003, 61.0, 17.0 ],
					"text" : "r #0_loadfile"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3776.0, 841.0, 72.0, 17.0 ],
					"text" : "s #0_reverseIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3959.970946999999796, 841.0, 60.0, 17.0 ],
					"text" : "s #0_loopIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3871.470946999999796, 841.0, 79.0, 17.0 ],
					"text" : "s #0_keeppitchIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3757.0, 939.5, 79.0, 17.0 ],
					"text" : "r #0_reverseOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3933.970946999999796, 939.5, 68.0, 17.0 ],
					"text" : "r #0_loopOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3839.470946999999796, 939.5, 87.0, 17.0 ],
					"text" : "r #0_keeppitchOUT"
				}

			}
, 			{
				"box" : 				{
					"comment" : "save info",
					"id" : "obj-53",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3507.5, 1046.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4033.5, 939.5, 61.0, 17.0 ],
					"text" : "r #0_filepath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2384.556152000000111, 799.5, 32.0, 22.0 ],
					"text" : "*~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2289.556152000000111, 724.5, 32.0, 22.0 ],
					"text" : "*~ 4"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"blinkcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-42",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 692.0, 783.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 28.0, 98.758625699658694, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1050.0, 1788.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-292",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 939.0, 1691.0, 105.0, 20.0 ],
					"text" : "PAN w/ACCEL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-288",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 889.0, 2201.0, 34.0, 22.0 ],
					"text" : "/ 10."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-289",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 889.0, 2165.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-290",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 889.0, 2125.0, 35.0, 22.0 ],
					"text" : "* 10."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 720.0, 2201.0, 34.0, 22.0 ],
					"text" : "/ 10."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-284",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 720.0, 2165.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 720.0, 2125.0, 35.0, 22.0 ],
					"text" : "* 10."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2188.0, 916.0, 34.0, 22.0 ],
					"text" : "+ 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2384.556152000000111, 895.0, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2285.556152000000111, 895.0, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-263",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2395.056152000000111, 841.0, 78.0, 22.0 ],
					"text" : "r #0_panR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-264",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2296.056152000000111, 837.0, 76.0, 22.0 ],
					"text" : "r #0_panL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 889.0, 2324.0, 80.0, 22.0 ],
					"text" : "s #0_panR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-258",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 720.0, 2328.0, 78.0, 22.0 ],
					"text" : "s #0_panL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 720.0, 2237.0, 41.0, 22.0 ],
					"text" : "$1 50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-221",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 720.0, 2275.0, 36.0, 22.0 ],
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-232",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 720.0, 2094.0, 116.0, 22.0 ],
					"text" : "expr pow((1.-$f1)\\,1)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 886.0, 2094.0, 94.0, 22.0 ],
					"text" : "expr pow($f1\\,1)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 889.0, 2237.0, 41.0, 22.0 ],
					"text" : "$1 50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 889.0, 2275.0, 36.0, 22.0 ],
					"text" : "line~"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-146",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 775.5, 1993.0, 100.0, 20.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 775.5, 1961.0, 93.0, 22.0 ],
					"text" : "scale -1. 1. 1. 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 923.0, 1950.0, 31.0, 22.0 ],
					"text" : "+ 1."
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-164",
					"maxclass" : "slider",
					"min" : -1.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 923.0, 1988.0, 100.0, 20.0 ],
					"size" : 2.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "float", "float", "float" ],
					"patching_rect" : [ 757.0, 1907.0, 69.0, 22.0 ],
					"text" : "unpack f f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 863.0, 1805.0, 126.0, 22.0 ],
					"text" : "if $i1 < 200 then bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "float", "" ],
					"patching_rect" : [ 863.0, 1774.0, 37.0, 22.0 ],
					"text" : "timer"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 869.0, 1734.0, 36.0, 22.0 ],
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 863.0, 1846.0, 37.0, 22.0 ],
					"text" : "0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 757.0, 1734.0, 62.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 757.0, 1673.766601999999921, 129.0, 22.0 ],
					"text" : "route /accxyz /1/accel"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1898.471068999999943, 213.0, 86.0, 17.0 ],
					"text" : "r #0_reverseOSCin"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 627.375, 1676.266601999999921, 87.0, 17.0 ],
					"text" : "s #0_reverseOSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 44.0, 1466.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4282.0, 452.0, 50.0, 22.0 ],
					"text" : "2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4122.0, 451.5, 50.0, 22.0 ],
					"text" : "44100."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 458.0, 265.0, 1252.0, 547.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1007.0, 225.0, 133.0, 22.0 ],
									"text" : "sprintf /%i/resetspeed"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 869.0, 225.0, 125.0, 22.0 ],
									"text" : "sprintf /%i/pitchtoggle"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 732.0, 225.0, 122.0, 22.0 ],
									"text" : "sprintf /%i/looptoggle"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 604.0, 225.0, 114.0, 22.0 ],
									"text" : "sprintf /%i/playback"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 453.0, 225.0, 133.0, 22.0 ],
									"text" : "sprintf /%i/pausebutton"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 340.0, 225.0, 99.0, 22.0 ],
									"text" : "sprintf /%i/speed"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 206.0, 225.0, 121.0, 22.0 ],
									"text" : "sprintf /%i/playtoggle"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 117.0, 225.0, 82.0, 22.0 ],
									"text" : "sprintf /%i/vol"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 8,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 404.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 7,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 364.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 6,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 322.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 5,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 279.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-5",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 241.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 201.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 159.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 116.0, 485.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 117.0, 22.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 7,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 6,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 5,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"order" : 4,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 3,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 2,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 167.199996999999996, 1540.800048999999944, 436.799987999999985, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p sprintfeador"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4349.699706999999762, 598.700012000000015, 95.0, 17.0 ],
					"text" : "s #0_channel-number"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2055.0, 466.366698999999983, 86.0, 17.0 ],
					"text" : "s #0_speedsliderIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 2055.0, 381.5, 30.0, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2046.0, 344.5, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2334.556152000000111, 528.5, 44.0, 22.0 ],
					"text" : "gate 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2354.056152000000111, 555.0, 33.0, 22.0 ],
					"text" : "* -1."
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1993.0, 213.0, 70.0, 17.0 ],
					"text" : "r #0_reverseIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1962.0, 309.366698999999983, 81.0, 17.0 ],
					"text" : "s #0_reverseOUT"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-99",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2031.0, 269.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 244.666401855251308, 93.183628699658698, 26.149994, 26.149994 ],
					"prototypename" : "mememe",
					"uncheckedcolor" : [ 0.65098, 0.666667, 0.662745, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3965.333496000000196, 57.333336000000003, 150.0, 20.0 ],
					"text" : "LOAD FILE"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4158.0, 214.200011999999987, 66.0, 17.0 ],
					"text" : "r #0_filename"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-254",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2668.470946999999796, 161.200011999999987, 130.000122000000005, 35.0 ],
					"text" : "if $f1 < 0.5 then $f1 else out2 $f1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-253",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2865.0, 194.0, 50.0, 20.0 ],
					"text" : "MaxVel"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-249",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2865.0, 217.5, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2774.47119100000009, 256.0, 100.0, 22.0 ],
					"text" : "zmap 0.5 1. 1. 4."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2668.470946999999796, 256.0, 100.0, 22.0 ],
					"text" : "zmap 0. 0.5 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-222",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3971.0, 271.0, 50.0, 22.0 ],
					"text" : "replace"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-215",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 239.500014999999991, 1747.266601999999921, 88.0, 17.0 ],
					"text" : "s #0_pause-to-OSC"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-186",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2265.056152000000111, 456.0, 68.0, 17.0 ],
					"text" : "r #0_stopOSC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-185",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 65.0, 1711.766601999999921, 71.0, 22.0 ],
					"text" : "routepass 0"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-182",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 65.0, 1747.766601999999921, 69.0, 17.0 ],
					"text" : "s #0_stopOSC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 620.15002400000003, 153.699996999999996, 18.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-162",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 640.950012000000015, 130.0, 19.0, 17.0 ],
					"text" : "t 1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 619.0, 130.0, 20.299999, 17.0 ],
					"text" : "t 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 619.0, 65.200005000000004, 15.5, 15.5 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 479.0, 573.40625, 34.0, 22.0 ],
					"text" : "0 $1"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 479.0, 518.0, 96.0, 17.0 ],
					"text" : "r #0_pos-sliderOSCIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 692.0, 745.0, 83.0, 17.0 ],
					"text" : "r #0_resetspeedIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 692.0, 866.0, 93.0, 17.0 ],
					"text" : "s #0_resetspeedOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 529.375, 1676.266601999999921, 85.0, 17.0 ],
					"text" : "s #0_resetspeedIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 444.999969000000021, 1676.266601999999921, 79.0, 17.0 ],
					"text" : "s #0_keeppitchIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2214.970946999999796, 317.900023999999974, 69.0, 17.0 ],
					"text" : "s #0_loopOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2115.470946999999796, 317.900023999999974, 88.0, 17.0 ],
					"text" : "s #0_keeppitchOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2214.970946999999796, 189.0, 59.0, 17.0 ],
					"text" : "r #0_loopIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2103.470946999999796, 189.0, 78.0, 17.0 ],
					"text" : "r #0_keeppitchIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 382.999969000000021, 1676.266601999999921, 60.0, 17.0 ],
					"text" : "s #0_loopIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 290.999969000000021, 1676.266601999999921, 97.0, 17.0 ],
					"text" : "s #0_pos-sliderOSCIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2109.0, 549.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2109.0, 579.5, 71.0, 22.0 ],
					"text" : "clickadd $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2059.0, 579.5, 37.0, 22.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.0, 2, 0.292553, 0.566667, 2, 0.920213, 0.886667, 0, 1.0, 1.0, 2 ],
					"domain" : 1.0,
					"id" : "obj-63",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1986.0, 617.5, 200.0, 100.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor2" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_color2" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 0,
					"fontsize" : 92.305279999999996,
					"gradient" : 1,
					"id" : "obj-6",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3531.0, 256.0, 122.0, 112.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 94.375, 12.855721000000017, 130.47773517799115, 112.0 ],
					"text" : "+",
					"textcolor" : [ 0.921569, 0.917647, 0.933333, 0.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 883.699951000000056, 45.0, 73.0, 17.0 ],
					"text" : "r #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 809.699951000000056, 45.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 809.699951000000056, 81.5, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 854.699951000000056, 81.5, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 809.699951000000056, 118.5, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 3347, "png", "IBkSG0fBZn....PCIgDQRA...rH...vhHX....PTYn1+....DLmPIQEBHf.B7g.YHB..LnbRDEDU3wY6c1ESbbcEG+uoUp9AGXqTkZI1FuCJFGm9PVC3lp9UVSxjMsNIlJAhzl1ls1hl5zHAkp9Pk.AtsRVNB4RjqSbTppgXhphBkv5TGIBF2MoOXvXvak7tfCM7wttwIO0Yw90joOL6r9NWlY2kkY24qyOoQbuCyt6cm4+dNm64dm4tE3cHH.7Af.L+EYJ+f440tJ.VIS4UxrIAfXY1jLyFpcksX0MfRDAyrEHy1tJwedoghnIJye8DBHmHA.PmP4hjrMYaE.L..Ztj8stLiS1xR..DFJWLxqkCQwPXm0TC1cc6AaaaaC0U2tQk2y8..f8d+6AUUUkF9ZmZ5YxV9pyNG..t1byh0VaMLxa8lEZ6MB.FKylizpiSSr3GJBjvHGBDAgZwScnlw8s65PiMTedECaVhmXdrvMVDKt3hX1qNSgHfFB.CBEKgDlLAgxuHMzreGc1k7YGZX4qGOgrUijTZ4wmXR4t6oWYAgZymqpNgRP1DaRBCkSnFJPFYzHxRRosZ8QN45wSHe7SzetDNR.nOnX4jXCRXXfHQTLj7YGZXau.wHFehIk6nytxk0lAAYoofnYXfHo6d5U9xScEq9ZsowpISIepSeFir1nZogDM5P.XP2d6tmdkWMYJq9ZaICIozxiLZDYQwPFIZBaEWPri3CJ+BxyIRziQFMhQVZhh6lwYOIAgNtbN7QZ2yIRXQRJctbO0mkbkxhY.nSfqtoXR1rrZxTFEHbL3QrxD.JeY0bB3Tm9LN1d2Tp4xScE8hmw0GKSyP4KY1uzszZa1hjnY2QRJs7wOQ+dltYuN2NG+D8SVS1fL9DSpWrLwfKQv3CbooWPnV4wmXRq97tikUSlRtkVaSO2RN53X7At3SH2NlGc2Su5IXbjSGB+fSnzQmcQtcLYFYzH5EGSXK5ZdQQ.vEHa28zqUed00xkm5J5EGSXK6p+Ff0ITN6PCa0mOc8b83IbbBF+fSnLxnQr5yidFbRBl0ELKITJ+3DDLjPwFgABFaS2p0jGEJFEqGcDL1h7vnIyrTudrOb83IrUY5sY1FSGc1kUe9gfCcxCyXVgPQSWjao01nDtYS4Tm9LV9bhIa.sBB0Rov2liNyKlxV7KZhSgFTP6ORRo4mSLqfxP7KAYEJG+D8a0mGHJPzIf2AJkBEefYNyRwo37Pm3WBVpDK8w9AQwo3LgatvrBJAtiBvJTN0oOiU+clnHQG2Q8Y1hknpu4hhgH2ONbzwcjeyRnnI4azsqgyGc5cTAkrtJJfiIaTyG9Hsiu4Cs+hPuQXmnpppDO2Qed1ccHT.A69Exy+OL.dV0JiEIRI8ghCQ4iGXu6Aot4Giqcs4T2k.Tt0RLj7YYoO0Bc2SunlctiMS6ivlQ6s2Na0GF4w5RtrrDFLVUN2vCSVUbYricr8Mj0kbYYoO0BjUE2KaDqKFYYIH.5PsBYUw8hNVW1BLn2QFYYoS0BjUE2Os8zOMa0mEFj2E8DK9gRWo..vAO3AMwlEgcjG6QaBhhgX2UX8NN8DKYOPQwPTdU7H7i+I+T1pg06XxoXg6MvSve+sOO9yu7qhjotoU2TJq7COzSxVcWn.RRmlALzqMFPrOCTDDp0y88maF0MHu3f2xRX0BczYWdtd.s3GdirkWd4kv7KbibbztO9AZiOccOQF3EKYOfu626gKQMIB6JO1i1DDDpUsZUfSvvJVzrt77nMErD2zHri7LZiSMHakJz6e3EcAA.bOYVRYT4+9w2xhZIVGA1W8rUMzxR1+A2Kvyv8s65zT+V2x6IV37nrKvjfNVwR1fT1eidSwBgxbcokVaicWAUKTA+NDDpEe8GXukkFFg8jFZTShXCpVXchkm5PNxmgcDlH0W+9Xql8tWrB9cv62lv6wCs+FYqlcYLdchkFafhWwqSUUUI+.KFD3thkr4WYu2+dJesJBaK6rlZXqF.PQrDTcOhhg7j4WgX8ru5afspe.EwR1aeQN0DgGlpqtZ1pYsrjMdkcWG4BhPgseuZDK9.3rrrssssxbShvtBWrqOH.mkk5pa2k4lDgcE8hcUyTTnRtARivaCyzU..HfF2PDDrbfldD1p9p.LYnixwBQtPiaHJGKD4hunU2.1LDOw73jm7OYZueoRlTS8yO1aiqM2ra5228UeC3Ed9maS+9XGH6L51owgOR65sRdYK2bhOC93N+FrPdX9PXBb6aeGqtIrYwmiVr3TtCDZo01bC2YmRN5XVB+ydFzzAdX7wlzDq9BW3B3O9GNV15c2Sulx85sKPn..Gd.t..0rycXZOkGt5ryoo9W8qUsq4BsYPE.XU0JSM8LVXSgvtSEP4orLA.tycb7AgZpr1ZqootiN.WyF160Y.ZJlNxa8lrUiowxBuOaBBFjH2PD5R7DyyVMMfhaHI08XFo1lvc.WRDiAnHVhotG9.ZH7tvERxJ.bhEt.ZH7v7oehlDctBvccCkVcub9p7T7OuzjZpeuZmzxdJVXgEXqFE3tccNq0kEtwhkuVjMikWdIM08xO+e47xrBvcEKQU26hK5cEKDJvkI+zfSrj0xxrWkR4uWmEtwGxVMpZg0YYYj25MQ5zTuh7xD6ZZ5ITT0BphEIvLfhSOyUKKMJ6D7ChJ2S+HOEmOhl04gnpEXGanrGw+5C9fReKxlSkU5Mm75SM8LrA5mFLgnvJVhpV3MF9bkkFFg8inuuFCEQYqnqkkkWdIOc9V7xboKNAaUM9i3mhBQTK7N+i2sD1jr+v87IwSP7DyiIlXb1coQrvuRlsUj44g6Rez+AczQGvqvN1w1wm84JOS8N3S7j3nO2u.acqeIqtYUV4ud1WGSdw2SsZDjmUiUef4dcY7Ilzpu0UHJivsvfGlWbv6FRB.CoV4cuvEJRMJgSi26hWJmtf.zeZUNnZgWZfS54Vjl7pvYXXHvLOmTQOwRTvjfty+Nj0E2NIScS7RCbR1cMndGmQSX6ATKbx9eQJ8+tbdsW6uvV8eCt7qnhQhkAQl43xxKuDt3kz80R3BHc503SB6.FcrFIVjXeQu5q7xlSKiv1w4di+Fa58WE4o6xFgevzM5QFMhU2yNBSFIozxBB0x1c49xkfHW2jYq.ltQ+a+M+ZJ1EWFuxq9Z7CZngtf.VeFb4IJ.NJ.1pjz+Caem9w2P6JFAgCkjotIdhC98Y20uCFDXqJ461WUSrKmr+Wjx6hKgicreOa0UQdrp.jeKK.JymgrVWjk2Bd7GOT9dMD1XlZ5YvK7qNJ6t94.XACN7MLgASvtWdpqX0wlQTjHIkleLfhZVhDVhp9AHJFRVRJsU+8lnH33mne9GNh9KTAPg3FRkX.3WB.rzReDp5K+Uv24a+s1.ubBqloldF7id5VY20wfNCXnYQeflBCNRzw8Srbbc1zHl5GnfPsxqlLkUedfn.niN6h28SfbcQ1rH.T5RsL.jao01r5yCD4gyNzv7Bk9JlK7ajXVT4Sxr0L.PhDwwm84.McffEymOQIl3IlGgD0r5d79PmYAWolAAM1Q1ZVMYJ9w9QBVzRFjOvD+Bn7uXqPRJsbKs1lkDmhQ3CLwuHHTqibAMvMhNBkvVmL4tnIfWRvX8zcO8xKTx639TNoYvz3HAi0gNBkAsNYgwDFjfwRQGgRIK6rlAgAIXrDzQnDCNfEK0vfDLkMLnWONBghJgA2x9FkGFymUSlROgxXvAITTIL3DLm5zmwpO+5Z35wSvmvMaavrEJZ5VM.j6nytn4BylDcFqGaW2iKVB.tL8JJFhhioHPRJsdidrsIgalE9fhuTxsTQxkm5J7yGE0w5wRSgeoj9.mfokVairxjCjjRq2TgTcty53BjciR.nbCrsNqLTrLZw.qIE87QwohOnDPllSBhhgntXKqzkXtUqc17m3Zc6jOBBcrxb3iztmb5NjCWNdNqIFgOnSrLvCIZjjRKepSeF8xahZrI9sfqK1Z7Cc5wDxHZbi2IAqlLkb28zqQhjUPlotJgwDDL2ParahhgjO6PC63CDd7Ilzn7kn1c3Nsfy6NZBBCDMHSlfcRVatd7DxG+D8aTuaTsjzI7.cGtTRPvM4vY2DDpUt6d5UdjQiX6r3b4otR9DHp8vIb49jZwvVr5FvF.+P4jZX.rKiNnVZsMzPi6G0W+9vCs+FQUUU9VcOlZ5YvB23CQrqMGNejwV2xnGGCAkeDDsbz1LCbRhEVBBEQSy.npbcfhhgvNqoFru5a.UWc0X62a0Xu2+d1Thn3IlG2912AWc14vm9I2BKrvBE5JWaDnDD+XPmmyr1cbphEVZFJhmlQNr3nGBB0hCzzij+CDJq40EwRYbZnX4vwJPXwMHVXwOTDNAgRlNevx7mup3PcqrbymWtvsIVzifPQ33Oye8gMuHRcE9ZkLaQYJ6ZwKHVxEphmBkXvg6JYyv+Gel5bbxaTSz......IUjSD4pPfIH" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-23",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 809.699951000000056, 181.0, 47.0, 47.0 ],
					"pic" : "zz_plusbuttonnew.png",
					"presentation" : 1,
					"presentation_rect" : [ 90.891601500000007, 6.0, 128.9375, 128.9375 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-281",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2618.5, 841.0, 60.0, 17.0 ],
					"text" : "s #0_playIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-280",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2618.5, 810.5, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-273",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 2618.5, 744.0, 67.0, 22.0 ],
					"text" : "onebang 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 2618.5, 710.5, 39.0, 22.0 ],
					"text" : "sel 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2618.5, 777.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2618.5, 673.5, 83.0, 22.0 ],
					"text" : "snapshot~ 50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1986.0, 745.5, 103.0, 22.0 ],
					"text" : "scale 0. 1. -70. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1891.971068999999943, 614.0, 29.5, 22.0 ],
					"text" : "0.8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1918.971068999999943, 561.0, 94.0, 20.0 ],
					"text" : "VOL Slider"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1891.971068999999943, 588.5, 41.0, 17.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1891.971068999999943, 642.0, 74.0, 17.0 ],
					"text" : "s #0_volsliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3531.0, 136.200011999999987, 73.0, 17.0 ],
					"text" : "r #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3531.0, 166.200011999999987, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3531.0, 218.700011999999987, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4168.0, 321.5, 73.0, 17.0 ],
					"text" : "r #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 4168.0, 351.200012000000015, 63.0, 22.0 ],
					"text" : "delay 500"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2068.970946999999796, 149.833344000000011, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2207.970946999999796, 56.833336000000003, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 2507.5, 711.5, 63.0, 22.0 ],
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3666.0, 146.200011999999987, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4412.0, 36.200012000000001, 29.5, 22.0 ],
					"text" : "."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 4412.0, 3.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4158.0, 245.0, 73.0, 22.0 ],
					"text" : "fromsymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-486",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 399.0, 573.40625, 53.0, 22.0 ],
					"text" : "set 0 $1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bordercolor" : [ 0.490196, 0.498039, 0.517647, 0.0 ],
					"fgcolor" : [ 0.239216, 0.254902, 0.278431, 0.25 ],
					"floatoutput" : 1,
					"id" : "obj-482",
					"maxclass" : "rslider",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 399.0, 611.666687000000024, 256.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 27.0, 173.661528799914663, 261.720703000000015, 28.855722599402753 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-471",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2501.5, 834.0, 80.0, 17.0 ],
					"text" : "s #0_pos-sliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-466",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2501.5, 799.5, 97.0, 17.0 ],
					"text" : "r #0_playback-position"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-465",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 4168.0, 379.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-463",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2884.0, 482.5, 89.0, 22.0 ],
					"text" : "425.71"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-461",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2911.0, 396.5, 77.0, 17.0 ],
					"text" : "r #0_file-duration"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-460",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2821.0, 425.0, 85.0, 22.0 ],
					"text" : "scale 0. 1. 0. f"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-459",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2821.0, 396.5, 87.0, 17.0 ],
					"text" : "r #0_pos-sliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-456",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 399.0, 518.0, 78.0, 17.0 ],
					"text" : "r #0_pos-sliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-457",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 393.5, 659.0, 89.0, 17.0 ],
					"text" : "s #0_pos-sliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-455",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2507.5, 748.5, 99.0, 17.0 ],
					"text" : "s #0_playback-position"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-454",
					"maxclass" : "number~",
					"mode" : 2,
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 2507.5, 673.5, 79.0, 23.0 ],
					"sig" : 0.0
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-453",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4232.5, 598.700012000000015, 79.0, 17.0 ],
					"text" : "s #0_file-duration"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-450",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 4232.5, 569.200012000000015, 111.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-448",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "float", "list", "float", "float", "float", "float", "float", "", "int" ],
					"patching_rect" : [ 4168.0, 409.200012000000015, 116.0, 22.0 ],
					"text" : "info~ #0_audiofile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-447",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2593.470946999999796, 365.0, 63.0, 33.0 ],
					"text" : "updates speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-445",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2557.470946999999796, 425.0, 34.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-444",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2485.333251999999902, 518.5, 50.0, 20.0 ],
					"text" : "Speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-442",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2365.470946999999796, 494.5, 322.0, 22.0 ],
					"text" : "1."
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-440",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2557.470946999999796, 324.0, 67.0, 17.0 ],
					"text" : "r #0_playOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-429",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 783.021483999999987, 834.666687000000024, 29.5, 22.0 ],
					"text" : "0.5"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-422",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 783.021483999999987, 867.066711000000055, 86.0, 17.0 ],
					"text" : "s #0_speedsliderIN"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-420",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2103.470946999999796, 269.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 28.0, 58.968749999999986, 22.000000000000004, 22.000000000000014 ],
					"prototypename" : "mememe",
					"uncheckedcolor" : [ 0.65098, 0.666667, 0.662745, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-421",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2103.470946999999796, 405.0, 92.0, 23.0 ],
					"text" : "timestretch $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-415",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 168.0, 905.0, 150.0, 20.0 ],
					"text" : "OSC COMMUNICATION"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-413",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1076.333374000000049, 285.0, 93.199996999999996, 20.0 ],
					"text" : "INIT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-410",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4158.0, 606.700012000000015, 67.0, 17.0 ],
					"text" : "s #0_filename"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-399",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3958.0, 394.5, 74.0, 17.0 ],
					"text" : "s #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-396",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2315.056152000000111, 230.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-395",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2314.056152000000111, 256.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-380",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2436.056152000000111, 336.5, 60.0, 17.0 ],
					"text" : "s #0_playIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-378",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2315.056152000000111, 334.0, 82.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-372",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 2315.056152000000111, 425.0, 83.0, 22.0 ],
					"text" : "routepass 0 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-371",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 2450.470946999999796, 264.0, 44.0, 22.0 ],
					"text" : "t 1 0 0"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-326",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 799.0, 375.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 59.0, 248.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 59.0, 182.0, 34.0, 22.0 ],
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 115.0, 103.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "playout",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 115.0, 57.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 59.0, 394.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 59.0, 57.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 620.0, 196.0, 59.0, 17.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p #0_pause"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-327",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 697.0, 35.200007999999997, 67.0, 17.0 ],
					"text" : "r #0_playOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-323",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2450.470946999999796, 230.0, 74.0, 17.0 ],
					"text" : "r #0_pauseOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-318",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 260.499969000000021, 1711.766601999999921, 67.0, 17.0 ],
					"text" : "s #0_pauseIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-246",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 161.500014999999991, 1676.266601999999921, 86.0, 17.0 ],
					"text" : "s #0_speedsliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-245",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 154.500014999999962, 1734.0, 60.0, 17.0 ],
					"text" : "s #0_playIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-244",
					"maxclass" : "newobj",
					"numinlets" : 10,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 44.0, 1600.266601999999921, 650.0, 22.0 ],
					"text" : "route /1/vol /1/playtoggle /1/speed /1/pausebutton /1/playback /1/looptoggle /1/pitchtoggle /1/resetspeed /1/reversetoggle"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-243",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 44.0, 1676.266601999999921, 74.0, 17.0 ],
					"text" : "s #0_volsliderIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-237",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 47.0, 1064.0, 150.0, 20.0 ],
					"text" : "OSC RECEIVE"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-235",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2884.0, 321.5, 122.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1076.333374000000049, 344.866698999999983, 29.5, 22.0 ],
					"text" : "0.5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1076.333374000000049, 322.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-228",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1076.333374000000049, 379.333373999999992, 86.0, 17.0 ],
					"text" : "s #0_speedsliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-227",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 399.0, 377.0, 85.0, 17.0 ],
					"text" : "r #0_speedsliderIN"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-226",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2896.0, 264.0, 122.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2896.0, 63.0, 93.0, 17.0 ],
					"text" : "r #0_speedsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-223",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 399.0, 465.0, 95.0, 17.0 ],
					"text" : "s #0_speedsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-211",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2201.470946999999796, 254.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 28.0, 12.855721000000017, 23.0, 23.0 ],
					"uncheckedcolor" : [ 1.0, 1.0, 1.0, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-194",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 875.199951000000056, 683.0, 90.0, 17.0 ],
					"text" : "s #0_volmeterROUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1980.0, 588.5, 82.0, 17.0 ],
					"text" : "r #0_volsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2378.056152000000111, 230.0, 67.0, 17.0 ],
					"text" : "r #0_playOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 2288.470946999999796, 939.0, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "live.gain~",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ -40 ]
						}

					}
,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-165",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2201.470946999999796, 405.0, 53.0, 23.0 ],
					"text" : "loop $1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-171",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2315.056152000000111, 598.5, 35.0, 23.0 ],
					"text" : "sig~"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 2288.470946999999796, 633.0, 155.0, 23.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"loopend" : [ 0.0, "ms" ],
						"loopstart" : [ 0.0, "ms" ],
						"mode" : "basic",
						"originallength" : [ 0.0, "ticks" ],
						"originaltempo" : 120.0,
						"phase" : [ 0.0, "ticks" ],
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"text" : "groove~ #0_audiofile 2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-157",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3855.5, 428.200012000000015, 114.0, 23.0 ],
					"text" : "set #0_audiofile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"linecount" : 5,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3958.0, 126.200012000000001, 320.0, 76.0 ],
					"text" : "\"Macintosh HD:/Users/gian/Google Drive/Proyectos/Adolf/AGLAYA /Recursos/Sons/SAMPLERS_CAIXA_DE_SONS/ArtificialIntelligence_Mini_SP/70_Cm_HyperCrunchFX_01_679.wav\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 3875.0, 112.200005000000004, 69.0, 22.0 ],
					"text" : "opendialog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3790.0, 250.200011999999987, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3861.0, 205.200011999999987, 59.0, 22.0 ],
					"text" : "tosymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3861.0, 238.200011999999987, 97.0, 22.0 ],
					"text" : "prepend replace"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 3861.0, 327.200012000000015, 147.0, 22.0 ],
					"text" : "buffer~ #0_audiofile 0 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2341.333251999999902, 53.0, 194.0, 20.0 ],
					"text" : "AUDIO CONTROL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 456.0, 340.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 282.0, 83.0, 60.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 172.0, 97.5, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "pause in",
									"id" : "obj-17",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 175.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 104.0, 179.0, 150.0, 20.0 ],
									"text" : "temps canvi"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 104.0, 40.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 179.0, 41.0, 22.0 ],
									"text" : "$1 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 155.0, 275.0, 50.0, 22.0 ],
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 50.0, 217.0, 43.0, 22.0 ],
									"text" : "line 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 135.0, 24.0, 22.0 ],
									"text" : "t 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 87.0, 135.0, 24.0, 22.0 ],
									"text" : "t 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 269.0, 87.0, 22.0 ],
									"text" : "prepend alpha"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontsize" : 8.0,
									"id" : "obj-77",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 50.0, 100.0, 72.0, 17.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-87",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 307.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-77", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"order" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 1,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1076.333374000000049, 100.333336000000003, 95.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p bigplay_alpha"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1076.333374000000049, 137.333344000000011, 73.0, 17.0 ],
					"text" : "s #0_bigplaypic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 475.149993999999992, 210.0, 18.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 495.950012000000015, 186.300003000000004, 19.0, 17.0 ],
					"text" : "t 1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 474.0, 186.300003000000004, 20.299999, 17.0 ],
					"text" : "t 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 474.0, 103.200005000000004, 15.5, 15.5 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1109.333374000000049, 19.066666000000001, 93.199996999999996, 20.0 ],
					"text" : "UPDATE UI"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1076.333374000000049, 70.866660999999993, 67.0, 17.0 ],
					"text" : "r #0_playOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 620.0, 224.571411000000012, 76.0, 17.0 ],
					"text" : "s #0_pauseOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 619.0, 35.200007999999997, 65.0, 17.0 ],
					"text" : "r #0_pauseIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 784.199951000000056, 683.0, 89.0, 17.0 ],
					"text" : "s #0_volmeterLOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 698.0, 692.0, 83.0, 17.0 ],
					"text" : "s #0_volsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 703.0, 322.0, 73.0, 17.0 ],
					"text" : "r #0_volsliderIN"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 465.5, 269.799987999999985, 71.0, 17.0 ],
					"text" : "r #0_bigplaypic"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 474.0, 240.0, 69.0, 17.0 ],
					"text" : "s #0_playOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 474.0, 74.300003000000004, 58.0, 17.0 ],
					"text" : "r #0_playIN"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hltcolor" : [ 1.0, 1.0, 1.0, 0.09 ],
					"id" : "obj-5",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 474.0, 130.0, 64.199996999999996, 40.671424999999999 ],
					"presentation" : 1,
					"presentation_rect" : [ 93.891601500000007, 6.0, 130.961133677991143, 129.9375 ],
					"rounded" : 205.199999999999989,
					"toggle" : 1
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"hltcolor" : [ 1.0, 1.0, 1.0, 0.48 ],
					"id" : "obj-11",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 619.0, 91.0, 55.0, 27.700005000000001 ],
					"presentation" : 1,
					"presentation_rect" : [ 240.549997210502625, 3.0, 36.38280328949736, 33.946429226497649 ],
					"rounded" : 79.239999999999995
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 5283, "png", "IBkSG0fBZn....PCIgDQRA...7J....rHX....PdR3yN....DLmPIQEBHf.B7g.YHB..TnURDEDU3wY6c9FbSbdmG+KTlz7BSv2qry0Dzd0zjbmIQhPuBWvLrpjN.gdItxmgROlqtxjYNldyXKeu+X88tlYhE4UPGrnJyvcMVT7QHomDYFBxwlf6zlv5.cXfJQD4ZJRuomMqdQeSu8dw50rZ0yp8OZ092mOyrC1Onc0iz9c+4eO+d9876YcfR6R2.HlENuh1b+Hzw5b6NfO.lUOXQiB08XSW+U..+p+bQ.r7p+N+p+LEMfJdaDFHINYW8esKApU49.nBjD0EAUP2.gcwKCjDpxGQbuthgYI7Hw7Ec0dhKSXT7FC.Ct5QT63BNThDn6t61vu96842CW8pEsi2Z.f2EORHWwttn9ABKhWF.LNjDrl155PIRfHQhf92Z+3q8W90PuO4Shd6sWzSO8z1crxkKi50qi6d26.AAA7wW6iaGw8R.H6pGAd2KBxh2tgjXcbXBKrCkHAdgW3EvN1wNvV9FeCzWe80w5f5wRKsDt6cuCV75KhaveCyJneWHIhC0tV32fAOxxindGwiyJNIGm3ByOunfffnWlpUqJVHedwIRkRjgIhte1V8nB.3fzCyT7nvBIe+z8F5PIRHlK2LhkJUxs0isE777hoSOkX73rFUHmERObSwi.KLfnMdbVwb4lQrZ0ptslqiPoRkDmjiynVjyBpH1UgE5HZYXhHNIGmu2BqYoP97hilLIUD6AIFzQzJak0q6CamlpUqZTqwYA0m3NJcCoujaonsP97tslwygfffXlLSqmHdYHMvNJ1LiiVD8fgRjPjmm2s0HddLnHtBjbIiRaRKcQfZo0ZXPQ7IA0UBKCGZw.wxkaF2VC36QPPPLc5ozyUhAcg689VXfT1SQ7KzzomJzOPL6lRkJoWzInVgM.i.M7sk5WammB4y2JWI3g0R59.OsLRBYxLsaeeMzffff3DoR0Jqvi37xCuKLPC2DFJQhP2DL3UXg4muUVgy5FBEuFwfFtIP8s08oZ0psxWXdDh8CdDnQjDVX94c66aTTPlLS2pnQD57ClCZ3lPPMwY76zB2HBUgSKKHHbmjii5lfGmRkJINThDg1AxkEznI3qQPPnU9AOhqnpb.xBBefoSuq+jI43BMB3rfNvr.GsXpkGwcjY1OYAAgKM9sACJjOefU.mETgafmfn.NKnB2PCAIA73fJbCcng.1WMQFi.BOAREtgCzX13VFcfE54Wwludw.v6.fGWYiExmG+sequkM+VQwKx111Kh0ut0ghEKpr4GG.wgj13OYWuW1o3saHsjcZn.dUHedru8uea7sghWm8vxBgG9Pb8EWTYy8Bf+ZHIfsErSwaAnplfMIGGF8XGyFeKn3WXW6ZW3t24N31291Ja94fT8wqnqzoz.NnxOmQSlzsc+hhKS0pU0JYdXsCQmcTkHYAvUU1P73r3RW58PWc0kMb4o3mYokVBwh0TvFVARCfqsJCqqucNYH4maSkPyyblooBWJ..HZznHWtYT27lfMrZLZWedeGnxO2b4lACLvtayKKkfD82e+jF.2yAIKvKR9r5rLHT4KyDoR41tYQwihff.I+eaq3+ZUed6FRkEnMI2.CSDbyadKp6BTzDM7+cNXwAvYU2FNM.1gxFd+268wV1xVr3kyYnVsZzGtbQ5s2dQ2cuIb4KeYkMy.osrKdhmjMCK7YtKnrXZHWeynK4H2AAAARUx8kgCsRjqn7MlgIhmVHnUrFYXhHlIyzd59dPEdddRw98jcZgaSYKlWeY7nwWTTQrKiFKinNV1m0MTUfP7CyhldhWpH1cPinOTrSId4TeC2OjliFU7pTDSKepNCZj9jr1svkQ8axjbbt8mcCgYEuxGwiyRqJkN.DF7lsG0grPk0I+RksQPPPSKrFQDOZxj9lOq9QzX0WLhcIbYTew8aEIDRhRdddwB4ya3MfOp+vcNHTDSpXWh2rJuvd8PiQBsDuxXTQL0UhNCZ3ZWaW+yZJBC9Mqthh5KdkIWtYLj6Dz5pl8CAquEaWwKmxKnezpqnnwEuhhOZmxQOALsh+Xurv7ya6QdnAqt9kHLnFyHdkQmBq7ZGSjJku7AZuHDbcKqUEtin9FkecT2VQ7JiNk2dpUXaDMh7.iUDuEgJKL9UHYA0L9taf8qLpUXaBBFJ3zRfp0x.hA.6QYC+S+venUd.HPPWc0EFe7TfmmGCkHglutoRmFO+yuUbsEVvA6cAKFarwT2zHZ8Z0R7NtxeIdbVDMZTMdogGhFMJ9EW3BHSlo070Tox8w.6d23eexIQ850cvdWvfibjef5lh.MF3lVh2Fhw1wO9wa6NkaQ850wxK2VKR0lHYxQQoRkZoU3SvwgW8U+6wRKsjs9dGzomd5AilLo5lGwnmeLDPFnlnHY+cgMFuZiDVM51xk4fv.2Lr0mSp7D8Co8XqPKAkcNYK5rQiHBP2PDMCZjOJMMiajbangWzvCOrQE89Jd3Censcs5qu9P129sQ5zSo4q4ByNK1xV1BN6YyXauuAU5pqtvDoRotYcmt3lbYvu+m6zJFscp+hBOOut4IA0Jr9XDWGTa4sA08PIR36Wss68auWG88KZzn3RW58HY4XMtvryhW9k2KN+4y4f8L+E6ZfAT2zlfpnNzRw6g+9G116TgA5pqtvaN0TXg4mGLLQH9ZpT493PG5v3XiNJpUqlC2C89ngqCrJ+Ekh2tgpR2zK9hauizwbR1zl1j9unND6ZfAvhK9qHE5m0HyYOK14N2A0JLA14e2NU2TCFWUJdYU9eDONK5qu95L8JGj92Z+t56eO8zClNSFREat0P1J7+vPCgxkK6f8NuMDLdFEJpuCZJdY2SC+Jk1jgG9P5NwFzHRzH80Wejb6hU9GzT7ticzP0bJvgcOqaFg95qOcmdY.fQG8XTqvqRhuWSOrSr1NDXlUMknQRNKB.WseYjPpAPW6b4xMi5uSJpV3xp7EvvDws6y1FsZou61XzTsLLu14JUpDouS.vibanASwNcrQcCzJDVNIJS0x3wY070c0qVDwhECm7joCcYplFAMHFfFh2WZWuTGtK497Fuwa31cg0PdhMZ0zKC.jJ0DgxLUiPnFY.dj3kQ4+yy9LOaGuC4Tzau8RrcuVLrMqU3vT9Bu4MuY0M0fk2FV0D89jOoCzkbF5omdH1tW8FurUX8hHwI33BMqZim5oeJ0Msl3soh5aPXxI7yzUWcYnDdWdUa7uNwDd1GFsC191+lpaZMMKKTMx1fFfvH38SidOrWGIpVsJwHNzjk2u9e0W2NenghMfrU3VkiDxVgChI5iVt9sdnJRCa842pSzebTHM.n6d2637cj1f95qu0xQhVElufZh9P3dHaSqjhm3IdBGoy3jP5ulHHH3B8j1mgG9P5loZAwzsjz8vlr7twMtQmp+PwhnLS0BiVgWEll748YBPw3MnirU3VspMBJoaIgINioc23ro3xzSO8n6p1.HXltkTwa.gcMv.3l27VszJLPvJcKaxmW+9BtLLiQV6b.MZE1GO4FcudnXyuFH7L6Z1YcavqgrUX8RzmQG8X9lD8gP91DKT31.orj6V27VtPOw4HnktkcQHJXgBwaXlfb5VREug.LqUX+Rh9PEugHhFMJ9vO7p5ltk9khjMU7FBwOltkUevCZps0CfUT1PPH9eTzGitL7ksBe4BEbndFY9x+vWptohqGp1nh8BOkQw4vnoa49OvA7bI5C0sAJlNcKcaqvxPEuTVCiltk6+.GvwGLGoIUZ8PUQ68S9jeiS0en3QYe6ee59ZtxUthCzSdDDlTI9M.IedeMGsmPwyw0VXAL6ryhoRm1PudBqnWmlk2f5V98+O+d2niPwEnVsZ3m+y+OwktzkvUuZQCedLLQvAO32sy0wHvU9vlszKa4cM9hu3Kbp9CEWhKWn.N+4OOxb1yZpyKdbVbzidTbnCcXGO6CqT49pap3FfJedu2meOGqCQw4nb4x3+3bmC+rr+LRBgVxjbbXu6cuj1mHbDzZtG1.TUxHMye9fh2l50qiqsvB3Lm4L3ByNqoN23wYwwO9wwANvq3543Mg4dXN.IwKfzrrsVd8Vtb4PSd8FDYokVBu6EuHNAGmoNOFlH3GMxOB+iG8ndp6+Dh.1x.OR7xCE0qrR+temmpySQepUqF9nOZNbpScJS+WOGMYRL7vCicMv.ttUVRPHHB7.ZHdILOxT7nX1PbICCSDL1XigibjeflUjFuBe1m8YpapAwaEk+Oe709XjL4nc9dkCQPaI+HGhq25sdKSO3qIRkBIRjv0F7kUff+5MY4cMHESM+LjVxO9sBns7fuZmPbcvC9c87VYUCgU1wJXUisxh2hJ+eqT49zAs4QncCw0qM3fHZzn5+h8nPnlxslgVkyv1bPgeue5m9ITwqKQ850Q97+2Xl2YFSGhqgRj.G96eXOQHtrCV75Kptohx+fRwaCCZawquHFd3C0Q6XTZjfVHtrCl8+poGdKJ+CaPUiio7jdyoZ8JNkR6SsZ0vu7W993bm6bAtPb0tTtbYhSKr7OnV7tFT+d6rDFBwU6xm9oeh5llS4unT7tLT426byUjJdsQBag3pcYl2ooMa7hs50ONTT2+GJQB2d6HvVfgIRS6oAExm2QduEDDDKjOu3nISp69Jg5i3wYEykal.yVoqYPPPfz2ID22gkIl5SHH7EGIgQmdCUoToRhSxwQ7AG8NljiyWsguzInP97p+dohZwp5jQmG.2G.qsJ79nOZNZTGLHsaHtd8W+0CrC9xr7AevGntohF47NIBXtNfNrk2EledwI43LsEVFlHhSxwIVpTIaquDDPCWFFzHh2.mqCcBwa0pUEyjYZw3wYMsnczjIcLet8iPvkgFVvD5QEkmb5zS41edrLZrk2a4GHKjOu3DoRYoAekN8T9dCANACkHg5u+xZFwKm5u38qvyySTLYFpVspX5zSYoAeMQpTA1clxNAZXrokQYPMLpu.90a.VU7JGhKBVALjU1vZHtZWHL1gJlQ3JyEUdQFMYR29ykkvrhWZHtbODDDH8893VQ7Nn5aN9QKI4xMCQKiJQPPPLWtYrzfuFJQBwB4yKJHH3ReBCNP5dETsOApjlJ5HJ3hPULe+om9z3e6Dmv7OF3hPZaZUdq.8ZKr.txUtBMKt7HbpScJ0M81vjQZPIMLcwvGZ8MSloa5oYFlHzPb4wfP3wDgzXurLcCIkeC904mHc5oLsHU4Q73rhYxLsu6gV+FDLlTrcDtxvo9Fpe5FoURHF.ZHtbRzvpKqdBSiTedOITU5++om9zF3z7eDONKxkaFHHHf2bpoBUoenaxO4M9IpaZNXSVdA7wVe0yxqb9EPCwk6fUs5ZFZx22IRkxs+baHzJgYng3x8QPPni4qqZ3TK.7CVqDDDVy5KCSDwzomhlEWdDHEIHXBqtqyDh2tgzT0sVA4anDIvu3BWvDWBJTjnVsZn2d6UcyuKLXpOB.7ULw62eB.UUdwu8suM5u++Fze+8ahKCEJ.iO1X3F23Fpa9.vDSJgYr7JSQnXQZxvDAKt3uJvuRVoXebsEV.Cr6cqt4IgjqoFFyX4UFd.7OK+KKu7J3+6O+mw91m96fLTnTudc7c9NuLVd4Fh958AvHP5ut2wogkJDf+MkIo3rnQh7aX+bUhUba.PZva7PQR6Pcefhdng6BlZPZJwJtM.HYdmGRl5Afj6C+u+w+Hd0WitktQoYpUqFdkC9JpcWXEHEZLK4tfUEu.RgM6u..6TtgabiafMu4mFaaauXabYoDDY7wFiTsX6HPUsg1IQN1u9tIufhygFSFQ11U7YUedURL.zP.6n9+RQFM7y89PR2X4DMGn8baPlpPx2k8K2vxKuB976cO7JG7f3wdrGyFdKn3Gob4xXvu2fj7yc+vhKrRkXGhW.fEAv1.vyI2vsu8swi+U+pXOrr1zaAE+D0qWG+K+3eLtdyU17iCfBtPWpkHG9rF7swOWvRnXcznjAj0kzlFBY+XZnSSW2WgKzHMTcsnJXFZpVmAp.NzfFB2JnEKgcuFi.BBX5THGrQCg6xvjkqIu.Msr4YXhPSD7.JAIgqLYAU.G3oE0l3QbGYm8QVP8ANvRPV3JSVPEvANBCBWYxBBePykaF29d.EShffPqJv1i3JpKGfrfvGX+VIjJLiffPqpYwi3JpJGDNnQcTfVCE71TpTIspawKCatPg3kYDPP.GONKMRDdTznx136CGlUYPPXpjA0OXOEBBBsZfY7nMKCo9YhABIyNfT4jh5Fg6RoRkZk+sWD9no7sSQ2PpVPzzWPLLQnqJCWBMJw9xGbtfNwSCGz3KqzomhZE1gnZ0pspxZtLr3J8ML.KzvO33wYoI1SGFMVqYT+aMAcCUaiVJOlHUJeSsA1u.OOud6QGbtfNvWyHPCqv.PLSlootRzlTsZU81JZ4QHLLX1EszJLCSDZX0r.5D9Kp0ValAgFgTCq5OLMIezGAAAwLYlVuc2yhfZs01oaHYMPSWIj2yeotSzHUqV0HaIsUPHH2DbaXfFI3i7gb46OrOvNdddi3dvxPxnPneBGbRhAMlbCkGSjJUnxkBStOJyApn0UgEsXPcPg03I43BrI9yByOudQNPok1SBZLa8Tv.cbmP9HdbVwzomxWOoGBBBhExmWbhTozyWVp6A9HjGXWEX.gLCSDwIRkRLWtY77Vk444EyjY5VkrLZEq1Qbpu7cRripDoWlAgzMNCWwqYXhf89s2Kdoc8RX6a+ahd6sWWoZWVtbYT8AO.+5eyuF25l2BYN6YMyouBjbk5jvmToZrBAcwqLcCIg73.HpYOYYA8l27lwS8zOEd1m4YQWabissvtb4xnd85n5Cd.9x+vWhe6s9sXkUVwrBUYjErxGAdBKhWkv.oA4MHLgEYcunqJv0CKJL0hPmfUIgQwqZXgjPNFTr+x4QYEHEdP4i.qKAFAp3sYXW8HFjrRaZ2LrQlCRC7rHjDpgZwpZnhWiAKj7aNlh+En8sTuBdjfrnh+sBrgJGdPGp309PVXqGU.UXZK7+CzLStsDhLO50.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-35",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 470.979918999999995, 300.600006000000008, 54.816662000000001, 55.12990006857143 ],
					"pic" : "zz_playbuttonv3.png",
					"presentation" : 1,
					"presentation_rect" : [ 90.821289000000007, 5.0, 129.061079545454561, 129.798571428571449 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 20.0,
					"id" : "obj-30",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 34.0, 21.0, 93.0, 29.0 ],
					"text" : "PLAYER"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 211.0, 326.600006000000008, 44.0, 20.0 ],
					"text" : "drag"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 215.0, 81.5, 150.0, 33.0 ],
					"text" : "fast\nswitch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 367.0, 130.0, 65.0, 20.0 ],
					"text" : "leftbuttons"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 619.0, 13.200006, 57.0, 20.0 ],
					"text" : "pause"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 474.0, 41.599997999999999, 57.0, 20.0 ],
					"text" : "play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 34.0, 88.0, 107.0, 20.0 ],
					"text" : "background"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 20.0,
					"id" : "obj-2",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 34.0, 57.0, 62.0, 29.0 ],
					"text" : "GUI"
				}

			}
, 			{
				"box" : 				{
					"allowdrag" : 0,
					"bgcolor" : [ 0.501961, 0.717647, 0.764706, 0.0 ],
					"buffername" : "#0_audiofile",
					"gridcolor" : [ 0.352941, 0.337255, 0.521569, 0.0 ],
					"id" : "obj-158",
					"ignoreclick" : 1,
					"maxclass" : "waveform~",
					"numinlets" : 5,
					"numoutlets" : 6,
					"outlettype" : [ "float", "float", "float", "float", "list", "" ],
					"patching_rect" : [ 3855.5, 482.700012000000015, 251.0, 77.594054999999997 ],
					"presentation" : 1,
					"presentation_rect" : [ 28.0, 167.498060599829387, 254.932800499999985, 32.01919079948803 ],
					"selectioncolor" : [ 0.313726, 0.498039, 0.807843, 0.0 ],
					"setunit" : 1,
					"vzoom" : 0.360000014305115,
					"waveformcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bordercolor" : [ 0.803922, 0.898039, 0.909804, 0.0 ],
					"drag_window" : 1,
					"grad1" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-14",
					"ignoreclick" : 0,
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 38.0, 300.600006000000008, 161.0, 72.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 360.0, 220.0 ],
					"proportion" : 0.39
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"background" : 1,
					"data" : [ 21565, "png", "IBkSG0fBZn....PCIgDQRA..BnD..D.tHX....ftQhrE....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6cuGcTUe2u3+cB2DYHIhJDPtjYrffJZPkJ9b9UXTSTdNkJvCWakilJBXU6xnXE5ZIRP7bD5QzziRKWkPev9nbQHJ8TjDwIzdpPAj6WDjjvEg.UwLgIJHRle+wd1jI6ruNY12l86Wq0rfr26YOe.BSdOeulFHhHhHulf.HK.jab+Jh8qYZf6SEw90cAfZAPn398oDRytK.hHhHxzjCDBEkKZLTzsaAutGCBAlBAf0AfpsfWShHhHhzTN.nXHDNIpC4Q0wpIwVthHhHhHKUtPn0ar6PQZ8HDDZkKWA10aDQ1Awt.HQDJIVGDkJHKHzZMOpdeB98G.CYf2NxNqNfq22UgtzgViN2tVA.f.sOJ7kdTEetQZHMT42IDe3rW7x3L0+CnpuJBhbgKhRV4ZMRcWA.J.N7tkiAkHhRVhePglEZ5.DMG.zKS30LLDFGD.BuYa0w98gj7qDkpJHDZEIEG.198G.+zexOF9uNen+cpcZFDpkplKkFp56RC68q9Vr6pNMVy52fZWdXHDVZclVA0BwfRDQFgX3mbgP3Gwe0LBAkrHFlp5XOBglFphH2pBAvaH2IJXriD2SfNi6tSsBY2FyKTjdTykRCa8bWFqZKGDkGZyJcY+R.Th0UU5GCJQDoDwPQAQigibxAhLJw.Tgh8qhgoHxMnDHSWsMpgMTLkAci315PCVeEoCketnnj+1ATJvjiLrDCJQDIJGHDJR7QpTnH8JLDBNI9XWpbsDYWJAxDRZdO2iiGtGsw5qFCJRCogk740g4rnUH2oG.bX++NFThHuqr.vHf2NXjVhO3DWKXHmfhAvyD+A76O.V3D+2crshjRJ8LMfoLmEJ8v6FNrkP.FThHukbgP3nQ.qYQmKUywfPfoPvAO3SoTVE.fkE+A76O.VwT92wM1N2UHIQuyItDl5quDoGdV.nHquZjGCJQTpubgvavNBXwsZznGy3PFYjA..FvcbmW43974C88l5iguee4oNMN8oO8U95u3HGFm+7mG..u8RWbKrZMrvPHrj3ChLS4Bg.5WY1s41CIIZ5eZMRWVABCggBfiXaPgAkHJ0jkDNJ+7ePzid1Sz69bSvmOe3tty6...C5tGnY8Rpo8efChye9H3Pe9gQjHQvN+rcf5pqNr5U8dl4KKCMQlscg3ZE3TkPR.ByJtQ9G2.pppJi+vOKD5lQaGCJQTpibfPvnBgIDN5wl3jPu6yMgd26dianac0VCCkn1+ANH9xScZb3CeDryOaG3S1zGK8MmSFBCgAaaIvgMnTIWqh.vLi+.qXFOAxqSoN+HbY5BNGyXUJ04ukIx6ZDPn0iFdx5Fle9OHt6AMHbi+ndiAdW2AtkateIqasiywOwIw119mgibjifcr8skra4ocCgOU75fCoaDHWmbfPf6qzkaO8iLd7hC3ZrsBxLTykRC49B+AoG1QLC3XPIhbmxBBsbTAHIz5QhAixc.2AF3ccGnm8n6szaoqU3v0gstssiO6y1YxL3jXWyUD3LmiLlRPbKE.98G.k8zCMgWYsq4Rogy98ogNzZzh61tidwzQ8+.PmaazjxhZ4qryuAu0e5ci+PNhteiAkHxcIWHDPR26oSJ4wl3jvOYvCIkuEiZoDawo+1lq.ePoqKYzUcUfFakIhTSN.np3OvBm9Tvv6R5IzM6SqKcLxYN+q70qcVOEtmLRrvRketnXBydAW4q23q7Ts3km.o2SH7+UB1htoIAsNIceBpiqImXORFBox4pF7SrQodBBgViXHsjaxyT3ygexfGBx69BhLyLijPYk5qm8n6nm8n6XTi7gPwuw7vFKeS3us4Mi2YE+mIZnogD6wwfv+lVRxqZoTLEE+WjWvAmvgj..Jc+mpIeckguHtmLRrEnxx+7yzjudum6h315PKawt7V6Xy9yVK586RVRjfRYAgl6OXrGJtQ7Yhlo1WRSraH7o2JFbbBPtKED6QB+FFLbTx0Cj28gGHu6Cy9kKpkFZpWPXMwoHH7dSk.99STixBRZ43B9I2rMUJMWEaa2M4q801V0humY2lnvu+.R++R4BadbJYznoE.gVq4Mfv.G0NBIkHtcHDtJDD9lOhb5J.B+eskgDHjznGy3vxV9JPs0FFE+FyCiZjODCIYBDCLUYkGEq98KEOSgOWhba5EDdO0pgPnI9dTDfv6AbE98GnEOK2N4Y+5VzyOdR+fA98kb5fpgLvlsN3lSR4F2BXjfRE.g2z1sDNRN2N33Bfb1BBgO8zxfAGj198G.u3LlI129O.V0JeWTvi7vLbjERrq4N1wOAdy4u.je9OnQuEYBgOPW0PxOjj7jJH9u3WNrVduPIcins+cpcs36YxVftzrOmfsuDAn2Hf4BIKa5RI1bYiZXCEcsSYA+WmO3qssRWoL+fC+0HxEt3U95928q8J+98dxll.NPWxBcnMMuI9BjY6PGZEPUQ9AD46uLp5qhHczyKZHP3a.KQyBiHqSPjfiAoQOlwgw+y+Erq0bH5YO5Nd5mbJ3oexofMV9lv+2+xeA+9hecibKxDM1kbEA9dUdQ4.IawPOTOZO.Z4yrLmNY9461dKrp2fREJ2AyK3fQA+jaVilCT6QA+so15AQOxVyme7uN2VGRG.oCziqA+7a9ovq+2OJVy52fzKtHv27gbFxABe+ngmEaOSgOGF+3GuqbgezqPb7L8bO2yhEu3kXzwxj3XXp.H78HgLkhjbhFQ7ewnF1PSJS+9jk8Tey6Lp.sO4TexzJW1dKJomtdqYCnL.foO4IfUL7awQuxfdisqAL26M.76OfzS0K3.9KexSKKH7C+pBFLjzKNiYhic7Sfhei4wPRtD8rGcGy9kKB6bm6Du47WfbumjZFB.9DHLrAxwLpOxwoIAk925SKecM6nWr4+39Njrl26.I755jafdBJET5Ad5GY7nv90wje0XB7kdT75E7uK2oJvhKEhDMBHLNjz8r2zu+.3Mm+BPs0FFy9kKxSufP5lkYlYfm9ImB14N2IV86WpQGGSCGBeeSQlRwQNIMoK3GTma4ikn5+gleLWx9Dms20a5InTyZ4kBu8NYBkh44dxnALpgMToGNnMTJj2VNPn6SVKz4.0VLfzN24NwS+jSgiAoTDYlYFXTi7gvF23FLZfo3Gv2AMoxirWAi+K76OfaIPSRgLcgWylFbVM8DTJGoGvM1DaOvs1retjs+W9jmRQPna1z8f09EmwLY.IO.w.SFrK45EZr63r8OwMkT0jFmPloKus6rW7xl181IluvvAkjokYbEt6NI6hgEGmRjYSbwRS2cyl3XPZ1ubQLfjGhXWxYv.SCGBstzHz35H2ibh+KheVf6TbFI8iWAicj1TkXML7ZgdGu51aF0goSbE+TB9IwHyTQ.XmPmsd4iMwIgOcK+SNFj7vDGCSgpnB7hyP2YqyDBcmKacoTCM4Cv2kjzHttpHxLHkHcwvAkxNqNXF0gkPllvjsnDYFLTqHke9OHV86WJV5RVDmEaD.ZbVx8oa4ehGahSRuOM15RoFxI9unysqku0f..D46aZ2kkp2JPISFNnz066pLi5vRHyJ9I+zWTxVgv.shzaN+EfUspUhQMxGxbqJxUZP28.wRWxhvxV9Jza2wI15REC99atUMY.014157FyNloZtTyVxgBaG0Q7R7sgXWH4VQuIJIIKHz0Gugdt3GahSB6a+GfCTaRWJ3QdXryctSizcbOCDlgkrUyc4bRKzjVgy98MKnjstg3B3wBJQjIIHD5xigq0E52e.rrkuBrzkrHbK2b+L65hRgjYlYbktiSmKm.2NDZcSY2YEHhzGOUPIesksnDkzUDDll1ZtYQ+XSbRHTEUfBdjG1zKJJ00ft6AhUspUhWctuldeJuAD1xlXWw47kicW.5QUeUDS6dalK8.IJ8DTpZytHrJxrA8FzFJCJ0fXWsoYegDeqHwYyFkLjYlYfo+BS0HstziB1UbtAthvrwuI1CjbWBCjtzC.GvdbnmJnDQII4Bg+yqlc01nGy3XqHQlFwVW5YJ74zyke6P36a4rhy4x1GON1s+UjKX2kPy3o55s5adK5UqMTFj61HfvOrQyY01qN2WCqZkuKaEIxTkYlYfhei4gU+9kpmYFm3rhqHSuvHJATSs0K8PgrgxnILbPImXZO8pxvWT5g77o2ICoPH7CYTc7H42e.7oa4eho+BS0ZpJhfvVgRnJp.idLiSOW9LAG2RjCTIqbsROT01PYzD5InTSBSHSZOh7BJA5Xp++XSbRXm6bmbgijrE8rGcGqZkuqdWFADG2RLrjyRSV2f1S8dmN9Ql0PI.WRPI18TjWl3f19Q05Be049ZXoKYQbcQhrcy9kKRucEm33VhCxamCOaOcruy2fzCUgcTGR4chpRjwkEzwf11u+.X0ueorq1HGkQMxGBe35WudlUbLrjyR0w+E68bMaHijxpxZclCOFFThH4kCzwf1N+7eP7gqe8bKHgbjtkateXUqZk5YbKkID998flcMQZpIgCLy0rHmlcW0okdnP1PYzLLnDQMm3lZqpgjF8XFGV0pVIWgsIGsLyLC8NtkxDBKdpEX5EEolPw+E+k+1+zlJCqUMWJMrl0uAoGNjMTJMCCJQTSItFIo5La6EmwLwRVLGORj6wre4hvxV9JzyktLvvR1ocg3FP2UUUkdhAz8VOWyV+d1MbHiQ5T++1mH8S2gjl8KWDCIQtNE7HOLV86WpdtTFVxdst3+hsblu0tpCKydOUXoGpDanLjECJQj.cERZ0ueoX1ubQVQ8PjoXTi7gv91+AzyLhigkrOMInzxVuiXxeYZp4Rog25O8tRObHanTjkgCJkL2SWHxgP2gj3f1lREbK2b+vGt90yvRNWqCR59sxOWTarbLWJzsaNhY7F.aQIhzLjj3z+mgjnTIhgkzwxG.CKYOJoIewe6.1TYzb9tp10judum7qaQ2uUskCJ8PkzhtgIYLnD4koqPRb5+SopLvxG.CKY8JN9un7Pa1wzpR9uNeIs60mVW5n7PaV5gKIo8BjDvfRjWUNPmgj3z+mRkkYlYfkr3Eo2vRivBJIRP0.X4wefR9aG.QZP1s4CWqR2+ojdnkCGxrcSjg2q2byaJtszlGjRYHtsjvPRDACEVpDvUvaqTQw+EkGZynzu76soRI4aO0mtbaBtkXCkhpL7d8F2TbIWNwskDEWLIYHIxKRmgkDWAuYXIqQ0PRqJM0WeI3nWL0nyf9ckuWoGpB3flsahRM9aahzuR.CIQjrLPXo0AgOzAY9JDwMC3..d8+9QMbWv4qsspIecEaa2s3BqknzyzfbiMohrgRQSLnD4kTBzXCt8+87dCFRh7zxLyLPQEMSsV5.5ED9j+Lrj4qVHYfzul0uArjOuNCcSt6N0zfR+zexONgKn92olNq2xNqNXnmeMWJMLm2qLoGtT3.aMI.fVa2E.QVjB.vip1Evk..hDHtzA7yF1vPUUUoRW1sCgYlUAVVg4csNHDj3JePu4rnU.+SeJX3cQes2Q1sIJ13q7TXum6h..X32PaAPhMK5tsNzvUtW9Zaqv8e8sxP2qh29ok66qJLgJFKfgaQoy+semYTGDYlBBgYrihXHIhZJctnT9nvg1cIofJ..GK9CLk4rPCsjAbacnA7v8nM3g6QafuzaYK0.h2qg2kzMz8pzyzfbCf6YAgwikijdCJck0OcY1ceIxIKWHY6.PpWbFyjgjHRF2xM2O7+ddugVW1LAaUIqPsPlkmgIL6E3XVekzxdpOcLk4rPoGd2vgG1liQIJUVVPXbIo3x.f3FbKQj7F0HeH8rQ5VL3LgyJrK.7KkdP2PXoidwzwTV5eU5gCCWPH6DJnzdpOkIeU01cAPlpRfJyvsQOlwgmepOm0UMD4RMpQ9P3EmwLU6RxDB++MN3tMek.EBK8Nm3RVe0nCG8hoiIrv+pRiKIGyd5lRRnDO02r8uN2AY1Peq1FJCxZTHTYFt42e.rjEuHjYlYXgkDQtWy9kKBO1DmjZWxsCG3hEXJpRfj0WI.g0XoWYmeiiZ06VkPRKGtjueIgBJs2u5aS10AQIS4B.EGXEhqURLjDQFyqOuWSqMQ2gCG7rWJESA.3Ykdv25O8t3I9v84H54mOsNUCIUf0WQIF892jMY04d2UcZSnTLet4seER2D2dRTDWqjHJwjYlYf2n32PqYB2a.NdkrJECY5FtxCsY7.u37whp7B1VqKsnJu.F4Lmubgj1MbQgj.zePolzGhqY8avQjV0njY6WIjMTFj4pDHrX3IqWctuFmgaD0BnyYBGW4tsNk.f6ERV8tA.do2bYH+2ZCV5.8dO0mNdpOtJ7RuorqHKKGtvPzsR6KA.BqCMAaxS755NxqG9R10ioZFq8+Gps1uI9CMK6pVHSQg.3YT5jidLiC+eJ90svxgnTS2b+tIbUWcGwGW9FU5RxB.8C.uq0UUdZUCfE.f6A.4D+Ips1uAu+l2N10EuZjQmudz41kNZqIzHS0bozPw64avu9MVFN3g+B4tDWU2sEO8FTJK.L93Ovt1+gPuGvch95y4LnwTy6bhKg26u9wwenJfKYfjQ5RNP3MkuJ4Noe+Av+0+0+EGWRDkj7+2+s+Mr+CbHbfCrektj9BgtY4PVWU4ocAH7yzBCg.SM48Bqr5ig2eyaGe3Q9ZDIitfqOi1iN05VdKM8o0kNl+mcZ7DE+eh+4t2mbWRX.7qfCesRRM5MkSN.nJ4NwBMvRntco7yEESX1KP5gcsoaIYsKnxRAvGU1GiGHu6yBKGhR8c7SbRDbHCQss4jvPnqVp1xJJBP3mYWLzXusLufCF2Ue5It0t3C96PqvM1tFz7FWykRC667Mf8clH38J6en1+1CHzfDE.W9+9ajlCpZnvX+nfwNRT3c0UjcabVK3U0bozvR124va8mjs0eeVH7MRj6WQPX0AVVu5beML8WXpVW0PjGxFKeS3Ay+9U6Rp.RF5FjkIHDd+wgn2mvnF1PQGu51K64jYqGQIGK1qaI58I3jYjfREBUlx0.B+E7Cbq8B980ZDn8Qaw6kLIhidwzw9p8GvdOUXkBHIxOb4obI.H7oU2oRmbziYbXUqjCSBhLSy42MO7am1yq1kvOXp8JHzXskKI4XP3emKARls7tYFInTVPXVhoX2aHm7BNXz8N2zE5wJ11t0p45fe+AvPFXyeoheQirpuJBhbAgcB4y+semQ1G5X2tk5P0tbae6+.bo.fHSV3v0gwLlwhxJ6iT7RfP2Akx7COcoxAB6WbE.C9yx0PoPHbjpKMKtUFcjXmKDBKo3dmkKvtgP5Z9eXc+TsUNey4u.7zO4TrvxgHuqsr0sg6YP+X0tjRgLapqjsIGz3LZOWXrfSU.gOjZHjhFNJdIxTVKKHzzZOZRtVLaggPcWLXHoTA4.g+iprg1erINIrzkrHKsfHxq6s9CKD+5m5IT6RFI7.+fUWrbfjkW.I1E7f+7yVxb6OGH7I5ChjaS30RTQb+9pi8nVzXxWJ0w5fJ829wN9IPO6Q2svxgHB.3AdfgpVWvcLHz5EdteXK4dkLWDjBF6WyEJuhrJMMpmLcJ0hED.ehRmjc4FQ1GczEbyBt30TGx6wcrZQRTSUMTXopfyxMhre5XVvwYcL4ZvfRjaSQPk0LoOcK+SLn6dfVW0PD0LgCWGFv.FfZytYt1JQtFN6kTahZprfv3hSVu5beMFRhHGfLyLCrfEsX0tjg.FThbIz6d8FQNAK..2sbmvu+.XIKdQ3ptp1YwkDQjbtw.9wIN4ovN24mozkbufKBkjK.aQIxsHGnxRRwKUzKyM7VhbXdtm6YU6z8Bbg+kbAXKJQtEqCJr9dL5wLN7+5UdYqsZHhzTmu9qGW0U2Q7wkuQktj6EBsT7ErtphHigClaxMHHTY4.fCfa2mvgqCG7PetcWF51WdpSiSe5Sq408EG4v3G069H645ZW6Jtgt0U..O02upiA1MWt.nTFiA.QAv1Sfm6z.PYwd9hO1driqGSF.qTxy+b.XN.3Zzndk6wJicdxcHDT3eKerINondc6a+GH5iMwIoz2qyGNzGidLiK5xV9JhVasgs6uExz8lyeAp82E0BkW68HxUIQBJE..GEp+FFGE.2oJ2CoArj6wjUodU6wbLveVH6QPnx+FdrieB69mAXqN1wOQT+9CX6+Pe9Hwe32efneTYerc+sRlt7y+AU6uGJBDkBvnAktFzXHoihl25QSSx4kqkghOjjzPMiQx4ySm060.fEF2yisrjyVHnvat9hyXl1868a6dwYLSa+GzyGImGq98K0t+1IS0pe+RU6O+rUknTBFMnjXWkscnb2icMwNezXWe7lVrieNndKNI95bTCVuhOuEpx8lrWAgJ+fEudqIEMpleJc9vk8Xe6+.182RYpXqJQo5LRPoqABAbhB0C4fXmW7+nDHtiKFfROiiIwVlJ9tfSq5cxZbdx9EBJ7lpr0jDnze+vGtyGo5i4N1pRjajYsNJMFHDVpR.rCMt1cD20H18YAPiArVkNd8DuFsBkImuIAdNj4KHDV8dk0jlziacUBQVj2doKFG+DmztKCSynF4Cg7y+AU5zYBttJQNPlUPIwtZqbcd8hAkBH4WqL1CsHdMFInj3XSRu0HYsJPoS7hyXlnm8n6VXoPj0YaaWwUx5TBS4W8jpcZE2hhHxtX1Akzaq0HFz4Zj7qI5yWM2IDFD34E69uHc9ZPVmbfJqB2r0jDrkstM6tDHEL5wLN7XSbR3wl3jfe+Az9IDG8rdM4loQqJ0KvVUhbXZscW.V.ww.kblBXWu4Do3mpjsljw7oa4eZ2kfh5V25pm4eKCGtNT9lBgeyTeV0V3EA.vN+LsFsBteS4W8jnrx9HkNcg.nDqqZHRclUPIwvG5oEd.Zrq19FI+Zh970xbgPKIomt0irVYAU9Dkie7iy5pjT.doU.ZmrLyLCLpQ9P3F5VWw8LnercWN1t7tufvu+.JEZ71gvXTLjEVRDoHypq2DCrHcsMRIhisnJk7qAPSmIbJQ7Zj6ihsCHrUsD+ioCFRxoZDPXPc1LO1DmDtkateVb4PTxyft6AhQOFF1OyLy.O2y+BpcIEXQkBQZxrBJsJHDVJ.zNrzchFCJINvpie1xomEDRwqI0uMqS8UjRmXbie7VXY37s8cn9f9UkwABYiFRv6U0y+Ia5isnJwd8+3g+4pc5GEboBfbHLyVTRLziZ6GahqR1.BgqhuUdDmx+SCZufSFH1ykCLa2sfPXvb1L4m+ChGHu6yZqFWtdzydZ2k.k.zZLLkpHyLy.u3LloZWBmAbjifYETBPXfRWIDB4H2Ff6zhc76L10MEImetPHrk3p2sRagIhsljzmO49TfRm3WLg+GVXYPDYEzXLGVfEUFDopDInT7qj1x8PLPy2.f7gPHn.wNtzqSrkfFKjefXmOZrkolljm+JQicq2T.WOjb6xBBiOIYMxg+yrvRwcHRjH1cIPTKxsby8SswrUufPqLSjsxLaQI.gPP2HDF7zRCxriXG+Fg5isn7gPPHoqP2eCDZ0oNA1kaoBTbPb+hyXlHyLyvhKGmuib3OW0y2st0MKpRnjIittK41M9e9uPsSWfEUFDoHir7.rJHLiwRDyM1iD0hfwCC0RpWx5UfRm3m9S+oVXYj5nKY2U6tDHYblZTeAk7duu62hpDmAMVp.FADZs4Zszhhn3X1snDQ5QNPg80s7y+A4ZADkR4Tm5T1cI3njYlYfGV4wfXlPktjmHq.CJQNAJ9FgOzHFoUVGtJdkoQdpl5pqN6tDbbznUiYPIxVwfRjSPAJchG5mwtcSIZMMx6ZWYWu4Ds5U8d1cI33Ln6dfpsteMbv0TIxFwfRjcKGHrkEzLidLiyyrWfYFtgtwfRtQC3NTaYiK008kW9pcZ1pRjsgAkH6lhuA3OcXbIAPIgCytugRs7yF1+c0NMCJQ1FFThraEnzItu6U1w2MAfCdH0WZ.HmoiehSZ2kfi0sby8ic+F4HwfRjcJGvtcyzzu9dS1cIPRbpSo9RC..vccm2gETINSr62HmHFThrSAU5Dra2Z43hzI41nQ2uEzhJChZBFThrSJ9IDY2totuTGsLAQtMZz8arEkHaACJQ1ogK2AyO+Gjc6lFN8oYPoTUd8EX06dPCRoSkIXqJQ1.FThrKAU5DZLNEHcPkMZThbz9ICdvpc5fVTYPzUvfRjcQwlQO3PT8MJI.DIRDUOeFYvwmjSzg97Ca2kfi2Cj28o1oY2uQVNFThrKAU5Dd8tdPONxg4xCfajVAbYKAJ3wl3jT5T2N3xD.YwXPIxNjETXYA3YJ74r3RgHmC1RfBzX0IOnEUFDA.FThrGAU5D4N.u6ZHSxTu6CWCkH2q6MHGmRjyACJQ1gbU5DC7tXPI83S1zGq5484ymEUIDk7cK2b+fe+AT5zJ99GDYFXPIxNDToSbK2b+rvxv8ppppztKAhLU268c+JcJtHqQVJFThrCx9FcpL.NIJkvWbD0m0acqacyhpDmONNkHmBFThrZJ1r4Z7FiTLgCWmlWSe5SusfJgLpye9yq546R1c0hpDmOM1y6X2uQVlVmjue4B6YpatK.TqM75RFWPkNgWdy.0HN3gzdoAHiN1QKnRHx7nwxDBCJQVlDMnzHh8HG376u3iAgfTqC.kXukBAgumQVb2tmHJdidLiCqdUumbmhAkHKiQCJEDBgM5URuRLO8J1igCfBfP.O15S1GYeCt7y+A4tcOQTSz291WkNkrqCaDYFLxXTp..7IvcERRpg.1pR1MYaAxa9VtEqtNbszy1fAacNJUvM9iTcr1EzhJCxiSusnTN.nX8dSKXriDYmUGv066p..P+6T6T7Zq+x.qXaGE..c7pauhWWEaa2npppDEL1QdkiE+qQ79WQt.Nxo9Jrl0uA4tUCGBspz5zyeVnjJEat7eTu6iUVGtZZsMX..15bNTu8RWrpmuqckCl6302aR02WHGKpLHON8FTpH.jozC52e.7zi79v8mcaQ1sIpJO8FT8leO2uesqf6IKR7mic...H.jDQAQUsulqnM.8qiXBC7ovyUxeUt0blhACJYGxQoSvYoEQ.2P2XPo3oQKiliEUFjGmd65slsiMWvXGIJ6oGJd3dzFMBIYetmLZ.qXJ+6xsBu1KvACncPw+NWiO4HEmyTyos6RfHKQlYlgZqP2AsvRg7vzSPofPRqIMpgMTLm6Ia3KcmY.o3cisqAL8wkubmpYg+HSWNJchd1itagkg61oN0oT87bGnmRknxJzcNVXYPdX5MnTSLiAqXBeGog2kzQdMeSVLnMTJdc4H2A4OXO4h6.8TpDU1fmcySrHxEQOAkxQ5Abpc0lZFyfZ1dHlSe8eJUjrc81MbC2fUWGDQtDZrAOygPAY5LbPoQMrgZNUhI6VyR1wsN+OYVqlMg..3LdynzZlSQNSaYqaSyqgKqCMmFqX+1wNAA4wX385M0lB+NY2X6ZPtAEH+OYVmfJcBM9DijAoRWUPNbbYcn45XGYKJQ1KCGTJ6r5fYTGVhgLvlsXtx+SlC.mwaIWL3IkJ4Vt4lMrIhG+vtjoyvAkjaAdzsHPWZ1+mh+mLqihgR03SLRw43m3j1cIPjSB+vtjoyvAkby5PaZkcWBdYJFJUiOwHEmScJtFJQdOO1DmjRmheXWxz4oBJQ1J9FZVDtJm6LU24OucWBoh36qPlNOUPIesksnjMR1lHmqgRIeYzwNZ2k.IiCe3in544+WPYcqacSoS0rAdJQIa5InTsldUXQ76qYKQ.r+ssYbwQzX19N9L6tDHSB++BJqKYy8.Ox9nmfR6xzqB6Ca1VhHhHRQdptdq9Ka2UfmlrqD5pzj5TBhKZgTplt1UUaQofVTYPdTFNnT8Wx8l1nxvWT5gBYCkAEG1j5FyYpQ6Y8FWzBoTM2P236SP1GC20aUdlTlgrDQtNm5TmxtKAJAEIRD6tDHhR.dpAyMQDYWNxg+bUO+.ti6zhpDhHivSMFkHJUGmh4DQTxECJQVAEWFF39Rlw71KcwpddNEyoTQZrMGkiEUFjGECJQVAEWFF3FhKQjVzXaNJGKpLHOJFThHhHhHEvfRD4RDNbcZdM8tObMTxo5DG+31cIPDk.XPIhbIN3gTeVSAvw7kSVYk8Qpd965NuCKpRHhLBCGTJPW3t9AQDQD4MX3fRcnMsxLpChHhHhbbXWuQjKwWdJs29R5Se5sETIDQj2ACJQVAEWc2OzmeXqrNb0N8o0NnTFcriVPkPj0Z+G3fpc5psnxf7nzSPopi+K9WQtf4TIVfp9JtWKYS1kRmf6+UjWvwOwI07ZzXQUzS67mW02mnZKpLHOJCGTplZq2bpDKPjKbQ6tDHh7fNkN51TMVTEIhrIrq2Hxk3KNh1cSY+5KWGkHhnjIFThHWhye9yq40jYlbudiHhRlXPIxVclZztKIHh71zyL9jHyhgCJc9u86Li5fR8UgbG7Tm5TVccPD4xnwL9LjEUFjGkdCJsaweyZV+FLoRgHRMu8RWrpmeziYbVTkPFkVKCF98GvhpDhHiRuAkTbcvgnVh5pS6M5URexHCN9jbpzZYv3duu62hpD2I1E8jcJgFiRG8hoLCsIF.z5H6ZozpW06Y00AQjKiJcQ+tU5DDkrjPIdNqKc4Hp+c+ZkdHEWHDojNFJkHJYiuuBY5RnfRUF1klThrSJ9FZZr8DP.XKacaZdM8tObMThRMox3yiAkHSWBMFk9GGV6kiemn5uzks6RvKSwVuSism.Rm74iaAFjmC6U.xzo2fRM4aFWy52.p4RoYBki4pxyzrO7QHanLHI3FiKkpSqUU8t0stYQUh6iFs3LaQIxzkviJ6kruykLqCKQEaii6OaTHkNA2XboTcZspp2kr6pEUItOZzhyrEkHSmdCJUszC7V+o2EketnI2pwDU5YZ.UUUkweHYW.DISUX4Nnd1Cy75zSqt0m9zaKnRHxZs8c7YpcZ1hRjoKg55MQSX1KvUDVZO0mNlxbVnzCy+Cl0S1uO5K+xuzpqCWG8zpaYzwNZAUBQVKM9de1hRjoyHAkNlbmXBydAXQUdADoAm2XVJRCog24DWBOvKNe4NM+OXVupk6fbsThHRIG4vetRmR1elDQIas1.WaI.XlxchW5MWFdI.L8IOAbqcwGt0NlNxtM1SKMEogzvdijF15WFFuWY+Coc2V7VmUVWD.THnD.vwOwIQO6Q2svRgHxM3S1zGqzop1BKCxCyHAkJF.EBfLU5BlyhVQS95BF6H07ldxy90n7Pa9JecdAGL5dma5BCoLKTjMw+JxEPM0VeytWpnBvVTxNn3emenO+vLnjJzSWuMn6dfVPkPIBs1m95ZW4f4VNgCWmZeXW9d3jkvHAkpE.AgvrWRwvRwqjUtVCWP5LnSKwtAvHL6WDRVUqzIN7gOBdf7tOKrTbWTo6GnT.2P2XPI4bvCo522yfRjkvnKO.6BBgkbqyXrkCg5mCja6ghuwFm4aDQRowr8rZKpLHONizhRhDCKED.E.gVmQWsvjERLHWsPndqFBsDV01S4PwoB.LDoG7.6e+1PoPD4jczu3Hpc5PVTYPdbIRPIQgPieiZt.HKHDdROpFMMzhzuVTVwt2589PNe6BxDTprx9HDNbcHyLyvFJI2uGahSxtKAhR5NzgNjRmhqdvjkokDTJdhcoRnjz8STslv8jrWUqzIN3g9bNfjUvIN9ws6RfHKmJKcHb7IQVlDdKLgnDTHkNgFq.udZkU1GY2k.kf1xV2llWS+56MYAUh6hF+8FCJQVFFThrZJ9Fb67y1gUVGD4Xvtbt4z3CNwfRjkgAkH6fryZRsVqYHkMf63Ns6RfnjJM9fSgrnxfHUCJMM.Hc40dx.3b.Hu3NuzGSK10J24DueRetGM18VN4oxqwjkbboesROuoE60T73mC.yI14JK1C4pg7TnFIiIjRmX+G3fVXYPD4ToxJxsac4ogboLRKJkG.VH.FK.JOtimljGyMtyMcYNOj44Vdr6c.Ud8yO10NEHDpYx.XQwNt34WjLuVSQRsMmXOVjjiyPPVGEa17sscNNkjJb35r6RfHK09OvA4JxM4Xn2fR2I.VIZdHojkoD6WGiNt1EEqFRjfMAfPqIMczz.cyE.2UBb+nDSHkNwt1ICJIkFqNwjCWcm+71cI357IpuCMDxhJCh.f9BJE.BcE0zAvpLo53ZL4qWjX3pEkfOeJ4nVnv5fxuu3W2hKkTC20cdG1cIPJ3vGV0EMQL5wLNKpRbO33ShbRzSPox.v2.kCIoz3.BPnKtjaLJI0bz30HdSFBsvUhDZSLf02nw0IcbQIcLKQsbgT5D5Y5TSTphLxfy3MoTYhcravsfJxhomfRhs9hRgEZIiQIwfH2ID5VOE6T5Xu9QgPnpDs0sDCHoUKRUNZZMmu5WNk.VmRmHTEl9FiLQjC0FKeSpcZEeeChLK5InzbgPPg.PX.WmLIFD4tf1i8IwAycmPSCiYDhuFJMC6HqSHkNwlJmMfGQdU+sMywmD4rn2AyckPnEelLZZWq41TIZbluE+eNlL.1tsTQdakJ2AKqrOBG+DmzpqEGKM1A0A.3V+BkxXqaYKJcpvfAkHafQVd.JGMcp4KJYLFkRTSFM1kfkA80RQSO1i3WKmlFLmYyGoNEaF8M8IboRQTjHQr6RfZA3+9oe6+.GTssqG1sajsPsfRyEMeLEIt1Csn3NuRiQI4Nm38St6sRDGuPxEjQ55ljzYylbGS70uSw87tQHDdBPnK9jNljTqFnDWHkNweY8enEVFta98q1xOFY2NxgUe4cfqp5M5CW++W0NcHKpLHpI3VXBYmpFJrLAr5U8dr62zo689te6tDHJoPiwmHaQIxVvfRjcqDkNA69M8oacqa1cIPTKlFc6VofKK.jMgAkH6lheJQ18a5SWxtq1cIPTKlFc6FaMIx1vfRjcqZvteqEoqckAkH2O1sajSECJQNAknzI9fO7uXgkgyjFamC3F5FCJQtaaYqaic6F4XwfRjSfheZwOXcq0JqChrbbe5C3u7WT8CDwVShrULnD4DTMTn62JqrOh68aDkBKb35v6rh+SEOMXPIxlwfRjSQwJcBM9zlDQtXkuoPnppTba9bcfc6FYyZscW.DEy5fPXoLkdhWY1yBO+TeNjYlbWVWNG5yOL2BSHWq28+5Oq1oKwhJCuhbAPVIg6Snjv8v0fAkHmhZgPXoGUtSt1R+PTvi7vVaE4PLf63NAV5hU776ZmeFfG8uaH2s8efChUup2SoSeL3w9AxFvHfPnmfw95gXekhtEF.6BB+aZIPXHW3JvtdibRJQoS7mUdLL3486K904xn.4J8tuqhgj.XqIIUV.nHH7gJWK.lIDBH4FBIAHzaACAB0cU.n.asZL.FThbRBAgOEYyTVYeD1X4axZqFGB8Lqnl0rdYKnRnDwIN9ws6RvQJb35vqL6Yo1kn33VzCJKH79iyDxL7DboVFbIgkXWuQNMEAg+CTy7du66hGHu6yZqFG.8L9id6ktXzst0ML6WtHyufHCQk0GHOs+y24+RsSubvAwc7BAfaukdS76O.Fx.k+1Tw118UFT898G.2wszGrl0uAEuWSexS.WuuqB..U8UQPjKbQ..b9u86T84Iwxfve1pVuOA6PZ1cAPjDYAg+Sirepo8s+Cfa4l6mkVPNAE9rSE+9hecMutGahSBO2y8rdx+NxIZikuI7f4q9lV7mtk+ombv3GHvMp1rc6dAGeRhJBBsjjrF0vFJ925S2Q+6T6txw5baihraSTKnzTVjFRCU9cogph7CnpyUOdux9GJ8u2KGN7VVpU1cAPjDW..YCfAI2I6z0dc39t2fVZA4Dj00zIrzkn7.5VzN24mg+veX9X+G3P3r+quFMzPTzPznbFCZSJojkiMuY02bme7GeRn6c+FrnJxYXMq8CvhV3eToSua.LcKrbb5JAxLS076O.V1y9KvT5WV31xrUnKsM5Ud3yA7S1aaZ.cosQQe8kFFz02NL9A1ab04bS3uui8H8RyEN7VPjsnD4DkCDFrex5XG+Dnm8n6VW03PLwGex3sUY1uoE+9Cf689DZcid2maB974C.McLP0u9dSLTURxV151v8LnerlWWs0F1y824OvCLT05RxeI3.4VTP.7IRO3nF1PwLFb.auUiRDuxN+F7V+o2U5geV3fGSZLnD4TUBTXoB3EmwL8jiEm8efChe1vFlZcWQRU7Aq..5V25F5R1Mtux00t10lsOy4E6BI4rl09A32L0mUW+aUzntueXWKwZV6GfQ+eLbkN8wfvGThDTDjzsa4EbvXA+raE9R2c98M0bozPtuveP5gKEBK4ANRLnD4TEDx7IoD4UaUIM9gLNNRCaIpicri3G069n3yymOenu2jxm2J7km5z3zm9zJd9u3HGFm+7muYG2Hs52nGy3vpVYy9z0ozzn0jbzsrfMHDjL8+23q7T315PC1S0jjT7AOOlyhVQ7GJLRNKDllBNq2HmpP.nBnvZDxhW7R7jspznF4CgU+9k5ZBKUUUUhpVp0zBXtQ8su80tKAK0ZV6GnVHovfc4lpF0vFpqOjD.vs1EeROTlPnkDq1pqE8fqiRjSVQJchWY1yxytHKNpQ9PXe6+.H+7eP6tTnVnexfGrcWBVpE9GaVWtDuhgCd.8ZSZxGTr2c65rq5HopysS1QadNVbYnaLnD4jEBBspjrV7hWh0UINL2xM2OrwMtA7lyeAvu+.1c4PIf7y+A8TqKX5n0jXWtoAw0sH2t.sW1wWUPKtLzMFThb5JRoS3kaUIQO8SNETYkGEq98KEOSgOmcWNjA77uvKX2kfkhslDIxsMPzSFiQofw86yBBqIBVsPpbtpgCseOIcIDTYrJMqY8xXoKYQVZA4DMpQ9PXTi7gvrJZlXqaa63vG9HXme1NvmroO1xlkbj98py807Tslza8GVHaMIRKN1AychLq2xA.EBg.Rs3kTcKRXHryzWH3mZwMJHTYFv4UWYi0qvgqCG7PeNp67mGG9vGA.McFawvTVG+9CfWpnWFE7HOrcWJVlvgqCCX.CPsuGaVPkVN1iqIM8x7dtGGObOZicUKIUSnz8ixCs43OTEvg18aFsEkJBprTp6fkIDVSdFAD9GhcYqUCYTgfJspz7l277bSwZiHyLy3JAI0pULN9ININ0oDlR7wGrBn4SGdFvR+76O.l7S7j3W7yGmmaYs3OtvEq12mvVSxip6c9Zs6RP2LRPohAvyXVEhEISHL8SsitGjZYJ.JrZcu5U8dXMq8WfQMxGxZqnTP8rGcuI+fbiz8PwGxRjRqEQmolSiScpSo385DG+319lIqRqATRUWc0gcr8s0rqs284lP1YmMF3ccGd18du8efChe6zdd0tD1J+p6X.nW1cQ30o2fRAgJgjjtiD2+tesnKcn0WYJ.Fn8QUbvaEogzvet5uCe6Eujpin+8dxu9J2aQ9ZaqfeeM+OBm8hWFUV6EwxVeEx8IYtcHzxXEo3KF4DUMD1Ofjc0592L0mE4ceA8baEDNIRCYQzq+5ugZm9XfqaRZoZvfR1N8FTpP4NXAicj3WbacSkE.KsWXr7kdTL4.WE.zXZO1ir0+qQGRCnSWEdne0Pwr2bkXMqeCRuhBACJ4FUHD59zLkdhpppR7GW3hwzegoZ8UEQTyrwx2jVqR4EXQkB4.EnKN1wtcynmkGfr.PyVFfm9jm.ly8jsidUBM61DEy8dCf7B1rE0sLgCdekgTTsPkwyvucZOO1xV2lEVNDQxIb35vSL4Io1kTATe1JSo35PajcQmzQROAkBJ8.SexS.E1uNl7qFSfuzihWHu9K2oBZwkBkbTDDZxdYMu4MOqqRHhjkFCfa.1ZR5Em3QN.5InTyF3yO9M4tFGH2VGZ.EL1QJ8vAsgRgRNJPoSr5U8d3s9CKzBKEhn3skstMsF.2yBbssSu3.c2AHgBJ41VUMA.x6l5hzC4VVCnnlKD.JUoS95u1uyyuhcSjc4klwLT6zGCb4.fbYz6XT5JjokYbEt0NJ6eT4xDf6UgPXMXoYpppJwrl0KawkCQjFq.2.b4.fbg7L60aY2lnxMntcOC6dRppgJybw2doKFk7mdGKqXHxqaKaca3W+TOgZWRoPXGRfHWECGTxMMk9jRlUBzf1PYPIOECfcqzI+kO5Dv9OvAsvxgHuovgqSqtbKL3.3lboLbPI2zT5Sp3WrJoTFEn1IKpnYYQkAQdW+wEtXs5xsh.6xMRcN1Y3mmoq2nTV6BByhFYs5U8dXN+NtjAPjYYikuIslkaU.N.tIs4XCRyfRTpfhfJcA2ucZOO1X4ax5pFh7HN9INoVKrjrK2HWOOUPotzAirG.StLi.JLK3..dhIOItjAPTR1Tm5yqmEVxpsjhgHShdBJUsYWDVEwMo23DzFJCxbTMTYVvUUUUhoNUU6d.hHCXN+t4gUup2SsKgyxMJkfmJnDkxqXnxBQIGuRDkbrl09AZMtjNFXWtQoH7Tc81Yu3kkdHG6fGiRXE.U1K39sS64wZV6GXcUCQoX1+ANH9MS8Y05xFA36uRoHLbPo+UjKXF0gk3L0+CROjic5HRIrZgvaRqneyTeVt9JQTBHb35PQEMKsFWROK36slrv+dzAPOAkBE+WTSs0aNUBQIO6BBuYsrpppJwOaXCCgCWmEVRD498bS840y3RhKE.IOrU4b.7Tc8F4on53UppppDO9jlLCKQjNMiWpH71KcwpcIbbIQojXPIJUVAPk0WoUup2Cu17dcqqZHxkZMq8CvqLaUWk6CCNtjLcU8UQr6RvShAknTY0BgvRJt9J8JydVXFuTQVU8PjqyZV6GfQ+eLbstrBAGOMltHW3h1cI3IwfRTptcAMFb2uxrmEmIbDICcNC2lE.Jw7qFhrGLnD4EDB.+R0tfQ+eLbFVhn3r+CbP7yF1vzZFtsbnxB8J0hwtxzAfAkHuhRfvapqHFVhHA5LjztgPWtQlG1clN.LnD4kT.TYlvAv0XIhz4ZkzwfvV.EawCJg3l16UMbPo928q0LpChrJE.UlIbhqwRLrD4EENbc3wmzj0ZsRhyvMaREaSw25x0Ql8dUGK1hRjWSsP3SByvRDEGCDRJHXWBYKznU9b6BY2EfRXPIxKRbaNQwkM.FVh7RzYHI.gVjkgjHOEFTh7ppFBexXFVh7zLPHoeI.VmETRjJhzPZ1cIjTHylTuikdBJ0jO8fadkAcum7qs6RfbV1EzYXoMV9lrrhhHqx9OvAwXFyX0aHoRL+JhjQSd+oJ+tTifRxrI06XomfRMY.6wUFTJEitBK8f4e+boCfRoHtD.TVYejVWJCIYuRI6py+UjKH8PN1IG.65MhzQXI.tNKQoNz45jD.CI43TUD2SKwnlse3iK8PN1.gLnDQBzcXo476lmkTPDYFVyZ+.bq2xMyPRtTUct5s6RnE6SqKcTdnMa2kgtwfRD0HcEV52NsmGy3kJBgCWmkTTDkrTxe5czyFbK.CI4jDJ9uXNKZEnlK4dGmRQZHML+OYu1cYXHFNnzIOKGPzTJMcEV5Ul8rviOoIyvRjqwLdohvu7QmfdtTFRxYoZoGX1atRW4reKRCogo8IUJWqIcL6ndzK8FTpBweiap4xHJAIFVR0kA2Uup2CiYLikKe.jiV3v0gI93SFuxrmklWJ.tWvPRNMgjdf0r9Mfo8IUhidQ2SmBsm5SGOwGtOrl0uA4Nsic7IAvtdiHknqvRkU1Gge1vFFGj2jij3z++sW5h05REWwsCY10DYXUi3ZrBQqY8a.+2l97QwG773SqyY9ixizPZn7yEES+SqAOvKNe0ZnEG85yUBsqzsm5SG2VGZHYWKlNeWU6jdnpsgxfbOD2tSJA.JNvNpppJwn+OFNd049ZX5uvTsnRiH0sl09A32L0mUOCZ6cCtha6zUD.9D4NwbVzJ..fe+AvPF3si928qE9ZaqfeeF6Guu2y03R+iRO+.sOJ7kdzlc7ZtTZ3reuPWAVUje.mo9uGUdlZQIqbs54kNLRECJc1KdYfN395eT+WmOoGpZanLH2Ews6jR.vip1E9am1yiib3OGu97dMjYlYXE0FQxZN+t4ge6zdd8bo6FBeX.G6ZXCA.gV56WBfkozETUUU5V2K3J.N7u+KgZutJqkK5jjmSA.3Y05hd6ktXLfAL.rkstMyuhHRhiehShwL1wq2PRKGLjjaRIPHrjpSzDWjv.Xjvg2ZR.5OnT0w+EaduGM4WIV.2zJAJ4HULD9O1p9FUUUUk3dFzOFu0eXgVSUQDD5psfCYH5Y6HA.XVvE7I4oloD.jKDB45VCLsaH7gNyAtfPR.5uq2pN9un7PaF6Iu965FmR0TayVntXexSF05PiiaoaWsK7W+TOApHzmf4MuWC8rGc2BJMxKJb35vLKZV32W7qqqKGBAjbE+.JRVUCg+MDPXXAjKDdOorfFumTLgQy+YegRJUl52uZk400UPuCznh.vLi+.iZXCEy+98mzKHyRjFRC+no9Gh+Pggv2XQThHKnwf7Vje+AvKUzKiBdjG1zKJxaYKaca3klwLzy90F.Gz1DkPZkNutr.v3i+.G7veAZcu5KFz02rYRlize3PmG+8crm3OzVAWuPnD2E.v6BgOrQP0tvZq8aPoq68wIN4ov.Fv.3.8lZwBGtNT7+m2B+7wOFTYk5ZnPTJDZ8gpM0BinTP5MnzE.PgRO3eeG6wUDV5cNwkvK8VkH8vU.17yTKWHH78RiD.WkZW3N24mgRWWoHyq45Pt29sYE0FkBZKaca3I+UOIVzB+i58o7rP38ua1fzjHRa5MnTsP3GDjszS722wdPUs45PmttqE8ncMe8UvNsm5SGuwNNM9cK4OK2oWN.1hEWRTpopAvB.v8.gAnnhhu0k9Q+neD570e8VP4QoBBGtN7+7+0qhG8QlfdaEocCf+cvOPHQsHFYwPp.nxZ3..PdAGLFb+uQDHq1gN2tVoqA6cjFRCU9cMVFctsQQ1sIwBbUykRCU8cog89UeK17dOpVa2J9AaFZJ4qP.7F58hey4u.7+3g+4r63HUYfEORQKGBeuHmUaD0BYzUMxcA8Mp5SpJXriT0yexy90FcOn62CY5JQhRRxE5XVwIJ+7ePLke0ShQMxGxTKJx8YKacaXwKdw5YKHQDmUaDYyxBBgkh5heTRx9uTHRAEAC78lO1DmTzOcK+ynDcriehnu3LloQeus0ANSdIxQHKH7C.pE1enGi7nZz3ZOAQVkbgA+vEu3LlYzic7SX2+rZxFTasgi9lyeAQ86OfQduMwsYGhHGlrPiMwaKMzT0PX1CI8gddtx87JJtGAgFCvVhr.EAC9+Kd049ZQqs1v18O6lrHIP.I1JRdCKDM+e2WoNtln.3n.HuX+9wHy8dxwNW.MtG5sNT55VH.tlXm+nJ753Y1JCBpyG4XoUEQNC4.8+A.hBfn98GH5aN+Ev.SovV86WZz7y+ASjObYPK46ZI61BQigU.ZL3yBU4Zj5nP9PMkE6gdtG5oNJCMFNSzz.v1gPXLwZwyDJhHJwHtv+w.SdXIX.oZgPqSRdGxEfoLIGSqPNSCBe+y0D2wBD6XSVm2C8TG5okgXPIhHcQbr9Y3tpligI2qDbLHEe2rkiU7MmjihbAT1NZrkfT5ZhmXnnoE2wlF.NGZL7ThDTRZcTVr64BQisfjTLnDQjgjCD9AfITfINK4bGDmEaIX.oPfcylWlz.JhsNzXjbMpM9h.DBwr839ZoAVzyXTRq53Zj49bTzz.Zd9wnDQThIHL33WR7wnGy3ht52uT1sbNPeTYebzmovmKQBGINNjJvz+NOxoSt.LSVlqQqkqcwAt8chFGeQwOVhzSKJoUcH0cBgwFU7WKaQIhnVjfHAWqx76OPzWctuVz8s+CX24C7zN1wOQz2b9KHQF+QhOpELfD0n3CvbmPnqs1NZ53MROAktlXO24nv0ajtdSo5PNR61OFThHJon.XvA7c7OF8XFWzks7UvVYxBs52uzVRqGIFPpHvo6O0TRCvHWHE8DTR75NWrGSSlyYjwnjb0wQgPPr3I1hRbVuAuM8tB..LA2IQTPTQDYJJ.IXWxI93YJ74XWyYR9nx93VxXOJ9tXqPv.Rj7jK.iXHkiF62qmwnDPic4VTz7VBxniQI4pi4flOFj1NZZW74ZGiRRK3sCg+PKpLYtl36eyIil+G93SUpzaPnzegEEMcjzGOi9ZsRz32PnzqEPiCLMoOllBOWoCPMhLSAQKLvDXnojhjT3Hw.REXleSCQTxS7ABtFHDTR5nkWofKhITkNsCiez0K87xoLnchRi9ZcmnoMymZ0gXPIkH2qqXPLhrJha3tszeHczGahSJ5xV9J3XZRCG63m3JcqVRHbTTHD3ka4HD4xnTH.QpETRbsXvH2e4nmfRIxqU7OmjYPI.goEY7srFQVEw0gopQRHzT94+fQewYLynq98K0yuFMUasgi9Qk8wQe049ZQG8XFWxHXTTHL9iJAbcPhHWmVKywB.gt2ZU57dTIDZEpUBf4BfcjbJsj1qkViH+VhUAfuABAkJ2DecHRJwA9aQPn0IJ..COQuYkU1GgxJ6itxWme9OHt6AMHbi+ndiAdW2Atkatesnh0I63m3jXaa+yvQNxQvN191vpW06kLu86F.EiF2SLIhboj9oej1cRZMFklFZ53FR532QoOkkzWC8LXtzyqU7c8l3zgTq5POiQI4ZMJNB9ImhbfvfBtZjbZEjl0Ucu5besnq98K00tPWtu8efneTYebz2b9KH5iMwIkr5JM4Z8nhgP2jRD4xkVreMJ.lNDZkFww6ycE20I1sa4qi6Y.HDLYLwt9xkb+URYPnEilhdJbMdsh2hh6dpVcLsX2qzj4bp8bOWrWioaf5lHyVtPnUlFA.5kY8hje9OH5QO6IFvcHL2Otq67N..vft6AZVujZZ+G3f37mOBNzmeXDIRDryOaGnt5pKY2RQREFBsZj3ChnTDx00aiEBgklCRre3ekwddiAJuWujrnzqkVgxRVxCBcsG61MxoYWPn0kJDlXnoqzccKcwxd9QOlwgLxHC.fqDlB.vmOenu2TeL7q2WdpSiSe5Seku9KNxgw4O+4A.vaqPMXhX3Hh7Pj1sRiQxwTavbuPz7M.OwE0JwikrFL2szWqj4f4VbPhytcibSxEBiqoDZ0+lOP0Pna03rViHOF4BPLmXG+Ng5iQoIKy4ktNLoza5DO8DTRuuVpETRo5fqiRjWSVPnklJAlz3ZJE3QsPn0hJDbFqQjmjRiGGhHumbfvBao3idYekhsILDVmiDerKarVHhb.XPIhHkjED5ptfwdjCRsBOEFBAg1EZLTT01X8PD4.wfRDQFgX3obgPvIwe0IGfRLPT0wdDJteOQDoJFThHJYIWzXPprfP.pbhctbf4DlRLDDfv3IR72GRxuRDQIDFThHxNHFlJQDJIVGDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQDQj0IM6t.Hhb7xI1C2tpi8fH8JK.jqcWDFT0feedRkUFTRsugK2Xm2tjCRM9AAFQN.nW1cQPDQjqPX.rKC9b1E.pMAuN89bMcIZPobPiAKBF2wCJ4Z3OHlHhHhZINFDZkrZQig0BAKp0yzSPoQ.g.P4BF9gHhHhbVp.BAl1EDBPYzV9RUJETJK.TXrGYlLeAIhHhHxDEFBAlVWrGsntvStfR4F6FyVNhHhHhb6p..kfDLzjzfR4BgTXrUjHhHhnTIggPXohfAFaSRCJsK.b6IsRhHhHhHmmkCcFXJ9fREAfYZJkC4HUvXGocWBDQjq1IO6WixCsYK60yo+91UrscipppR6tLzqv.nXHj+QQhAkxBBopLktbaTCanniWc6k8bA5RVnCsoU559zkNzZz41ouqUO5PqAtw10PR69QDQDQMWMWJMb1uuw1lo9KCTY3KdkutpuJBhbAgu1FBasaHLC+qVtSJV0E.fko0cJufCFCt+2H5+0c0nCsBHP6iBeoGMYUnDQDQDA.fHMjFp76R6Jgpp+RWFUdlZMqfTggvRgTyVZADCJo4XSZgSeJX3cI8jcgQDQDQjgHFhZum6hnpuJBNzwOUxnKPkMrTZPna29F0dlLjDQDQD4jUykRCa8bWF68Tgwa8md2D81DFBKt1WYYDHMHzubqUomQdAGLVwvukD8EjHhHhHKUjFRCao1FP4e9YPIqTwHNJoTHjMB..sB.iGMcOZqIl13GJ5qOqbuykHhHhnDWaSCHP6SC40CeXB2+OFY4+lveeG6QuO89hF2VTP5Pnq2TzslUqaI0JQDQDQ1lraSTTX+5H9+MmmBiZXCUuOsBE+MoCgUiaEwoOOQDQD41cisqAL+62Od4e8uTOW9vQrFRhiPahHhHxyXxAtJrvoOE8boAADBJo5FDWjF33ShHhHhRcL7tjNd5GY7ZcY4BHDTpYKtRwauQXPIhHhHJ0xieqcRqKIHfNZQo89UeaxohHhHhHxgH61DUW6cdZ1hRaduGMYUSDQDQD4Xz+tesZdMoCfPPXknTVkGZyn7yw8yMhHhHx6QbVusN0tnR9aGvBJEhHhHhrN68jesZmdW.5LnT4g1LdmSbojTYQDQDQj8JRCoo01aR0.MMnzwT6pm5quDbzKxkcIhHhHx8qzu7605R1Efvd8lnZQbaBbx43+P6wCzmtf1xUL.hHhHxkJRCoge8epLTaseiRWRX.7D.Mck4tDHrIvonxCsYLsOoRtHTRDQDQtV+4p+NTUUUp1kbkgjTqjbhcgXInTxAO7WfursWKFRNchsrDQDQD4p7o0kNdh4tHstrmEwFiRRCJUC.RCwVMJUxAO7WfO7HeM9w2ZePWZKW5.HhHhHmuidwzwTV5eUstbCPXLaWn3WHMnDfv5pz8BfbT6tTaseC9O2z1Pq6UeQNYcUvmb2IhHhHhb.hzPZX1atR7O9maSqK8YQbKF2J04YYE6h5kdKfm9QFOFTuxB2ZGSGY2F1JSDQDQjyPMWRHjzZV+Fz5R2MhsY3JRsQYTtPn0kxznETdAGL5aO6F7ec9fu11J32WqA.PmaaTFhhHhHhLcG8hoipp+xXemIBlyhVgdeZ2KDx9bEZMbrS3vRF0nF1PQGu512ji46pZG7ec9j856emZml2yaqCMjTpMhHhHRa6odisdKVUje.Q99KK64p+RWFUdlZMbMnwhHoZ98HtwljH8Lu0xABSStaOQekS032e.LjAx+5fHhHmuVPvAujcCgIxVyRlo2I3eV.nH.7LIsRhHhHhH6WXHDRZWxcR8NW0t..1.DVPJG..xNYTYDQDQDYiTMjDf9CJIpZ.r.HzDUcEZrDBPDQDQjC0w.vPgJgj.zeWuojbfv9C2H.vPZg2KhHhHhLagAPwwdn4nEOYuIjDL1ibh8fgmHhHhH61wfvr3ODD1aa0MqZ2ZKXreMGzX20kEZ5h5DCUQDQDQZ4XH19vVb1EZZqCUKZrK0B0Rdwbpaqs4flN9mj90.BgrxRxwxBbYLfHhHRI6F5n6lZABYvqO9.MxQZ.HKmSMnjYHGnuAetduNmlpQySXmLX6eSJQDQDQDQDQjCiWpEkHhTlzwLnWgVM6OQjGmWOnTPS59J23mxt4DqI8JG.zK6tHHhRXgg2LPZnD34DLIWCloPxbLoe3CW+GFwNCJozmfMnAt13wYMGQDQjyVEw86CE6WEGKrgjdwNAIqfRhAYhOPS7sfAmMZDQDQjVDa8wcAgInztfMGfJQBJkCDZ0mbi8fsjCQDQDYl1MZbAiLDrvYisdCJED.ED6W4XEgHhHhrS6F.qK1CScLPoUPof.nHvVMhHhHhblNFDBLULLg0SP0BJUL.dlj8KHQDQDQljJfvd4VIIqanRAkJA.OZx5EgnVB+9CfgLPNW.RUDnKYgNzlV0ji8uhbATSs0aSUDYEpXa6FUUUk1cYPdGGCB8HVIszajbAkbksjTdAGL5dmuVce8x8l0sT9ZaqfeesNodOSl5baihraST6tLHhHOgidwzQ8+f8VC2VGZvRe81S8oekee8WFnxvWD.M8CCUxJWqUVRs3.SRCJMB.XZ+IPZKCHWXk92o1I6y0p+GahHhHxbDogzPkeWZ3rW7x3L0+CXum7qwIO6WixCsYy5krB.THRfA9c7AkxJ1MvvypMwVyI6r5.tdeWUSZYE1JFDQDQjdHFfZum6hXum7qMiVeZVPnElzs3CJUD.loddRO8iLdz+tkI76q0rkdHhHhHSydpOcrky7sX2UcZrl0ugjwsrBHzCZ5ZsXJ9fRUCMZMom9QFOd7asSrEhHhHhHKWMWJMr0ycYrw8crVZnocCgvRUq0EJFTRywlzzm7DPg8qisjhhHhHhnjhZtTZ3iq46we8yNbhN1lBCg0KRUG2RhAkJAprb.32e.T1SOT3Kc1RRDQDQjyxmVW5nz8epDYLMcLHrcroX2vIFTpV.joRWDaMIhHhHxoaO0mN9y6wvAl1MDBKIqVE6jpttI8D+2+2Pf1mH6etDQDQDYM5Raih75gO7.+j6Fmtg1iJq9X54okM.tF.H6fdJcHz+bpxeGRtKLiDQDQDYVtsNz.VvO6Vwzm7Dz6S4YfB4gRG.4n0y9FaGWB.HhHhH2CeoGEE1uNhULimPuOkhk6foCU5WN.fQMrgZvRiHhHhHmg75TZ5MrzsCfBjdvzgvJxsh5c2ttDpvHhHhHxIv.gkJR5ARGBInTz066pRrphHhHhHGh75TZ3oejwq0k0KHoUkRW9qqQAxT9MoVhHhHhbSJ716D76OfVWVAw+EZFThS3MhHhHJUfuzihmdj2mVW1PPbSzMsCJ05VVQQDQDQjSwvug1pmKaDh+FMCJwkF.hHhHJUguzipmwpTAh+FMCJQDQDQTpjA0KUmv+.BSzsr.XPIhHhHxiYPYoq3OAAXPIhHhHxiwW5QQAicjZcY4BvfRDQDQjGT+690p0kvfRDQDQj2T+6jlqSj4.HDTZ2pcU0bozRNUDQDQDQNDctsQ05Rtc.gfR0p1Uc1umAkHhHhnTKY2FMCJA.cDThHhHhnTQiZXCUqKI2zAvtT6J164tXRqfHhHhHxoniWc605RxJc.TsZWQ8W5xIq5gHhHhHWEMCJU4YXOyQDQDQdSZ10aUrMUmTbDQDQDkxRbvbeLktfpppRtDAPDQDQdRhK3jp1pR667MXAkBQDQDQNJUqufRmIhETKDQDQD4nbkfRgT6p19gOt4WJDQDQDYgN4Y+ZMuFcETp7PaFG8hbagiHhHhRcTdnMq1o2MPS2TbKUsqde09CIgRhHhHhH6mNlnZ0BzzfRgT6p239TbhwQDQDQjqhN1KaCAXffRqY8afKS.DQDQTJAcrEsUMPSCJsKnx5oD.vGbhuqEUTDQDQD4Dr2Sp4.4tZflFTB.Xcp8LV15qHwqHhHhHhbHzwNORHflGTpD0dFUUUkn7yEMgKJhHhHhraG8hoipppR0tjqzxPRCJoY2uU9melDuxHhHhHxlskyp43SJj3uQtEGIU69sRV4ZwdpmqoRDQDQj6z+3vmTqKIj3uQtDOEq0y9OumSYrJhHhHhHGfZtTZXMqeCZcYgD+MxETpZDWeyIG1pRDQDQjazVO2k05RZxBvsRocJQq6BaUIhHhHxsYUa4fZcIMYHHoVPIUGT2rUkHhHhH2j8Te5Zs+tAnyfR..Eo0cZga4nZWUDQDQD4.roiGVqKoTDaOdSjZAkJAZzpRqY8a.uyItjdpMhHhHhrM0bozvbVzJz5xJQ5AzpuyJPq63Te8kvEgRhHhHxQ6iq4605RBCYVhjzJnTHHYzeKmIL6EvvRDQDQjiTjFRCu0Z2jVWVIxcvVoi6+F.vuB.WkZWz6u4siuJyd.+cNCzoVyPSDQDQjyvpN4kv68W+XstreNjL9j..RSmuFAAvmn2BZTCan3At0dg6tSsBY2FFZhHhHhrdQZHMT5W98Xpu9Rz5RWNTX3Fo2fRH1MXYF35A.PdAGLFb+uQz+q6pg+1GkAmHhHhHS0QuX53i+xuEaduGUOKG...2Kha03NdFInDPBFVJd4Ebvnu8rav+04C8uSsCctsL7DQDQDYL0bozvY+dgXLUE4GPju+xXum7qQEaa2npppzH2pJfPOmIKiFTBHIDVR1a5XGI7cUsC9uNevWaaE76q0..HP6iBeoyfTDQDQoRjtnUKF1QzdO4W2jym.AfzKEaMIfDKnD.vHfvnCOyD74mPJXrirIec+690p301+N0NytbzDasLhHhRlrpcDi8dtKp6qspuJBhbAkudSLfSxvrfFKv1IZPI.fbfPXogzBtGTRvnF1PQGu51a2kQBI6r5.tdepNgJIC5eE4BnlZq2tKCJEv4+1uSO6x5D4Vo3.3NdsjfRhFA.JF.8JIbuHhHhHxr8r++2d2O21fvfggwet1aLBbr2Ji.qPGwtAcDnaP5sbjtAIaPN74n5hhZHJlXP44mjkQ7GCGe4CvPjc4ply7nz0rOcx9F3EfWKvXJIIIUZe.7NwbD4rThJJMUCQUl5SMqzjjjjpkuH90j7Iv3sdvKQPooZA5HBM0guSSRRRp79gHHztr9g6cPeDAktj1TqOa4NdveEcRRRZS3HQvGR8GHBCk2VD0Jnz+oi3w20lZvemHnrhTRRRaGmqzyTiWX8CYKefeCGUMqwfR2pyAqlpIss0j9ZeA7DqA3sZeQHswjeW7ZcajxWUkgYteqh.MKkSvJHxXeBMwB1A....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-3",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 34.0, 130.0, 189.225220000000007, 142.080369965870318 ],
					"pic" : "z_playerinterfacenovol.png",
					"presentation" : 1,
					"presentation_rect" : [ 23.0, 4.0, 265.720703000000015, 199.517251399317416 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"bgcolor" : [ 0.811764705882353, 0.811764705882353, 0.811764705882353, 1.0 ],
					"hidden" : 1,
					"id" : "obj-142",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 34.0, 486.0, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 360.0, 220.0 ],
					"proportion" : 0.5,
					"prototypename" : "referencepanel_360x220"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 3 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 5 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-187", 0 ],
					"source" : [ "obj-107", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 4 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-482", 0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 8 ],
					"source" : [ "obj-112", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 7 ],
					"source" : [ "obj-112", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 6 ],
					"source" : [ "obj-112", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 5 ],
					"source" : [ "obj-112", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 4 ],
					"source" : [ "obj-112", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 3 ],
					"source" : [ "obj-112", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 2 ],
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 1 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"order" : 1,
					"source" : [ "obj-113", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 1 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"order" : 0,
					"source" : [ "obj-113", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-117", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 1 ],
					"order" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"order" : 1,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"source" : [ "obj-125", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-126", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 7 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"midpoints" : [ 2212.627685999999812, 1375.700012000000015, 2152.04253400000016, 1375.700012000000015, 2152.04253400000016, 1187.700012000000015, 2174.627685999999812, 1187.700012000000015 ],
					"order" : 2,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"order" : 1,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"order" : 0,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 1 ],
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 2 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-194", 0 ],
					"source" : [ "obj-141", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-141", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-144", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 0 ],
					"order" : 1,
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"order" : 0,
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"order" : 1,
					"source" : [ "obj-149", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-399", 0 ],
					"order" : 0,
					"source" : [ "obj-149", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 0,
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"order" : 1,
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"order" : 2,
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"order" : 1,
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"order" : 2,
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 1 ],
					"midpoints" : [ 3884.5, 153.200005000000004, 3942.0, 153.200005000000004, 3942.0, 119.200005000000004, 4268.5, 119.200005000000004 ],
					"order" : 0,
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"order" : 0,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"order" : 1,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-326", 0 ],
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-168", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 1 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-170", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 0 ],
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-176", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-178", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"order" : 1,
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 1 ],
					"order" : 0,
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"order" : 0,
					"source" : [ "obj-178", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-454", 0 ],
					"order" : 1,
					"source" : [ "obj-178", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 4330.5, 270.0, 4308.0, 270.0, 4308.0, 105.0, 4323.0, 105.0, 4323.0, 33.0, 4244.5, 33.0 ],
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 2 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 3 ],
					"source" : [ "obj-181", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 0,
					"source" : [ "obj-183", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 1 ],
					"midpoints" : [ 2305.220946999999796, 1125.0, 2103.0, 1125.0, 2103.0, 1411.0, 2221.58520499999986, 1411.0 ],
					"order" : 1,
					"source" : [ "obj-183", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 1 ],
					"midpoints" : [ 2297.970946999999796, 1125.0, 2103.0, 1125.0 ],
					"order" : 1,
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 1 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-182", 0 ],
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"order" : 0,
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"order" : 1,
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 3 ],
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 8 ],
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 0 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 1,
					"source" : [ "obj-204", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"order" : 0,
					"source" : [ "obj-204", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"order" : 0,
					"source" : [ "obj-204", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 8 ],
					"order" : 0,
					"source" : [ "obj-204", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 7 ],
					"order" : 0,
					"source" : [ "obj-204", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 5 ],
					"order" : 1,
					"source" : [ "obj-204", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 4 ],
					"order" : 1,
					"source" : [ "obj-204", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 3 ],
					"order" : 1,
					"source" : [ "obj-204", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 2 ],
					"order" : 1,
					"source" : [ "obj-204", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 1 ],
					"order" : 1,
					"source" : [ "obj-204", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"order" : 0,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"order" : 0,
					"source" : [ "obj-204", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"order" : 0,
					"source" : [ "obj-204", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"order" : 1,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"order" : 0,
					"source" : [ "obj-204", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"order" : 1,
					"source" : [ "obj-204", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-204", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"order" : 1,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"order" : 0,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"source" : [ "obj-220", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-258", 0 ],
					"source" : [ "obj-221", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"order" : 0,
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-254", 0 ],
					"order" : 1,
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"order" : 0,
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-442", 1 ],
					"order" : 1,
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-445", 1 ],
					"order" : 2,
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"order" : 0,
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-442", 1 ],
					"order" : 1,
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-445", 1 ],
					"order" : 2,
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-228", 0 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"source" : [ "obj-232", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"source" : [ "obj-242", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-244", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"order" : 0,
					"source" : [ "obj-244", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-244", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"order" : 1,
					"source" : [ "obj-244", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"order" : 1,
					"source" : [ "obj-244", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-243", 0 ],
					"source" : [ "obj-244", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"source" : [ "obj-244", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-318", 0 ],
					"order" : 0,
					"source" : [ "obj-244", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-244", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"source" : [ "obj-244", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"source" : [ "obj-244", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-244", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-270", 0 ],
					"source" : [ "obj-247", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 4 ],
					"source" : [ "obj-249", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"source" : [ "obj-254", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"source" : [ "obj-257", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 1 ],
					"source" : [ "obj-263", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 1 ],
					"source" : [ "obj-264", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 1 ],
					"source" : [ "obj-265", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 1 ],
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 1 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"source" : [ "obj-273", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-179", 0 ],
					"midpoints" : [ 4167.5, 277.0, 4249.0, 277.0, 4249.0, 235.0, 4330.5, 235.0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"source" : [ "obj-280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-284", 0 ],
					"source" : [ "obj-282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"source" : [ "obj-284", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 0 ],
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"source" : [ "obj-289", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-289", 0 ],
					"source" : [ "obj-290", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 6 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-371", 0 ],
					"source" : [ "obj-323", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-326", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-326", 1 ],
					"source" : [ "obj-327", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-34", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"midpoints" : [ 2484.970946999999796, 585.0, 2324.556152000000111, 585.0 ],
					"order" : 0,
					"source" : [ "obj-371", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 0 ],
					"order" : 1,
					"source" : [ "obj-371", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 0 ],
					"source" : [ "obj-371", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"source" : [ "obj-371", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"order" : 0,
					"source" : [ "obj-372", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"order" : 1,
					"source" : [ "obj-372", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-442", 0 ],
					"source" : [ "obj-372", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-372", 0 ],
					"source" : [ "obj-378", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 0 ],
					"source" : [ "obj-395", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-395", 0 ],
					"source" : [ "obj-396", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-465", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-429", 0 ],
					"order" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"order" : 1,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-421", 0 ],
					"order" : 1,
					"source" : [ "obj-420", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"order" : 0,
					"source" : [ "obj-420", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-421", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-422", 0 ],
					"source" : [ "obj-429", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-455", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-445", 0 ],
					"source" : [ "obj-440", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 1 ],
					"source" : [ "obj-442", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-442", 0 ],
					"source" : [ "obj-445", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 1 ],
					"order" : 1,
					"source" : [ "obj-448", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"order" : 0,
					"source" : [ "obj-448", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"source" : [ "obj-448", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-410", 0 ],
					"source" : [ "obj-448", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-450", 0 ],
					"source" : [ "obj-448", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-412", 1 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-453", 0 ],
					"source" : [ "obj-450", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-454", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-486", 0 ],
					"source" : [ "obj-456", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-460", 0 ],
					"source" : [ "obj-459", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"order" : 1,
					"source" : [ "obj-460", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-463", 1 ],
					"order" : 0,
					"source" : [ "obj-460", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-460", 4 ],
					"source" : [ "obj-461", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-448", 0 ],
					"source" : [ "obj-465", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-471", 0 ],
					"source" : [ "obj-466", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-457", 0 ],
					"source" : [ "obj-482", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-482", 0 ],
					"source" : [ "obj-486", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 3675.5, 199.0, 3540.5, 199.0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 6 ],
					"order" : 0,
					"source" : [ "obj-57", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"order" : 1,
					"source" : [ "obj-57", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-420", 0 ],
					"order" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"order" : 1,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"midpoints" : [ 3540.5, 378.0, 3777.0, 378.0, 3777.0, 246.0, 3799.5, 246.0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-247", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-420", 0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"source" : [ "obj-91", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"order" : 1,
					"source" : [ "obj-99", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"order" : 0,
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-183" : [ "live.gain~", "live.gain~", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "volslider.maxpat",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "sliderGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
					"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"bgcolor" : [ 0.878431, 0.878431, 0.858824, 0.0 ]
	}

}
