{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 44.0, 60.0, 218.0, 313.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 0,
		"enablevscroll" : 0,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 582.0, 97.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 112.0, 293.0, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"allwindowsactive" : 0,
						"appicon_mac" : "",
						"appicon_win" : "",
						"audiosupport" : 1,
						"bundleidentifier" : "com.mycompany.myprogram",
						"cantclosetoplevelpatchers" : 1,
						"cefsupport" : 1,
						"copysupport" : 1,
						"database" : 0,
						"extensions" : 1,
						"gensupport" : 1,
						"midisupport" : 1,
						"noloadbangdefeating" : 0,
						"overdrive" : 0,
						"preffilename" : "Max 8 Preferences",
						"searchformissingfiles" : 1,
						"statusvisible" : 0,
						"usesearchpath" : 0
					}
,
					"text" : "standalone"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 530.0, 1062.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 563.0, 1031.0, 65.0, 22.0 ],
					"text" : "route Click"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-89",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "audiobuttonlogo.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 240.0, 781.0, 53.0, 53.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.0, 12.965813000000001, 60.5, 58.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-85",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 549.0, 201.0, 47.0, 45.307994999999998 ],
					"presentation" : 1,
					"presentation_rect" : [ 11.25, 81.860709999999997, 192.5, 118.891289 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 54610, "png", "IBkSG0fBZn....PCIgDQRA..GnL..Pf1HX....fNGy9n....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6c+GjcWWeu3+k8GCAX4PBfC4rbHr6B6xlqVS1kD71VCrIvZBgnIi2D2Kh+.S9FwLkKPAo5zaoJJxEkJZneq7qF.aghojgRPDCFEhhcZk.Iv2qJgDf3OVxhybAjso.Nsyb+9GqA4G4G6d974bde9bNOdLSlJD9758SsYbl1m66WueKA.........ieCTmNme5u4WPt6sj5.........PKgIGQLyTGh8hN9M+pQRGQ9loNhHNtbbdMp9d6k+da507u9QiH9U6k+9zBRQ4.......vdyLiwJ1rQRiZQqCj5.rWzQzZTLJjU+r32dy0es+ZSoINTunnb......n3qQs7vFwhV6HZ7t4nQDwol5...uIuXL1sPeSwXkm+n+leQS.EkC.....TeoPywuNhFyBMmYDwgm5P..Px78hwJOeOkn+q1e+CSiIEkC....v9mxCG+FH0AXunivZmE..n15whwJLeSQD2URSBiaJJG...flIcDJOb7pQr72IGQLiTGB...fLZ8wXEleWgaadCKEkC...oz.oN.6EMpkG1Hthd8NZB....6eJMuAkhxA..FuZDKOrivMGc7pivZmE....fTZ8QD2RX8r2PPQ4..Mm5HTd33UiX4uV6r......Mu9YwXEl+UB2x7jQQ4.zXXfTGf8hNhFuhVs1YA......Zl70hH9LQD+zzFiVOJJGl3FH0AXuninwqPyHbKMA.......FOTXdclhxa98Fu8kcDu4BUGu2PS2XR........n1Qg40IJJu3ZOka+ZK4dfWyumaLK........TLs5XrBy8FlWinn7Fa6oD78TH9.gRvA.......fVAuXLVY4ekDmilRJJuwQGwusT7A9M+0GW5hC........PCfGKh3bhHdzDmilJJJOcFH9skhOPDwgmvr.........zX6xhwtg4jCTTd8yqsT7Emzj.........TD41kmSTTds0R9M+ZfvZTG........H6dwHhKLh3VRbNJzTTd9aIuleYcpC........TK70hwJL+Wk5fTDon77w.wXq3.kiC........TuXUrWkTTd0qiXr+P24DVq5.........owKFicgd2ThyQghhxm3FHFqb7ORZiA.........upOZ3cKebSQ4iemyu4WmZZiA.........6UqNF6cKmC.EkefcNQDelv5UG........nw2WKFqiS1OTT9914DJHG........n3QY4G.JJ+MafXrc2uBxA........JpTV99wuSpCPCjYFQroHhGHTRN........Pw1GIF6BBydwuapCPCfIGQbkQD2bDQGoMJ.........jalYDQmQD2UpCRilV8hxWRL1sH+TSbN.........nVPY46EspEkO4HhudDwmNhXRINK.........TKMyHhWLh3eM0AoQQqXQ464VjOiDmC.........pWVPDwOKh3QScPZD7VRc.pilbL1iU+hSbN.........HEdwHhABkk2xTT9Liw149GWpCB.........IzKFQzQDwuJw4Ho9cRc.pCtvHhsFJIG........fCOF6optkVy7aT9jiHttHhOYpCB.........MPlZDwThH1PpCRpzrVTdGwX+uTmehyA.........Mh9uFQ7XQDaK0AIEZFeixmYL1pB3vSbN.........nQ1KFi0u5OMw4ntqY6MJ+bhwdOxURN.........6eGdDwsj5PjBMSqd8yIh3lScH.........n.oiXraV9+ZhyQcUyxpW+VhH9HoND.........T.0xsB1aFV852Rnjb.........pVsbqf8h9pW+VBkjC........PV0QDwOKh3QSbNpKJxqd8aITRN.........4kWLFqv7eUhyQMWQ8FkeKgRxA........HOMoHhCNhXCoNH0ZEwaT9sDJIG........fZk9hl7UvdQqn7KLh3Km5P........PygN6rqXty6zRcLdcZqsCKN9t6tFL21hd5ombet6Q6kO5nRkJYZFCO7vwtF4WNg+tc+u8uEaeG630826w15Vd0+02zZtwLkqVPeuHhARcHpkJREkeNQD2bpCA.......jNKeEqL0Q3MYpkaON5oN0TGi2j96u+TGg2j7nHUHK11iusXzc+uGaYKaI9228timbGaOdf6+6F6bmOcpiVin4FQroTGhZkhRQ4CDQ7.oND.......ieKcYCEkJUJ0w30oVcKMyJEZBPZM7vCGOxi7nwi9naM17O7eM13FuuTGoFA+rHhNRcHpUJBEkOyXreREN7DmC.....flVCN37iicZSK0w3MYF803UdX4oVNJ2d4TGiWmRscnQuSu2TGC.flF6o37evC98iqY0WcpiSJ8QiHtkTGhZgF8hxmbL1iD+wk5f.....PiDqc1wmZ86vY0p2Sr6FtaYK..vd2niNZ7.Ov2O9GW6sGq6NVapiS8VS6sJuQun7MEQbpoND....PVXsyN90S2cGscXGVpiwqi0NK...rGOzlej3acu2abEW9kk5nTO0TdqxajKJ+qDQbAoND...PqkN6rqXty6zRcLdSr1YG+N4YeRoNB....zja3gGNVyZt4VkByaJuU4MpEkujHh+oTGB..fFeV6rie82eiWQqV6r.....EYa6w2Vr5Uu53lVyMl5nTq0zcqxaDKJuiXr2k7COw4..fb1fCN+3Xm1zRcLdSZDuklMhqc1RscnQuSu2TGC.....fFN25sc6wkeYe5Xm67oScTpUZ5tU4MhEk+nQDyH0g.fVcV6rieV6r.......CO7vwm+x+7My2t74FQroTGh7RiVQ4dWxqQV5xF50sRKyi0A5u7Ye13YGYW60euewO+mGabi2WllOSLV6rieV6r.......PsxW5K8ki+7O0kj5XTK78hHFH0gHuzHUT9.QDOPpCQQzdVis64VVtmR.aTKd6g17ij5HjIsW9niJUpj5X........PCp0u96Id+KcIoNF0BcFQ7SScHxCMJEkO4XrUt9wk5fznaoKannmSr2Xlyrunb6ksVcA.......fFPqe82S7IujKpY6cK+qEQbNoND4gFkhxsx02GV5xFJ5+jlUzWe8EyZV82PdCwA........dydnM+Hwb9idmoNF4oWLhniHheUhyQl0HTT9.gUt9qyRW1PwYblKJF3TmiU7M........Tf0DtF1+nQD2RpCQVk5hxsx0+MFbv4GKZwKIVzYdFJGG........Zhbq21sGq3b9PoNF4kGKhXloNDY0uWhO+OSzhWR9RW1Pw6enyJV7hWTpiB........PMvG7rOq3odxmLthK+xRcTxCyHFa8q+SSaLxle2Dd1yLh3lS34mTKeEqLtl+lqMN+y+7hd6smTGG........fZn96uuXm67mF+jexON0QIO7VhH1PpCQVjxUu9lhHN0Dd9IwfCN+3u7x9rwIO6SJ0QA........nNZaO91hY7Nd6oNF4geVL1sJuv52IQm64DsXkj2YmcEq4V96i64d+lJIG........ZA06z6MVys72m5XjGNtHhkj5PjEoX0qO4Xrqg+jRvYmDm+EbQwW8Z+pw65c8Gm5n.........jPui2wePrqcMRr0stkTGkr5WGQbWoNDUqTr50+LQDe5Dbt0cc1YWwW85tgXdyafDmD........fFEMIqf8WLF6RRWHUuuQ4cDQbKQKvsIe4qXkwe6ZVSLiY7NRcT.........ZfbTu0iJNjC8vhu62YioNJYwjhHdrHhsk5fTMp2uQ4elHhCuNel0cWwUdUw0dcWaToRkTGE........fFPqbkqH5rytRcLxpB66Td87Fk2QDwMWGOu5tN6rq3F9au43i9Q+HoNJ.........MvNnC5fhIeDGUb220+TpiRVzYDwUl5PTMpmuQ42RDQSaCxc1YWwcs96N5c58l5n.........TPrnEdlwF238k5XjE8EQ7noNDST0qUudGQSbI4KcYCEOzC+vJIG........XBYkm6pRcDxpyI0AnZTuJJ+yTmNm5tktrghq85t1nToRoNJ.........ELKdwKJFbv4m5XjEEx2o75QQ4cDMo2lbkjC........jUKZwExtl2iiKFqS3Bk5QQ4el5vYT2ojb........f7vG3rFJ5rytRcLxhBWS++t0342QDwMWiOi5NkjC........jWNnC5fhm64+UwC98+doNJUqecDwWO0gXhnVeix+L034W2ojb........f71Pu+kk5HjECj5.LQUKuQ4SNh35hHlTM7Lpq5ryth+9a8Vi25a8sl5n.........zD4ndqGUrss8DwO4m7iScTpFSJhX8QDOapCx3Us7FkeNQDGdMb90Uc1YWwcs96NpToRpiB........PSn2+PmUpiPVTndmxeK0vY+SiHNtZ37qq9GW2cEKdwKJ0w.........nI0niNZ7VOxiH0wnZ88hBzJXuVcixWRzDUR9UbkWkRxA........poJUpTb9WvEk5XTsN0TGfIhZUQ4mSMZt0cKeEqLt3K9OM0w.........nEv6ZNmRpiPVLPpCv3UsX0q2QDwNqAystqyN6JdnG9giRkJk5n.........zBnfu90+SiH9JoNDiG+t0fYdgQA5mTf8maes2QzaumXpiA........PKhC5fNnXzQ2c7C+g+KoNJUiecDwWO0gX7nVr50OmZvLq6thq7ph4MuARbJ.........Z0TfW+5yL0AX7JuKJefHhiKmmYc2fCN+XkqbEoNF.........sfNoSpvz27azwEi8Tc2vKuKJ+bx44kDWzm3R7tjC........jDUpTIV5xFJ0wnZUHZ4OuKJeI477p6N+K3hrx0A........Rp9OoYk5HTsZ4JJeIQDGdNNu5tN6rq3BtvyO0w.........nEWe80WpiP0ZfTGfwi7tn7BsK7hujnRkJoNF.........s3l27FHwInp0RdixKrFbv4Ge7yckoNF.........DQDwxWQgr+xCOhniTGhCj7pn7B+ZWekm6pRcD.........3UcBc2SpiP0pg+VkmmEkWXM3fyOV7hWTpiA.........upB76TdKSQ4CjSyIItnOwkj5H.........vqyrlU+oNBUqARc.NPxihxmYDwwkCyIIFbv4GyadCj3T.........vqWoRkhAGb9oNFUiNRc.NPxihxGHGlQx3sIG........nQ0z+u71RcDpFM7Wz57nn7B66St2lb........fFYGe2cm5HTsFH0AX+IOJJ+TygYjDtM4.........Mx5u+B66TdGoN.6OYsn7AxiPjBc1YWwbm6oj5X.........v9TumXg8Fk2QpCv9SKaQ4m0Y+ghRkJk5X.........v9ToRkhAGb9oNFUiARc.1eZYKJen2+xRcD.........3.5Xm1zRcDpFSN0AX+IqEkWHeexW5xFJ5c58l5X.........vAzL5qP9NkOiTGf8mrTT9.4UHp2NiybQoNB.........LtTdpkScDpVyL0AXeIKEk2v9uoNPduumyL0Q.........fwkxsWXKJugc8q2xUT94eAWTTpToTGC.........FWN4YeRoNBUqARc.1WZ4JJeF80Wpi..........SHCN37ScDZpjkhxane702WF3TmSpi..........SHG6zlVpiP0XfTGf8kpsn7AxyPTurzkMTToRkTGC.........lPNgt6I0QnZzz8Fk2QdFh5k9OoYk5H.........vD1g1VaoNBUiF1sTdKUQ4y4TNkTGA.........lv5u+9ScDpVMj2p7VpUu9IO6SJ0Q.........fIrRscnoNBUqYl5.r2zxbixW9JVYpi..........UkdmduoNBMUp1hxOtbME0AynuB6pH.........fXvAmepiP0XfTGf8lpon7FxqF+AROc2cpi..........UsicZSK0QnoQ0TTdC4is9ARO8bBoNB.........T0lZ41ScDpFMjWD6VlaTdkJURcD.........npczScpoNBUiFxKhcKwMJe4qXkoNB.........jIkmZ4TGgpQGoN.6MUSQ4cj2gnVqftBB.........3UUt8BYQ4GWpCvdSKQQ4EzUP..........upRscnoNBMMplhxKb5o6tScD.........HS5c58l5HTsFH0A3Mpk3Fk21gcXoNB.........PChpon7FxcHO.........M6V9JVYpiP0niTGf2nVhUuN.........jLcj5.7F0RTT9H6ZjTGA.........xrSn6dRcDZJzZTT9ypnb........fhuCss1RcDpFyL0A3MZhVT9.0hP..........MslbpCvaTKwMJG........flA82e+oNBMEZIJJu7TKm5H.........PqpSM0A3Mp0nn71UTN........PwWo1NzTGglBsDEkC........PyfdmduoNBUqFp2o7Vhhx68D6N0Q.........fVYyL0A30pknn7RkJk5H.........PtnyN6J0Qnvqknnb.........ZVL24cZoNBUiB8pWefZQH.........flZV850aCO7voNB.........PChVhhx20H+xTGA.........xEynu9ScDpFV850airqQRcD.........nUlUud81HOqhxA........fwzRTT9Ssicj5H.........Ptn7TKm5HT3MQKJ+TqIonF6w+I+3TGA.........xEkauPVTdCUWysD2n7Mtw6KFczQScL.........fF.sDEkGQDa6Ir90A........fIVQ4yrlkh5fsrksj5H.........Pl06I1cpiP0pgoy4IRQ4Stlkh5fGaqJJG........n3qToRoNBUqFlNmeKSf+YGHh3ApQ4nt3W+e7el5H.........PlcP+9+doNBUi4FQroTGhHZgV85QDwCs4GI0Q.........fVUcj5.rGsLqd8H7NkC........zbXoKanTGgpQGoN.6wDon7Bu+8cu6TGA.........xrB76TdCgIRQ4cTqBQ8xStism5H.........PqpFlsXdKUQ4.........PxLyTGf8nkZ0qC........Pyf1Z6vRcDJzlHEk2vbM3A........nU1w2c2oNBEZSjhxmQMKE.........PyNqd8TXpkaO0Q.........fVUGdpCvdzRUT9QO0ol5H..........I13sn7Apkgndo+96O0Q.........fLq7TKm5HTn0xbix6rythSd1mTpiA.........YV41KrEkOPpCPDsPEkeVm8GJ0Q..........Z.LdKJel0zTTGbFKbgoNB.........PCfwaQ4StllhZrkuhUZsqC........P50QpCPDsHqd8O5J9+I0Q.........fbS6kO5TGgpUGoN.QL9KJuiZYHpkN+K3hbaxA........ZpToRkTGgBsl5hx6rythK8u7uH0w..........ZfzTu50uq0e2QoRkRcL.........fwzQpCPDMwEkeEW4UE8N8dScL.........feqNRc.hX7WT9oVSSQMvbNkSI0Q.........fZlAGb9oNBEVMs2nb.........ZlcrSaZoNBEVJJG.........pWlYpCPDiuhx6nVGhZgdOwtScD.........fWuCO0AHhl3hxKUpTpi..........z.xpWG........fBnoVt8TGgBqlxhx6rytRcD.........nl5nm5TScDpVI+cJe7TT9.05Pj2l67NsTGA.........XuaxoN.Mk2nb.........f8EEkC.........sTFOEk2QsND4sSn6dRcD.........nlp+96O0QnZ0QpCPSYQ4GZask5H..........6ccj5.X0qC.........sTTTN.........zR42ab7OyLq4oHmUf2E+Pc2niNZrsmXG60eusrksLtmS4oVNJ2d42ze+RscnQuSu2pNe........r20d4iN0QnZM4TGfwSQ4GdMOE.4lWaw26on6+8cu63I2w1e0e+0cGqMY4avAmebrSaZQDQzVaGVb7c2cDQD8zc2QaG1goXc.......fwoJUpj5HTsR9k0d7TTNPCj8TD9H6ZjXjmcj3W9rOa7rirq3W7y+4wF238k53c.MdyXmc1UL24cZuZY564Fq26I1cTpToZbJA.......nY1aYb7Oy+2ZdJxYO0N+oE4e5InE2vCObrqQ9kw1291icu6c+pEgGQDOv8+cictymNwIrwvxWwJioVt83nm5Ti96ueEnC.......sjNne+B4ci96EQLPJCvApn7AhHdf5PNxU+5+i+yTGAXbYaO91hGdKaMdpm7IimcjcE2zZtwTGoBs8rV2mQe8q7b.......nkPu8zSQ7hVl7hxKj+3E.EU6oX7+4G76qT7Zf81ZceoKann+SZVQe80WLqY0uhyA......flJycdmVry0T3JJ+TSc.TTNTic+2+lhst0sFOv286THdCwa1rt6Xsw5ti09p+0KcYCEy4TGHF3TlSz6z6MgIC.......fT4.UT9jqKoHGszkMTpi.DCO7vwZVyMG29s82WDW0EM0dsEmO3fyOl6oc5wYtvyPo4........sPNPEkOy5RJxQVqxjRqe82Srgu02zZUufXia79hMtw6K9y+TWRL3fyOVzhWRrny7LhJUpj5nA.......PMjUuNjQiN5nw+vsu13dV+cY0pWfsmRyufyKhy+BtnXYCMTbxy9jRcr........1uNgt6I0QnZ0QDwOMUG9uSpNXnnazQGMttq+FiSdVyJtfyaUJIuIx0r5qNlyez6LVzBOyX8q+dRcb........1mNz1ZK0QnZ0QJO7CTQ4cTOBQdZF80epi.s.t0a61e0Bx8Fj27Zia79h2+RWRrnEdlw8e+aJ0wA.......fbRSWQ4PszCs4GIVzBOyXEmyGRA4sP13Fuu3Ll+oGq5iupXaO91Rcb........Hir50gwggGd33R9D+Ywb9idmVw5svto0biwLdGu83K8k9xwniNZpiC.......PzVwc0qmTMcEk6OHPd6VusaON84Mu3ZV8Um5nPCh+7O0kDef+6mU7Pa9QRcT.......fVb8zSOoNBUqAR4g+6c.986ndDh7TA9OHPClQGcz3y8Yu7VtBxKWtb7G9G9GmoYbm245xozz3Zia79hMtw6KV8+uWa7wO2Ul53........vDvApn7iqtjBnAyCs4GI9vm8YU3eGxWvBVXbHGxgDG7gbHQkJGaDQDSZRSJJ2d6u5+LceBGetet+Im24859qetm+Ehm+4e9W8udmO8X+mqu7K+xwv+hedDQwsb8K37VU7XacKwW3K9EhRkJk53........v3vApnbnky0c82XbAm2pRcLFW5qu9iN6rqnxwNs3fO3CNJ2d6wjlzjhi4XNl3PN3Ik538pNxiXJwQdDS4U+q2akyumx0G9Y1U7xu7KGirqcEuxq7JwvC+KhW9kdoF5hzuo0biwniNZboW5kF8N8dScb........3.3sb.98++VWRQN5w9+6GonJpJMxqZ8ErfEFG4QcTQkJGaTt81ioLkoDUNl1OveXSnc7jOU7JuxqDirqcECO7uHdt+O+ehMrg6M0wJhHhN6rq3tV+c6+NH.......nt5f98Kj2O5UGQbgo5v2eEk2QDwNqS4H27q+O9OScDn.ZzQGMV0GeUw5ti0l5nDuu22RiJG6zh1au8XxSYJ0jUidyngelcEirqcE6ZW6JF9W7yS1MPWY4.......P8VAsn7uWDw.o5v2eEkOPDwCTmxQtQQ4LQssGeawRV76MIuG4KXAKLl1wcbQWcc7Q41auk8VhWqrim7ohc9zOcris+D00hyUVN.......TOon7INEkSKs5cI480W+w6d9mQzYWc0v8Nh2r6kd4WIdpm5ohexO9GEei69thQFYjZ540YmcEem6+9iJUpTSOG........EkOw0TUTdmc1Urssu8TGCJHpmkje1evObLmS4TsF0aP7Ru7qD+3ezOJ1zCb+0z227ktrghq85t1nToR0ry........XQK7LiMtw6K0wXhJoEku+9QKXx0sTjSl67NsTGAJHpWkj+wN2UEm9fu63HOhoTSOGlXNjCdRwrm8rhYO6YEK488eK1xi7vwMb8WateNq6NVazd6GSbU+Uewbe1........6wwNsok5HTMRZez6uhxmYcKEPcT8njbEjWbz8Ib7Q2mvwGm9fu63q+Oba49aY90r5qNlQe8Eevy9rx04B.......TvMiTd3+No7vg5sZcI4KXAKLttaXMwPCMjRxKXNxiXJwex4cdw0cCqI5qu9y0Yuhy4CEa6w2VtNS........pdMUEk2VaGVpi.MvFczQqYkjWtb43S+Y9bwk7m8m4cHufq6S33iO6keEwG6bWUtN2O2m6ykqyC.......fp29qn7B2aT9w2c2oNBzfZzQGMV0GeU0jRxeeuukFq9u9qFmxoLmbe1jFGxAOoXngFJtxuveUtMy0cGqMt0a61ys4A.......rGyHm2VtsB1eEk6MJmlFetO6kGq6NVatO2O4m5+Y7mbdmm0rdSpYO6YEW2Mrlnb4x4x7t7K6SGiN5n4xr........ZBjrKucS0pWG1attq+FiqY0WctNyxkKGW2Mrl3c+tGLWmKMd59DN93J+heobor7ctymN9xe4UmCoB.......noPxt71JJmlZOzlej3BNu78sldAKXgwp+q+pdKxagT4XZOtxu3WJWl0Ub4WVL7vCmKyB.......fpSSUQ482ucuO+ViN5nw2v9YR1...H.jDQAQE9rOqbclm8G7CG+I+ONeqZ8VPUNl1ys2r70rlaNWlC.......PDQTdp4yyHaqDuQ4zz5S9m8IictymN2l2G6bWUr7ku73PN3IkayjhkYO6YEm+EbQYdNtU4.......Pdpb6JJehZ+UT9gW2RAjyttq+FiaZM2XtMuO14tpXngFJ2lGEWKdwu2XAKXgYdN2y27akCoA.......nPaxo5fapV85PDQrsGea456RtRx4M5rN6OXlmwW4KcU4PR........JzR1VNuopn7dOwtScDnAvm6y84xsYojb1apbLsGeryMa+vXryc9zw8e+aJeBD........SH6qhx6ndFh7RoRkRcDHwttq+Fi0cGqMWlkRxY+487dWbTtb1duO9V268lSoA.......Zkcxy9jRcDJbZpJJmVaCO7v415rdAKXgJIm8qC4fmT7g+HKOSy3ZV8UGiN5n4Th........X7poZ0qSqsU+UtlXm67oy7bVvBVX7m7+37ygDQyt20blSluU4OvC78yoz........T3L4TcvMMEkO3fyO0QfD5g17iDWypu5LOmxkKGm0Y+AiC4fmTNjJZ1cHG7jh2y6cIYZF+fGTQ4.......PKqYlpCtoon7icZSK0QfDZ0e4rWRdDQbAW3EGUNl1ykYQqgSev2cl9973GvC........Wr3Il8UQ4CTOCAjEqe82Srt6XsYdNm+EbQwrm8rxgDQqji7HlRrfErvLMi6+92T9DF.......nkkKV7DSSyMJmVW230esYdFKXAKLF7cmsaFLstdm+W+Cyz2+fO3ClSIA.......fwCEkSg15W+8Dabi2Wlmi2kbxh21a+smouey+v+0bJI........LdzzTT9L5q+TGARf731j+I+T+O8tjSlj00u9F238ECO7v4Xh........JDN0Tcv6qhxmbcMEPUHOtM4KXAKLd2u6AyoDQqrYLy9xz2+HOxilSIA.......ZE4hEOwruJJel00T.Ug731juj22+sbHIPDc1UWY56ezGcq4TR........3.ooY0qSqk731j+wN2UEceBGeNkHZ0k0+rz1ehskSIA.......fCjllhxaqs1RcDnN5ebs2dl99xkKGm9fu6bJMvXdeuukV0e65ti0FiN5n4XZ........Xeooon7d5omTGApSdnM+Hw5ti0loY7g+HKONxiXJ4ThfwzcOmXl99s8D6HmRB.......zpo7TKm5HTsRxyB99pn7Npmg.lH9V268louub4xw6ZNyImRC7aUt81yz2uksrkbJI.......PqlxsWXKJexo3P2WEkeb00T.iSiN5nwUb4WVllwG9ir73PN3IkSIB9slxTx1VJ3W9rOaNkD........1eZZV85zZ3t+FeyL88tM4TKU4Xx1MJe6Ow1xoj........v9SSSQ48dhcm5HPcv25adOY56caxoVaAKXgU82tt6Xs4XR........Xeooon7RkJk5HPM11d7skohDcaxod3HOpiJSe+vCObNkD.......nUxIO6SJ0QnZ0QJNz8VQ4cTuCALd7Mu2uUl992y6cItM4TyUoxwloueWi7Kyoj........THzQJNTEkSgwC7c+NY56+ieWtM4T6MoIkseXL1912dNkD........1WZZV85zbaaO91hMtw6qp+9ErfEFUNl1ywDA6ckaOa+4rcu6cmSIA.......f8klhhxW5xFJ0QfZrrt10GXtyKmRBTa8T6XGoNB.......PAUmc1UpiPgQSQQ4kJUJ0QfZrrt10eau82dNkDX+q6S33yz2u6c+ukSIA.......Z0L24cZoNBEFdixog2vCOblV65erycUwgbvY6ciF........nlXfTbnJJmFda568fY56+u71baxo33lVyMl5H........zzqoX0qSysGaqaMSe+eva+skSIAFedeuukl5H........v9QSQQ4mP28j5HPMz23tuqp9aO6O3GNGSB........z3RuoieMEEkens0Vpi.0HOzlejXm67oq5u+O3O3cjioApOFczQScD.......fBH8lN9s2JJex08T.6CaYKaISeeWG+wmSIApe11SriTGA.......f5kYlhCcuUTdRBBr27Xas5KJuu95ONxiXJ4XZ.........xYGdJNzlhUuNMudf6+6V0e6rO42YNlD........flEMEEk2S2cm5HPMvvCObldex6pKqccRii5s9VScD.......fVP82e+oNBEFMEEk21gcXoNBTCr8s+jY5689jSpLkobDoNB........rezTTTNMm19N1QU+skKW16SNIyvC+KRcD........JR5ndef6shxmY8NDvdyis0sT0e6e3e3ebNlDXh4keoWJ0Q........nHoi58At2JJ+vq2g.1aFczQq5usxwNsbLI........Piu1KezoNBEFMEqd8RscnoNBTCrt6XsU82NkoXsqC.......PqkJUpj5HTXzTTTduSu2TGAxYCO7vY56K2d64TR.........Z1zTTTNMe10H+xL88GwQbD4TR.........pwlb89.UTNMj18+1+Vl99i7Hr50IcdoW5kxz2exy9jxoj........THLy58A9FKJef5c.f8lsuicT0eae80eNlDXhaCa3dScD.......fVTKcYCk5HTH3FkSSmN6rqTGA........HIJUpTpiPgPgunb+DQ.zH4kd4WISeuePO........p8J7Ek6mHhlSO1V2RU+sG0a8sliIAlXdlm4Yxz2O24cZ4TR........JLlb89.K7EkCuQSYJGQpi.........v32Lq2G3arn75dS8.zLYmO8SmouepkaOmRB.......zJZF80epiPgvarn75dS8.vu0QO0ol5H........zzypWGfbzN19Sjouu7TKmSIA.......f8kBeQ4Vc..MRdoW5kxz2WtcEkC.......TqU3KJGfFIaXC2al991Kez4TR.......fVQEzsW6oVuOPEkCPNY3mYWYdFUpTIGRB.......zpx1qc7QQ4.jSdgW3Exz2uzkMTNkD........1ediEkOPJBA7FcBc2SU+s6X6OQNlDX7ajcksaTd6seL4TR........X+wMJmFRGZask5H.SXY8GRiiu6tyoj........v9Sgun7dTrDPCh+k+k+4L889uOC.......xpdOwBaeCCTOOrBeQ4scXGVpi.0.kmZ4p9auy6bc4XRfwmm64egXjQFISynmdNgbJM.......PqpRkJk5HTHT3KJmlSkau5KJGRgm9odpL88c1YWQkJUxoz........v9ihxogTo1NzL8863IyVokvD0t10txz2O24cZ4TR........3.4MVT9LSRJf2fdmduY56ekW4Uxoj.iOa+I1Vl99YzW+4TR........Jjlb87vdiEke30yCG1e5rytp5ucjLd6dgIhW5kekXCa3dyzL5ueEkC.......4ikuhUl5HTMpqWp6B+pWu8xGcpi.0HYYUTO7v+hbLIv92O9G8ix7L58D6NGRB........iGE9hxqToRpi.0HmP28T0e6O4Gm8hKgwqm9oepL88KcYCEkJUJmRC........GHE9hxo40QO0oV0e6V25Vhm64egbLMv91leneXl999OoYkSIA.......fwCEkSCqd5o5uQ4QDwS+TY6V9BiGC+L6J15V2RllQe80WNkF.......fHZqsCK0QnZzQ87vTTNMrN4YeRY56y55vFFOdjG9gy7Ll27FHyy........XON9t6N0QnZzQ87vdsEkOP87fgwiktrgp5uMqqCaX7X6Ow1xz2u7UrxbJI........Ld4FkSCsr71Mu0stkX3mYW4XZfWum64egXCa3dyzL9imyojSoA.......fwqBcQ4Y41FSwPVe6l8NkSsTdr10G3TmSNjD........lHJzEkWpToTGApwl0r5OSe+O7e8eImRB7l8suuuUl99AGb9QkJUxoz........LlxSsbpiP0Xl0yCqPWTNM+JUpTl1b.aXC2a7bO+KjiIBFyvOythst0sjoYL2S6zyoz........7aUt8BYQ4Gd87vTTNM7xx6TdDQ7CdvGLmRB7a8O+Cx9et5LW3YjCIA.......fIJEkSCurVl3C982T9DD303ab22Ul99AGb9QuSu2bJM........LQ7ZKJutty2gwqdmduQmc1UU+8acqaI9e+i9w4XhnU2l27CGiLxHYZFV65........oyqsn7ImrT.G.m0Y+gxz2+82zCjSIAhXSOv8m4YLzPKKGRB.......vaVumX2oNBUqApWGTgd0qeBc2Spi.0IyYNyISe+cdmqKdtm+ExozPqrc7jOUrgMbuYZFKcYCEUpTImRD.......vqWoRkRcDZ3UnKJ+PaqsTGApSl27FHFbv4moY7c132NWxBs11xi7vYdFu+gNqbHI........TsJzEkSqkr9lNeCW+05VkSl7bO+KD2v0esYZFc1YWwbm6ojSIB.......fpghxovHOdSmcqxIKxi+7yYc1eHq6D.......fDSQ4TXToRkX4qXkYZFtU4TsdoW9UhuwceWYdNC89y9OvG........GHKcYCk5HTMFndcPu1hxmb85Pgp0BNiyLyy3q+Oba4PRnUyO3AevXjQFISyX4qXkQuSu2bJQ........6a1vs6eu1hxmYxRALNs3EunnyN6JSy3Nuy0E63IepbJQzJ3kd4WI969Z2TlmSd7C5A........YWgd0q2Vask5HPBbgW7kj4Yb8W6eS7Ru7qjCogVAei6d8Y91jO3fyOV7hWTNkH........xhBcQ48zSOoNBj.efyZnLeqx25V2R7CdvGLmRDMydtm+Eha35u1LOm+6m8GLGRC.......PSs51yEdgtnbZMUpTo3rN6OTlmyW3J+7wvOytxgDQyr0eW+SYdFc1YWw688XsqC.......0Oynu9ScDpF0smKbEkSgzJVwGMWlysea2ZtLGZNsim7oha6V+6x7btvK9RhRkJkCIB.......f7fhxoPpRkJwe9ewmNyyYCa3diu82di4PhnYz0es+MYdFc1YWwG3rFJGRC........4EEkSg0JVwGMyuU4QL1JXeGO4SkCIhlIe6u8Fist0sj4431jC.......z3QQ4TXUoRk3Bu3KIWl0k8o+KhW5kekbYVT78bO+KD+cesaJyywsIG.......Rk1ZqsTGgpQG0qC50VT9oVuNTHu7ANqgxkaU9HiLRbyq4uMGRDMC95+C2VLxHij447W7ouL2lb.......HI5omdRcDpFGW85fbixoPqToRwW3pt5bYV24cttX8q+tykYQw02+6+fwcdmqKyyYvAme7AO6yJGRD........4sBcQ4m7rOoTGAZ.r3EunXoKKeVu0Wypu5Xya9gykYQwyy87uPbCW+WMWl0E8IxmmE.........xeE5hxg83RuzKM2l0m5S9Ihc7jOUtMOJNto+1aLWV45KeEqLl27FHyyA.......fZCEkSSgdmduwUbkWUtMuK6S+WDC+L6J2lGM991e6MFaXC2atLqK3BtfbYN........UqRscnoNBUqNpGGhhxoowJW4JhAGb94xrFYjQhO0e1Eqr7VD+u+Q+33Kbke9bYVWwUdUQuSu2bYV........UqBbeEcTONDEkSSiRkJE+kW1mM2lmxxaM7bO+KDeg+WWdtLqAGb9wJW4JxkYA.......PsihxooxIO6SJWWA6iLxHwsea2pxxaR8Ru7qjauK4QDwe4k8YiRkJkKyB.......fZm2xu4+4jiHdgTFjpwu9+3+L0QfFTm8G3ri0cGqM2lW4xkiq7K9khJGS641LI8toa5lha6V+6xkYc9WvEEW0e0WLWlE..PykgGd3XWi7Kisu8sG6d2690860VasE8zSOQ6kO5nRkJIJg....zr5f98+8RcDpFyMhXS05CYOEkOPDwCTqOr7lhxYeYaO91hkr32aryc9z41LUVdyk0t10F2v0es4xr5rythG5geX2lb..fHhwJFeSeuGLdrst03ab22039+6R5ryth2y6cIwL5quXfScNJNG...fLaQK7LiMtw6K0wXh5OMh3qTqOjB4OBAQL1+OP.1W5c58Fegq5pi2+RWRtMy87lkqr7huMu4GN2JIOhH96tsaWI4...wCs4GIti0t13ZV8UWUe+N24S+591kuhUFK3LNyXwKdQ4UDA...nEywNsok5HTMlb83PJruQ4ycdmVpi.M3V7hWTt9dkGwXkk+Q9PefXya9gy04R8yl27CGepO4mH2l2UbkWUbxy9jxs4A..T77Pa9QhU8wWULm+n2YUWR9dyMslaLd+KcIwhV3YF2+8uobat.....E3hxgwiK9h+SikuhUl6y8S8I+Dw29auwbetTak2kjuzkMTbwW7eZtMO..fhmuzW5KGy4O5cF2zZtwZ1YrwMdewYL+SOV0GeUwvCObM6b....fVIJJmldegu3WHV5xFJ+m6U94i0t10FuzK+J49rI+k2kj2YmcEW5kdo417...JVFczQiy9Cb1we9m5Rpam4MslaLN84MO2tb....HGnnbZ5UpTo3RuzKsl7t1eCW+0F+M+0WSL7yrqbe1jex6RxiHhu50cCQuSu2bcl...ECa6w2VbxyZVw5ti0V2O6ctymNNi4e5w5W+8T2Oa.f++Yu60nr5x67E7+FWYVSOiEaHy7Bo3TRSgVPgl0QpBAs6S6MzJzpDKS2f3s9zcDKESaBQQSrYAVh3wkWhnXaGjToHYhsZiPm.1nFREQRnmLxspzyJJkUoTdvJT3ZVceBaqbYVyrVy7B6hVMHPs2+26+6Ke97FSI442y2kgfP8c+77..P4mwW6DR6HjKldwXSTTNUEZbZMFaZyOeAor7ezO5Ei65quDua4knJDkje+OvCGyd1WXhNS..fxCYylMtikrjXfA1epliqZdWoxxA...335TF+3S6HjKFWwXSTTNUMZbZMFe+m9YKHydngFJtquwc3pXuDy5W+5S7Rx+pK918tjC..UwtkEcKQWcs0zNFQDJKG....xGJJmpJyZlyHdtMtoB17+1qcMwcurkF8+1uSAaO33627a+cw5W+5iu8ZWShN24M+EDK+tWVhNS..fxGO4Z6HUtt0OVtp4ckQu6q2zNF....PYmQJJeRoYHfhoVactEzxx6omtiEcSKL17ledmt7Tv+x+5+83u6u8wS7Rxqu9IGO3C8fQlLYRz4B..TdXvAGLV7sdKocLNptikrjHa1rocL....fxJJJmpRE5xxiHhGe0qxoKuHq+29chE+U9xwO5G8hI5bqu9IGaZyOeTWc0knyE..n7Qmc9cS6H7opqt1Z7LOao0IcG...fRC0N9ZS6HjKldwXSJau50GesSHsi.k4For75qexEr8XjSW95V25h+k+0+6Er8gH9w+3thEcSKLFZngR7YuoM+7QiSqwDet...kGFbvAi6+9VQZGiioEeq2RL3fCl1w....fRL0NgxxhxGawXSJaKJ+TF+3S6HPEfVactwl17yWPKKOhHd5+9uer3uxWN9w+3tbcrmvF7Wdv3genGJdvG3+RAY9O2F2jRxA.fpba4EdozNBmPV+52PZGA....nrQYaQ4PRowo0XroM+7QKsLmB59LzPCEO3C7eIt6kszX26dOEz8pZwO9G2UbWe8kj3W05i3413lhVactEjYC..T9XKatv9rMkT5XsqIsi.....T1PQ4P7gkk+L+COaLu4ufB9d0SOcG2023Nh6XI2tByyQezSQdg3pVOBkjC..7gFbvAit5ZqocLNgLv.6O1111dZGC....nrfhxg+MYxjIVyStlXoKq8hx9ov7Queyu82Eqe8qO9K+Kt1B1oHOBkjC..7uqu9d6zNBiJ6XG6Hsi.....kPlPsmRZGgb0zKzavmoPuAP4jLYxDs29xiS6zO8Xg+U+EEk8rmd5N5omtilZp4XAW80Fm4m6yE+u7+7ePQYuKW7a9s+t3edG6H99+uutB1IHOhHpu9IGaZyOu2jb..finu96Osivnxt24ql1Q....fRH0UWcocDxUiqPuAJJGNJt9q6Zhyt4lh6XIKoncMKNRg40Vasw7upqI9SNuyK9e6+0OaQYuKUUrJHOBkjC..bz89G5PocDFU5pqsFCN3fkyeiP....fhBW85vmhQd2x+pK91Kp66PCMT73qdUwUMuuX728DOQU40x9u4296he7Otq3ltwuTA8cHeDya9KH9IaaaJIG..32ygF5focDF0N3PueZGA....njmSTNbLjISl3g+lOTboW1kEe4EcSw.Cr+h59+C9AaL9A+fMF0VaswW3Jtx3+zex4E08eXBE0LTLM3u7fw+G+y6H91qcMEs87qt3aOV9curHSlLEs8D..fBo95quXVybFocL....fRZiTT9jRyP.k5l8ruvXW6YOwJu26Kd7Uuph99OzPCEe60tl3au10DM0Tyw4c9WXbFm4YFMb5mVQOKIs+k+0+6wa7K9Ewyu4eXzSOcWT266+Ad3XIK41Jp6I...EZCO7vocD....nDRKsLmh1SMbBZ5QDauPtAJJGNA8QOc4q5a9vo1ufxHuk4QDG4jlO4IeZwjOsSqr4MMejxw24q9+Y7i9QuXQe+qu9IGeqm7aGyd1WXQeuA.......JlN0INwzNB4hwUn2fx1qd8ZGesocDnJ0rm8EFm8Y2b7LO65iG6Qd3h90w9G0Hmz7QzTSMGybVmSIWw4+le6uKdm24ch8+NuSrie11K5mb7Op4M+EDO3C8fQc0UWpkA........RWkuEkOAEkS5ISlLwht41h4d4WZzYme239uuUj1QJh3ieZyi3COw4+Q+Q+mhFlxTiO6m8yFi6y9YK3WW6+le6uK9k+xeYLzAOXLzPGL18t1YpVL9GkqZc..fpA+ZW85....vwUYaQ4Pof5pqtn81WdrfqZ9wpW8pi00YGocj9XFZnghevOXi+d+8Go.8HhngoL0HhH9C9C9ChZmvDNwm8AOX769c+tHhH5uu2JhHNp6UofVZYNwcuh6Ml0LmQZGE..fxLmUSMm1QXT6s6uuzNB....PIOEkCIfFmViwZdx0DeoEdiw2syuSIWg4eReZEnWI59efGNZqsEFYxjIsiB.......PQ23q8D+fRVBY5E5M3jJza.TMYVybFwZdx0D63muy3FVXaocbppMu4ufXG+7cFKYI2lRxA......fpVmx3GeZGgbw3JzafhxgBfQJL+cF3ciktr1S63TUo95mbz426ohm9YdZW05........bTonbn.Zj2v72Yf2MV8Srln95mbZGoJZ2+C7vwt1ydhq+5tlzNJ........TBSQ4PQPc0UWrnatsXW6YOwyswMEszxbR6HUQYoKq83cF3ccMqC.......vIDEkCEQYxjIZs04Fa4EegXG+7c5ZYOOMRA4s29xi5pqtzNN...UflRCMj1QXTacc1QZGA....JgT63qMsiPtX5E5M3yTn2.fitYMyYDyZlyHtsaawwq7J+r34V+yFabCqOsiUIu5qexw0bc+EwBW3WR43...Eb0Llwj1Q.....xK0NgxxhxGagdCTTNjxF4Tl2ZqyMdvG5Ais7BuTrkMuonqt1ZZGsRJszxbhq95t93J9BWtqWc........xKiTT9Ejpo.Hh3e+sLeQ2baQu6q23EdwWJ5du6op9jluzk0dboW1kEyZlyHsiB........UHbhxgRTMNsFiFmViQDQ7fOzCFa+mti306om3wW8pR4jU38UW7sG+Im24GWzEc9N83........j39e3e6u9+Wplhbv+W+K+qJPipVaaaaO5omdpXNs4i7tiO8o2jxwA..Jorqcu2379iOmzNFiZ+e++y+uocD....nDwfCNXbZ0OozNF4hlhHdsB0vKaKJ2ene3CkMa1n22p+n6t6Nd8d5Ndks8xw.Cr+zNVGS0W+jiuvUbkwY0TSwY2bSG4jyC..Pold2WuwY8e7yk1wXT6cF3ci5pqtzNF....Th3+o+GKKunwunHhsWnFthxgJPCN3fQe881Qe82e7N82ebvC9KSsSdd80O43hl8EGmUSMG0N9ZioNkSWw3...kUJG+lIrie9NiYMyYj1w....fRDki+YaCEkezonbXzavAGLN3PueLzAGJF5PCE+5gGNd696Kh3COY54RY5ya9K3HWU5mUSMGQDwTZngnlwLFei4...pHTN9MSPQ4....7QUN9msMJvEkWV9OQ.xM0UWcGyqewm9Yd5hXZ........fzwIk1A..........JbZok4j1QHWL8B4vUTN.........UvN0INwzNB4hwUHGthxA........fpJJJG....NFl27WPZGgQst6t6zNB....PIMEkC....GCYxjIsi.....PBSQ4.........TAqlZFSZGgbwjJjCWQ4.........TA6zZngzNB4hIUHGthxA........fpJmTTfahG.........nThhxA........fpJt50A...figSugoj1QXT606o6zNB....TBolZpIsiPtXRExgWVVT9Mrv1R6H....PUhSt77al.....bDSYJkeeHviH9CKjCurrnb.........fbkhxA........fpJJJG.........ppnnb.........nTz3JTCVQ4.........TAaVybFocDxUSuPMXEkC....GCSogFR6HLpstN6Hsi.....TRSQ4....vwPMiYLocD.....RXJJG.........ppnnb.........nphhxA........nB27l+BR6HjKtvB0fUTN.........U3xjISZGgRJJJG.........ppnnb..377drF...B.IQTPTA.3XHSMmbZGgbxfCNXZGA....njkhxA...figFmViocDxIGbn2Osi.....TxRQ4.........Tg6zaXJocDxEWXgZvJJG........fJbmbM0j1QnjhhxA........fpJJJG.........ppbRQA7dcG.........nTiSTN.........U3pc70l1QHWL8B0fUTN....bbTe8SNsivn1PGbnzNB....TBo1ITVVT9XKTCVQ4....vwwEM6KNsivn1PGRQ4....vmlxxhxGesSHsi..........Tlprrn7SY7iOsi..........Tlprrnb.........3DWlZN4zNB4pIUHFphxA........nBWiSqwzNB4pIUHFphxA........fpJJJG.........ppnnb....nBzud3gS6H.....krTTN....bbbVM0bZGgQs2t+9R6H.....IgwUHFphxA........nJvMrv1R6HjKldgXnJJG.........ppnnb.........nphhxA........fpJJJG.........ppnnb.........pBL9ZmPZGgbwzKDCUQ4.........TE3TF+3S6HjKFWgXnJJG....NNpolZR6HLp8dG3.ocD....fRVJJG....NNlxTlRZGgQst5ZqocD....fRVJJG.........ppnnb.........pBTN9zhEQLoBwPUTN.........UAJGeZwhH9CKDCUQ4.........PUEEkC.........UUTTN.........TUQQ4.........PorwkzCTQ4....PEprYyl1Q....fRHMN0FR6HjqldROPEkC....GGkqeiD58s5Osi.....kPxjISZGgRFJJG....NN7MR.....prnnb.........nphhxA........fpJJJG........fpDszxbR6HjKldROPEkC........PUhSchSLsiPtXbI8.UTN.........TUQQ4.........PUkOSZG.fBqd2WuQ1g+0QDQzWe8ECO7vQDQ79G5PwgF5fer+69Ja6kiAFX+Ix9Nu4ufHSlLG4qqolwDmVCMbjudJMzPTyXFSDQDyZlyHQ1S........3DghxgxXCN3fwAG58igN3PwPGZniT9c1rYiMtg0mpYKW1+VZYNwoNwIdjR0qolZhoLkoDYp4jiFmViEfTB.......PYfD+MJWQ4PIrO5oAu6t6NhHhWumtKIJBuPnqt15w7GejhzOqlZ9Hkn23Ta3icx0A..nPYdyeAkc+9v6t6tcCNA...vGyY0TyocDxESOoGnhxgR.6Z26M5qu9h2+PGJd696KQuBzqj7oUjd80O43hl8EGmUSMGSogFhoLkSOpqt5JxoC..nRmOfl....PkCEkCEYYylMdkW4mEu1q0Sz2a0aY2IRoTz.Cr+XfN+3evBpu9IGegq3Jiypolhyt4lb0sC.......vQnnbnHn280a7Bu3KEcu28nX7hjAFX+wiu5Ucju9iVb9EdAmmSbN.......PULEkCEH6Z26MdoW7EicuyW8391aSg2mr37VZYNwEcwWRb4W1k5zlC.......TkQQ4PBZjqU8ma8OqSNdItt5ZqQWcs0Xo20cFszxbh415UFy8xuTmzb.......nhVsiu1zNB4hKHoGnhxgDvfCNXr90ugni0tlXfA1+weATRYjRyW7sFwMrv1h+zK8xiVactocr........Rb0NgxxhxSbJJGxCCN3fQmc9ci6+9VQZGERHqqyNh00YGQKsLm3putqOthuvkGYxjIsiE........IHEkC4.EjW4ajSYd80O43qsj6Lt1qYAJLG.......nBwIk1A.JmjMa13IWaGwkL6Yqj7pDCLv9iEeq2RLqy9riMu4sj1wA.......fDfhxgSPaaaaOt1q9ZhEeq2h2g7pPCLv9iqZdWYbcW60E8tudS63........jSxTyIm1QHWMojbXJJGNNxlMabm2wWOtz4bIQWcs0zNNjx13FVebV+G+bwi7HOZjMa1zNN...EQiu1Ij1QXT6c5u+zNB....Thowo0XZGgb0jRxgonb3XXaaa6wrN6yNd7UupzNJThYo20cF2xhtEmtb..nJxoL9wm1QXTa3g+fzNB....PIoOSZG.nTT1rYiG8QWcE26P97l+BhLYxDQDQCSYpQM0TyQ9wN6Yz7QcMSXB0FS7Tq6XN223M2W7Aevv+d+8+kGbnXngF5He86enghCdvCFQDw6cfCT1eB823FVer28r63Ae3UEs15bS63........vIHEkCeB8tudiUtxUFabCqOsixnxHkf2TyyHhHhoLkFhLiYLmPEcmuNyyXZ405e0ct6HhH58s5KFd3gi2t+9hO3C9f3U11KWx+dvOxaW98+.Obrjkbaocb........3DfhxgOhssssGe4EcSkrky1RKyIN0INwnolmQTSM0DMN0oDSqwoFicrYR6nkWN2yYler+5mzHEoum81cL7vCG822aUxchzW5ccmwvCObzd6KOsiB........GGJJG927jqsiXw25sj1w3HtgE1VzvTlZzPCMD+GlPsepkHWM3XUj9qtycejq38d5duo5oP+9uuUD88V8Fq4IWyQth6A.......J0Lu4ufxtaW4HhKLhX6I0vTTNDQrhUrxT88HejRwat4lhFm5TJ3WU5URNZkmef2avX26o6305o6Xmu5qVTO44i7uTQY4.......PoJcXnnbpxkMa1Xk268EO9pWUQaOaok4DmwYdlwzap4Xlmcy4866M+9l3oVWLwSst3O+KdEQD+6Emuie1OMV8iU3+eq23FVeLko1nqgc.......fRTJJmpVYylMtkEcKEsqUhks71iK+xu7p5qP8zxGs37UbOsG+jss83EegsDqqyNJX64H2PAJKG.......nzyIk1A.RCEqRxqu9IG+s+cOY7q9UGNV48dOJIuDvXGal3O+KdEQmemuc7e6.uWrrk2dTe8StfrW2+8shXyadKEjYC.......PtSQ4T0oXTR9HEj2SO8D25W9liwNVuyCkhl3oVWrx68dhd5om3u8u6IKHEleUy6Jid2WuI9bA.......pxLtjbXJJmpNeiu92nfVR9xVd6JHuLyXGal3V+x2br8e5OMV7W61S74uxUtxHa1rI9bA.......xEmUSMm1QHWL8jbXJJmpJqXEqrf8tTOu4uf3W7FuYrx68dTPdYpIdp0EO1i9HwV65kSzSW9F2v5iN5nyDad...bhxGXS....3nSQ4T03QdjGMt+6aEEjY+2928jwFdt+g3LOioUPlOEWe9KY1QO8zSbCKrsDalK8ttyXW6duI17...J9Zt4xuOs8ExaSK....nblhxopvl27VhkdW2YhO2QNE425W9lS7YS5ZriMSz424aGKa4smXyb0O5pRrYA.......PtSQ4Twq280abUy6JS74trk2d7c53a6TjWgak268jXkkuwMr9XyadKIxr........H2onbpnkMa13Ja8JR74twevl8VjWEIIKKui0tlDYN........4pZGesocDxESOIGlhxoh123q+MhAFX+I17pu9IG+h23Mi+7uXxW9Nk1Rpxx6pqs5TkC.......opZmPYYQ4iMIGlhxoh0St1Nh00YGI17pu9IG+SaYKtp0qhcGK41i4M+Ej2ywoJG.......HcUVVT96ze+ocDnDWu6q2Xw25sjXyadyeAQO8ziRxqxM1wlItm6o8n95mbdMGmpb.......fzUYYQ4CO7Gj1QfRXYylMtikrjDadya9KH9Nc7s8djSDQDm4YLs3tum6MumyO5kdgDHM........jKJKKJGNV5niNit5ZqIxrTRNGM+U+mut79JXecc1Qz695MgRD.......PUgwkTCRQ4TQYW6duwRuq6LQlU80OYkjympkj.2ZAuvK9RIPR........Fcl0LmQZGgb0zSpAonbpnr5GcUIxbpu9IG+SaYKJImOUm64Ly3FVXa40LdkW9mjPoA.......fQCEkSEimbscDabCqOQl0y7r+CwYdFSKQlEUtZqs7qn7t5Zqt90A.......RAJJmJBYylMdrG4gSjYswevliy8blYhLKprkDmpbW+5........EeJJmJBO5it5XfA1edOmks71i+7u3Uj.IhpEK3pu57Z8t90A.......J9TTNk85ce8F2+8sh7dNya9KHtikb6IPhnZxm+RlcTe8SNmWuqec..n7QlZN4zNB4jAGbvzNB....TBJe52HEMojZPmTDw6lTCCRCqd0qNumQ80O43dtm1iwN1LIPhnZyMsnubds9s+y1QBkD..fBoFmViocDxIGbn2Osi.....kftnYewocDxESJoFjhxor1t18di00YG48bt664diy7LlVBjHpF8El6kkWq+06o6DJI........bhvUuNk09tc9cx6YbCKrs3u5+70k.ogpUm4YLsnkVlSNu900YGQ1rYSvDA.......vwhhxorURbZxqu9IGs29cmPIhpYmy4dt4052ydbpxA.......JVTTNksRhSS9seGe8XhmZcIPZnZ24c9meds995u+DJI........b7nnbJKM3fCl2ml7VZYNws9ku4DJQTs6bl4YmWq26TN.......TLM9ZmPZGgbwElTCRQ4TVpyN+t48Lt2UtxDHIvGZriMSLu4ufbd846G7C.......fQiSY7iOsiPpRQ4T1Ia1rw8eeqHulwMrv1hy8blYBkH3C0XiMlWqu280aBkD........NVTTNkcdlmc848Lt8a+1Rfj.ebm1o2Pds92pu2NgRB........GKJJmxNaYyaJuV+xVd6wYdFSKgRC7uqwoNk7Z8u8a2eBkD........NVTTNkU1111dzUWaMulQasciITZfOt7857+s6uuDJI........UjFWRMHEkSYkW5Eew7Z8Ka4sGS7TqKgRC76qkVlSNu1WYaubBlD.......fOcM2byocDxEmURMHEkSYirYyFO9pWUdMiK+xu7DJMvQ2oNwIlyqcfA1ejMa1DLM........bznnbJa7JuxOKuV+Mrv1x6qFa33ogoL07Z889VdmxA.......JzTTNkM9QuzKjWq+xt74lPIA9zUSM0jWqenCNTBkD........9znnbJKL3fCFqqyNx402RKyI9y+hWQBlH3naJSog7Z8CcHEkC.......TnUVVT96cfCj1Qfhr8t2WKuV+rujVRnj.GaYFyXxq0+qGd3DJI........e5xTyIm1QHWMojXHkkEk2UWaMsi.EY+y6H+dexu1qYAITRfisILgZyq0+182WBkD..fBg5qexocDF0779....vQSiSqwzNB4pIkDCorrnbptjMa13wW8px40Ou4ufXhmZcIXhfOc94Z..PksKZ1WbZGgQMOuO....vuOEkSIu8rmtyq0e4y8KjPIAJ7dks8xocD........p3onbJ4sicri7Z8y9htfDJIvIlVZYN47ZGXf8mfIA.......fiFEkSIucuyWMmWqqccRCm5DmXZGA........NFTTNkzFbvAit5Zq475ufK7hRvz.........UNxmaI2TzzShgbRQD+pjXPPgvd26qkWq+htvyOgRBT7rqcu2zNB.......PUfxzaI2wkDC4jhHxulHgBnW605ImWa80O43LOiokfoA........nRfqdcJokOuO4WQqWYBlD........fJEJJmRVYylMudexmdSMmfoANwMlwLlzNB........bLnnbJY06a0eds9Fm5TRnj.iNmdC94d........kxTTNkr5t6tyq0etmyLSnj.iNCO7vocD........NtFesSHsiPt3BShgnnbJY89G5P47Zm27WPBlDXzo+9dqzNB........GWmx3GeZGgTihxoj0gF5f47ZarwFSvj.........TIQQ4TxZcc1QNu1SY70lfIA........nRhhxojT1rYyq0e1yn4DJI.........UZTTNkj58s5OuV+XFSMITR.........pzT1VT9t18dS6HPIry7LlVZGAHm03TaHsi........TEXJMTV1IwEjDCorsnbpr0c2cm1Q.xYqqyNxq0mISlDJI........e5pYLiIsiPpQQ4Tw4FVXaocD.........Jgonb.........nphhxojz6ze+ocDfbxqtycmWq2Mh........PgmhxojzvC+A47Zap4YjfIA........nDy3x2ALRQ4GNeGD.Dwu7fCkWqulZFSBkD.......fisYMyx1Cf5zy2ALRQ4uV9NH.HhgFJ+JJ+zZngDJI........7owUuN.In2t+9R6H........vwghxAHA8AevGjWqu4laNgRB........eZTTN.In00YG405yTyImPIA.......fOMJJGfDxgOb17dFMNsFSfj...TnL9ZmPZGgQs2o+9S6H.....IswkuCPQ4.jP1WuuUds9VZYNITR...JTNkwO9zNBiZCOb987.A...PksaXgsk1QHWL87c.JJmRR0TyXx4099GZnDLIvIt8r2tyq0epSbhITR........3Xorsn795quzNBT.cZMzPNu1CdvClfIANwkueHMNqlZNgRB........GKksEkO7vCm1Q.fOld6s27Z80N9ZSnj........vwRYaQ4vml26.GHsi.UgN7gyFabCqOulwTmxomPoA.......fiEEkSIolaN2uBp6pqslfIANwrudeq7dFMNsFSfj........Twab46.TTNUjNv6MXZGApxrm81cds9aXgskPIA.......3D2Y0Tte.VSQSOeGfhxojTiSsg7Z8G7fCkPIANwzS26MuV+o2vTRnj........vwihxojTlLYxq0mumtWXzZcc1Qds9S+zyuObH........bhajhx2dZFB3nIetJpe+C4DkSwyqtycm2yXFyHuugP........3DjSTNkrpolwjyqs2d6MASBbrku2fAszxbh5pqtDJM........b7nnbJYcZMj6WE0abCqOASBbr8S29qjWqelmy4lPIA.......npv3x2AnnbJY0byMmWqOItNrgimC7dCl2evLl9zaJgRC.......vnS91IWJ4rx2AnnbJY03Ty8STdD4+0gMbhX26I++4YdexA.......JtTTNkrxjISzRKyImWe9dcXCmH1wO6mlWq26SN.......PwWYaQ4udONsvUCl1Ybl47Z23FVeb3CmMASC7wc3CmMV8isp7ZFWzEeIITZ........3DUYaQ4Tc3rZJ+d6l+Iaa6ISPfihj3mecdm+4m+AA.......fQEEkSIsyt47qn778ZwFNVx2e9U80O4XVybFITZ........ppLt7YwJJmRZMNsFi5qex475W8isJW+5TPjDW65Wy08WjPoA.......H2TFen9ld9rXEkSIu7sLQW+5TH7C27+TdOiy67NuDHI........LZonbJ4M8omeW+5u3KrkDJIv+tm4u+oxq0We8SNl8ruvjHJ........LJMRQ4uaZFB3X4htnyOuV+55ri3.u2fITZfHd0ct6nqt1ZdMCW65........oGEkSIuLYxDe0Ee640L5niuSBkFHhW3Edg7dFt10A.......ROt50orvex4kemp7m9u+ohCe3rITZnZ1gOb139V4JxqYzRKyw0tN.......P9Yb4yhUTNkEx2qe8AFX+wS8zOaBkFplkD+7nK5hujDHI........IiaXgsk1QHWL87YwJJmxBIw0u9p9lOjSUN4sU8Men7dFKXAyOARB........4px1hxeuCbfzNBTjcoW1kkWq2oJm70+3O74iAFX+40LtgE1VTWc0kPIB.......fbQYaQ4c00VS6HPQ1rm8EFszxbxqY3TkS9Xsq4ak2y3O8Ru7DHI........jOJaKJmpSys0qLuVuSUN4p+we3ym2e.cpu9IGs15bSnDA.......PtRQ4TVYtW9kl2yXUeyGJNv6MXBjFpljDml7u1RtyDHI........juTTNkUpqt5hktr1yqYLv.6OV0pdzDJQTMHINM4QDw0dMKHARC.......Px5zaXJocDxEWX9rXEkSYmK8xtr7dFq9wVU7p6b2IPZnR2gOb1D4zjuzk0djISlDHQ........IqStlZR6HTzMRQ4+pTMEvnvrl4LhaXgsk2y4tW9xiCe3rIPhnR1S8zOahbZxW3B+RIPZ........HILRQ4uVplBXT5KsvaLumQWcs0XMqsiDHMTo5vGNarpu4Ck2yYoKq8nt5pKARD........IAW85TVJoNU4+Mei6vUvNep9lOxphAFX+48bbZxA.......JsnnbJakDmp7Hh3Zulq1UvN+dd0ct639V4Jx643zjC.......T5ortn7rYUtY0rj5TkOv.6OZ+dx+BQoxxi7HORhLGmlb.......nT2TZngzNB4hKHeVbYcQ489V8m1QfT1hW7hSj4r5GaUw266+zIxrn72S7sVarwMr97dNNM4.......P4fZFyXR6HTzUVWTNz3zZLV5xZOQl0W5u758dkS7Fu49huxe8hRjY4zjC.......TZRQ4T161tsEG0W+jSjYcsWyUGuwatuDYVTd5dRnqg+U+DqwoIG.......nDkhxorWlLYhk0dxTt4.Cr+3dtmUDG9vYSj4Q4kj5JWu95mbbsWyBRfDA.......Pgfhxohv0ecWSLu4mLEStwMr93Fa6lTVdUlWcm6Nwtx0evGdUQlLYRjYA.......PxSQ4TwX4Ke4I1r13FVe7MejUkXyiRaG9vYiq8Zt5DYV2vBaKZs04lHyB.......nXXB0dJocDxUSOWW3Gsn7WOABBjZZbZMFq9IVShMu6akqHV9ceOI17nz0suj6HFXf8mHyZwKdwIxb........JVpqt5R6HjqFWttvOZQ4+pDHHPpZQ2baI1UvdDJKuZvxu66IVWmcjHyZ0OwZhFmViIxr........nvwUuNUbV9xWdTe8SNwlmxxqb8O9Ce939V4JRjY0RKyIt1qI49PZ........PgSYcQ4C+AePZGAJA03zZLdvGNYeewUVdkmWcm6Nl2eVqI17t6UbuQlLYRr4A.......PgSYcQ480e+ocDnDUqsN2XoKq8Dcl22JWQrva7lhCe3rI5bo36Mdy8EW60b0I17V8SrlXVybFI17........nvprtnb3X41tsEmnuW4QDw55ri3FaSY4kydi2bewWXtyMFXf8mHyadyeAwht41RjYA.......jVRxm13hnIkqKTQ4TwJSlLwC9POXh++odiaX8w7m+UEuwatuDctT3c3CmMtm6YEIVI40W+jiku7kmHyB.......HMcQy9hS6HjKlTttPEkSEs5pqt36+zOahO2t5ZqwWXtyM9w+jsk3ylBiCe3rwM11MEabCqOwl4C9vqJZbZMlXyC.......fhCEkSEuYMyYDO2F2ThO2AFX+wbZ4him3as1De1jrJDkjuzk0dzZqyMwlG........EOJJmpBs15biU+DqofL6uxe8hhEdid2xKU8Fu49hlZpoDsj74M+ED21ss3Dad........Tb8QKJe6oUHfhgEcysEKcYsWPl855rinolZJd0ct6Bx7I27Fu49huvbmah8ljGwG9tj+fOzCFYxjIwlI........EWNQ4TUo81WdAqr7AFX+wez4Nq3AdnGofLeFc9G+gOehWRdDQroM+7Qc0UWhNS........xIiKWWnhxopSgrr7Hh3u4abGw7upqNdi2beEr8fism3as1Xd+Ysl3kj+babSQiSqwDcl........kBNqlZNsiPtX545BKqKJ+06o6zNBTlpPWV9F2v5iO2YdFwC7POh2t7hnCe3rwW61VR7U9qWThO6N+dOUzZqyMwmK........Eek0EkC4iBcY4Q7uc5xm+UE+3ex1Jn6Ce36Q9M11MEq9wVUhO6ktr1iq+5tlDet........jNTTNU0JFkk2UWaMlSKWbrva7lhC7dCVP2qpUeuu+SGety7LhMtg0m3ydoKq8n81WdhOW........ROJJmpds29xiN+dOUAeeVWmcD+gS7TccrmfNv6MXrva7lhuze40WPluRxA..9jd+CcnzNBiZ0TyXR6H.....kbTTNDQb8W20DO2F2TQYu9a9F2QzTSMEOw2ZsJLOO7899OcbgWvEDqqyNJHyWI4...GMGZnCl1QXT6zZngzNB....PIGEkC+aZs04Fu9+0eQTe8StfuWCLv9iuxe8hTXdN3U24ti4eUWc7k9Ku9XfA1eAYOTRN.......Tso1wWaZGgbwzy0E9QKJ+0Rff.k0ZbZMF6ZO6Il27WPQY+TX9ItC7dCFesaaIwez4NqBxaQ9HTRN.......TMp1ITVVT9Xy0E9QKJ+Wk.AAJ6kISl3oelmNt+G3gKZ64HElOtwM1X4288Duwatuh1dWp6.u2fwxu66I9Cm3oFq9wVUAcut+G3gURN.......PUfx5qd8rYc5aovYIK41hWZq+jhxUw9G08sxUDety7LhEdi2T7O9Ce9h5dWJ4iVP98sxUTv2umaiaJVxRtsB99........P5qrtn7B40uLDQDyd1WXrq8rm3FVXaE88dcc1QLu+rViIO4Sqp5Tl+p6b2wW61VRQqf75qexwN946LZs04Vv2K........JMTVWTNTLjISlXMO4ZhN+dOUQ+zkGwGdsrOxoL+y+4+SiG3gdjJtRyOv6MX7899Oc74+7+owez4NqB9Ur9Hl27WProM+7wrl4LJJ6G........kF9Loc.fxEW+0cMwEdAmWr5G6wiGe0Emhb+j5pqsFc02zDWD...f.PRDEDU0Vi+luwcDszxbhYeIsDM2bSwm+Rlcpjm7wgOb13mrssG63m8SKZEi+Q8UW7sGK+tWVjISlh9dC.......TpISMmbZGgb0jhHd2Q6hTTNLJTWc0EO727gh4ufED2a62czUWaM0xxHklOhaXgsEM07LhydFMGm64LyTKWGKuwatuX26o63E1x+Tp9zIz426ohq+5tlTa+A.......J0z3zZLsiPtZRghxghiYMyYDa4Eeg3IWaGwi8HObLv.6OsiTrtN6HhN63He8Mrv1hFlxTiFZngnwo1PblmwzJp44vGNarudeqXO6s63s6uu3427lR8+4TKsLm3a9HOR47uPO........I.EkC4gEcysEW60rf3Yd10GK9VukzNNeLq6iTZ9Hl27WPjISlnol+v2j6ydFMGQDwzZbpwXG6n+JHejxviHh8r2tigGd3n+9dq38NvAR0Sa+Qy8+.ObzVaKzUsN........erhx2dZEBnbVlLYhEcysEy8xuznyN+tw8eeqHsizmpibcmeTJQ+i5FVXaep+Xux1d4T+jgOZzRKyIt6Ubuwrl4LR6n........PIBmnbHgTWc0Es29xiEtvuTIeg4GOGsSid4Hmhb........NZNozN.4qcs68l1Q.9XFov72Yf2MV8Srln95mbZGopN2vBaK1wOemwRVxsojb........98T1WTNTppt5pKVzM2Vrq8rmX0OwZhVZYNocjp30RKyIdtMtoXMO4ZbUqC.......LJMu4ufzNB4hKLWVjhxgBrQdCy2xK9BwyswMcLe+uI2Te8SNV8Srl3Y9Gd1n0VmaZGG.......fxRUS2TuJJGJhZs04Fq4IWS75+W+EwRWV6tV1ySiTP9t1ydhEcysUU8KdC.......PtSQ4PJnwo0Xzd6KO10d1S7babSkqWiEoFEjC.......P9PQ4PJJSlLQqsN23oelmNdmAd239efG1aY9wvHuA481WeJHG.......fb1m4S70+2hH9CSif.U6pqt5hkrjaKVxRtsn280a7Bu3KEuxK+Sht5ZqoczRcKcYsGW5kcYwrl4LR6n........PEfOYQ4uannbH003zZLZbZMdjRy29OaGwN9oaO13FVeZGshlu5hu83O47N+3htny2IGG.......nHnlZFSZGgbwjxkE8IKJurSe80mSYJUzFoz7EcysEO3C8fwd26qEu1q0S7rO8SECLv9S63knTNN.......P54zZngzNB4hIkKKprun7gGd3zNBPQSc0UWTWc0Es15bi1ae4Qu6q2XOc2S7Nu8aG6dmuZY20z97l+BhlmwYGM0TSwrm8ElxoA.......fpEk8EkCUyF4zlOhrYyF6YOcG80e+w6ze+w9dy2njo775qexwEM6KNNqlZNlRCMDm8Y2rSMN........oBEkCUPxjISL6Yeg+dmN6cs68FC+AePzW+8Gu+gNTbngNX7dG3.EjRzm27WPjISl3zaXJwIWSMQyM2bLgZOknt5pKw2K........HWnnbnJvrl4LhHhO0q27d2WuQ1g+0G4q6qu9NtOqAM2byG4+blZN4O1IaG........JkonbfeuRtGoXc..F8xlMaz6a0+IzG7LfxOu2ANPZGgQsWumtimbscj1w.pZ0byMGMN0F77iA..PIlOYQ4uaDwEjB4...fxV8tudiW3Eeo3Ud4eRA4oMAf7w55TI4PofVZYNwEcwWRbdm+46CnN..PIqozPCocDxE4T+1GshxA..fS.adyaI5XsqQ43..bb0UWa8H+dFZok4Dscy2RzZqyMkSE...eb0Llwj1Qnn4jR6.jud8d5Nsi...PUlcs68FW20dcwUMuqTI4..Lp0UWaMtp4ckwburKO10t2aZGG...pJU1WTN...ESO4Z6HNu+3yI13FVeZGE..Jy0UWaMNu+3yIdjG4QS6n...PUGEkC..vIfrYyF24c70iEeq2RZGE..pvrz65Niq6ZutHa1rocT...npwm7MJG...9DxlMabKK5VbJxA.nfYjeeFq4IWSjISlTNM...T4SQ4...bb7nO5pURN..Ebi7623oelmNkSB..P0pL0bxocDxUSJh3cGMKvUuN...GCO4Z6Ht+6aEocL..nJwF2v58lkC..jZZbZMl1QHWMoQ6B9jEk+ZISN...n7Wu6qWuI4..Tzsz65Nics68l1w...fJZexhx+UoRJxCu2ANPZGA..fJTqbkqLsi...Uot21u6zNB...TQqr+pWuqt1ZZGA..fJPadyaw6RN..olt5ZqwSt1NR6X...PEqx9hxA..nPni0tlzNB..TkaKadSocD...nhkhxA..3SXaaa6t4h..H00UWaM17l2RZGC...pHonb...9D1wN1QZGA..HhHh+4c7yR6H...TkokVlSZGgbwzGsKPQ4...7I7rO8Sk1Q...hHh3wW8phrYyl1w...nJxoNwIl1QHWLtQ6B9jEk+qRnf...PYocs68FCLv9S6X...Gwd1S2ocD...nhymrn7WKURQdxmpV..fjR2c6aDM..kV5omdR6H...PEmJhqd8deq9S6H...Tg306QQ4..TZ4s6uuzNB...Twohnnb...RJtsh..nTyqrsWNsi....UbTTN...eDabCqOsi...7wLv.6Osi....k5F2ncAJJG.........hHh3zaXJocDxESezt.EkC.....ThaW6duocD...pRbx0TSZGghBEkC......v++r28aTVY48ci9emtx6bbiuSfNRYPlwAsqJLHjz9TTDyDhQhioAbhASaCD7Ooog.jj15iHh5xSLFMw7jm.FBz7jDiAg1pVMojQQR7b5QDkAeVUYbFkwLLEv2IaFdwYcNq047BefnF9yr2226809Oe97JlYee8a+cQHtf46955B.flJmphx+0U8TjQicrik5H......TwL24L6TGA...ZnzPrixGbngRcD.........f5DMDEkC..Pdo6tWXpi...79zVaSK0Q...fFNJJG..f2iKXJSI0Q...detxEbUoNB...Tqapk5BTTN...uGW5r5J0Q...deld6cj5H...zDoqtpK+4i8GUpKPQ4...7dzQ6sm5H...uOSe596m...PdSQ4...7drfEL+Dm...386JuxKO0Q...fFNmphx2WUOEYzwGarTGA..fFHe4Ut5TGA..Hh3c+6kTnPgTGC...Z3bpJJ+cp5oHidigFL0Q...nAxU+I9DoNB..PDQD+4yytIG..fJAG85...7ArfEL+n6tWXpiA..M4ZqsoE8zyhRcL...ngjhxA..3TXE27sl5H..PStuxZ9ZoNB...MgJzx4j5HTtlZo7vJJG..fSgd5YQ1U4..jLc28Bia4lWQpiA..PSnNmQmoNBkqoVJOrhxA..3z3NV+ck5H..PSJ+8P...nxRQ4...bZL24L63d+F2epiA..MYt2uw8GycNyN0w...fFZmphx2WUOEYzAGYjTGA..fFTqYMqJV1xcrmB.P0whWRuwZVypRcL...ng2opn72opmhLpu91Qpi...PCr66adewhWRuoNF..zfawKo2XCabCoNF...zTvQuN...mEEJTH1vF2fcVN..ULmnj7BEJj5n...PSAEkC..v3vIJK2cVN..4s68ab+wi7ydDkjC..Tyn6tWXpiP4XlkxCqnb...JAqYMqJd9+8cWu9OV..fZHc28Bim+ee2tSxA..p4bASYJoNBkiyqTdXEkC..PIZtyY1wS8Kd53w19iqvb..JYc28BiGa6Od7T+hmNl6blcpiC...Mk9PoN....TupmdVTzSOKJ14N2U77O+yG6Y2uPzWe6H0wB.fZPc28Bi47g+Hw7l27hErf4m3z....mphx2U0ND4gA1+.QmynyTGC..flPKXAy+j+.uGczQiAG7MhAGZnHhHd6ibj3HG9PIJY.4oCNxH0ceXXl9zaOt7qX9oNFPSmINoIGm+DmXDQDczd6QGcL8n0VaMwoB...duZX1Q4EG63oNB...Ds1ZqQqs1pcJFz.Z8q+tq6JJ+SbMex39+VeyTGC....nli6nb....Xbnd7zg3Bau8TGA....nljhxA........fSZhSZxoNBkiYVJOrhxA........fS57m3DScDJGmWo7vJJG.........Zpb5JJ+2VUSQNXricrTGA.........nNvoqn72pZFh7vfCMTpi..........TGvQuN.........zTQQ4.........PSEEkC.........mzjl3jRcDJGyrTdXEkC.........mzjlbcYQ4SnTdXEkC....iCEKVL0Q.....Hmb5JJeWUyPjGdk92api.....Mv1911ZpiPIqqt5J0Q.....pIYGkC.........MUTTN.........zTQQ4.........PSEEkC.........mzjmz4m5HTtl438AUTN.........bRs1ZqoNBkqya79fmthxeq7IGUOO2Ne1TGA.........nNPCSQ4CO7ARcD.........f5.N50A........flJenTG.fpqhEKFC75CcxudricrXvgF5264N9XiEuwPCVRy9RmUW+deuVZokniN53jecgVNmnyYzYIMW........HOonbnAwniNZbnC+1wgOzgiCejCGQDwqz+diHd2xw2911ZJi2oTasMs3JWvUEQDwzaui3bZokSVrtB0A..pk7h64kScDJKcdQsm5H.....0jTTNTG4E2yKexc.9aejiDG4vGJN3HiD802NRczJKCO7Ahg27ANiOyIJSehSZxw4OwIFczd6QKm64FycNytJkR...peUnPgTGA....pS0c2KrdrCpYFQrqwyCd5JJ+cxsnTEM5niFs1ZqoNFPY6E2yKexcD9aNzPwXicr55hvyCmoxz6t6EFWvTlRboypqni1aOl7jlncgN.......PN3BlxTRcDJGm238AOcEkuubJHUUG5vushxotwKtmWNFbvAiWo+9i8+ZuZScY3kqS2umsrkuhSVd9kcYcYWz........v6iidcnJYzQGM10u94iWo+9iu6C8foNNMz1xl2z66q6t6EFy4C+Qh4Mu4o3b........TTNTIMv9GHd5ewuLdtm8YriwSn95aGuue+ewKo23pulEEy+JlmSgB.......flPJJGxYEKVLdx+0mN94OxOU430n1911Zr8ss0Hh2c2l+YV5Mpzb.......flHJJGxICr+Ahs9XaKt26Y8oNJTBdu617ks7UDe7q9Zhd5YQINU........TI8GbFdsiV0RQN4vG5voNBzD5E2yKG25sbqwk9m7Gqj75baYyaJt9EecQmczQrwGdSwniNZpiD.......PRzRKmapiP4Xpi2G7LUT99xdNptN7QTTNUOmnf748m8gisr4Mk53PNZ3gOPrxuzsFWXaSMV+5uaElC..DCN3foNBkrt6dgoNB....TG6Bau8TGgxwTGuO3YpnbfSgQGcz3q8U+5JHuIw8dOqOtv1lZ7.Ov2NJVrXpiC..PhL1Xik5HTxtfoLkTGA....nlkhxgwohEKFa7g2TbgsM0369POXpiCUY21e+WKl6kcYwO8QdzTGE........xHEkCiC6bm6J9relaHV4W5VScTHgFd3CDK+u9yEK8ytzXf8OPpiC........kIEkCmAEKVL9Ze0udb0K7iF802NRcbnFw121ViK8O4ON13C6n2G.......ndzG5L7ZuSUKE4j29HGI0QfFHu3dd43tV2czvWP9xV9Jdee8jm7jiyehSJSy7MFZv3XG6Xm7qKVrXr8ss0LMyZQq7Kcqwqz+di66adeQgBERcb........Xb5LUT99hH5oZEj7vQN7gRcDnAwFe3MUWeLqehxuauiKJZokVhVZoknyKpiHhHl7jmTLkKn0Djpe9I+Uuvt2SDQDEO1whAGbnHhH5euubDQDO2Ne1X3gOP0Odkosr4MEO2Ne13wehmL5bFcl53........zL67FuO3YpnbnoSwhEi+tu9eWrkMWaejZ2VaSKtxEbUmb2e2QGsGEN2yM9He34j5nMt7dy4G6itfS4y7pu19iicrwhAd8AiwFarSVjds3+ayvCef3554Zi669evnmdVTpiC.......Pl0UWck5HTNtzw6Cpnb3+kA1+.wWcMqol5nV+DEh2dGWTLwINwnyKpiXFcdQwDlPi+w78kbwyHhH98J+ey+vePbziVL1+.u96qD8TuSzGd3CDW+hut3w19iqrb.......fZbJJGh289H+ubo2PRKZ8DkhOqtlczQGsGcdQcjniH8ZeSXBEhOxGdN+dkneziVL18ddoXvAGJ5euubR184JKG..ZL8lCMTpiPIahSZxoNB....PMKEkSSum3Idp35W70U0eeW7R5Ml8kMmn81aOlyk0kRwyASXBEhO1GcAm73bey+veP7pu19imaW+l3WuqmK1911ZUIGJKG..Z7L1XGK0Qnjc9SbhoNB....PMqyTQ4uSUKE4jCNxHoNBTmoZVRdasMsXo23mKtlq4ZpatKwaDbIW7LhK4hmQ7k9h2bbzi9ChmYm6J9EO8SUw2s4W+hut34+22cL24L6J56C........kt+fyvqsupVJxI0R2szT6qZURd2cuv3e7+wOM5u+9i69ttSkjmPSXBEhO8m5ZiM+C+AwucjCF+29uuwns1lVE686ubo2PLv9GnhMe........JOmohxgFVUiRx6t6EFa+e9Ihe0u5eK9q+KWZLgITnh99QoYJWPqwW5KdywANvaFa+e9Iht6dg496wvCef3tu66NJVrXtOa........JeJJmlNU5RxaqsocxBx+zepqsh89P94S+ot13W8q92pH6v7sussFe6u8CkqyD.......nRqyKp8TGgx07GOOjhxooRktj7+2uuuUze+8qf75Teou3MG82e+wJ+JqNWm68dOqO14N2UtNS.......fJoBEZrOsjUTNMMFX+CTwJIewKo23+3Ues3u+quFGw504lvDJDemu8CDa+e9Ix0cW9W7VtIGA6........0HNSEk+NUsTjiFX+Cj5HPMnA1+.w00SkYWd+e6+9Fis8X+73Rt3YTQlOowm9Scsw+5S8T41cW9vCefXSaZy4xr...p9N3Hij5HTxZokVRcD....fZVmohx2WUKE4nhic7TGApwTrXw3tu66NFd3Cjqyss1lV7+0K7hwW5Kdy45bo1wkbwyH111drXwKo2bYd21e+WyGlG..nNUe8siTGgRVGczQpi.....TyxQuNM7t665dhsusslqybwKo2n+96O9He34jqykZOSXBEhe3l9A4VY4OzC8P4xb........n7onbZnswGdSw28gdvbcl29ZWWrsG6m6tHuIxIJKOOtyx2xl2T7h64kygTA.......P4RQ4zv5E2yKGq7Kcq45L+G+e7Si69ttybclTeXBSnP7u9TOUtTV9+3l+g4Ph........prxidQRfoNddHEkSCohEKF205tibcla+e9Ih+5+xklqyj5KWxEOi39efuclmyV17lbWkC.......07txEbUoNBkioNddnyVQ4+1rmipq8t28l5HPMf69ttmnu91QtMus+O+Dwm9Scs417n90m9ScswJ+JqNyyYqO11xgz........P43rUT9aUMBAjm14N2Utdujqjb9fV+cttLeTibu2y5ihEKlSIB.......fRgidcZnTrXw3KdK2TtMOkjyoxDlPgX0e0udlmyy8b+lbHM...UZ0qWaNSdRmepi.....TyRQ4zP4a+senX3gOPtLKkjyYxW5KdyQ2cuvLMiGaqOZNkF..fJohic7TGgxRqs1Zpi.....TyRQ4zv3E2yKG268r9bYV29ZWmRx4r5lu0uXlV+121ViQGczbJM........Ld0vUT9wGarTGARjG5amO2K4q7qr53tuq6LWlEM19zepqMy6p7c8qe9bJM........DQbdimG5rUT99xgfTU8FCMXpi.IvO8QdzX6aaqYdNKdI8Fq+NWWNjHZVbsW2mJSq+W9zOUNkD.......f70kNqtRcDJGyb77PmshxembHHPEUwhEi6Y8Yub61ZaZwcdmqKlvDJjCohlEetkdCYZ8aeaaMJVrXNkF........FOZ3N50o4yO6Q2ZL7vGHyy4Nty6JtjKdF4PhnYxDlPgXkekUmoY7bO2uImRC........iGJJm5ZEKVL9NOv8m44rxuxpi+5+xklCIhlQy6xuhLs9+OddEkC..0xF6XGK0Q.....HmonbpqkG6l71ZaZtWxIS9nKX9YZ8+qO4imK4...nxXvgFJ0QnjsrkuhTGA....nlVC2cT9ysymM0Qfpj7Z2juwevlbujSlj0ie8gG9.w.6efbLQ........blb1JJeeUkTjixi6pZpOjG6l7U9UVc7w9nKHmRDMyl4r5JSq+k1a+4TR........3rwQuN0kxicSdasMsX0qdU4ThnY2btrrUT9qzuhxA......fZKszRKoNBkioNddHEkSco7X2ju5u5WOlxEzZNkHZ1cIW7Lh1ZaZk85cOkC.......0Z5niNRcDJG+QimGRQ4TW5odhrUpX2cuv3yszaHmRC7tt1dttxdsCO7AhQGczbLM........b5zPVTdwhEScDnB5IdhmJ5qucjoYby25WLlvDJjSIBdWSu8r8opZvAeibJI........blb1JJ+spFgHuMvqOTpi.UP+a+xmNSqu6tWX7o+TWaNkF324xlc1tmxGbH+2t..fZQuR+6M0Qnjk0ODm....PitFxhxow0niNZrkMuoLMia9V+h4TZf2uIO4Iko0+lJJG..HmbNszRpi.....TSqg7nWmFWacqaKSq2tImJoobAslo0u+W6Uyoj........vYhhxotxy8rOSlVucSNUZKa4qnrWae8sibLI........b5nnbparyctqLUjncSNUCSdxSNSqef8OPNkD.......froyKp8TGgx07OaOPCYQ4CN3foNBTA77O+ymo0esW2mJmRBb5c9SLa2S4EG634TR........xlBEJj5HTwLdJJ+WWwSQNarwFK0QfJfG8Q9IYZ8W6m7Zxoj.mdSZRYqn78t28lSIA..HuTrXwTGA....fbVC4NJmFO6bm6JFd3CT1qekekUGS4BZMGSDbp8GN4rUTN..Psmsussl5HTx5pqtRcD....fZZJJm5BY8XW+SbM1M4TcbtmaKYZ8uR+1Q4........UZJJm5BY4XWus1lV7w9nKHGSCb5cIW7LRcD........3rngrn7i6NJugx.6efLcrqeS2xWLGSCTY49uD.......nxa7TT9aUoCQd6MFZvTGAxQ652jsic8O4h9D4TRfwm1ZaZk8ZqGu+KA.......pwbdmsGngrnbZrjk6r41ZaZNJrop6JWvUk5H........jKV1xWQpiP4XlmsGng7nWmFGEKVL1xl2TYu9kdietbLM...PylQGczTGgxRgVNmTGA....nllhxol1K8Rk+tIOhHl2ke44TRfpm50eXr..PinCc32N0Qnrz4L5L0Q.....poonbpo0e+8mo0+gmykkSIApdpW+gwB.......TuX7TT96TwSQN6414yl5HPN4MFZvxdsKdI8FSXBExwz.........zHX7TT99p3oHmM7vGH0QfbRVtexm8kMmbLI.........MJbzqSMqA1+.YZ8c00rxoj.kl163hRcD........3LPQ4Ty50G7Mxz5c+jSpzRKsj5H...jSF6XGK0Q.....RpVZ4bScDJGS8r8.JJmZVuwaLTYu1t6dgtexIY5euubpi...PNYvgJ++cIoxxV9JRcD....nAxE1d6oNBkiod1dfF1hxGczQScDHidigFrrW6ELkojiIA........nQx3on78UwSQEvgN7am5HPFcvQForW6r5Z14XR.........ZjLdJJ+cp3o.NE5qucT1qcRSZR4XR.........ZjzvdzqS8sA1+.YZ8+gSVQ4.........mZJJmZREG63YZ8ynyKJmRBT8UnkyI0Q........nglhxolzgOzgyz5mvDJjSIAJcGbjQxz56bFclSIA..Hqdk92apiPIa5s2Qpi.....Tya7VT9ushlhJf8t25ueXF76b3iT9Eku3kzaNlDnz0We6H0Q...nI14zRKoNB....z.YRSrt7JOdlmsGX7VT9aksb.UOEJX2jC........4gIM45xhxmvY6AbzqSMoiO1XoNBPYYjCNZlVe2cuvbJI........b5nnbpI8FCMXYu1163hxwj.klCcnx+ZCHhHtfoLkbJI........b5nnbZ3zh6iORnhG6XoNB........bVLdKJ+cpnonB3MGZnTGAflPCNX19u8L816HmRB..PdXKadSoNBkrNZu8TGA....nl23sn78UQSQEvXiYWcBT8M1Xiko0eNNQD...xnVN2yM0Q.....p44nWGfbzPC95YZ818O........UdJJGfbzAGYjLsd69G.......nVxjmz4m5HTtl4Y5EUTN.4n95aGYZ8cdQ1Q4.......PsiVas0TGgx04cldwF1hxy5t5DfR0Kr68j4YTnPgbHI...4ghEKl5H.....TgLdKJeWUxPTIj0c0I0uFarwRcDnI0+4gNblV+xV9Jxoj...jGF30GJ0QnrL24L6TGA....nlWC6NJm5aW5r5prW6PC954XRfwugFJa+fTm3jlbNkD........NSTTN.4jr9gz37m3Dyoj........vYhhxogi6QPRksr4Mko02UWk+Io.........ieMzEkqvz5Wczd6k8Z2911ZNlDX74Ues8m4Yz4EU9+4d........F+FuEkuOtJdkC...H.jDQAQkJZJpPF30y18ELoSKm64lo0ezi5CIAUW64k1alVe2cuvnPgB4TZ...xCG9PGN0Qnj0VaSK0Q....fFPc28BScDJGy7L8hi2hxembHHv31jmz4mo0u+Ax1cEMTpdy2HaevblwEeI4TR...xKG9H0eEkekK3pRcD....nAzELkoj5HTNNuyzK1PezqS8qVas0Ls9+y5vc9A0218K7BYZ8W5rlUNkD........NaTTN0rxxQ3vgOrhxo5YjCNZzWe6HSyniN5HmRC........mMMzEkO1wNVpi.YPVNBG5euubNlD3LamO2uNyyXtyY14PR........X7nTJJ+2VwRQExfCks6LXRqKcVcU1q8414yliIANy1W+6MSqeYKeE4TR........X7nTJJ+spTg.NUlzDmTYu1gG9.wq9Z6OGSCb58jOwimo0mkOTH...UNuRF+.QlBSucWoO....v3QC8QuN02tnNldlV+ddo5uenVT+4W8L6LFd3CjoYL+Ked4TZ...Z1cNszRpi.....TWPQ4TypyYzYlVeVONrgwim+27axz5aqsok4+rN.......PkTKsbtoNBkiodldQEkSMsrb2Mm0iCaX7X2uvKjo0+Iu1qKmRB.......PkwE1d6oNBkiodldwRon72Ia4n5qd79ji2urb2MO7vGHdgcumbLMv62q9Z6O5qucjoY7mOuKOmRC........iWkRQ46qhkB3znqtJ+hxiHhc8qy1whMbl7y+4aMyy3JuREkC..0pN3Hij5HTxZwcTN....Lt3nWmZZycNyNSqemOSe4TRfeeOxO8mjo0u3kzaTnPgbJM...4srd5AkBczQGoNB....PcAEkSMurbOk2We6vwuNUD+pmYmwvCefLMiq9ZVTNkF........JEJJmZd+Wx3c37S+zOcNkD324WjC+4p4eEyKGRB........kpF5hx2xl2Tpi.4frVl3i7S+IwQOZwbJMPDibvQiG567fYZFKdI8Fs1Zq4Th........nTTJEkuqJUHfyjVas0n6tWXYu9gG9.wyryckeAhldO4+Z12M4Weu2PNjD..fJkhE8gsE....pycdmoWrgdGkSiiq7p9nYZ8O7F994TRfHdxG+eIyy3JuxrckB...TYMvqOTpiPYYtyY1oNB....z.pqt5J0QnbbomoWTQ4TW3Z9DWclVee8si3E18dxozPyr+o+kmL5qucjoY7kW4piBEJjSIB.......fRkhxotPmynyXwKo2LMiMsI2Y8jc+hm9ox7Lt5OwmHGRB........kqF9hxGX+Cj5HPN4pulEko0ukMuI6pbxjWX26I1xly1G3ht6dgwBVv7yi3........PYpTJJeeUrTTAUbrim5HPN4Z+jWSlmgcUNYwO+m+yy7LVTOWWNjD..fJswN1wRcD.....pfJkhxemJVJfwgBEJD21sutLMisr4ME+pmYm4ThnYxHGbz3g9NOXlmyhtlqNGRC..Pk1fCMTpiPIaYKeEoNB....PciF9idcZrjG2syequ42LGRBMa1zl9gYdF21sutn0VaMGRC........Yghxotxbmyrit6dgYZF802NhezO9QxoDQyfWX26Itm6d8YdN4wGzC........xtF9hxGbvAScDHmsha9Vy7Ltq67NhQN3n4PZnYPdb21urkuhXtyY14PZ........pt57hZO0QnbM+S2KTpEk+ayVNp9FarwRcDHm0SOKJy6p7gG9.wC9fe6bJQzH6E18dhsr4rWT9G+pulbHM...UKG2+VR....3jJTnPpiPtqTKJ+spDg.JUKpmqKyy3g9NOX7qdlclCogFYOvC7.YdFc28Bid5YQ4PZ...pVdigp+NcxtzY0Upi.....T2ng+nWmFSe1an2ns1lVlmysbSqHN5QKlCIhFQ+ne7iDaeaaMyyIOtt.........H+nnbpKUnPg3qrluVlmyvCefXc245ygDQilidzhwccm2QlmicSN.......PsmF9hxeygFJ0QfJjO6Mzaluqxi3cOB1+Q+3GIGRDMR9VOvCFCO7Ax7braxA.......p8TpEk+NUjTTAM1XGK0QfJjBEJjakP94+qtw3Ues8mKyh5euvt2SbO2c1OoAraxA.f5WEK5JZB....ZjUpEkuuJRJfxTO8rnXwKo2bYVq5qrJ2W4DG8nEi6XsqMWl0cr96JWlC..P02121VScDJYc0UWoNB....PciF9idcZ7sxUs5bYN802NhUuluZtLKpesgGdSQe8siLOmks7UDycNyNGRD.......P50VaSK0QnbL0S2Knnbp6M24L63Kux7or7sr4MEq8NtybYVT+4E18dh+g+t74CKwJW4Jyk4........TK3JWvUk5HTNl5o6EZ3KJ+414yl5HPUvZuiaO29TrbO285iezO9QxkYQ8iidzhwm8F9L4xrtsaecQmynybYV........j+Z3KJe3gOPpi.UAEJTHt80s9bade9+paL9m9WdxbadT6a0q4qlK+2KZqsoEqZU1M4..P8rhEKl5H.....TgUpEkuuJRJfbvMtzaHV1xWQtMuE+WzixxaR7899ObrkMuobYV228+fQgBExkYA..jFC75Ck5HTVl6blcpi.....T2nTKJ+cpHo.xI+Wu8+q41QvdDJKuYvKr68D+s+M2RtLqks7UD8zyhxkYA.......PkSC+QuNMWZs0Vi669evbclJKuw0q9Z6O2tWxi3c+fZ........PsulhhxGX+Cj5HPUTO8rn3KuxUmqyTY4MdN5QKF24ct9b4dIOhHdnu2FhVas0bYV........TY0TTTdwwNdpi.UYq8Nt8n6tWXtNSkk233nGsX7EVwMEaeaaMWl2xV9Jha4lWQtLK..fz6vG5voNBkr77JnB....ZFTpEk+VUhP.4sBEJDeqG3Ax8eXQK9unm3G8iejbclT88sdfGL2JIus1llibc..nAygOR8WQ4W4BtpTGA....ZvM816H0QnbL+S2KnnbZX04L5L2uuxiHhO+e0MFq8NtybetTcr163Ni64tWetMu669ePG45.......PCuyokVRcDxUMEG85z7pmdVTbuei6O2m68b2qOV9W3lhidzh49roxIuKI+1t80E8zyhxs4A.......P0QSQQ4CN3foNBjPqYMqJ9xqb049b2xl2Trjkb8wq9Z6O2mM4u7tj7EujdiUspUlayC.......fpmlhhxGarwRcDHwV6cb6whWRu49b6qucDexEsn3e5e4Iy8YS9IuKIus1lVr10t1nPgB41LA..pc7J8u2TGgRVc58DG....jLMEEkCEJTH1vF2PEor7gG9.wh+K5IV6cbmNJ1qwbziVL2KIOh28dIuyYzYtNS...xhFs6IN....nRqbJJ+Wm6o.pBJTnPbeey6KZqsoUQl+8b2qO9Bq3lbTrWi3nGsX7EVwMk6kj+PeuM3dIG.......nNmcTNMUZs0ViG+IdxJVY4aeaaM9iujKN9de+GthLeFed0Wa+wWXE2Tr8ss0bct21sut3Vt4UjqyD.......fpulhhxeygFJ0QfZHcNiNqnkkGQD+s+M2Rrjq+yX2km.+pmYmwmbQKJ2KIewKo2XUqZk45LA..pMUrnqTI....nAw4c5dglhhxGarik5HPMlpQY4u2cWt6t7piuw27AhE18UECO7Ax04t3kzargMtgnPgB45bA..pMk2enKqF5pqtRcD....nAWc5+1yK8z8BMEEkCmJmnr7EujdqnuO+s+M2Rrjkb8wu5Y1YE88oY1HGbzXIW+mI9G969p49raqsoojb.......fFLkSQ46K2SAjHcNiNiMrwMTwKKuu91QrvtupX4egaJdgcumJ56UylezO9Qh4eEWQEYW+zVaSKd7m3IURN.......PClxon72I2SAjPEJTH1vF2P7kW4pq3uWaYyaJ9S+HyMV6cbmwHGbzJ96WirQN3nwx+B2T74+qtwb+nVOheWI4cNiNy8YC.......PZ0Tbzq+b67YScDnFWgBEh6+a8Mi68ab+Uk2u64tWe7GMkKPg4kou22+gi4eEWQrkMuoJx7URN..z7Zf8OPpiPYYxS57ScD....f5JMEEkWI1sozXZMqYUwis8Gup89ov7RyKr68DerO1GO9a+atkJ1++5t6dgJIG..ZhUbrim5HTVZs0VScD....f5JMEEkCkhd5YQwq7+7+H5t6EV0dOOQg4ekUsF2g4mBuvt2Sr7uvME+oej4F802NpXuOKdI8F+re9ipjb.......fFbtixgSgNmQmwO6m+nUk6s72qG567fwe5GYtwRt9OS7O8u7jwQOZwp56esl2aA4UpiY8SXwKo2XCabCQgBEpnuO........jdkSQ46K2SATC5D2a4a9G8Sh1ZaZU026sussFK9unmXVyZVwZui6Ld0Wa+U02+T6W8L6rpUPdDQba295hG4m8HJIG..HN7gNbpiPIqZ+uWA...flSEZ4bRcDJWS8T8MaZN50GX+Cj5HPcpabo2P7L6bmwxV9Jp5u2CO7Ah64tWe7GeIWb7w9Xe73688e3F16x7QN3nwO5G+HwG6i8wiE18UUUJHOhH17O5mDqacqsp7dA..T66vGo9qn7qbAWUpi.....MApiu5Zm5o5a9gpxgHYJN1wScDnNVqs1ZrgMtg3+x7t73dV+5hgG9.U8LzWe6H5qucD+s+Mu6wD9rur4DexE8IhK4hmQUOK4kidzhwyrycEO+u4WGOz24Aqpu2s01zhu+F+AwBVv7qpuu........jdMMEkC4gabo2PL+qXdwC8c9tw28gptE69ds8ss0X6aaqw+ve2WMZqsoEK8F+bwLmUWwbtrthobAslrbMdLxAGM1yKs2jTN9Ir3kzabeey6KZs0Z6euB.......fJCEkCknVas039+VeyXI81a7Pe6GL1911ZRyyINd1Ogt6dgwG9i7QhKb5sGy4x5J4637QN3nw.u9fwd2a+wK+R6I4+9089Mt+XMqYUIMC........jVkSQ46K2SATGZtyY1wi7ydj3568FhM8vaH5qucj5HEQ76Nh1euV7R5M5ryNiyehSJ5ni1iBm64FejO7bx826WX26IJdriECN3PQ+68kiCNxH0L+9RasMs3G+HOZL24L6TGE..fZXuR+6M0QnjM816H0Q.....p6TNEk+N4dJpB16d2qBxnhnmdVTzSOKJ13Cuo367.2eRt+xOaNS6h6ks7Ubxe8r5Z7++G4MFZv3XG6XQDQrkMuoxObUAe4Ut5Xs2wsGEJTH0QA..fb24zRKoNB....PcGG85PN4Vt4UDe1an23m8naslsv7Sk2WI203EdWpZqsoEe+M9ChErf4m3j........Psj+fTG.nQRgBEha4lWQ7huzKEa9G8Sht6dgoNRMst2uw8Gu3K8RJIG.......feOJJGp.JTnPbiK8Fhm5W7zwis8GWg4UQKa4qHdk+m+GwZVypbTqC..Txdtc9roNBkrIMwIk5H....PSh1ZaZoNBkiodp9lkaQ4Gs7yAzbomdVT7T+hmNd9+8c+9tOvIe0c2KL9k63YhMrwMDcNiNScb...pSUubEJ8dMoIqnb....pNtxEbUoNBkiodp9lkaQ46q7yQZ7J8u2TGAZxM24L6XCabCwaN7aE26239qW+D2Tyo6tWX7Xa+wim5W7zNl0A.......XbwQuNTk0ZqsFqYMqJdwW5khGa6OtcYdY58VPdO8rnTGG........pinnbHQJTnPzSOK58sKycWle1srkuBEjC..TQL5nil5HTVJzx4j5H.....0c9PoN..+tcY9ZVyphA1+.wVersEO5i7SpKueDqDZqsoE2vR+bQuW+Rb+iC..TwbnC+1oNBkE+cjA...fRmhxgZLcNiNi0st0FqacqMdw87xwy+a9Mwy8rOSzWe6H0QqpaYKeEwG+pul3JuxKOJTnPpiC........MHJ2hx2UDwUji4nhaKadSwF13FRcLfRxbmyri4NmYexcZ9Ks29i+Oe9eSrkMuoTGsJlEujdiq9ZVTL+qXdQqs1ZpiC........Mfrixg5DcNiNiNmQmwMtzaH1vF2Pryctqn+96OdigFrtt371ZaZwm7Zut3OedWdL6YOSkiC..jLicrik5H.....TknnbnN0BVv7iErf4GQDwF13FhWbOubr28t23MGZnX+u1qVydTs2c2KLlwEeIwkNqYEWVWyx8oH..PMiAGZnTGgR1xV9JRcD....nIRKsbtoNBkiodp9lJJGZPbhio8SnXwhw.u9PwfCNX71G4HwaLzfwAGYjpZA5Ka4qHZokyMtv1aO5pqthIOoy2NFG.......nN0E1d6oNBkiodp9lMUEkWrXwnPgBoNFPUQgBE98JO+DFczQiCc32NF6XG68sqYdk92aI+9boypqS9q6pq28W24E0t++Z........TypbKJ+sxyPTsLvqOzorzPnYSqs15I2Y2KXAyOkQA..fZFGerwRcD.....pR9CJy08V4YH....fT6MFZvTGgR168DdB....X7qbKJG.........ntjhxA........flJMUEkO1wNVpi.....0nJVrXpi.....PURSUQ4CNzPoNB....TiZ6aaqoNBkrt5xcTN....TNJ2hx2UdFB.........fJfy6T8Map1Q4.........P4oN8jM6ROUeSEkC...PSO2O4....Pyklphxe6ibjTGA....pAMvqOTpiPYYtyY1oNB....PcolphxOxgOTpi..........jXYon7eatkB.........fpjrTT9akWg.....RowN1wRcD.....phZpN50A...fSkAGp96NJeYKeEoNB....PcqlphxO3Hij5H..........IVSUQ4802NRcD.........fDqopnb....3T43iMVpi.....TyaxS57ScDJWy7C9MxRQ46JCqE....pY7FCMXpiPI6RmUWoNB....zjo0VaM0QnbcdevugcTN.........zTooqn7A1+.oNB.........PB0zUTdwwNdpi.....0XN3Hij5HTxZokVRcD....f5VMcEkC....eP802NRcDJYczQGoNB....PcqrTT9akWg..........nZQQ4.........PSkltid8AGbvTGA....pgLv9GH0QnrL4Ic9oNB....PcqlthxGarwRcD....nFRwwNdpiPYo0VaM0Q.....pWbdevuQSWQ4.........P4YYKeEoNBkiY9A+FJJG.........Zpjkhx2UdEB....HU16d2apiPIawKo2TGA....ntVS2NJ+U5u96G.B....7dUnPgTGA....ntVSWQ4.........PyMEkC...PSsiO1XoNB.....UYJJG...flZuwPCl5HTxtzY0Upi.....TWKqEk+aykT..........Tkj0hxeq7HDUSaYyaJ0Q..........RHG85....zT6fiLRpiPIqkVZI0Q....flTSbRSN0QnbLyO32PQ4....zTqu91QpiPIqiN5H0Q....flTm+DmXpiP4379feCEkC.........MUxZQ4uStjhprhEKl5H..........IRVKJee4RJpxF30GJ0Q....fZ.Cr+ARcDJKSdRmepi.....TWyQuN....MsJN1wScDJKs1ZqoNB....PcMEkC.........MUTTN....MsF6XGK0Q.....HAZJKJ2OHD....hHhAGZnTGgR1xV9JRcD....f5dYsn78kKonJqd7GDB.........jOxZQ4uStjB.........nl2jl3jRcDJGy7C9MZJO50A...fHh3MqCOwwl3jlbpi.....MwlzjqKKJeBevughxA...no0XicrTGgR14OwIl5H.....08ZJKJ+sOxQRcD.........fDoo7NJ+HG9PoNB....TCnXwhoNB.....IPVKJee4RJ....fDX6aaqoNBkrt5pqTGA....ntWS4QuN.........z7RQ4.........PSklxhxO3Hij5H.....kkNun1ScD....f5dMkEk2We6H0Q.....JKEJTH0Q.....p6kGEk+J4vL.....NKV1xWQpi.....M4l7jN+TGgx0LeueQdTT96jCy.....3r3RmUWoNB....zjq0VaM0Qnbcdu2uno7nWG....hHhEujdScDJIc0khxA...f7PSaQ4Cr+ARcD....Hw53h5L0QXbqs1lVL24L6TGC....ngPSaQ4EG63oNB....jXW3zmdpiv31Jt4aM0Q.....ZXjGEk+V4vL....fpt4eEyK0QXbq2dWRpi.....zvPQ4....zzp0Vast3dJ+1t80Es1ZqoNF....PCil1idc....HhHt9dugTGgyn1ZaZwxW9mO0w.....ZnzzVT9fCNXpi.....0.5omEEc28BScLNst80sd6lb....Hm0zVT9XiMVpi.....0HV8W8qk5HbJ8kW4piabo0163c....ndTSaQ4....vIrfEL+31t80k5X79r3kzar1631ScL....feO0xmLamAy789E4QQ46JGlA....jTqZUqLV7R5M0wHh3cKIeCabCQgBERcT....feOWvTlRpiP437duewGJUo.n1yKtmWN2l0jmz46dTD..ntRgBEhMrwMDQDw121VSVNTRN....T40zVT9qz+dScDfJlhEKFC75CEQDwfCNXL1XiEQ79+y8GbjQh95aGU0bsrkuhS9qmd6cDmSKsDQDQGs2dzx4dtJWG..H4RcY4e4Ut5Xs2wsqjb....nBqosnbnd1niNZbnC+1wgOzgiCejCGu4PCEiM1wRR42khsr4MMtdt1ZaZwUtfqJZokyMtv1aOZokVhN5nCEoC..TUbhxx63h5Lt26Y8Uk2y1ZaZwsut0G23Rugpx6G....zrSQ4PMnA1+.QwwN9ob2fOdKatd1vCefX3MefS6qu3kzaL4I+GFWX6sGczd6wjmzDiNmQmUwDB..znqPgBw5V2Zi4Mu4EO3259qnefTusaecwpV0JsKxA...fpn7nn78kCy.ZJchiH88t28Fu4PCEG5P+mI8tPrdwo62iV1xWQL816Hl9zaOtnNltxyA..xrErf4GKXAyOdhm3ohM8vaH2JLus1lVbCK8yEKe4edmZR....PBjGEk+N4vLflBEKVLdtm62D6ae8G6Y2uPM8wjd8nO3ts+DGg6W5r5J5pqth4NmYmnjA..TuqmdVTzSOKJdw87xwu7W7KJq+97mnb7YNyYE8zyhpPIE....X73+sbZN++kSyop5+6+e9+M0Qfl.iN5nwt90Oe7Ke5mxtEOwZqsoEexq85h+74c4wrm8Lsyc...xjQGczXvAeiXvgFJh32ccIcBW5r5JhHhNZu8niNlt+9m....zv3Vukasd75Bd8QD24I9BEkCU.EKVL9YO5Vim5Idb6Z7ZXKdI8FW80rnX9Ww77CsD.......XbpQnn773nWG3+kSbLLdu2y5ScTXbX6aaqmbW9u3kzab88dCwUdkWdTnPgDmL........xYm268KxqhxOZDwDxoYA0cdhm3ohGaqOpiV85Xmnz7SbuQt7k+4sKyA.......NENw0MVclY9d+h+fbZn6KmlCTW4IdhmJVzm3ZhqewWmRxaPL7vGHt26Y8wE11Tiu1W8qGu3dd4TGI........xY4UQ40kT.FkqctyccxBxcGj23569POXLu+rObbq2xs5+dA.......PCjl5hxgR0.6ef3VukaMt5E9QUPdSjsr4MEy6O6CGqe82cL5nil53........PFonbXbnXwhwC7.e63R+S9iisr4Mk53Phbu2y5iO5BVPrwG1eF........ndlhxgyhWbOub7Y+L2Pba+8esTGEpAL7vGHV4W5Vik9YWZLv9GH0wA.......fxvGJmlythHthbZVPMghEKFaZSatouf7EujdiBEJbFelCNxHMcGE8aeaaMd4WZOw8c+OXzSOKJ0wA.......fRPdUT9++r2caTVY888B+e1ldRWqZF7bV8rhRGsNj.Nn8tJff1SWFARlPSDESKHwpMQkfOjSOQQ7gVqJhRRyCpQ6IGgHBwDCQQvJPPMDL7fz6VUjA7rJv3L5rQbJfq608cY1LtV8rNmU68KlhAM7vv955Z+eu2ymOuZd55202kLIu467+2+5Rs2d6w3F6XRcLnFTG6ri39tu6KV9xVZpiRl0VaSJN0S6zhHhXTi9W966m6XF8G3m67Ouwl6u6d6sbryNdi2+y+m1ydi8t28FQDw6tu8F6YO6IhHpaWm8kJ0cbYS8Ri63NmSLqYciGy+fB........n1vf5hxgCmUtxUG29sdyQoRcm5nLfbvS78AKA+fEfOxVOiXHCIsE2Njgzz.p.9E8XOZDQDaeG6LNvA5K53M5L5qu9h2rqNiCbfCDqec+hZ5+83aLu4Fc9FcDyeAyWY4........0ATTNbHdfG36Vytp0ulYLyXnCcnwG+jOk3bGynigNzSINsSs4TGqb0YcliLh3He51e4WYyQ4CbfnyN658OQ50JknevsOvccW2Uz5HaMwoA.......fiFEkCQ+2G429sc60Dq.71ZaRwYdVmU7IG9HhQLhgGsdFingqP7J0AKP+y9Yl3G3qevU79qsk1i2rqNicr8smj6L8CdukuhUtJkkC.......MrNkS9TRcDpDmyg9I4UQ4aKmlSU060WeoNBTCnb4xwMb82PRtOxaokgESXhe5XTidLw4NlQWSrtzqGcvU79gdRz286zSzwazYzd6aMV2Kt1pVw4kJ0cboS4RhWbcqKZtY+AN........MdNkgVWVT9PNzOIuJJe+4zbppdyt5L0QfDqic1QbKyd18QmHv....B.IQTPTUsRTaokgEWwU9mEmynFcz5YL72eUiS96zN0liS6TaN9relIF+E21ric+N8Dqa8aLdtU+SK7+nHJUp631usa2cVN.......PMJqdcFzpic1QboS4RpJ2u0ScZSO9hW9eZ7m7EtjB+cwg2ocpMGW0W5Jhq5KcEwtem6OV0O84hUshmsv9ij3fkwujexRJj4C.......Pk6WK0A.RgpUI4WyLlY7O7xuZrrm9oTRdMjS6TaN9y+pWW7y+4+rX4+sqLtlYLyB48r7kszXty89JjYC.......PkSQ4LnS0nj7CVP9hdrG8CbuYSsm+juvkDK5wdz3e3ke0XpSa5497+FyatwJW4py84B.......PkKuJJut7NJub4xoNBTkUtb43Vl8rKrRxaqsIEK+uckJHuNz4ediMV1S+TwO3G9iiVZYX45ru8a8lid5ombcl........T4xqhx2VNMmppCdGByfCkKWNtgq+FJr6j5+5u08GKaYOsUrdctq5KcEwF13Fy00wdoRcGe8480ys4A.......P1X0qyfF29sc6ExebDs01jh+wsui3u31lcLjgzTtOep9NsSs4XQO1iF+2+erfbalKdQKzJXG.......nFghxYPg4N26KV7hVXtO267tlSrrk8zwYcliL2mMo2e9W85hk+2txbad29sdytxG........pAnnbZ3sxUt53aLu4lqyrkVFVr7+1UF228dONE4M39S9BWRtUVdoRcG+jmzU9........PhL9C9AC5KJ2o6rwVG6ri3xl5klqyrs1lT7SW8pcWjOHRdVV9C8.eG++6........00F5o7wScDxr7rn7Mliyppoi2nqTGAJHkKWNtkYO6bclScZS2pVePp+juvkD+fe3ONyywoJG.......p20byMm5HjYC5OQ4z359t24EqcsqI2l2MdS2b7XK7Qsp0GD6p9RWQbi2zMm443TkC.......jVJJmFRqbkqN9ad3GL2l2cdWyIdnu6Cnjbh4dOyIZqsIkoYTpT2wp9oOWNkH........NdonbZ3zSO8D29sl8S86Acm20bh66dumbadTeaHCoo3duu6Kyy4oVR1Wi6........TYFzWTdeG3.oNBjy95y6qGkJ0ctLKkjygy4ediMyqf80t10DqacaHeBD........GWxyhx2VNNqplN6pqTGAxQqbkqNV7hVXtLqoNsoqjbNht4adVYdFaZSaJGRB........Guxyhx2eNNK33V4xkysUt9Tm1ziGagOZtLKZLcZmZywcdWyISy3IWxSDkKWNmRD........CTC5W85z3369ce3bYkq2RKCKdfG39igLjlxgTQirYNyuRld9Rk5NV+5eobJM........bLL9C9AJJmFBu5l2R7Ml2bykY8SW8piS6TaNWlEM1xiSU9111Vyoz........v.0f9hxeu95K0QfbvOXQOV9Lme3ONNqybj4xrXvgu3Wb5Y54exk7D4TR........XfJOKJea43rpZdyt5L0QfLZcqaCwhWzBy7btwa5liq5KcE4PhXvjy5LGYL0oU4kkWpT2w5V2Fxu.A.......TEjk9QpEjmEku+bbVv.1Cd+emLOi1ZaRwbumrsBsYvqKZxWbld9stUqec.......n9RSM0TpiPlLne0qS8sUtxUGqcsqIyy4duu6KFxPpu+eLS5LwIbgY54aeKuVNkD........FHTTN00V32e9YdF+0eq6ON+yar4PZXvpS6TaNSqWjkurkF8zSO4Xh........3nYPeQ4kKWN0QfJTdbZxaqsIE2v0MybJQLX1XN2r8GawV1x1xoj........vwRdVT9txwYU0r7kszTGApP4woI+VtsayJWmbw3uvOUld927M6JmRB........GAm9A+fA8EkS8o0stMj4SS90LiYFe1OyDyoDwfcYc886dJG.......nvc5G7CFzu50o9zxd5ruI.lybt6bHIvuz0LiJeM9a6V........T8nnbp6zSO8DKdQKLSy3u9ac+wocpMmSIB52vGwYjom+U27Vxoj........vQihxotyhVzOHSOeKsLr3FttJ+j+BGICe3COSOemc1YNkD........NZx6hx6MmmWUgSwY8ixkKGO4RdhLMia9VtsXHCoobJQvuTqmQ1JJ+sdy2LmRB.......Pw5rG0nScDxj7tn7skyyC9.V+5eonTotq3mukVFV7mcEWdNlH3W5rNyQlomee6cO4TR........3nwpWm5JO8RexL87W60+UcZxoPM0oM8J9YW7hVXNlD........NRTTN0M5omdhkurkloY7md4UdIlv.QSMks+PL5omdxoj........vGxIcvOPQ4T2XCabSY54uy6ZNwocpMmSoAN7F0nGSld98r22MmRB........eHm8A+f7tn78myyqpn81aO0QfAfmZI+3L87WzEcQ4TRfhSmc1Ypi........PCu7tn7skyyChHhnic1Qr10tlJ94m5zldb9m2XywDAGdm6XFcld995qubJI........bjX0qScgWq8slom+hl7EmSIAJVu691Wpi........PCOEkScgW34Vcld9uvTTTNUGir0yHSO+916dxoj........TbNwS7DScDxDEkSMud5omX4KaoU7yei2zMGCYHMkiIBNx76Z.......vfAiXDiH0QHSx6hx2eNOuph2pqtRcD3nXKaYaY54ufO0ElSIAJdKdQKL0Q........ngWdWTd1ZzLQ5quCj5HvQwe2ldoL87elIN9bIGv.0Tm1zScD........3nvpWmZd+zUshJ9Ys10IEZpI+NG.......PMpyIBEkSMtN1YGQoRcWwO+4LpQmioApNJWtbpi........PipSJBEkSMtWq8slomehSv8SN0e53M5J0Q........ngVdWT99y44UUr908KRcD3H302ZkWTdasMo3zN0lywz.........zHHuKJea477pJxxp8lhUVtexOuy+7ywj.........znvpWmZV8zSOtexotzPG5PScD........JTiariI0QHSTTN0r5ry2LSOeqmwvyoj.Ge93m7oj5H........vQghxolUmc0Uld9y5LGYNkD33y6tu8l5H........vQQQTTduEvLKb8zSOoNB7g7VYnn7qYFyLGSBb7YO6YOoNB........b3c5QTLEkusBXlEt8r22M0QfOj8rm+oJ9YG9HNibLI.........MHN8Hr50oF1xW1Rq3m8jO4SNGSB........PiDEkSMortJ7a8LFQNkD........fFMJJmZRYcU3OzgdJ4TR.........ZzTDEku+BXlEt9NvARcD3Pj0+83zN0lyoj.G+JWtbld9wM1wjSIA.......n3L0oM8TGgJVQTT91JfYV35rqtRcD3Pjk+8nkVFVNlD332xW1RScD........JbM0TSoNBULqdcZ3LgI9oScD.........pgonb.xQ81a1V651HB........EOEkSMoWeqsWwO6G6i8wxwj.Ge1YGuQlddaDA.......fB04DQwTT99KfYBCXexgOhTGA........fZSmTDESQ4aq.lYgKKmfY.NnNdiNyzyexmxPyoj........vQhUuN.4n95quL87e7S9jyoj........vQhhxAHGs012Rld9S7DOwbJI........Eq54MkqhxAHGUtb4L87iXDiHmRB.......PwpddS4VDEku+BXl.TWX4KaoY54G5o7wyoj........vQRQTT91JfYV3V+59EoNB.04186zSlmQyM2bNjD........NZr50+2UpT2oNB.0453M5LSO+Tm1zyoj........vQihxolzIeJCshe12rqrUVITo5rytxzyOzg96jSIA.......fifSJBEkSMpO9IexU7ydfCbfbLIv.WV+iz3SL7gmSIA.......fifyNhhqn72tflK.0r1w12dld9Qnnb.......fphhpn7cUPysP0SO8j5Hv+tS7DOwJ9YW7hVXNlDXfY2uSOwZW6ZxzLFwH9j4TZ........3nwpW+Prm89toNB7uaDiXDoNBvwkNdirs10aokgEM2by4TZ........JdidziN0QnhonbZHs8cryTGAFjo812Zld9ILwOcNkD........NVTTN0jF2XGSld9+o8r2bJIv.yVdsMmom+rGU86ewU........0aJphx2VAMWFDokVFVE+rc1YW4XRfitd6sbr7kszLMiQL7gmSoA.......fikhpn78WPysP02ANPpi.GhrrJpeytx18EMb73U17qk4YbtmqSTN.......P0hUu9gnytbJjqkbxmxPq3mcGae64XRfitrd+jO0oM8nolZJmRC........GKJJmZVehO4mrhe10t10D81a4bLMvQ15dw0lomezi4byoj........v.ghxol0HFwHxzymGqCa3XY66XmwZW6ZxzLF0nFUNkF........F.FeQUT9FJn4xfHiariISOeVWG1v.wles1y7Lb+jC.......Tc4DkSMsoNsoWwOaVWG1v.wys5eZldd2O4.......P8pVOigm5HTwTT9g3s5pqTGA9PFwYzZE+rqcsqI186zSNlF3CZ2uSOwxW1RyzL9bWzjyoz........TcUOeX.UT9gnu9NPpi.eHmy4js6t40s9MlSIA9UkG+904NZ2O4........UaEUQ46uflKCxLlwbNY54y5ZwFNZx5ue0VaSJZcjU9VS........fJSQUT91Jn4xfLM2byQasMoJ94W9xVp0uNEh7XsqOgO8mImRC........GOr50olWVKSz5WmhvO4IyVI4QDwE84+b4PR........33khxODkKWN0QfCiQMprcGN+S9wOQNkD3W5QWvijom2ZWG.......HcTT9gHqqQYJFSbhiOZokgUwO+ZW6ZhW9U1btkG3Yd1UEkJ0cllg0tN.......P5TjEk+1E3rYPlK+J9yxzy+TO0SkSIAh34etUm4YXsqC.......jNEYQ46p.mMCxbAWvEjom+genGL186zSNkFFLa2uSOwhWzByzLl5zlt0tN.......PBY0qScgINwrs90iHhEtvGKWxBCtkG+dzm6hlbNjD........pPmthxotQVW+5y69lqSUNYRu8VNl28M2LOmK4hunbHM........o20LiYl5HTITT9GVG6riTGANBl9kMsLOCmpbxhmXIOYlmwcbmyIZpolxgz........PkpHKJeaE3rKLk668RcD3Hn0Q1ZL0oM8LMCmpbpT81a43Au+uclmym6y+4ygz........PVTjEku+Bb1LH0kM8KOyyvoJmJwSrjmLJUp6LMi1ZaRw3F6XxoDA.......PkxpWm5JSXBepnkVFVllw7tu4Fu7qr4bJQLXPdcZxm40cC4PZ........HqTTN0UZpolxkxFefG3AxgzvfE4woIukVFVLkoL4bJQ........jEV85eH8cfCj5Hvwvzm9zx7LV9xVZ73+nkjCogFc69c5IWNM42zru0bHM........jGJxhx2VAN6BSmc0Upi.GCM2bywcbmyIyy4dum6N186zSNjHZjsvE9X4xoI+O8xmdNkH........xJqdcpKMiYb0YdFkJ0cL24du4PZnQ012wNi4ceyMyy4ll8sFM0TS4Ph........HOnnbpKkWmp7EunEZEryQzC9fe2LOCmlb.......fZOJJm5V4woJOhHt5u7UFaeG6LWlEMNdlmcUwhWzBy7bbZxA.......p8TjEkuqBb1PtcpxiHh64dlazaukykYQ8ud6sbbqydVYdNNM4........0lTTN00l0rtwnkVFVlmyxW1Ri4bOY+tnlFCy4dlaTpT2YdNNM4........0lr50+Pd8s1dpi.GGZpolhaZ12ZtLqG9gdP2W4D+7WbcwC+POXlmiSSN.......PsKEkScuq+5lYzVaSJWl0U+kux3m+hqKWlE0e5s2xw0esyLWl02567fNM4........0nTTNMDt64du41rt9qclw12wNys4Q8iad12Rtrx0m5zldLkoL4bHQ........01N6QM5TGgJRQWT9FK34CQDQLtwNl3qci2btLqRk5Nt3IO4X2uSO4x7n9vi+iVRr3EsvbYV23rxmeWD.......fhgSTNMLtq69NiVZYX4xrJUp6X1y9Vhd6sbtLOpss8cry3p+xWYtLq63NmSLtwNlbYV........TLTTNMLZpolhGYAOZtMukurkFekYdsJKuAWu8VNt3ImOqI8VZYXwrl0MlKyB.......fhihxogxDm33i63NmStMOkk236qLyqMWtWxiHhGYAOZzTSMkKyB.......fhSQWT9tJ34C+Jl0rtwns1lTtMOkk235tt66IV9xVZtLqqYFyLl3DGetLK........JVJJ+CYwKZgoNBjQM0TSw8+.OPtNSkk234w+QKIl28M2bYVszxvh+p67uJWlE........Etwa0qSCoVGYqwhd7mHWm4xW1RiQMpQEaeG6LWmKUeOyytp3p+xWYtMuGYAOZzbyMmayC.......fhkhxog0UdEWdtdekGQDkJ0cbwSdxJKuN1O+EWWL0+3ojay6qci2rUtN.......PcFEkSCsYMqaLl5zldtNyRk5N98NqyLdlmcU45bo3s8cry35u1Ylayqs1lTbW28clayC.......fpihtn7MTvyGNpZpolhu029aEszxvx8YO0+3oDeyuc9dWnSwY66XmwEO4IGkJ0ctMy6+AdfnolZJ2lG........UGNQ4zvq4laNVwJWUgTV9e4seKwL9JWazauky8YS94kekMm6kj+zKeEQqir0bad........T8nnbFTn0Q1Z7HK3QKjYu3EsvXTiZTwK+JatPlOYyy7rqJ9CN+wkqkjeG24bhoLkImayC.......fpKEkyfFSbhiOd5kuhBY1kJ0c7Gb9iK9dOx2uPlOUlm4YWUL0+3ojqybpSa5wrl0MlqyD.......fpqhtn7sUvyGNtLkoL4Bqr7Hh3+1+0qOl1k8EisuicVXuCFXtq69dx8RxaokgEyeAy28RN.......Pcthtn78WvyuPzwN6H0QfBTQWV9xW1RiKdxSNd7ezRJr2AGY81a4XFekqMl28M2bctszxvhUrxUojb.......fF.V85GFk668RcDnfUzkkWpT2wU+kuRmt7prsuicFSaZWVr3Esvbe1+nk7jQqir0bet........T8onbFzpnKKOh9Oc4+dm0YFeyu8CD81a4B8cMX2i+iVR76cVmYr10tlbe1O8xWQLtwNlbet........jFUihx6sJ7NfJR0nr7Hh3u71ukXTiZTwy7rqpveWC1zaukiaZVyNt5u7UVHy+oW9JhoLkIWHyF.......fznZTT91pBuCnhMkoL430+e9OFszxvJz2SoRcGS8OdJwztruX7xuxlKz20fE+7WbcwnF0nhG9gdvBY9JIG.......nwjUuNDQz5HaMVwJWUL0oM8B+cs7ksz3O37GWLiux0pv7JztemdhaZVyNlTae5nTotKj2gRxA.......Zbonb3eWqir0X9KX9w0LiYVUdeKdQKTg4Gm5s2xw26Q99w3uvKrvNE4Qnjb.......fFcJJGNDM0TSw7Wv7iuw276T0dmGZg4+7WbcUs2a8lm4YWULsocYw+s+qWegcJxiPI4........CBrspQQ4anJ7NxU6cO6M0QfDa1ydVwKrlWrvu2xOTKdQKLlTae53y9Y+ihG+Gsjn2dKW0d20xdlmcUwm8y9GES8OdJwZW6ZJr2SKsLr3EVyKpjb.......fFe62IJ+vXu6SQ4DwDm33iWbcqqpsJ1On0t10DW8W9JiQMpQE20ceOw12wNqpu+ZEUqBxin+RxWwJWULwIN9B88........PsAEkCGEM2byw7Wv7iG96M+p96tToti4ceyM98Nqy78Ok469c5opmipoc+N8DeuG46GCaXehpRA4QDwTm1ziW80dsn0Q1Zg+t........n1fhxgAfq+5lY75+O+Gq5mt7C5fmx7e2S6TiocYewFtRyelmcUwMMqYG+tm1oV32A4Gp63NmSL+EL+nolZpp79........n1vGoJ7N1UU3c.EtVGYqw7Wv7i+vK3SEyatyopUl6G1xW1RikurkFQz+ogdLm6Xiwegep37Ouwlj7TI5s2xwqr4WK1zK8RwR9wOQR9ukK5weh3JuhKup+dA.......H8TTNbb5JuhKOtjK9hhu628giuw7laRyxgVZdDQbMyXlwnF8XhycLitlq37suicFa90ZO1zKswXwKZgIKGs01jh6+Ad.qZc.......fAwpFEkCMbZpolh4Lm6J9be9Oe7CVzikzheOTKdQKLhCIKScZSOZs0ViOwmb3QqmwHhQ15YDCYHE+ZFe2uSOQGuQmQmc1Ur012Rr908KR1Iv+P80twaNtq69Nsp0A.......xIu691WpiPEQQ4GF0q+iIUeiariIF2XGSb0y3qD26bt6XsqcMoNRe.G5oM+Pcv6Z8QM5wDQDwHFwvil9Xer2+6ezNM5u7qr42+iKefCDc1YWQDQr012RTtb4i36LkZokgEOxBdzXhSb7INI........MV12d2SpiPEoZTT99qBuibU85+XR5LtwNlX0O+yEqbkqNV32e90bEl+g89m.9ZjSBeQxoHG.......fOrespv6XaUg2ATSXJSYxwpe9mKd5kuhXpSa5oNNCp0VaSJdg07hw249+1JIG.......fOfpQQ4vfNSYJSNVxOYIwl96ek2eMmS0yC+8le7SdpmzpVG.......fCKEkCEnwM1wDyeAyO1ze+qD2wcNmTGmFdesa7li2pzthq+5loSQN........GQJJGpBF2XGSLm4bWwaUZWwC+8lezRKCK0Qpgx0LiYFa5u+Uhuy8+silat4TGG........pwUsJJeiUo2StXwKZgoNBzfp4laNt9qalQGc1Y7zKeEVK6YzAKHe9KX9w3F6XRcb........nNgSTNjHSYJSNl+Ble7Vk1U7M9lemns1lTpiTciu1MdyJHG.......fJ1GI0A.Frq4laNl8rmUL6YOq3U27VhW34e93IWxSDkJ0cpiVMkVZYXwLutaHl9zml0qN........YhhxgZHiari48uOyW251ProMsoA8kleMyXlwezm6hhILgOUzTSMk53........PCfpUQ4aHh3BqRuqbQO8ziSsJI0Dm33iINwwGyYN2U7padKwldoWJV+u3Ei0t10j1fUEL0oM83ycQSNF+EdA9eGB.......Pda+NQ4GA6YuuqB5nlwAOo4yd1yJ5omdhsrksEaaaasg5zleMyXlwe3E7oTNN.......Pcj2Y26N0QnRrsSnJ8htmHh4TkdW4hM82+Jw3F6XRcLfioN1YGwaz4aFaaaaM57M5HV9xVZpiz.xTm1ziQOlyMF0nFUbtm6nsV0A.......pC8Q+MpKOa1SnZUT9MEQ7cqRuqbghxod1qt4sDc1Ymwa8luYru8tmXwKZgIMOScZSOF5P+chOwvGdL5QO5n0yX3JFG.......nAfhxO5FeDw5qRuqbwSu7UDSYJSN0w.xMkKWN53M5J16d1ar28s2385qu3M6pyHhHV+59EYZEt2RKCKlvD+zQDQ7IG9HheqS7DiQL7gGC8TN4n0Q1Ztje........p8TuVTdcYpqF1691api.jqZpolFvaIgN1YGQ49dui7rNweKEfC.......PcKEkC7qPI3........zH6WqJ8d1VU58jadu95K0Q..........J.Uqhx2eU58jaN3c2L.........zXoZUTN.........z.oic1QpiPkZ+Uyhxe8p36JyJWtbpi..........0rJ226k5HTo1V0rn75p0u9xW1RScD.........fBfUuN.........LnR0rn7sUEeW4hd5omTGA.........HmY0qeTrm89toNB.........PNSQ4.........vws8tm8l5HTwr50OJZu81ScD.........nlzd2WcYQ4aLhpaQ4..........ImST9Qwa0UWoNB.........PNycT9QQe8cfTGA.........HmUsW858VkeeYx6r6cm5H.........PMo2qu9RcDpD6OhpeQ40Uqe80t10j5H.........PMo2rqNScDpDaKhpeQ40c5omdRcD.........fbT0tn7MTkeeY1d166l5H..........4Hmn7igN6rtbcA..........GANQ4GC8UedAzC........Pg5c18tScDpDaHBmn7io2pqtRcD.........nlyZW6ZRcDpXNQ4GC6YO+SoNB.........PN5DRv67eKAuyL4+0+6+OoNB.........TS4i9a7QRcDpDsDQrqTr502XBdmYRO8zSpi..........0L5Xmcj5HTo1UDo4NJe+I3clI6Yuuapi..........0LJ226k5HjIonn7skf2Ylzd6sm5H..........YSuG7CRQQ46JAuyL4s5pqTGA.........pYz2ANPpiPk38OT2JJe.XO64eJ0Q.........fZFcVmeXis50G.V9xVZpi..........jM6+fePJJJe+wgr62qW7padKoNB.........TS385quTGgJQRW85ef.TuXu6YuoNB.........TS3M6pyTGgLQQ4CPu4aVeui8A........XPtccvOHUEkuqi4OQMl12xqk5H.........PMg2Y26N0QnRrqC9AmPhBv3iHVehd2Ur+W+u++j5H....+VHhJB..TDbRDEDU.....Px8Q+M9HoNBUhIDQrgHr50Ot7padKoNB.........PkYCG7CRUQ46Ohn2D8tqXs2d6oNB.........jTMBGv3TUTdD0gmp72pqtRcD.........fieu9g9Iorn7Mjv2cE4mtpUj5H.........PR0Ymcl5HTI1+g9Iorn7ckv2cEoTotiN1YGoNF.........jL80WeoNBUhccnehUu9woWq8sl5H.........Px7t6aeoNBUhccnehhxON85aUQ4.........Cdsu8tmTGgJwtNzOIkEkGQDaLwu+iatmxA........FLa8q6Wj5HTI10g9Iotn75tSUt6ob........fAyJUp6TGgJwtNzOQQ4UfM7RaJ0Q.........fpt53CU7tNzOI0EkugD+9qHaZiaH0Q.........fptx88doNBUh29C+ERcQ46Jhn2DmgiaKeYKM5omdRcL.........npp81aO0QnRrqO7WH0EkGQc5oJeKaotbqwC........PE6c229RcDpD+Jk6pn7Jze2ldoTGA.........pp12d2SpiPkX+e3uvIjhT7gL9Hh0m5PTI9+4+2++hlZpoTGC.........pJZcDiHJUp6TGiiWSH9PGfamn7LX8q2oJG........XvgxkKWOVRdD0n2Q4QDwFSc.pDV+5.........CVzwazUpiPkZWe3uPsRQ4aH0AnR727vOXTtb4TGC.........Jbs2d6oNBUhW+v8EUTdFY8qC........LXv6tu8k5HTI10g6Kpn7L5m8BOWpi..........EtNeiNRcDpDa6v8EOgpcJNJVQDwTRcHpDuUocEM2byoNF.........TX9n+FejTGgJwWH5uK5OfZkSTdD0wmp7U+buPpi..........ElWcyaI0QnRcXOQ4JJOGr5U9q7Gf..........ML5ryNScDpT65v8EqkJJeaQDucpCQkXsqcMw5V2FRcL.........nP75acqoNBUhMdj9F0REkGQc7oJ+Ed9mO0Q.........fBwN2w1ScDpDG10tdDQbBUyTL.boQDOapCQk5sJsqn4laN0w.........fbSO8zS7IZ4zScLpDWcDwie39FNQ44nEsnePpi..........4psrki3AytV2tNReiZshx2eDwJScHpTO4Rdhnb4xoNF.........jady2rqTGgJ0FNReiZshxiHhUj5.ToJUp63m7jKM0w.........fbS6a40RcDpDa7n8Mq0tixiHhSJh3eN0gnR0RKCKd0W60hlZpoTGE.........xj536m7GNh3lNReyZwST99iHd8TGhJUoRcGqe8uTpiA.........YVc78S9QM30hEkGQDOdpCPVrvu+7ScD.........Hy1111ZpiPk5nVTds3pWOhHN8HhRoNDYwSu7UDSYJSN0w.........fJVqiXDQoRcm5Xb7p2n+q76inZ0ST9thiwkqdsta+Vu4nb4xoNF.........TQ5XmcTOVRdDQrgi0OPsZQ4QTmu90KUp63m7jKM0w.........fJxy87uPpiPk5XdwpWKWT9JRc.xpG5A9NNU4.........0kZeKuVpiPkZCGqefe8pPHpT+KQDiJhn0TGjJ092++b7e7+zuc7e4+xePpiB.........CX8zSOw0esyL0wnRc0GqefZ4STdD04qe8Hh3N9Kt0nmd5I0w.........fArMrwMk5HTo13.4GpVun7UDQzapCQV80m2WO0Q.........fArW34VcpiPkZCCjenSnfCQd3ghHtwTGhr5EVyKFSbhiOwo.........fitN1YGwY+6+6k5XTolPL.JKuV+DkGQ+EkW26qd8WaTtb4TGC.........Npdtm+ERcDxhMLP9gpGJJeWw.bOxWKqTotiEtvEk5X.........vQ05+EuXpiPkZkCzev5ghxiHhGO0AHObG+E2Z7padKoNF.........bXst0sgXsqcMoNFUpMLP+AqmJJu2TGh7v8Nm61JXG........nlzK77OepiPVrhA5O3udQlhb1uYDw3ScHxpt69shei+C+lw3G+El5n.........v6qmd5IthKe5oNFUp2Nh3dFn+v0Kmn7HZPV+5QDw2XdyMV251PpiA.........uuU+buPpiPVLfOM4QTeUT9thH9goND4ku50esQO8zSpiA.........QDQ7POv2I0QHK1vwyOb8TQ4QDwCk5.jWJUp631usaO0w.........fXkqb0QoRcm5XTo5MNNOQ40S2Q4QDw9hHlPDwom3bjK1wN1d7u9ucBtuxA........Rpa4lmUzc2uUpiQkZMQDO0wyCTuchxi333BXudv2XdyMV4JWcpiA........vfTqbkqNV6ZWSpiQVbbcZxin9rn7MDQ75oND4oKapWZ7padKoNF.........CB8zK8IScDxpAEEkGQCzcU9A8kthKO5Xmcj5X.........LHxJW4pikurkl5XjEqLhX+GuOT8ZQ4OdDwam5PjmJUp63RmxkDkKWN0QA........XPhE98mepiPVcbeZxin9sn7HZvtqxin+xxugq+FTVN........PgqA3tIOhAgEk+3QC1oJOhHV9xVpxxA........JTkKWtQ3zjWQqc8HpuKJOhHtpTGfhfxxA........JR+jmboCZOM4QDwIjmoHQ1PDwEl5PTDl5zldL+EL+nolZJ0QA........nAQO8zS7IZ4zScLxpdiHN8XP5IJOhFv6p7CxIKG........Hu80m2WO0QHOrhnBKIOhH90ywfjJ6JhXTQDsl3bTH1wN1dTpzthy67NOmrb........fLYcqaCwsby2TpiQdXtQDcToObivpWOh9OR8kRcHJRszxvhUrxUEsNxFx+d..........JXkKWNF24dtQoRcm5njUuczeGwUrFgUudD8epxe3TGhhToRcGW5TtjXcqaCoNJ.........0gtu6cdMBkjGQDOdVGPixIJOhHNon+ByGRhyQgaQO9SDW4Ub4oNF.........0I9wK4IiYbU+YoNF4kVh96Fth0HbGkeP+KQD6Kh3RScPJZqZEOa7u9ucBwnG8nhO5G8il53.........TCqic1Q7Uu9qK1+9+mScTxCqLhXAYcHMREkGQDaKhXBQF2G80C1zKswnTocEm0YdVwu8+4e6TGG........fZPkKWNtsa61h+9+u+6RcTxK+kQDcj0gznUTdD8WV90m5PTMricr8XM+reVbR+m9sie+e++uRcb.........pw7M+le636uf+GoNF4k2NxotfaDKJeeQ+285iOw4npX+6+eNV0Jd1nb49hy+7OOqhc........fHhHdfG36F2yc+Wk5Xjmd3HhMjGC5DxigTC5jh9OY4+toNHUSszxvhGYAOZLwIN9DmD........fTZkqb0wkM0KM0wHu8eLhX+4wfZDOQ4QDw+RzeQ4WUhyQU092++brje7OxoKG........FDqAsj7eXDwSkWCqQsn7HhXWQ++EEb9INGUcuxq7ODKeYKycWN........LHSCZI4QDwWHxoSSdDM1EkGQDubDwkG8uJ1GT4f2c4czwaDm5oc5wuyuyPScj.........JPyct2W709yugTGihvFiHdn7bfM5EkOnbEren1wN1d7CVziE+q+amP7I+jehnolZJ0QB........HGUtb43ltwaJ9ad3GL0Qonb0Q+aT7bSidQ4Qz++A6DhHFeZiQZsoWZiweyC+Pw+4O9oDiXDC28WN........z.3U27Vhu1e9ed7LK+oScTJJaLh3dx6gNXnn7HhXCQ+6r9SNw4H49YO+yEKeYKK9H+G9MUXN........Tmpb4xw2668HwUd4WVzc2uUpiSQZVQDcj2C8Dx6AVC6zi9WC6CIw4nlQKsLr3ll8sF+oW9zsR1A.......f5DqbkqNt8a8liRk5N0Qon81Q+87l6FLUTdDQboQDOapCQslVZYXwkeE+YwLlwUGM2byoNN.........GFqbkqNV32e9wZW6ZRcTpVt5HhGuHF7fshxiHhGJh3FScHpU80twaNl1zmdLtwNlTGE........XPuxkKGq5m9bwSsje7foBxin+6l7wWTCevXQ4Qz+JX+rScHpk0VaSJ9hWwUFWxEeQVK6........PU15V2FhMsoMEei4M2TGkTYBQDannF9f0hxOoHhcEtuxGPti6bNwEbAWPLwIN9DmD........nwTG6ri30Zeqwqu0sF+MO7Cl53jZE5oIOhAuEkGQDmSDwVScHpmzVaSJlvm9yDWzm+yEsNxVScb........f5Ru5l2Rr28r2Xu6auwqu01i0uteQTpT2oNV0RFUz+VBuvLXtn7Hh3phH9AoND0iTZN........DQO8zSrm89tefuVeG3.Qmc006+4u9VaOhHTH9.yOL5uG2B0f8hxiHhGJh3FScHpm0VaSJF64c9wEbAWPbtm6ncmlC......PClWcyaI0QnPzd6sm5Hj6d28suXe6cOoNF0LV7hVXpi.Ge5Mh3ziH1eQ+hTTd+d7HhubpCQihqYFyL9CufOUbtidTNs4......T0b3NQeMBN3p4sQya0UWQe8cfTGibmSKJ.Yxrh9OnyENEk2uSJhXCQDmchyQCmVZYXwEeIWZb1iZTJNG....f+cMpk40YmcF80WeoNF4tCtpTajTtb4X4KaooNF..vg50iHNmp0KSQ4+RJKuJ4ZlwLiydTiNFwvGdLhQ7Iilat4TGI....NB5XmcDk668RcLxcMhqXyHTlG...TWaTQDaqZ8xTT9GzIEQrqHhgj3bLnRasMo3TOsSS44...UrxkKGc7Fck5Xj6ZTWwlMp2WdVwl....PEatQD2S07Epn7eUmSz+IKWY4I10LiYFm7oLz3iexmbLhgO7XnmxIa0sC.08TlW8kF06KuEunEl5H......bPuczeGs6uZ9RUT9gmxxqg0RKCKlvD+zueI5mxIeJwoLzSIZ5D+sTjNjibe4UeoQbEaFgx7......fAAppqb8CRQ4GYJKuN10LiYFQDwIdher3SL7gGQDueg5Gz3F6XRR1p0LPJCMut69ZTWwluyt2cr10tlTGC.......f5MU8Ut9Aon7iNkkOHxAKW+Pc1iZzGwe9S7DOwXDiXDERV56.GH5rqA1Z48XU9b4xkikurklWQC........xCudzeerIghxO1TVN.........4mdi96gcWoJ.+Zo5EWGYaQDiO5+RjG.........xlqJRXI4Qnn7ApsE8+Wzvqm5f.........PcrGNhXEoNDV85GeNon+0v9Ym3b.........P8lMF8uMuSNmn7iO6O5+jk+CScP.........nNxaGQbooNDGzudpCPcpUD8eZ7GehyA.........055Mh3OJR78R9gRQ4UtMD8+W8vDhH9MSaT.........nl0mKh3kScHNTtixytyI5+Dl+6l5f.........PMlqNh3wScH9vTTd93jh9KK+BScP.........nFQMYI4QX0qmW9Wh9+GX2a4.........DwCGQ7MScHNRbhxyeiO5+zkOjDmC.........RgeXDwUk5Pbznn7hgUwN........vfQ07kjGgUudQ4fqh8diH9ChH9MSZZ.........n3UWTRdDNQ4UCmSzeo4mchyA.........EkYEQ7PoNDCTNQ4Eu8EQrfn++nDFeZiB.........4tqN5uSz5FNQ4UWmdz+oK2cWN........P8tdiHton+NPqq3DkWcs+vcWN........P8udi92n1+rDmiJhSTd5bRQ+6n+ubpCB.........GGd8n+Rx2ehyQE6WK0AXPr8GQbUQDSH5+Wj.........nV2OLh3bh53Rx++u8tCtsIBBCCC+0A3NHKU.gJ.2AjRHzATBPG.23HzAPGrtCh6fkNH4H23vjEGYEjR.ud1c1mGoUqyo7aIe6U+yjHT9bPeJ+P5cob7D.........vbycozz75JOGmDtixmOtIIeII+JIuNt+xA........lG1mjqxB89H+w3NJedx8WN........vbvmSx6q8PbpIT97VWR9PDLG........375mobLq2W2wXZ3NJedaHke78xj7spNI.........qEeLkk5sutiwzwFkurzEaXN........vzXWJKx6PcGiomP4KSaR4d.38I4EUdV.........V11kxB61W2w37Qn7ksMI4pT9Q6E0cT.........VX1mRqwuW443rSn71wUobLH71JOG.........yaqtMH+XBk2d5R4HY+53XYG........n3tT1b7Okjap7rTcBk21rk4........v519Thi+8jbakmkYCgxWG1jRv7qSxqp5j.........L01mjulRb7gpNIyTBku9zkxQy9UI4h5NJ.........mH+Hk6bbwweBDJec6xT1xbQyA.......fkkcoDFe7gmAgxYTWNbml63YG........lO1kjaN5g+CBkyiYSJQy2d+6WT0oA........Ze6SxsoDA+1T1R7g3XTeRHTNOEWlCQyeScGE........XQXL78nadveOjCAv6OaSD+gP47uX68OiAzsw4.......vRzco8OBqG2P4VyCiNWSyk4fmIgx4T3xbHZ9kwcbN......bJ0pg7Z03cCoMOljGRa98BXkRnblJayg.5cwQ1N.....qA+LsYDkVcSw5q8.LQ5q8...v7mP4bNMFMe7cWDPG...f5336JvVQesGfIfsMD...N4DJm4ftbHf9lT1F8DQzA..HIYWsGfIfnWKKCoM+dA...vJlP4rDr8n2iA06RxEm+wA.fGg6KukkgzlQuZ0iDU.....3DSnbZEau+8lTBoe7mSrg5.EsZLuVMNTesGfIPqFdE......VLDJm0piinmbXS0ent6e9aV5w2qww3YeE9eN0Z0nWCoM21P.......Xk62.ZpM.NoJ2vb.....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-84",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 599.0, 201.0, 60.0, 37.372116349047147 ],
					"pic" : "z_add_device.png",
					"presentation" : 1,
					"presentation_rect" : [ 11.25, 81.860709999999997, 192.5, 119.902206619859584 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1156.0, 386.0, 63.0, 22.0 ],
					"text" : "pipe 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "bang", "" ],
					"patching_rect" : [ 1165.0, 329.0, 43.0, 22.0 ],
					"text" : "t s b s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1283.0, 356.0, 59.0, 22.0 ],
					"text" : "s savedir"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1165.0, 433.0, 83.0, 22.0 ],
					"text" : "prepend write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "bang" ],
					"patching_rect" : [ 1165.0, 289.0, 68.0, 22.0 ],
					"text" : "savedialog"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.38 ],
					"bgcolor2" : [ 0.862745, 0.870588, 0.878431, 0.39 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.862745, 0.870588, 0.878431, 0.38 ],
					"bgfillcolor_color2" : [ 0.862745, 0.870588, 0.878431, 0.39 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 1,
					"fontname" : "Comic Sans MS",
					"fontsize" : 12.0,
					"gradient" : 1,
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1692.0, 452.0, 46.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 69.25, 210.751999000000012, 46.0, 25.0 ],
					"text" : "LOAD",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1692.0, 499.0, 35.0, 22.0 ],
					"text" : "read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1584.5, 814.5, 53.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1804.0, 993.0, 56.0, 22.0 ],
					"text" : "s loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1517.5, 1045.0, 97.0, 22.0 ],
					"text" : "s openinstances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1702.0, 1007.0, 64.0, 22.0 ],
					"text" : "LOAD OK"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1702.0, 1044.0, 64.0, 22.0 ],
					"text" : "print Load"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1291.0, 667.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 1383.0, 811.0, 34.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1412.0, 855.0, 53.0, 22.0 ],
					"text" : "delete 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1622.0, 1045.0, 59.0, 22.0 ],
					"text" : "s loadme"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1517.5, 1007.0, 92.0, 22.0 ],
					"text" : "route instances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1383.0, 781.0, 129.0, 22.0 ],
					"text" : "if $i1 == $i2 then bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1491.0, 695.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1584.5, 974.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1383.0, 735.0, 34.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1398.0, 667.0, 99.0, 22.0 ],
					"text" : "r instanceloaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1383.0, 891.0, 41.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1517.5, 942.0, 50.5, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1517.5, 735.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1331.0, 278.0, 109.0, 22.0 ],
					"text" : "prepend instances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1331.0, 239.0, 71.0, 22.0 ],
					"text" : "v instances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "bang", "bang", "bang", "bang", "bang" ],
					"patching_rect" : [ 1373.0, 143.0, 131.0, 22.0 ],
					"text" : "t b b b b b"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-65",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1532.5, 644.5, 44.0, 22.0 ],
					"text" : "LOAD"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.38 ],
					"bgcolor2" : [ 0.862745, 0.870588, 0.878431, 0.39 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.862745, 0.870588, 0.878431, 0.38 ],
					"bgfillcolor_color2" : [ 0.862745, 0.870588, 0.878431, 0.39 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 1,
					"fontname" : "Comic Sans MS",
					"fontsize" : 12.0,
					"gradient" : 1,
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1367.5, 66.0, 45.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.0, 210.751999000000012, 45.0, 25.0 ],
					"prototypename" : "button_cs",
					"text" : "SAVE",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1437.0, 194.0, 37.0, 22.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"linecount" : 15,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1762.0, 135.0, 636.0, 208.0 ],
					"text" : "Guia\n\n[index] [instance number] [instance/mod] [...]\n\n[index] [instance number] instanceinfo [posH] [posV] [sizeH] [sizeV] [port] [activemod]\n\n[index] [instance number] modinfo mod[x] [...]\n\n[index] [instance number] modinfo mod[x] inletoutlet.maxpat [not relevant data]\n\nSampler:\n[index] [instance number] modinfo mod[x] Sampler_v[x].maxpat [speed] [pos] [vol] [rev] [keeppitch] [loop] [filename]\n\nMultiSampler:\n[index] [instance number] modinfo mod[x]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1480.333374000000049, 194.0, 37.0, 22.0 ],
					"text" : "set 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1553.0, 427.0, 41.0, 22.0 ],
					"text" : "zl join"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1553.0, 553.0, 69.0, 22.0 ],
					"text" : "print saved"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1510.5, 335.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 1480.333374000000049, 374.0, 61.0, 22.0 ],
					"text" : "counter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1221.0, 369.0, 62.0, 22.0 ],
					"text" : "s saveme"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1441.0, 553.0, 86.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll SaveLoad"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1575.0, 249.0, 63.0, 22.0 ],
					"text" : "r saveinfo"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 549.0, 114.5, 30.0, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "int" ],
					"patching_rect" : [ 549.0, 144.5, 40.0, 22.0 ],
					"text" : "uzi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 549.0, 80.5, 95.0, 22.0 ],
					"text" : "r openinstances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 567.0, 828.0, 88.0, 22.0 ],
					"text" : "set Click for IP"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 314.0, 411.5, 49.0, 22.0 ],
					"text" : "v zoom"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 385.75, 651.0, 71.0, 22.0 ],
					"text" : "v instances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 385.75, 607.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 346.0, 717.5, 60.0, 22.0 ],
					"text" : "s closeall"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.38 ],
					"bgcolor2" : [ 0.862745, 0.870588, 0.878431, 0.39 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.862745, 0.870588, 0.878431, 0.38 ],
					"bgfillcolor_color2" : [ 0.862745, 0.870588, 0.878431, 0.39 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontname" : "Comic Sans MS",
					"fontsize" : 12.0,
					"gradient" : 1,
					"id" : "obj-49",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 346.0, 484.0, 79.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 118.0, 210.751999000000012, 79.0, 25.0 ],
					"text" : "CLOSE ALL",
					"textcolor" : [ 1.0, 0.187189, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 110.0, 793.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 110.0, 857.0, 71.0, 22.0 ],
					"text" : "v instances"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontname" : "Comic Sans MS",
					"fontsize" : 15.0,
					"id" : "obj-14",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 367.0, 988.5, 37.0, 69.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.875, 238.0, 80.0, 27.0 ],
					"text" : "HOST IP",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.32 ],
					"bgcolor2" : [ 0.862745, 0.870588, 0.878431, 0.24 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.862745, 0.870588, 0.878431, 0.32 ],
					"bgfillcolor_color2" : [ 0.862745, 0.870588, 0.878431, 0.24 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontname" : "Comic Sans MS",
					"fontsize" : 14.0,
					"gradient" : 1,
					"id" : "obj-6",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 475.0, 980.0, 235.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.0, 264.145629999999983, 121.75, 28.0 ],
					"text" : "Click for IP",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 567.0, 789.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 403.0, 856.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 195.0, 91.0, 846.0, 795.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "Bang me!",
									"id" : "obj-4",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 169.0, 48.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 219.0, 862.0, 29.5, 22.0 ],
									"text" : "- 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 38.0, 788.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 38.0, 928.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 79.0, 812.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 38.0, 876.0, 61.0, 22.0 ],
									"text" : "counter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 332.5, 887.0, 50.0, 22.0 ],
									"text" : "2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 286.0, 797.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 240.0, 826.0, 61.0, 22.0 ],
									"text" : "counter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 240.0, 797.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 240.0, 733.0, 79.0, 22.0 ],
									"text" : "route append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 160.0, 659.0, 96.0, 22.0 ],
									"text" : "prepend append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"items" : [ "192.168.0.37", ",", "192.168.0.24" ],
									"maxclass" : "umenu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 165.0, 957.0, 140.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 179.5, 353.0, 79.0, 22.0 ],
									"text" : "route append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 297.0, 300.0, 32.0, 22.0 ],
									"text" : "print"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 374.0, 93.0, 79.0, 22.0 ],
									"text" : "route append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 675.0, 365.0, 37.0, 22.0 ],
									"text" : "clear"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-16",
									"items" : [ "utun1", ",", "utun0", ",", "awdl0", ",", "en5", ",", "en0", ",", "en8", ",", "lo0" ],
									"labelclick" : 1,
									"maxclass" : "umenu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 233.0, 230.0, 100.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 183.5, 545.0, 69.0, 22.0 ],
									"text" : "route 0 127"
								}

							}
, 							{
								"box" : 								{
									"comment" : "IP as List",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 473.0, 711.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "IP",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 165.0, 1003.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "Bang me!",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 35.0, 389.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-386",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "int", "int", "int", "int" ],
									"patching_rect" : [ 183.5, 437.0, 177.0, 22.0 ],
									"text" : "unpack i i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-387",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 183.5, 407.0, 211.0, 22.0 ],
									"text" : "fromsymbol fromsymbol @separator ."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-388",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 388.5, 499.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-389",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 320.5, 499.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-390",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 253.166504000000003, 502.277466000000004, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-391",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 183.5, 502.277466000000004, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-392",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 263.5, 578.0, 60.0, 22.0 ],
									"text" : "pack i i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-393",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 160.0, 631.0, 123.0, 22.0 ],
									"text" : "sprintf %s.%s.%s.%s"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-385",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 165.0, 178.0, 87.0, 22.0 ],
									"text" : "mxj net.local"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-392", 0 ],
									"source" : [ "obj-14", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-385", 0 ],
									"source" : [ "obj-16", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-385", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 1 ],
									"order" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"order" : 1,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 3 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-387", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-32", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"order" : 1,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"order" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 1,
									"source" : [ "obj-385", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 0,
									"source" : [ "obj-385", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 0,
									"source" : [ "obj-385", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"order" : 1,
									"source" : [ "obj-385", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-388", 0 ],
									"source" : [ "obj-386", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-389", 0 ],
									"source" : [ "obj-386", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-390", 0 ],
									"source" : [ "obj-386", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-391", 0 ],
									"source" : [ "obj-386", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-386", 0 ],
									"source" : [ "obj-387", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-392", 3 ],
									"source" : [ "obj-388", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-392", 2 ],
									"source" : [ "obj-389", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"order" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"order" : 1,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-392", 1 ],
									"source" : [ "obj-390", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-391", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 0,
									"source" : [ "obj-392", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-393", 0 ],
									"order" : 1,
									"source" : [ "obj-392", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-393", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-385", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 3 ],
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 4 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"order" : 2,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"order" : 3,
									"source" : [ "obj-5", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ],
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 403.0, 901.0, 71.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p getlocalip"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 240.0, 750.0, 72.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 549.0, 291.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 278.0, 336.0, 85.0, 22.0 ],
					"text" : "loadmess 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 696.0, 390.5, 121.0, 22.0 ],
					"text" : "Record_v0.1.maxpat"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-5",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 754.0, 326.000030999999979, 33.0, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 143.005813999999987, 16.392272999999999, 50.0, 48.358086 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 3336, "png", "IBkSG0fBZn....PCIgDQRA...zG....eHX.....Qb+Kf....DLmPIQEBHf.B7g.YHB..L7aRDEDU3wY6c1GTSblGG+qvITSZENLEbYFRCb.h8vyfJ1NV4kYp8jvLRYpFbtdsEas8l15IXK8OpUudc5KB2ebhAmw9lHRm1dybV6obyPzV8JJJinUw6JshXpDByAJFY.wr1Rqm2erIKIjMY2Mjjc2jmOy7L4Y284Y2eY9tOO6y6Oy.xGRD.58y0KNLYGAJVc57Ei572D8w080++QAv44ve.yLlt2foPwN+cp+Az4z49w2WP9YGMQ+.nMmtCfIegRPDnhtdvHv5Ai.VT.deHDbnY.7Fv+4zDPTL.1KXdq5NDmryMJ.VG2Rm3Ycf4MHo9OEwIL2d4PCELIBluYH0+IHNw61g2xI+nGjrwU5tx8RUcBWEjSOXRgmfuhDAEAiAlBY6UI6mpn6JK8EFpsnJLZzqykc1YC02sZNCexImLR9dSNfdVc1Ymn2d6E..4sn773ZpUoFomd5..X3qMLFd3g848wxkrfwGebuNedKJOj6uNWdsizzpEpToh2vY2tcbc61gCGNPeV6Cccttveee6i23wAMCNJb2TE82..+YwbWqvnQj2hxymhhJUpPZZ0JlaIANfllFG+XGC0VWcXngFRLQMcLkpx4tnmnyKxa15TTTnpp1HLXnTA8lKgfGzzzXuM0D1gISBMJl.vlb+DtK5aB.0y2cXqaYKXMFMRDaIlCY1Lpp5pERPGCSooei0M+0AOapTunwF2MJqrGEyblyTr1HgfLYlUVHgYOab71amufdW.3eCfdbcB2E8l7WLqs1sgRJwP.ajDB9rP85gEKWBVrXguf9ifoM5AvjYuWL.9JeECJJJX1rYRV5xPFvlM7vqXE7ELOxhOFm+pyewnTCFHBtLkzzpEah+usm.bqWOEjnmYVYNsLLBgVV0pVkPBVwt7HHQOccoGvFDgPOooUKLXnD9BVwt7HHQWsZtakLBxGJrvB4KH5b4wkn6qgvCAEBKM+kxWPXaZ8Xl5IHnLQfM0sdfIE8fwMjfDCWch0THQ.FQWGegjTcMkAYmc17EjhADnnSPYPxIKrtdVPYuSPYfqwEfenX.FQmTx8HDD5mgiA9eVkPPAgPKvMI68nKJBP.ht.pF.AYD4m+R3MLjT5QXHj9IgH5QgPJHWzGIRpxVzG5IYuGEBQzivP.s+NQzizvWSKL2gH5QgPD8nPHhdTH+Bo1.BGbwd5gyyGINiZUqh+uoGQI5zzznmKbAzc2cid6sWz9INgflVuTTTnfkubjc1YibyMWjy7muhczBIj9TOhPzau8iiNNYGnw8rm.J9CMzPdMo+W+y7LXYOzxvhW7RTru.3KTzhd6seb7du26gyblul2vVowx75bMuuV7Y3abO6AMtm8.JJJr1Jp.Ur10BMZzLsrW4BJRQ+h8zCdy25M4TrKuzGAKMiTv7i+GPR+jCj5Xi37JW0qvt0Je...LXBIgQloZX6miGmavw83kggFZHrCSlvNLYB0V61hHVHFTbh9G0by3sem2wiyUbAKCOwR0gEbqqhjbbCfwuAf2KML9jTGaDjJFA4BfRUATyyU.9lYmJN00tM14G8Yrgaya90vm+4eNpq15TzE.TwTkMZZZr0srEuDbSU+6wGlwsQQ1+djjiaFTdVyZhIvRsaEUMiAPmu3CiW6O76Xu1YNyWiGdEq.s29wCJOKo.EgnOfMavfACdTXqJMVFN7K+nnzQ4cB4OsHIG2DO8OdYb3W9QQwErL1yu90+rJVgW1K5CXyFdhm7I8npW6tlGGaU0UQFW+JgM6HiqeEzPNwhM9Tqg8bqe8OKFvlsvlMHDDvmczGCXV23jkvkf+I0rVTj8uWRrmYMwDnpYLfGB+qt4WEzzzRh8vEBnPlIJaSoa2tcuD7s8GeBrT6VkNixIOW7WEkW5i..luwa1bqRrEINjsh9NpudVAu7ReDr6ZdbXb7KIwVECyZhIvKjyjonZngcJqRsyGxRQ+PlMyVnMc5tOrYc+OIKKceQFW+JrM3yPCMDN6Y4uAhjKH6DcZZZTac0wdb8qN+fVUwB1Tt13X82wI6PBsDwgrSzMatU1r0qzXYHW6CHwVjuwcaqUylkPKQbH6D8FZXmr9eFpeRBsDgg6YwqTHFHxc5mPIm6rm0iT4S1t4xWl8rlLKde0u8xMhAAg84qfEm5Tmh0+uU6rjPKQ3j4uLdo1DDMxpr2OYGmj0+BtwfRnkHbbb6f8VaWnGYinSSSy1UoUZrLLqIlPhsHgwEu5MX8OGER+sKaDc2aC64kxrkPKQbLliaw5WoLHKjMhtCGNX8qN16HgVhv4VwEGNPqeI.D17BWtfrQz6yZeRsIHZ99YmBq+GZYOjDZIhCYcurI24LiO4NbwB9MKPBsDwgrIktPFu1xM9zuXxldc9y+9kPKQbHaDc2Gu1JgpAcZM5fUq8C.l0kGkRg3.jQht6c9u6UCRtxGd5Ia28UVxJkPKQ7HaDc2GlO9a7nKG3zZzg1ZmIqcJJJTPA7tVqKqP1H5.dt7k0slzjPKw+3dp7ppZiRnkDX3RzOluBP6m3DgISAX4Ke4r9+WWKr8XEEslXlroxyO+k.CFJUhsHwCuozCmcYXgEUDq+c9QeFFQ8cG1d1BgASHITsoOg83m+4edE4rcQVk8tJUp7X6lZWWS9TMtaEWbndK+L6wUXznh6a4tPVI5..Ur10x5u480hr4a6e3OlBaStRQQgM8RujDaQANxNQWiFMn1Z2F6wuz9OCFLgjjPKBno3yvi4zV8ae6Jp5kOUjchN.fACkxtOiY0Z+ndK+LtUbwwSrBMzZhYhs8A+M1i25V1BVzhWrjXKAKbI5xlgLE.y21ekZdEPQQA.fCz5WhW++dWgcguo3yviBtsopqFOUkUFVsgPAtDcYyPlxEooUK9f2+8YO9.s9knpdtcXIq9aEWbng6jlGov2T0UiWbCaHj+rCGHKyd2EyKmbPiMta1iaq8NPks7c3XZ9Ugrm4kmybQU8347RORRvADnnKkixyBJnPbzibD1r5sZse7r+0OEuMcJ3xyYtAsmyHpuazT7YfUt8Cx13K..MXxTDkfCHySo6hzzpE6e+62iMQ1l2WKXka+f3soSAmVit.9d2slzPSwmAdfccTOxN2fgRvQOxQPIFLLcLcYIJlkeDMZz.SlZ.FJwLpst5Xaovl2WKnYvLm2VUg4iEduwgztiCPM9HbN3Ju7blKFXFpwkG+N3S+hNfUqc5w0onnPUUsQr5UuFuhajBJFQ2EkXv.JrnhfYyshFZXmrhuUq8ic5r+sCDxO+kfG6wdrHhERH9vknaUJMBwhJUpvpW8ZfACkhyd1uNfWC4bsbg8fO3Cp3q6sXv0PToX.7U9JP+yVZAyKmbBKFzzgK1SOnu95CCO7vn2d60qqeO2y8fLyJSjbxICc2mNE8JDku3h8zCVUYdul44FGTPYu2821shPzmWN4nHrSIlyqHJ8NgfKDQOJDhnGEhKQuMozHHDdQPoz65bcEpsCBgQHYuGEBQzi9viprMljYFDBmLp6htOGHEgyw9NgPOBJ6ckzxkEA9g7M8nPHhdTHtK5s4u.pTVX7HvKjNbIJjQIhdTHtK59cBOzWeJuU+IBbifpmN.fCZG96xDTPPxdOBC2WDF4fiAHBQ+pWw6ssRBxODxhvnfqx1fCpLVUlIvOBNktRbY7LZDdF6CVA7Vz8YOsIjsqZBRO7j3zJf2hteKAOoU4j2X2tc9RbNJf2htU+EiN6rS+cYBRLW3BeGeA47.hTzO7Wb3.2hHDx4a9OeCeA47.SNslbQwvOSuI.kyTbJZCZZZnWud+Ej9AfN.uSo2Fe27c8t6JPsKBgPDvl7Ka40hkiKlG.7YRYKVrfTSkB2+8qbVeyizwtc6nlZpA27l9c6I8u.mBOWh9cAfx8WrO5QOJxJyLQlYkU.anDBNPSSiW+0+Snqt3csh5oAvO.vsnaE.u.XDeeh4CcHP6vAxImbh3mD+xUFvlM7Vu0aBylODeA8f.XutNfKQ+G.ifWLe2ot5pKzXiMhXiIFDarwBpTSU3VLg.lArYCexG+w3E2vFfEKVDRTdA3VMy709lQhNCTBh0fpvnQjZpohTlaJ7GXmnVkZO1NODKgqZSPSS6w9GmPPkJUSqE+fArYCzzzn6usaX4RVPqlMK1Qm7wvTR.6uMKkxAv+P7lIAgP94uDjtNteQu8Sbhf0vNeL.nGhb4k4M.vcHNEqyuEH2erCYfwSbh2sNNzRQw5.Si0K0+QHN9cVASV5AEzAlh8K0+oHNtciBlOGmHmp2zDcfIKeqxf+nDGvA.SNwBVrmta0g5.S0Az6zkH.V3z7dRfa5GLIzb4ZCA3xFSnb+sz0KAv4u966L7ccWz1zHtbQQAX7ltL0sDsQgmCfkyiImGBsEre3+ensNGMcaUhbq.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-4",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 689.0, 322.696013999999991, 49.0, 48.607999999999997 ],
					"pic" : "recbutton.png",
					"presentation" : 1,
					"presentation_rect" : [ 143.005813999999987, 16.712273, 49.762267999999999, 49.364169855999997 ]
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-46",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 769.0, 24.5, 48.0, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 84.505814000000001, 17.392272999999999, 47.988373000000003, 46.573540000000001 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 3958, "png", "IBkSG0fBZn....PCIgDQRA...rG...vdHX.....UG++e....DLmPIQEBHf.B7g.YHB..OzRRDEDU3wY6c1GaSbdGG+aRHFCNw3RBwkWZ80Enqz0NBzVMJIzYWRqMsosAyZIBQYzjffJs0hoRfTmf3jH0MpTIrosRpHAwKQS.UKk1sVBk1hYjPYZqz3M1ZAEuZ5KDRfPMI1PvDGu+vwdN9dt6db7amubejNQ7y86dtGyW+79ummmLPxih.fFd9LWgEIZ.v7n7cdA.3jRaiVbRQbaSf6Sy22viGghOdIiX4gGA8HPBtnQ9b3eA9owg3WlQic.bX.rGj39gL..X.vFF4k4D.9kuRoWGdDMItxZ.PmhfubxWrubMh9DyTNjyAmtbYknBRAZPfhHR0eAjuhtq0PPK4EMPtH6z4qfMTlEjZMtMH2J5DFlLVFl7jyg38Nym+ovoyKDquB6fCAORw1J.pIVeaQRjeAUmqVLQEpYY2vd4uKm8cEEQ06UiFeH6I2G..xLCEXnaN4n54A.xJ6aA+Y3g38xvmZ3yWlb9rCze1vq2nu2spTMLTkiWLTFWFezmbvwxO.dADnqYihvSIL.3qnM1LYrLLqYduH6rxMz+INV+xIC+Tvz8f193cFMh9E.gtjEtxrG.7yEJVptxMgaNvLgGOb+KZYh+nPge3MiSgVemCP6irLDnQ1gHbw1E.lBWOoIikgYjWovkqrh5DpLwOTmeWXO662RiouKBz04PDL6Y4P.gVyjdbYgVDvf8WHLYrLZL8YPDi6dPwVOeO0LxqT45hEI30aFXF4UJslSLmMm8MaMq9kkyQKxvkqrv5pdKzXp9v+PPwly9UqLqYM1SUxjv3ZWYZfgQmPlwJmMuct06MyNFSVxjHvq2LfwRWsPlMEDVWvxD7TDN.jKBWDy0uV9zXVH8Utyxow3wSlv7xpPHyjEaoBZy+GIjI5C9GYhDfmNHSxC2tXOGCQPn1jIK1o43wSlnjhWLelDx4LErX77x2a7HMISBjYW3BDxDF.45rkDLoIJXqxY.jEaIAzNO8xhsDfA5WvA9pH.YwVRfWuYHzPmpA.XBImji3lBzNHFJiKiu869Onsi9WXceSFKC2t1Bwf8Wnnc1+Vv7eXA8jkI.AldSoLELcO3re46iV+fSxqcA+APfQqp3jPJK5Q4DI6Digif4r8MjzarwyKeu3KbbXAE4Ho024.v7SHNE6byY57ca8.TH1RsIBYp292fcs6WOUmLRILtoAZJT3GXhcflhAgt5J2TbLEEeIC+pDzlwEhsBE9gqa7gb5YlFLnGG5PGDc0UWngF1NQaJo3EC2WU75HG9tkv9cv3hViqTsCz1gY2Ja.flatITYkUA..61sCKV1HQ6lagki9th3rk3zhjWr0NSWXm6hrq211QNBLZxTnOWec0QztUVQ0Q8pQQLhjtXbEJ7iibrlHduHE5i1Va3O0ZqrrigQGxx28mvRiISjzhsR0NHNPCazhkQIz..a602Fw3vXoqNsX0uPwPlJcEaEJ7SbkSvvnCaZyadTg0Q6siiebaDs8ZWYZIpjXbEAFYOF.IrXeaS6ZDCeKaYKPqVsiJrVIT7MPfb0zN7n4kuWnTscbceGEJUaOPW8DOnCPB2.su5q+aDC+4dtULpO61sar8FZfkcQStZUpFFG6u96FUUFqrhpA7R6N3UxAIYNaUpFl3DZTqUqHmbF8XHauyNIFGk9nqf5b0dFh8hn+Tm9XTlZSdHMEa02fX3OS4kyJryc9yQz1aNvLo5cUf1AINXMNcdAwVQ4RSwdH+WkUXFLnGyadrKV8TcbJVgYxXYT0BbEJ7iyd92iy6mq5aIXbjLQRJ1W86+ZVg8zO8SS8yq6NlOU1Mk7uLZuCtm4rrxVVrS3PpX0G5AeHpedg1aW.Bres7VMUOu1v0dwRpBImXqQiOhgWbIkPL767NuSVgM3M3uSJJT3GWruOJ5SboXjbhshIxtnypprRNset26bYEV1YyeCqTp1AwV6mJQkpgExjhjbhs+r5mUXKp3Ewo8OxivdooqXRCxo8QwdZRREkSZHgLQijSrGvc2rB6AdfGjS60pUKp0p0QEVae7NYsRXToZXnTscNEZJVX7objbh8U+9KwJLRc4JbV25W+nDKmNu.109dEbceGEXhcfq66nX+u8Ki+3AHOCZMzv1wRdzkDaI7j.RtgKMx5RWtYyB9LZ0pEsr+VPIKdzKPNZpWdiVrfMrAK3ms7kGcIzT.Rpb1jVDhkrXxsBORJtjRPaG4HQ06a4lMiZGwgGHMW3hMjThclJbwJr4dOrasMWXzjIzUWcACFzKns0Z0J1yd2KxImbfCGNHZSF9DbsSmTQRI1W3a9bVgUz7oazvBRgEVHdu26OilatIhM5xfA8n8SdRr0ZpIzjpbly7YDiKw1lOjjoNaEJ7iVivoBYXzwZtqogbxIGTYkUgJqrJX2tcb9yeNnNW0X1yYNnvBKjk8G7.GjX7H174dIiXSxYELuLgablPLu4MOdaMeO8zCw5qMurJ.tYL+5oFRUgwxljP5Ho.ImUXgO7BS3u22pwFIFNEarMIcjDhcd46kX2jVvBdfD560gCGnlHFPlfPwFaSRGIgXeKvdJMMXPOw5Wimr4MQd4.YdYUHJ8HUwWJJJQkpgINDlQy7WOVntZqky9VWvT4cSiLkQZuXm0j9uDC+odpDmXWWs0xYw2kT7hQucK7hrKUPZsXmW9dIlqtpJqLgTDta2t4UnABrlvDqjV20q9F3uSL7e4K8Rw82kc61Q80UGuCK5ZV8KKpWSXoshcfErGa2OZiVrH3rbEMX2tcru8tWh9Vd3XxXYXv9SrMHLVQPwViFehtQBhuErWjKsmwBNb3.m3D1vG79e.USvACiNLM0OF73Qbujdm.D3D5KqI3C.hKwNSk+ShKXulatonZ3Qc61Mr2Ym3bm+b3TcbJ3xkqnd1qLYrrQDZweyeR6JFOPw2ryUavfdVKsGtnmd5AuUiMxaCsngpqbSv8UmknOGcPRqDaUpFlyhuqut5YszdHQGs2NV0yupX5Luz7xp.4k6Cg9tj3swXjHsRrub+Giy0aMWtJb3bz1ZClV5RGSuaFFcP+iTNxbn6BtbkE5KINIGwKRaD6od6eCZc2rG+aFFcg7VD9ni1aOpE5fBrxrlE5sGkn+qDUOtnizBwt.sChF4XKspk82hfEe2SO8fU87qhp2EIAlsyImdhnWrUoZXz1w+8DuWCMrcpJ99W8pupf0QWRwKF2287jn2tUIoD3vQzK1bUOsAC5Q0UuVAe929sODZd26ly6GtH2KaWNWRwD.fSdMXhWG.olVcpN+tPq6ir67tqc0jfEe61sarINlFRf.Cu4f8WH5s6zitNEqHnXOr+TyYDRAS2CZrYxq9hCcnCR0Dcr823M3r360W0uQzN6TIJDkC6iFM9Pae7NIduMZwBd1m84DLN3yKRFOJz.hPwN3xgkq5oooaV..+5W60HF9Kt15GWJz.hPwNmo9sD8mLFFcTUOM.2MJakUTM546Ddg1KUQTI1EncPN2hnabmMRU8z8zSODaTFCiNL7f+3XNMlNinQrUnvOm8m9PG5fr19I4hWeaaiXU.lL7KDsmuGIKDMhMWSaYsVsRUCx.BLjnjbxfUVQ0n2dTFyowzcDEhcAZGj3Zed4lMiM9JuBUwga2tINjnRocUX93ldEbL+blxEatJ9lgQG9Cu4aRUCx..pYqakXICoK6pvwJ8OPOBYRpWr4Zahtk82B0dcBWEeWRwKNsYWENYPJcrw0nwG1MAWAtgF1Nl8blC1wNZ.m8ecVrnhWDdxmrLhhOeynUfw7d7cixBGAE6.0ETPB4k65FmlUXFLnGKYIkhEtveRnb7A6ybsVsh0s90OJQmqYzxjwxF2N3IbgfEiSQcAiIxKeuD2IBqut5QsVsRT.qwpUboK8+2fb3aFstCsFheIVIBor5r838KXE1xMaFydNyIjGdZvfdzUWcMpstpO6y9G.HvXeykCFJUN.Wh2jRDaEJ7SrqVqnhULpbt5+o5gVsZgsSXKTX4latvsa2XsqsZhw83ktZMVHkH1Zx65DC+tu6e3nVMG0X0JxM2bCc9cvvnCKcoOApYqak3Y5Av3mtZEkXGHEI186oKhgawxFfCGNPyMS1cgabmMBGNbv4RwwjwxvkuXhowjo43BHEI1j1EBA.N9wsgYO6YCf.qtika1LXXzgMZwB5ryNQwkTBJu7mgy3cF4UZBI8lNvzxaNBZSJQrEZmCrpppFszRKXEUrBb3C+tXSady3Rc2Mt+6+93zySdw0VunaMoI1HkLnJLL5DzaOO9wswY8xQx384olVDLmcu89cw8W5hV3iE2hKSFKab+7TSKBJ17cFXLVQspYGWhGSFKCZlziOtedpo.a.ADag2szhyza2pBrovECXdYUHKzQIYB.xmhYgAEGIAQMJ7unwrfutp2BvMKVVniRnpAZJmzPvim36vOFPnJFquphfyuscAagdv0fkua7Cvkun7flLVfJwNQtpP5saUXxYYDO+y9XP4jFh0dvYF9UAu2PIb4Jqz9UQYplfh8I.Oa0F2x2..Hw10FOdxbjROjGArD.1.nbPUHcB3IS5GAEaa7YzY97OMwmRjIlfBGNLjX6jOib57BbdR3Ii3.AbxjNAnTrA.xdx8E6oHYRXHPouiZVurITj8u+BAMQlTDpTMLey0f8f+Q3MP6D7Egs2wIIdTJISpmbzva80g5Ka3hsMghTt1XXkI0BoiLivvVv+Hbw9vBEos9NG.ZmYRenzkgG35HyHLBMb3QN3xNAffmfnUtxcH6n.h.TnvONcmbuUhLB2F3vsj1AMujO4TMfBzx8wNrLIdznwGbciOTHg9cQX0YGYNaMHPt6oPyKL3F0p7rOkbQ6LcgibrlnY+W8E.vdB9ARpjU.TCsuXFFcnzGcEX3AmgbQ6IPznwGxT4EwWdtOkVGJ4Z.fA7jyFHPt6NAE0cGILL5vBl+CGxSGyvmZ3yW7a5HGn+rEMkhLV5FJMo+fwaVYeK3OCOX.2ciScZxa7eBfEPY0x5Afe4qz1KhNjBWk65DAx0qmi6Ki3kqg.5Fq9HyWkr1.vcA.w4IRlLjHnP+ki0HXMH0Wrj7EcEcyPTAiRJBAxomp+BIew9xE.1.mJWLfdDneatDAeIGuecXDnTWpIV5Gi9QtXF4pHP4fwHSTicDnQycNxkMLF72+DUmV0.xMrSeTDGLHNUGTDnA.wuizuXmvmZYai7uNi3Jtf3XDJDmnOADmNQbT7hV9ePj9m4Gy2hvoB....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-45",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 712.0, 21.0, 49.0, 49.0 ],
					"pic" : "claueta.png",
					"presentation" : 1,
					"presentation_rect" : [ 84.505814000000001, 17.387115000000001, 47.931624999999997, 47.931624999999997 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 40.0, 16.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 52.0, 566.0, 97.0, 22.0 ],
					"text" : "enablevscroll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 23.0, 526.0, 98.0, 22.0 ],
					"text" : "enablehscroll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 40.0, 56.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-28",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 769.0, 88.5, 64.0, 35.0 ],
					"text" : ";\rdsp open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 549.0, 390.5, 134.0, 22.0 ],
					"text" : "FastSwitch_OK.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 549.0, 436.5, 100.0, 22.0 ],
					"text" : "prepend load"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 549.0, 468.5, 53.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 549.0, 508.024962999999957, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 177.0, 419.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-20",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 191.0, 566.0, 98.0, 21.0 ],
					"text" : "window flags title"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 162.0, 535.0, 123.0, 21.0 ],
					"text" : "window flags nomenu"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 183.0, 510.0, 119.0, 21.0 ],
					"text" : "window flags nogrow"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 191.0, 484.0, 107.0, 21.0 ],
					"text" : "window flags close"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-26",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 174.0, 452.0, 123.0, 21.0 ],
					"text" : "window flags nozoom"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 197.0, 625.0, 77.0, 21.0 ],
					"text" : "window exec"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 178.0, 677.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"bgcolor2" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"bgfillcolor_color2" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 1,
					"fontsize" : 8.0,
					"gradient" : 1,
					"id" : "obj-17",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 337.0, 16.0, 15.0, 17.0 ],
					"text" : "X",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 376.0, 336.0, 63.0, 22.0 ],
					"text" : "delay 500"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 411.5, 86.0, 22.0 ],
					"text" : "s masterzoom"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Comic Sans MS",
					"fontsize" : 14.0,
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 293.0, 49.0, 26.0 ],
					"text" : "zoom"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.07 ],
					"fontface" : 0,
					"fontname" : "Comic Sans MS",
					"fontsize" : 14.0,
					"htricolor" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"id" : "obj-7",
					"maxclass" : "number",
					"maximum" : 100,
					"minimum" : 50,
					"mousefilter" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 376.0, 373.0, 59.0, 28.0 ],
					"textcolor" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"triangle" : 0,
					"tricolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"annotation" : "dragpanel",
					"drag_window" : 1,
					"grad1" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-34",
					"ignoreclick" : 0,
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 346.5, 80.5, 79.5, 76.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 314.5, 316.0, 249.0, 310.5 ],
					"proportion" : 0.39
				}

			}
, 			{
				"box" : 				{
					"attr" : "statusvisible",
					"id" : "obj-29",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 112.0, 263.0, 150.0, 22.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"midpoints" : [ 1594.0, 997.0, 1570.0, 997.0, 1570.0, 976.0, 1369.0, 976.0, 1369.0, 730.0, 1392.5, 730.0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 1 ],
					"order" : 1,
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-104", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"order" : 0,
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-108", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 1 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"order" : 4,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 3,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"order" : 1,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"order" : 2,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"order" : 5,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"order" : 6,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"order" : 1,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 1,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"order" : 0,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"order" : 1,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 1 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"order" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"order" : 1,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-64", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-64", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-64", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"order" : 1,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"order" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"order" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"order" : 0,
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"order" : 1,
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-83", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-83", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"order" : 2,
					"source" : [ "obj-94", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"order" : 1,
					"source" : [ "obj-94", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 0,
					"source" : [ "obj-94", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-94", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 1 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "z_add_device.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "audiobuttonlogo.maxpat",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "z_logo_Aglaya_BW.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "z_logo_Aglaya.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "sliderGold-1",
				"default" : 				{
					"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ],
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"textcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
		"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
		"editing_bgcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
	}

}
