{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 173.0, 307.0, 767.0, 642.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 567.866759999999999, 74.0, 59.0, 22.0 ],
					"text" : "active $1"
				}

			}
, 			{
				"box" : 				{
					"comment" : "enabled",
					"id" : "obj-2",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 567.866759999999999, 35.666668000000001, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "RdB",
					"id" : "obj-10",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 538.700072999999975, 349.333344000000011, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "LdB",
					"id" : "obj-9",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 504.700072999999975, 349.333344000000011, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "audioR",
					"id" : "obj-8",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 485.866730000000018, 48.666668000000001, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "audioL",
					"id" : "obj-7",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 453.866730000000018, 48.666668000000001, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "volsliderIN",
					"id" : "obj-6",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 412.000030999999979, 48.666668000000001, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "volsliderOUT",
					"id" : "obj-5",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 412.000030999999979, 349.333344000000011, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.203922, 0.215686, 0.223529, 0.0 ],
					"elementcolor" : [ 0.376471, 0.384314, 0.4, 0.25 ],
					"floatoutput" : 1,
					"id" : "obj-34",
					"knobcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"knobshape" : 5,
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 469.700072999999975, 116.434532000000004, 27.666665999999999, 207.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.466644, 0.76786, 40.0, 192.771084337349407 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.223529, 0.235294, 0.219608, 0.0 ],
					"id" : "obj-4",
					"ignoreclick" : 1,
					"maxclass" : "live.meter~",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "int" ],
					"patching_rect" : [ 529.200072999999975, 116.434532000000004, 15.0, 207.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.466644000000002, 4.216536000000019, 13.0, 183.0 ],
					"slidercolor" : [ 0.223529, 0.235294, 0.219608, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.223529, 0.235294, 0.219608, 0.0 ],
					"id" : "obj-3",
					"ignoreclick" : 1,
					"maxclass" : "live.meter~",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "int" ],
					"patching_rect" : [ 512.200072999999975, 116.434532000000004, 15.0, 207.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.466644, 6.216536000000005, 14.0, 181.0 ],
					"slidercolor" : [ 0.223529, 0.235294, 0.219608, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 4878, "png", "IBkSG0fBZn....PCIgDQRA...XJ..L.HHX....PpIlwK....DLmPIQEBHf.B7g.YHB..RTbRDEDU3wY6c2dcairlEF885DnYFzHCrx.yPnmHXTH3IBtJD7DAilLvg.cFnNCnx.4Hv2e.CKJ1TeXUEHNjbuWKrrj5FPkEeLJP.Pp+UwK4pppU674q6z18tppG14y2zos6Yi+0RO.VPC+bYJ9l9ygpp+boFTUUe6m+41et7P8XHe2xLjN9tDBygZL5lVFpp93BNdZ02qw.c6OW1ryGe13bKLWUiS2d0N+4erfimisuUiA5c6r7vKsBo5TOLmBwokS48DNWtuF2q5TntYIGLuUmhg4zdCutDhuWeqdLV2TAtW0Skv7pZLD+qZYehImq96ZLPmVhKTSxpZLFuqp5GVNpK2UU8kpemdryBqpptoF+WsK8CPVFeb31Zb1pKRBx7WlhzgC8.34nOWBxSskuViG6+YoqJGC4o9xs0Sursm7toV9enZoOKOTmAOQI6k77c4y0Ip+pbrjm6K2Vcvw7DreSU0+9H9868X+6rm8+3ccnu9Pc3mw5zcuzg9+6b754++TimGz2siUXdaU0+8Q560q495wqa715w6Pmk1t25c69mC0o4U65+pFel6uKycXtpFePeIul1SWW3M0I7caS8XrttdbOteZ4FNupuWiiw3948pZYdRNWZWNsqpwic+l5wqy8ReblSKu68XNWN1Q4c03yHb3H72sSAC038YvWpk+LfrdN+K5uiiUT9PM9C9y1q9PmstF2q5Wqwiq9XElal6+h8VcaMu+EcaMt2fypq1vBXnFODfiwdUWeT9azK31Z9CRlGC03gCss5+icK5wZd8AFP8X4gRPdrstFmBtmONNb7F9O5p2w.8sr7kxT1KoddWecywcnOFN89TTrsB33RnpZbmNaq97X5Q0W6vfd2kaK6kLMCUe14yQ6Ln74NLX2c4j8tS4BPONbsaNFCzgpeSg+PcA+ZJ4DxMUaONeTdKtYSiCxciRmj7SC834SLqGlVulBWTd54KUaOlOayL1ymEtn7zSqGqYS2mlujaabfMsb8bM.Y1ssd+OtuYNFP85DoeybL33n41psG+6tMMNfls+ECGUWWAcHbqabv7iZ7XScxyO805LmW2yAylFGL+nbtJOmDwgxstwAxOp.uM6oIap.NbtVFDSSgOzqACQ31582Cu5U.5CugAv5p8WMdeoNydyqmld7rKupYa8tGxS347zeUs0ECs7Menwu4c8.cIJs9LyW2x27Vutn1a44sVZiW7Vb70NFyqaXPW0XXG26DCzM+cCq66dGVWWsOMt8Vddqkm+wKd5Ceo8X15IC++ur2xycsbi+9t1o0P09dKW2vflSCWWs87OdVO2dLacukS+ZhiyaaaXceWumf15aYHy1MCJQYU0Vm7acWFMz32re6ugbRaVNbuCMU9y9+7azz6XubYnkSYzytCrCElsd7ktKhtrzxYd4Yel4ywdL2z35yoksMrtCO2+g8Cy0U6+FTvdLurrsg0c349ObnvrEe60+egyLGkoxW2v2jpLM9knVdhtO68k49gYqmlmMMt9v+POdMiykmY4jru6dLacuksb9r3zUq2nNG73L6YX5jpS2zyvbaiqOmtZY1x0G5K1yvbSiqOmt598c6TXtpZ+Dqusw0mKSu3wX1i6FnscXavooVd9Eu3yJengMbUthOW5lsoxG58FFZQulJeSiqOWt9zg9h69jef2qM8dCZOlDoovr0SUDzh+wNF+P0mowc4HoE+iF7CUeNGldG23x1lduAeKuwsBGc8Xpb2tazp06+E5wT4lFmtyT4zKeumaLgI8RWOyL83XLcphnUG77X5XLYocvyiIDGgI8x1dtwDlzKa64FSXRBF1+KzivzS9gV8m6+E5QX5zEQ2Ypbhjvjdo6W4mC9hAB9M00mqg8XRJF18SDljhgc+DgIQ5CUmuO5fd3CkyCI8gm7CQp6mtHHNBShjvjT7j6hcgIo3IuDeDlDIgIQRXRO0sKVSOByscXav4gtctLElDoOTdapl.4XLIRBShjvjHILIRBShjvjTrd2OwcvNQ5Ck26gneh5J+.S51N4DlDIgIQRXRjDlDImtHhjSWDQxT4DIgIQRXRjDlDIgIQRXRjDlDIgIQRXRO49wjH49wjyaBShjvjTLr6mHLIE+4tehvjHILIRBShzTX5WzoDkovzq6GhhoxIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILom1z35uZ5CDljjql9.gIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgI818MrtdURxrYaCqqWW4jMgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRj5QXNzgsA7DBShjoxIRBShjvjHILIRBShjvjHILIRBSRhWkjLa1zv550UNYSXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQZJL+zhNJf8XOlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlzaOzv55M7.lM20v59woOPXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlzas7NwQUUstJgI8WKu2E8KBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILYN72MrtWUkvj4QKu+EspJgIgRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXxbnk2TsFpRXx73tFV2gpDlDJgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQZJLueQGEvdlBysK4f.1moxIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjH0ivbcG1FvSXOlDIgIQRXRjDlDIgIQRXRZVUkvj4wlFV2OVkvjPILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBSRzJgIygMMt9WILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRSg4lkbP.6ydLIRBShjvjHILIR8HLW2gsA7D1iIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXxb4uaXc8aFMlMOzv552kjjIgIQRXRjDlDIgIQRXRjDlDIgIQZJL2tjCBXeBShTOlJeUG1FvSziv7icXa.Ogm7CQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQZJLa4WVPP2MEl2sniBXO8Zp70cZ6.UUNFSBkvjHILIRBShjvjHILIRBSlKMcQaDlLWZ4h1rVXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDocCy6aX6Lz33.dhcCysMrcFZaX.OkoxIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIR6Fls7qXsgFGGvSraX1xuh0FZbb.OgoxIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShTudUR9oVGHvt50qRRnqLUNQRXRjDlDIgIQRXRjDljnUBSlKsb5G+nvj4RKWvFSkSlDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRj50q4mpp5pFWe3W54q4mUMt9vuXpbhjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHILIRBShjvjHseXdeCaq0MrtvSreXtcIFDv9LUNQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDodFlCcbawEt8CyMMrsFZXcgmvT4DIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRj54abqW0v5BOQOCy+ng0EdBSkSjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDoddauUk6IS5jdGlqZb8gpJSkSnDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQRXRj5cXNz4sGWnDlDISkSjDlDoCEl2ezGEvdNTXt8XOHf8YpbhjvjHILIRBShjvjHILIRBShjvjH06vz6cQzE8NL8t8FcgoxIRBShjvjHILIRBShjvjHILIRBShjWZEDIgIQxT4DIgIQxMwAQp2g4ez4sGWnLUNQRXRjDlDIgIQRXRjDlDIgIQRXRjDlDIgIQ5Pg4li8f.12brGy0yv1jKLlJmHILIRBShjvjHILIRBShjvjHILIRBShj2INHRBShjqUNI5ucLljnGDlDIgIQRXRjDlDIgIQ54ByueTGEvddtv7ti5nfyQM8l3qoxYtzzu65EljnsBSRjvjLILIRt6hHROWXt8XNH3rzPKqrvj4xPKqriwjDsQXRjDlDIgIyEWRRhzGaYketvbSKaTnQ2YOljHuJIISBSlCqacCLGg4mlgsIWNtupmOLe3HNPfcssJuleXdLz5FvwXxbXn0MfvjzroJgIyiltbjUILYdzzqo7pd4v7aMrcGZXc4x1cUMe6wbXl1tbZXng08gpLUNyi+r0MfvjzropWNL2dTFFbtYcO1HBSRxud6ubtlJe8LscIeCMrt+5Rg6XLo2F5wFQXRu0xU84MsGS2gQ7dzxU84W2tkuTX1x8jYyWRJNY0xdL2N8Ay0T4MeQ74jUKurc2N8ANFS5ogFW+2zT4svdLuLMz35O6mtnldWXfSVCMrtO42sTlJmdZng08ImEHWRR5oVNaLa28SlyvbciqOmdFZXc2t6mXpb5otbphpZdCSOy7KKsdQU1t6mLmgoq9ykkVe79M+jefeGCMrteu16RfaOlzKqaXc+G2vPuVX1xKgWGi4kkgFV2M6+Ely8XNLiaaxxppsWYj+16wrEM+R3jSFqab8OpgYU1q4khVd9D2WG3h47ZgYq2E6CMt9bZXcCq6lC8Eesvr02YgGZb84zPKu8lu4PeQSkSq9qFW+MG5K9Zg41F+l5bYd9acCq6AO9xpl+vz4x77WK6w7qO2+g4dpb+pU4715psSK3lV9l+iFWFZ4aNQ61582EM+qrmVCy0sN.HRCUacwsuzF+sLU9e+tF1OZciqOY5lFW+aac.rolw+kAmjFp1Zhsu12f2xdLc0eXe2tvqeUUUetZ+3L47QO5gtbZDW2gAhSz94g0U6svs8ZvrpCClq60fgEyU03o3o0VXnmCpVGPeomCFN5ttZOH65dKmrowAj2DXOMspFurg8HJenlgKQ8McXf45leZ4yUel5dZ4yywfbcGFXsd6Qwww0034YrWA4rNiYOdBP2NWCNZ1PMNq31puA4zT3y5Yk4tNLHMcdNVUi6crWGC4ysb8b+WjuzgAomc9xZnNNw3QcVxq5zf0Ia+3YUMdr8eo5yLdwEkS11gA71xT5ykqpw8HtDg3tK2UG4Gi6wz4SCb64rMqqGivM0xEg6u70ZA1wSulN+G03yVaVN2VmYFpwoiuoFePeI2S3qsb6b7Cf2psUe+KylxMSbUi6kYcM9OVusxZufukkttSl+06Xcttp5+qmChe595wokNmuDlC+b4p5wXbnNceud59ZbO5c8wr2SXtpF2q4ezyAxd9d8XftcukzMEbq14im9yyse+G8+ViGhQyuvx126ILqZbv7u6333202qW9egtsNbD+be8Wy5W4qMTmt6w683a03T2wMy1ppuWfeKmFKaqSf64gqqk+GTVNNK2UmX2r2apk+GZVlukuVKzYL48dLlSFpw+0zb9Dg3359Z7zUcacZ7jMeV+Us7+KaKss7PMFhqqyLWWK+Obs79hw3exLs55Z4+gskWd4tZ7hXbwceJbcs7+v2xiKS6U75x6HJ0UU12nAmyKOTiOS5OWWf6U7s5lxIgetW1VmogXqmtnWyzqqjOWWVWxt4x2pwYi1TOdeDbVZtCycMc2UutN+tYF5so6EfM0X7cWE30kdNcLCycMTiA5zxk5dSm9s1vc0SCvte25bpYoBy8Mcqgstd79U7SK2voalBuGpGCtc+SdFoDlOmofspGupDC0SO0GGi.9P+9nY+3Z28zsY9GRm2ROLawzMm6qYyLON3c3+.zJ+0RyLIeaB.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-1",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 132.466644000000002, 218.666668000000016, 61.650664999999975, 297.111638554216768 ],
					"pic" : "zz_faderhueco.png",
					"presentation" : 1,
					"presentation_rect" : [ 2.466644, 0.76786, 40.0, 192.771084337349407 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 1,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "zz_faderhueco.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "sliderGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
					"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
	}

}
