{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 68.0, 142.0, 131.0, 184.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 477.0, 247.0, 72.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"elementcolor" : [ 0.376471, 0.384314, 0.4, 0.23 ],
					"floatoutput" : 1,
					"id" : "obj-17",
					"knobcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 427.0, 361.0, 69.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.0, 111.0, 101.0, 10.0 ],
					"size" : 10.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 395.0, 430.0, 36.0, 22.0 ],
					"text" : "*~ 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 351.0, 430.0, 36.0, 22.0 ],
					"text" : "*~ 1."
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"id" : "obj-35",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 670.0, 207.0, 33.0, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 84.0, 160.5, 24.0, 18.872222999999991 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 4252, "png", "IBkSG0fBZn....PCIgDQRA...fI...vYHX.....+FrX6....DLmPIQEBHf.B7g.YHB..PLURDEDU3wY6ctGbSbc1F+QxVxRrVHYrQ9hvxxWJASbcbKWBgaVjR.xPXrIMwSJLDCDB8KYxEllIjFxkRRHj1z.gD5zz1.IlRBMEZAC7wWHFxWMgfaiAZb.JWbiwB6ZIeK1RVR1xRVx8OVzpaqjkMRZkVs+l4L9r6Y2Uud0iNWeOmCOvtnb.rAl1HFAZ..58Jt6miinTTCfgigC5AP0f7GHxBsuZ3HTvlAyKRBkhMUgxWNLE7YZCHDRILsADBQJ.VMSaDgBXSBL1VwJrhevjHSa.gPn6Kj8..MQX6XjPFbYqkNBWGGQQPWcYTyjFTPhL.TE701afAsoPFi1bvTAWU9z8eIFr3rI4QJtUJlQEX1JZeGfrgKdi5HqY3WpEjs5cL+8oLPVQypAYwLLcKq3BQmgZwX3GxqFj41vzFOWH1HnGiBQVUQAFLWH1KnAzzvjD753MCfm16KhCNBBjAfAAYQlTvys3p.PyQN6I5FUpxAkLa0LsYfjUVHFVTxLsYPQWm+jnliVs+R1.7JWL2EXUAfJo6tVzxJGSbZKDcHUIFLwwEJrSOHAwYAd7SJj+b4H7fcK5PKu4JgFM2ftjWNHaXH.bIvjAfd89JUoJGj8yrajHQtgECkiXWFxby3zO4BoKo2At4QKNGpH0zckJ23GyIt3fVRjHWrnkUNcI4QqI4S2IA.JeEUhDDkYXvz3fsf7aeVzc5fSfQL4YDFLINXSXc7xo6zRc+.mBLe5+ByMd1vfIwAaBcRTNhWCaxcc3H5ApLrbJvT48UHrnRiTFCGwnvKAQ9KIppb4Tfki2WQvj8GGw2DLMBjqHRNBqvIv3HrBe3GmpK.kuxAGAM9Ufw0IqbDJfqHRNtkvOCWjZmQ3DXbbKw3jHMfoyll1ZgEraQGF1tE..n76tJsWiS2XhWBh3pZgWvIvtI1snCYnuQ.MM.qlLfFpqV+4uSiHyY9pghoTLDj0jQKoNk3ZORIQDmOAOGeOmCcej2Cm4KpMj8LOyWTKfaOOUpxAy8gdLzZt2C3KHt30M0+jIB+3pNFhn1SjmgL2LbbfsfC6Ggk54MajSlSD2Vlo.hD4gBSZ.pzTLfAHZHqTG2jjIB.fuaXQnSq7v+r0dvd1+goRWila.M+xeNTo58vbV8yBsJu2vy+TL.jdcyd79zTZp3xhHUz3AvG8K+4dbtJqnL7CydBnvjFvMAjI.Gl.rBxfeHeicQ92ad78kNvK+j+HbIIYgC9s8QI1zn4FPyleBrhM7hPWwqIz+OVTHwUBrgcLHjW+uCeze3coNWkUTFVYAhIEIVMEPgznkhLpEEkNvJ2zCge62zCp9X0..f8sisfUrADWHxhq5lhrtz9v9cSbsqmas3kS2DUNPgKx2XWXa4YGa5wVE0412N1BRvv+Jr94FMPbi.SQiG.6aGaA.jU5tlM8PXA8O1Zk3XkGIQsdHxF3PaOh94GAwG+AiUyPla1i5b8F+jED1y0xerRQcC0ya1.fr0liumywH1QnhVRcJzc56vYj3BAliCrEp3uyyrFLSSsxX1hngrhe5bmL0wI03YXLaIR.qWfM9dNGUebU9RWDtOqsvrFD.looVgJUj93Y80b3Q3pisg0Kv59HuGU7G+Nl.CZIdxxTem.frqKraQGCaMgOX0BrDL7unx8pxJJiwp2EcTPpDTwcNVmrQX0BL42ndp3KI+TXPKwWTkzPTwyzHyWr8XkDDmk+RRM.KWfUW06kJdwV5fAsDeQLrQEWf09YPK4ViQZQqg0JvbXSOk2PTYEk4wXGxQjC1q.ypqEKneX1QOUtOdCVq.ycmCTtvgYPKI9FVq.ycRkWzcqzrILzun9EEfLf3DAVzHWYPwTwi0mE8kuBZWXLKAfSfwXzQ+t5lB17bPkOnYu7oq+Sj0KCBG3dwNe2vQeeAd4Vb0sIr4IJBsBrPo+oyT3dwNMGk0MSVRTHkyG5mhWXMvZKhz8dXt9uUKCZI9xEDkNUb19JIIqUfwieRTy53pOVMnWwDivcD4nNctxRsU4rhsER+BqUfA34hT6YrmJCZItnWwDXmUse.PN+IYC0+JYkER2oUCvxEXsl68PE+s+q++vRhBYPqgjCZz0TsWwRdXFzRBcDnchDVs.iu.YXEa3EA.oeWcXGoOB2Q3kdESfs9djC.uJU4fNkS6x.NqBVs.C.nsBWNU7M8NUQMIYYB1Smt5tj4r5mMtX6yg1to..rFurju.YXka92Pc7522eGZIh79FV8ImMUcuToJGz1jt6HtMvD3WAFaxKK0p7doZQoFM2.a6h8EQqOlVhTvy+m9aTGWvSuy3hbu.hCJhzIVKaqTSzhpOVMXoG5xQjbxzRjBV0mbVJeSqh0+TvtzaOr+4FsPbi.iG+jfxM9wThLMZtAV0mbVTexYG19L8VbsnkUN5bl+OgsOOlhNjR6f0G+MX2IHJSnbierGEW9SdipvNLlZHuiXujjr7PbMm4qFVKaqrxhF8ydHpTf3LAF.oHyZYa0iw.bmUseL827H3OyO6aYgVuhIvNLlJJaq60ibtRbkaiUJtFIbtgj1.ba5dC.TwV+.zUFr6sSlI19oP8+gWwmUxvmb0UfElURnfA5Nn8kesDofO0fXp94xIkuhJg9694X0hq.r4jxy4x2jduSIVdltDrzUFkhbd9ChYekCQsvn.Pli1NuY7JqnLbaYlBjKhOTlfmuStxfhQG8ODp6RMgZO8m6QZpTkCl45+EnqLJ0i8s53MhqVevnC9BjAcEuFT5ue4PwUNDpq585QNZtuREFLnRUNX1kuJnsnUftXw4ZErD2KvbhSglphVAtC8WDD+mKha7O+xf123pX8OEFbxyAFj88gN9IEWmqk6vIv7Bd7SB8Mgoi9lvzgvhWCJck5gCq8hLM1hOUav4xWdhD4BmKJAbBKOwo.SC.7nF87rXJhaLQivWfLvWfLzkeVJx49EJ.eg9sCqKwY2Tnw6TL0xUBW1CGrLBvRytr3t9AiiHKbBLNBqvpqBgHYhwyMsTvTRUHRmfOZncqnUSCgSnc.bkVMA6Vs626U.g.bW4HASKUgH6jo+0zGznQbolX6aYE2ZvZEX21k2MdqW4EAw3b4jekjgPTBDhkUv3fYKRva9OZGmt4g74dS8XaDUsm8.Bh.OrQOp8DwS2j+SelYXESSXenvrSCNrXB5zpEsosM7o+eeJL6fODKQJ5dVUhDD420XqXdXsBrYTXdfXbh8a5DhDhWQsRrSSeENTWd5kqqacqaDEW..M0oQZOedJDh2YdoABwdNgeKt3hA.vZW6ifG7Ad.70082fR0O+H94DKieEX8aL1Nq+oOcWy2vS0hErkS2Ara0NxSgP7tklNFWRB..vZWPQ3XeT8vpvbnt9LyxUNJa9qziy28fd7rsZ1JrY1Fni6u.93Il2jFQ6i28sALiRtLhtVV7F6rnkUNp4nU6y48q.qliVMVvx2VX0nhT7VWz.U8stdaVwibZ83OsPxbsHHHvOhWa3SgKAlybZ..Ne2CBycZNn9bRIsDwSLOW9F0EaqOr0qLH5ti9gcq1gHYhgTIIBCFGBVFNK.ErmhF82FSJqsHx.QGsZDMztTTRFjtMsbw.v2phMp4sJQ.U7iesdwaVWudjtE8C.K93VArZJItsaJ9x1cMmCrayyh.u90u9n94k93LhbylbRz1og98QbEmhL2GpHVEMdsq4QQc9jdetpCkHooAzmqzLYx0vjUQtICCJHarP2C5.mu6AosNXKNYW0Y82dUt0CVmvZEX8Yru.l98MQWKqlWcPOmYx5zpkRbtpo5aqIMaCXYejm4xIxtqAB26FEDLrl6Lcb+eOBPH.nodGB0oaP7G+5tCXe0EMw3jS+baHfEQxVlajtiCa8hoa6avhJLMpy82Gt.OtlSbhSD3GhUyHkZeCONU+c21X1lJJeoXUSkTbs2KS1fhYmYRwLhK..dxxftSKKfUxmsL2Hy3Pu.10t2Mdfe7OFyctyEq6QeTpzZncq97E41191wCWYkT4h8YG+3nMstDP6ae6CRUjWHy9l73IabvdurY7geUG3CCYOYFmRhKZE46uqcg2eW6h1zdoySekwyO+7ohu89tcXimqcHMrxEfvQU3+rOXankC+GQSeaS3dW9CBy2ySEy2K+wEBL+QMm7yQesjDseI5dO46uNU0cLLnq5zUQtIiOLH66L2YFSZ7.k8vXFxx.V+2mE8mf+GIhXEbVGrZoKwX48Pmfg4bWyBEq6KCIOqqqqap32+2i.IHLgf9dOZiFfYylwSugMfG8o1H5N2EidU+7fufnq8WowBArR9rkYVzxNVG3t+vqiEu+VwoZwU8JIHHPoe+7wvN7stllMO5xARqDk3BW3BjOWA.O7OHsQ3NbgMy1vi8ru.9riebTpRQn5GbRiJAZzLtKvh8WZoGArY1FdkOWK9MM3pKLleokhjL1nOWavLX2tSuoMK7969CnNdUSk.yepA+VXSqSa8npibR7Zu5qBBgIf2dF1oU3Gsxv5am1y6t.Si2IZSquu3iUv8A616wR7fec2nCyN..fb4xQII5atUc1YmTwIjGbhs+ctpwe4.Gf53Memxv1u2IghxWJHjS.QxDizyVBt+ePZ9jCEO9hPGy3wwmmwRwWc4lQQSNOTfl+2f5yMZf96j9soZ2qjuF30D+PutX25fkbx9eYcD.nlaL.UmnluT9nduFKx1aucHWtb..bzklN5vrCny3PnUSCgwKjOjJj72l+pKY.czJoa6XUZw3udsSBIG+3XwKYI.fzGzd2L7c8gcJB6G69fGAcnXgfPNAxURh3RMY.7EjBZDxwcBfLkHDAvcyhIHf4fUyQqFC6Xz2qzQCjWdj8SUys2Csou+lcMbP2ddJgCad1wCm6bm0iiSmfOJICRmUrTkhHcdwLDhcUpm9RVGJVHd6qZAuvK8xdjKn2XukFvm7R+TLrCKXwJDi2c9ohmofdvOqPWB+lSRQv+OLCiebuqFbOGrZAvuv6qPp9Kh9lvzCSlU3iW6UeUTV4kiS2rd.3q+YYtSyvrUGf3l4DkYmmGcnv05qvq+5uNTjkBpbh7GmSmu0SxpzhQcRlLt5dNIlkTGPtXfLlXZPhDI3pW6pnWiCfK2ZW3gdseOZG.UeAcXpCdcrz4Q9d1b+CfpNwYQaieZi8W.QXnyWv.fd2mmnx.7s+Cmy7UCgqd2gK6hQQ.g.HjPHrn2R.GVFQxDiDD5YCtGo6IdB6Vzgu3wmKcIs.2eqoG.ei2WwY9hZwDa+TgKaiQwlYavbmlGQghE8CPlimaANwkKxtyF7WRM3c+fsC5tp8uo0hw2y4BoFEGrCbXSONSU+Z5R5a.fdu6MuF.vZvM2LIcmqUyeAkPX.lSWYflp3bDmvvNFDR6sAbssuNeVe0tI+J.7Onas5nb.bn.8vUoJGTxrUSsQNYJ4IhdRZzsUsLgA+NjrotF4KLFCaZaDVMEaOgYBDMTWs9SP4M4h.3mgUAfg4BbgwXXyHHnpn.CkKD6E7nF+AZDUqF.F.vcAfnusLVNhF4T.XI.XTMHpp.YtY5Ay+qCtPzYnZPV2ceXztf7UNHWf8Uey+R+rsbzAaqS1z.V3jnwKp0Ow8gPwJ9npaFBD5gWkMyQ7A+WfFb9pf40084B....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-31",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 664.0, 104.772002999999998, 109.0, 73.861842105263165 ],
					"pic" : "Macintosh HD:/Users/gian/Desktop/1x/Recurso 1.png",
					"presentation" : 1,
					"presentation_rect" : [ 83.0, 159.5, 25.5, 17.279605263157897 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 681.0, 464.0, 59.0, 22.0 ],
					"text" : "tosymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 602.0, 603.0, 266.0, 49.0 ],
					"text" : "file:///Volumes/HD2TB/Gian/Macro/Drive Google/Proyectos/Adolf/Nou Sistema - AGLAYA /Sistema_Dev/Recordings"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 674.0, 409.0, 306.0, 22.0 ],
					"text" : "regexp ([^:]+):/(.+)/.+ @substitute file:///Volumes/%1/%2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-27",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 670.0, 502.0, 131.0, 35.0 ],
					"text" : ";\rmax launchbrowser $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 670.0, 301.0, 172.0, 35.0 ],
					"text" : "SSD256:/Users/gian/Desktop/eu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 249.0, 477.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 112.0, 494.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 231.0, 537.0, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 164.0, 535.0, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 312.0, 313.0, 130.0, 22.0 ],
					"text" : "sprintf symout %s.wav"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 156.0, 160.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"format" : 6,
					"hidden" : 1,
					"htricolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-18",
					"ignoreclick" : 1,
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 291.0, 594.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.5, 158.5, 50.0, 22.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 522.0, 394.0, 81.0, 22.0 ],
					"text" : "monotone $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 522.0, 358.0, 79.0, 22.0 ],
					"text" : "scale 0 1 1 0"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-34",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 36.0, 64.0, 39.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.0, 8.0, 101.0, 101.0 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"forceaspect" : 1,
					"id" : "obj-33",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 33.0, 93.772002999999998, 57.0, 56.543999999999997 ],
					"pic" : "zz_newrecording.png",
					"presentation" : 1,
					"presentation_rect" : [ 12.0, 8.0, 101.0, 100.191999999999993 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"id" : "obj-77",
					"maxclass" : "meter~",
					"monotone" : 1,
					"numinlets" : 1,
					"numoutlets" : 1,
					"offcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"oncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 516.0, 494.0, 80.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.5, 142.0, 92.0, 13.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"id" : "obj-76",
					"maxclass" : "meter~",
					"monotone" : 1,
					"numinlets" : 1,
					"numoutlets" : 1,
					"offcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"oncolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 516.0, 473.0, 80.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.5, 127.0, 92.0, 13.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 98.0, 264.0, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 38.0, 40.771999000000001, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 38.0, 9.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 185.5, 2.0, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 458.0, 537.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 351.0, 552.0, 47.0, 22.0 ],
					"text" : "/ 1000."
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-52",
					"maxclass" : "number~",
					"mode" : 2,
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 314.0, 510.0, 56.0, 22.0 ],
					"sig" : 0.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 312.0, 351.0, 85.0, 22.0 ],
					"text" : "open $1 wave"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 312.0, 178.0, 33.0, 22.0 ],
					"text" : "t b s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 217.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 346.0, 217.0, 57.0, 22.0 ],
					"text" : "name $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "bang" ],
					"patching_rect" : [ 312.0, 273.0, 68.0, 22.0 ],
					"text" : "savedialog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 107.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 615.0, 336.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 147.0, 402.0, 96.0, 22.0 ],
									"text" : "print REC_ARM"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 274.0, 382.0, 216.0, 22.0 ],
									"text" : "Aglaya_REC_6-29-2019_20.17.22"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 105.0, 80.0, 63.0, 22.0 ],
									"text" : "time, date"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "int", "int" ],
									"patching_rect" : [ 193.0, 179.0, 67.0, 22.0 ],
									"text" : "unpack i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "int", "int" ],
									"patching_rect" : [ 105.0, 179.0, 67.0, 22.0 ],
									"text" : "unpack i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "list", "list", "int" ],
									"patching_rect" : [ 105.0, 111.0, 92.0, 22.0 ],
									"text" : "date"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 105.0, 267.0, 265.0, 22.0 ],
									"text" : "sprintf symout Aglaya_REC_%i-%i-%i_%i.%i.%i"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 105.0, 398.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 105.0, 32.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 1 ],
									"order" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"order" : 1,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 2,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 2 ],
									"source" : [ "obj-4", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 1 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 5 ],
									"source" : [ "obj-5", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 4 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 3 ],
									"source" : [ "obj-5", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 312.0, 146.0, 66.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p namefile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 202.0, 180.772003000000012, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 182.25, 137.772003000000012, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 219.25, 137.772003000000012, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-4",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 202.0, 84.772002999999998, 64.0, 39.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.0, 8.0, 101.0, 101.0 ],
					"toggle" : 1
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"forceaspect" : 1,
					"id" : "obj-2",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 98.0, 328.5, 57.0, 56.543999999999997 ],
					"pic" : "zz_recbutton.png",
					"presentation" : 1,
					"presentation_rect" : [ 12.0, 8.0, 101.0, 100.191999999999993 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 314.0, 468.0, 76.0, 23.0 ],
					"text" : "sfrecord~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 333.0, 387.0, 64.0, 22.0 ],
					"text" : "adoutput~"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"forceaspect" : 1,
					"id" : "obj-3",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 28.0, 328.5, 57.0, 57.0 ],
					"pic" : "zz_recstopbutton.png",
					"presentation" : 1,
					"presentation_rect" : [ 12.0, 8.0, 101.0, 101.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"order" : 1,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"order" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"order" : 1,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"order" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"order" : 2,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"order" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"order" : 1,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"order" : 1,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"order" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"order" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 1,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 1 ],
					"order" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"order" : 1,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-47", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 1,
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 0,
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 2,
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-52", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"order" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 1,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"order" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "zz_recstopbutton.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "zz_recbutton.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "zz_newrecording.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "Recurso 1.png",
				"bootpath" : "~/Desktop/1x",
				"patcherrelativepath" : "../../../../../../../../Desktop/1x",
				"type" : "PNG",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
	}

}
